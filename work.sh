npm run build

git add .

if [ -z "$1" ]; then
  git commit --no-status --author="highlander-rpg <grib_killill@list.ru>" -m "Update"
else
  git commit --no-status --author="highlander-rpg <grib_killill@list.ru>" -m "$1"
fi

git push
