document.addEventListener('DOMContentLoaded', () => {
    const tableBody = document.getElementById('plantsTableBody');
    const searchInput = document.getElementById('searchInput');
    const edibleFilters = document.querySelectorAll('input[name="edibleFilter"]');

    // Создаем модальное окно
    const modal = document.createElement('div');
    modal.className = 'modal';
    modal.innerHTML = `
        <div class="modal-content">
            <button class="modal-close">&times;</button>
            <img class="modal-image" src="" alt="">
            <div class="modal-controls">
                <button class="modal-button modal-prev">&#10094; Предыдущее</button>
                <button class="modal-button modal-next">Следующее &#10095;</button>
            </div>
        </div>
    `;
    document.body.appendChild(modal);

    function renderPlantsTable(plants) {
        tableBody.innerHTML = '';
        
        plants.forEach(plant => {
            const row = document.createElement('tr');
            
            row.innerHTML = `
                <td>
                    <div class="image-placeholder">
                        Нажмите чтобы показать фото
                    </div>
                    <div class="slideshow-container">
                        <img class="plant-image" alt="${plant.rusName}" data-src="${plant.images[0]}"
                            onerror="this.src='https://via.placeholder.com/100x100?text=Нет+фото'">
                        <div class="slide-controls">
                            <button class="slide-button prev">&#10094;</button>
                            <span class="slide-counter">1/${plant.images.length}</span>
                            <button class="slide-button next">&#10095;</button>
                        </div>
                    </div>
                </td>
                <td>${plant.rusName}</td>
                <td>${plant.latName}</td>
                <td>${plant.family}</td>
                <td class="${plant.isEdible ? 'edible' : 'not-edible'}">
                    ${plant.isEdible ? `Съедобное (${plant.edibleDesc})` : 'Несъедобное'}
                </td>
            `;
            
            const imageCell = row.querySelector('td:first-child');
            const placeholder = imageCell.querySelector('.image-placeholder');
            const slideshowContainer = imageCell.querySelector('.slideshow-container');
            const image = slideshowContainer.querySelector('.plant-image');
            let currentImageIndex = 0;
            let isImageLoaded = false;

            // Обработчики для слайдшоу
            const prevButton = slideshowContainer.querySelector('.prev');
            const nextButton = slideshowContainer.querySelector('.next');
            const counter = slideshowContainer.querySelector('.slide-counter');

            function updateImage() {
                image.src = plant.images[currentImageIndex];
                counter.textContent = `${currentImageIndex + 1}/${plant.images.length}`;
            }

            prevButton.addEventListener('click', (e) => {
                e.stopPropagation();
                currentImageIndex = (currentImageIndex - 1 + plant.images.length) % plant.images.length;
                updateImage();
            });

            nextButton.addEventListener('click', (e) => {
                e.stopPropagation();
                currentImageIndex = (currentImageIndex + 1) % plant.images.length;
                updateImage();
            });

            // Обработчик для открытия модального окна
            image.addEventListener('click', (e) => {
                e.stopPropagation();
                const modalImage = modal.querySelector('.modal-image');
                modalImage.src = plant.images[currentImageIndex];
                modal.style.display = 'flex';

                // Обработчики для модального окна
                const modalPrev = modal.querySelector('.modal-prev');
                const modalNext = modal.querySelector('.modal-next');

                function updateModalImage() {
                    modalImage.src = plant.images[currentImageIndex];
                }

                modalPrev.onclick = () => {
                    currentImageIndex = (currentImageIndex - 1 + plant.images.length) % plant.images.length;
                    updateModalImage();
                };

                modalNext.onclick = () => {
                    currentImageIndex = (currentImageIndex + 1) % plant.images.length;
                    updateModalImage();
                };
            });

            // Обработчик для закрытия модального окна
            modal.querySelector('.modal-close').onclick = () => {
                modal.style.display = 'none';
            };

            modal.onclick = (e) => {
                if (e.target === modal) {
                    modal.style.display = 'none';
                }
            };

            imageCell.addEventListener('click', () => {
                if (!isImageLoaded) {
                    image.src = plant.images[0];
                    isImageLoaded = true;
                }

                if (image.classList.contains('show-image')) {
                    image.classList.remove('show-image');
                    slideshowContainer.style.display = 'none';
                    placeholder.style.display = 'flex';
                } else {
                    image.classList.add('show-image');
                    slideshowContainer.style.display = 'block';
                    placeholder.style.display = 'none';
                }
            });
            
            tableBody.appendChild(row);
        });
    }

    function filterPlants(searchTerm, edibleFilter) {
        return window.plantsObjects.filter(plant => {
            const matchesSearch = 
                plant.rusName.toLowerCase().includes(searchTerm.toLowerCase()) ||
                plant.latName.toLowerCase().includes(searchTerm.toLowerCase()) ||
                plant.family.toLowerCase().includes(searchTerm.toLowerCase());

            const matchesEdible = 
                edibleFilter === 'all' ||
                (edibleFilter === 'edible' && plant.isEdible) ||
                (edibleFilter === 'nonedible' && !plant.isEdible);

            return matchesSearch && matchesEdible;
        });
    }

    function updateTable() {
        const searchTerm = searchInput.value;
        const edibleFilter = document.querySelector('input[name="edibleFilter"]:checked').value;
        const filteredPlants = filterPlants(searchTerm, edibleFilter);
        renderPlantsTable(filteredPlants);
    }

    searchInput.addEventListener('input', updateTable);
    edibleFilters.forEach(filter => {
        filter.addEventListener('change', updateTable);
    });

    // Начальная отрисовка таблицы
    renderPlantsTable(window.plantsObjects);
});
