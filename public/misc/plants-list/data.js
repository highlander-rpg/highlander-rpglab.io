window.plantsObjects = [
  {
    "latName": "Abies_alba",
    "rusName": "Пихта белая",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Abies_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Abies_alba_R1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/European_Silver-fir_Vallombrosa_%28FI%29%2C_Italy.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Illustration_Abies_alba0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Silver_fir_seedlings.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_campestre",
    "rusName": "Клён полевой",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_campestre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/237_Acer_campestre.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Acer-campestre-flowers.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Acer-campestre.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Acer_campestre_%284%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Acer_campestre_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Acer_campestre_002.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Acer_campestre_003.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Acer_campestre_004.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Acer_campestre_005.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/df/Acer_campestre_006.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_ginnala",
    "rusName": "Клён приречный",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_ginnala",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Acer_ginnala.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_negundo",
    "rusName": "Клён ясенелистный",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_negundo",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/38/2014-10-11_12_48_07_Box_Elder_Maple_foliage_during_autumn_in_Elko%2C_Nevada.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/2020_year._Herbarium._Acer_negundo._img-010.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Acer_negunde_Variegatum_Detail.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Acer_negundo_2018-05-01_9940.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Acer_negundo_31026528.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/Acer_negundo_31685019.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Acer_negundo_44809168.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Acer_negundo_44890747.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Acer_negundo_60052613.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Acer_negundo_female_flowers_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_platanoides",
    "rusName": "Клён остролистный",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_platanoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/34/2014-10-30_09_32_43_Norway_Maple_foliage_during_autumn_in_Ewing%2C_New_Jersey.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/df/2014-10-30_10_39_54_Norway_Maple_foliage_during_autumn_on_Durham_Avenue_in_Ewing%2C_New_Jersey.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/55/2020_year._Herbarium._Acer_platanoides._img-001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/2020_year._Herbarium._Acer_platanoides._img-002.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/72/2020_year._Herbarium._Acer_platanoides._img-032.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Acer_Platanoides_%27schwedleri%27_leaf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Acer_platanoides_1aJPG.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Acer_platanoides_flower_kz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Acer_platanoides_fruit_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_pseudoplatanus",
    "rusName": "Клён белый",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_pseudoplatanus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/Acer_pseudoplatanusAA.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/Acer_pseudoplatanus_005.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Acer_pseudoplatanus_Chaltenbrunnen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Acer_pseudoplatanus_buds_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Acer_pseudoplatanus_textura_del_tronco.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Ahornallee_zwischen_Altweitra_und_H%C3%B6rmanns_2017-10.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_rubrum",
    "rusName": "Клён красный",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_rubrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/2013-05-10_13_40_17_Immature_foliage_of_Red_Maple_in_Brendan_T_Byrne_State_Forest%2C_New_Jersey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/2014-10-30_11_09_40_Red_Maple_during_autumn_on_Lower_Ferry_Road_in_Ewing%2C_New_Jersey.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/2014-10-30_13_47_46_Red_Maple_during_autumn_along_Terrace_Boulevard_in_Ewing%2C_New_Jersey.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/2015-04-12_16_31_55_Male_Red_Maple_flowers_on_Bayberry_Road_in_Ewing%2C_New_Jersey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/2016-03-02_17_22_36_Female_Red_Maple_blossoms_along_McLearen_Road_%28Virginia_State_Secondary_Route_668%29_in_Oak_Hill%2C_Fairfax_County%2C_Virginia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Acer_rubrum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Acerbark8344.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Autumn_Blaze_Maple_in_Toronto.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_saccharinum",
    "rusName": "Клён серебристый",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_saccharinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c1/2014-11-02_15_41_21_Silver_Maple_foliage_during_autumn_along_Glen_Mawr_Drive_in_Ewing%2C_New_Jersey.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Acer_saccharinum_female_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/be/MAPLE_SILVER_twig.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Silber-Ahorn_%28Acer_saccharinum%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Silver-maple-bark.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/Silver_maple_leaf.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_tataricum",
    "rusName": "Клён татарский",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_tataricum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/2020_year._Herbarium._Acer_tataricum._img-005.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/2020_year._Herbarium._Acer_tataricum._img-006.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2a/2020_year._Herbarium._Acer_tataricum._img-026.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/2020_year._Herbarium._Acer_tataricum._img-028.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Acer_tataricum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Acer_tataricum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Acer_tataricum1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acer_tegmentosum",
    "rusName": "Клён зеленокорый",
    "family": "Aceraceae",
    "url": "https://ru.wikipedia.org/wiki/Acer_tegmentosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Acer_rubrum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Acer_tegmentosum1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Achillea_collina",
    "rusName": "Тысячелистник щетинистый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Achillea_collina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Achillea_collina_-_Botanischer_Garten_Mainz_IMG_5466.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Achillea_millefolium",
    "rusName": "Тысячелистник обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Achillea_millefolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Achillea20090912_079.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Achillea_millefolium_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/Achillea_millefolium_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Achillea_millefolium_5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Achillea_millefolium_5Dsr_9042.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Achillea_millefolium_6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Achillea_millefolium_ENBLA03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Achillea_millefolium_capitula_2002-11-18.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Achillea_millefolium_leaves_-_Tumalo_State_Park.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Пряность. В большом количестве он может вызвать отравление."
  },
  {
    "latName": "Achillea_nobilis",
    "rusName": "Тысячелистник благородный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Achillea_nobilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Achillea_nobilis_002.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Achillea_nobilis_Sturm40.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/aa/Achillea_nobilis_foliage.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Achillea_submillefolium",
    "rusName": "Тысячелистник обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Achillea_submillefolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Achillea_millefolium_%28yarrow%29.jpg/275px-Achillea_millefolium_%28yarrow%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Achillea_millefolium_-_roosa_harilik_raudrohi_Valingu.jpg/220px-Achillea_millefolium_-_roosa_harilik_raudrohi_Valingu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Achillea_millefolium_%28K%C3%B6hler%29.jpg/220px-Achillea_millefolium_%28K%C3%B6hler%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Achillea_ptarmica_millefolium_040807.jpg/260px-Achillea_ptarmica_millefolium_040807.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Achillea_millefolium_corimbo.jpg/220px-Achillea_millefolium_corimbo.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Achillea_millefolium_cap%C3%ADtulo.jpg/220px-Achillea_millefolium_cap%C3%ADtulo.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Achillea_millefolium_occidentalis_seeds.jpg/220px-Achillea_millefolium_occidentalis_seeds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Achillea_millefolium_hoja_y_tallo.jpg/220px-Achillea_millefolium_hoja_y_tallo.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Achillea_%28cultivar%29_03.JPG/250px-Achillea_%28cultivar%29_03.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acinos_arvensis",
    "rusName": "Щебрушка полевая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Acinos_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Acinos_arvensis_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Horminum_pyrenaicum_-_Saint-Hilaire.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aconitum_lasiostomum",
    "rusName": "Борец шерстистоустый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Aconitum_lasiostomum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Aconitum_lasiostomum_45131052.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aconitum_lycoctonum",
    "rusName": "Борец северный",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Aconitum_lycoctonum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/Aconitum_lycoctonum_subsp._vulparia_MHNT.BOT.2004.0.787.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Aconitum_septentrionale.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aconitum_septentrionale",
    "rusName": "Борец северный",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Aconitum_septentrionale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Aconitum_lycoctonum_stems_1_AB.jpg/275px-Aconitum_lycoctonum_stems_1_AB.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Aconitum_septentrionale.jpg/180px-Aconitum_septentrionale.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Aconitum_lycoctonum_leaves_1_AB.jpg/235px-Aconitum_lycoctonum_leaves_1_AB.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Aconitum_lycoctonum_buds_1_AB.jpg/130px-Aconitum_lycoctonum_buds_1_AB.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Aconitum_septentrionale_dissected_flower_D039_0284.jpg/117px-Aconitum_septentrionale_dissected_flower_D039_0284.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Aconitum_lycoctonum_flower_front_1_AB.jpg/132px-Aconitum_lycoctonum_flower_front_1_AB.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Acorus_calamus",
    "rusName": "Аир обыкновенный",
    "family": "Araceae",
    "url": "https://ru.wikipedia.org/wiki/Acorus_calamus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Acorus-calamus1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/37/Acorus_calamus1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/25/Illustration_Acorus_calamus0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/95/Sweet_flag%2C_Chapman_S.P.%2C_Pennsylvania.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "В качестве пряности высушенные корневища. Корневище варят в сиропе, засахаривают для кондитерских изделий. Из оснований листовых пластинок варят ароматное варенье"
  },
  {
    "latName": "Acroptilon_repens",
    "rusName": "Горчак ползучий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Acroptilon_repens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Rhaponticum_repens_1.jpg/275px-Rhaponticum_repens_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Rhaponticum_repens_5.jpg/180px-Rhaponticum_repens_5.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Actaea_spicata",
    "rusName": "Воронец колосистый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Actaea_spicata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9e/Actaea-spicata-berries.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Illustration_Actaea_spicata0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Adenophora_lilifolia",
    "rusName": "Бубенчик лилиелистный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Adenophora_lilifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Adelil.jpg/237px-Adelil.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Adenophora_cf_lilifolia_white_%289491831335%29.jpg/220px-Adenophora_cf_lilifolia_white_%289491831335%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Adoxa_moschatellina",
    "rusName": "Адокса мускусная",
    "family": "Adoxaceae",
    "url": "https://ru.wikipedia.org/wiki/Adoxa_moschatellina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Adoxa_moschatellina01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Illustration_Adoxa_moschatellina0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aegilops_cylindrica",
    "rusName": "Эгилопс цилиндрический",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Aegilops_cylindrica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Aegilops_cylindrica_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v20.jpg/275px-Aegilops_cylindrica_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v20.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Aegilops_cylindrica_seeds.jpg/220px-Aegilops_cylindrica_seeds.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aegilops_triuncialis",
    "rusName": "Эгилопс трёхдюймовый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Aegilops_triuncialis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Aegilops_triuncialis_%286127016117%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Aegilops_triuncialis_%286127564288%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Aegilops_triuncialis_Enfoque_2010-5-26_DehesaBoyaldePuertollano.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d9/Aegilops_triuncialis_L._-_barbed_goatgrass_-_AETR_-_Steve_Hurst_%40_USDA-NRCS_PLANTS_Database.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aegopodium_podagraria",
    "rusName": "Сныть обыкновенная",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Aegopodium_podagraria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Aegopodium_podagraria-%28dkrb%29-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Aegopodium_podagraria1_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Aegopodium_podagraria_-_stem_profile.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Aegopodium_podagraria_PID1588-2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Illustration_Aegopodium_podagraria0_clean_no-description.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Phyllopertha_horticola_on_Aegopodium_podagraria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Zevenbladbloem_R01.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья и черешки (Однако, легко спутать с другими зонтичными, которые ядовиты)"
  },
  {
    "latName": "Aesculus_hippocastanum",
    "rusName": "Конский каштан обыкновенный",
    "family": "Hippocastanaceae",
    "url": "https://ru.wikipedia.org/wiki/Aesculus_hippocastanum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/10_Pest_damage_-_horse-chestnut_leaf_miner_%28Cameraria_ohridella%29_in_Parma%2C_Italy.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/2010.05.09_Horse_chestnut_blossom%2C_Kyiv%2C_Ukraine_001c.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/AesculusHippocastanumTrunk.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/05/Aesculus_hippocastanum-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Aesculus_hippocastanum_Seed.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Horse-chestnut_800.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aethusa_cynapium",
    "rusName": "Кокорыш",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Aethusa_cynapium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Aethusa_cynapium_002.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Illustration_Aethusa_cynapium0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/QALace2675.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Agrimonia_eupatoria",
    "rusName": "Репешок обыкновенный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Agrimonia_eupatoria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Agrimonia_eupatoria02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Agrimonia_eupatoria_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Agrimonia_eupatoria_MHNT.BOT.2004.0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Illustration_Agrimonia_eupatoria0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Agrimonia_pilosa",
    "rusName": "Репешок волосистый",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Agrimonia_pilosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Agrimonia_pilosa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Agrimonia_pilosa1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Agropyron_cristatum",
    "rusName": "Житняк гребневидный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Agropyron_cristatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Agropyron_cristatum_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/13/Agropyron_cristatum_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/Agropyroncristatum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Agrostemma_githago",
    "rusName": "Куколь обыкновенный",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Agrostemma_githago",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Agrostemma_githago_120605.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Corn_cockle.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Agrostis_canina",
    "rusName": "Полевица собачья",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Agrostis_canina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Agrostis.canina.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Agrostis.canina2.-.lindsey.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Agrostis_gigantea",
    "rusName": "Полевица гигантская",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Agrostis_gigantea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Agrostis_gigantea.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Agrostis_gigantea_ligula.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Agrostis_stolonifera",
    "rusName": "Полевица побегоносная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Agrostis_stolonifera",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Agrostis_Blatt.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/Agrostis_Wuchs.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Agrostis_ligula.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Agrostis_tenuis",
    "rusName": "Полевица тонкая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Agrostis_tenuis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Agrostis.capillaris.2.jpg/265px-Agrostis.capillaris.2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Agrostis_tenuis_a.jpg/220px-Agrostis_tenuis_a.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ajuga_genevensis",
    "rusName": "Живучка женевская",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Ajuga_genevensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/AjugaGenevensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Ajuga_reptans_a1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ajuga_pyramidalis",
    "rusName": "Живучка пирамидальная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Ajuga_pyramidalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/82/0_Ajuga_pyramidalis_-_Vallorcine_%281%29.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ajuga_reptans",
    "rusName": "Живучка ползучая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Ajuga_reptans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/%28MHNT%29_Ajuga_reptans_-_Inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Ajuga_reptans_20070429_132711_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Ajuga_reptans_atropurpurea_0.11_R.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Ajuga_reptans_young_plant.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alchemilla_hirsuticaulis",
    "rusName": "Манжетка жёстковолосистостебельная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Alchemilla_hirsuticaulis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Alchemilla_hirsuticaulis_kz01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aldrovanda_vesiculosa",
    "rusName": "Альдрованда",
    "family": "Droseraceae",
    "url": "https://ru.wikipedia.org/wiki/Aldrovanda_vesiculosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/AldrovandaVesiculosaHabit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/AldrovandaVesiculosaSeedsGermination.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Aldrovanda_vesiculosa_specimen2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alisma_gramineum",
    "rusName": "Частуха злаковая",
    "family": "Alismataceae",
    "url": "https://ru.wikipedia.org/wiki/Alisma_gramineum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a3/Alisma_gramineum_plant.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alisma_lanceolatum",
    "rusName": "Частуха ланцетная",
    "family": "Alismataceae",
    "url": "https://ru.wikipedia.org/wiki/Alisma_lanceolatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/AlismaLanceolatum2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2a/Alismalanceolatum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alisma_plantago-aquatica",
    "rusName": "Частуха обыкновенная",
    "family": "Alismataceae",
    "url": "https://ru.wikipedia.org/wiki/Alisma_plantago-aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/61/AlismaPlant1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/AlismaPlantagoBlossom.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "содержит вещества, которые могут вызывать раздражение при контакте с кожей человека. Корневище растения богато крахмалом, съедобно. Съедобность корневища после отмирания верхней части растения остаётся под вопросом."
  },
  {
    "latName": "Alliaria_petiolata",
    "rusName": "Чесночница черешчатая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Alliaria_petiolata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5f/Alliaria_petiolata_marais-belloy-sur-somme_80_26042007_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Alliaria_petiolataseeds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/90/Garlic_Mustard_close_800.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья"
  },
  {
    "latName": "Allium_angulosum",
    "rusName": "Лук угловатый",
    "family": "Alliaceae",
    "url": "https://ru.wikipedia.org/wiki/Allium_angulosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6a/Allium_angulosum.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "листья"
  },
  {
    "latName": "Allium_oleraceum",
    "rusName": "Лук огородный",
    "family": "Alliaceae",
    "url": "https://ru.wikipedia.org/wiki/Allium_oleraceum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Allium_oleraceum_-_rohulauk_Keilas2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Backl%C3%B6k_%28Allium_oleraceum%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Illustration_Allium_oleraceum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Iridaceae_-_Allium_neapolitanum_%288303567329%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и луковицы"
  },
  {
    "latName": "Allium_schoenoprasum",
    "rusName": "Лук скорода",
    "family": "Alliaceae",
    "url": "https://ru.wikipedia.org/wiki/Allium_schoenoprasum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Allium_schoenoprasum_a1.jpg/275px-Allium_schoenoprasum_a1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Illustration_Allium_schoenoprasum_and_Allium_cepa0.jpg/220px-Illustration_Allium_schoenoprasum_and_Allium_cepa0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Starr_070906-8816_Allium_schoenoprasum.jpg/122px-Starr_070906-8816_Allium_schoenoprasum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Spinne_P4252850.jpg/122px-Spinne_P4252850.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Chives_flower.jpg/180px-Chives_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Allium.schoenoprasum.seeds.jpg/161px-Allium.schoenoprasum.seeds.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "листья, луковицы и цветки"
  },
  {
    "latName": "Allium_scorodoprasum",
    "rusName": "Лук причесночный",
    "family": "Alliaceae",
    "url": "https://ru.wikipedia.org/wiki/Allium_scorodoprasum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Illustration_Allium_scorodoprasum_and_Allium_porrum0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Allium_ursinum",
    "rusName": "Черемша",
    "family": "Alliaceae",
    "url": "https://ru.wikipedia.org/wiki/Allium_ursinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/387_Allium_ursinum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/40/AlliumUrsinumAspekt.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d2/Allium_ursinum2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Allium_ursinum_%28B%C3%A4rlauch%29_-_Bl%C3%BCte.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Allium_ursinum_sl1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Baerlauch_Bluete01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Daslook_%28Allium_ursinum%29_d.j.b_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Daslook_%28Allium_ursinum%29_d.j.b_05.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Daslook_%28Allium_ursinum%29_d.j.b_07.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "стебель, листья и луковицы"
  },
  {
    "latName": "Allium_vineale",
    "rusName": "Лук виноградничный",
    "family": "Alliaceae",
    "url": "https://ru.wikipedia.org/wiki/Allium_vineale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/da/Alliumvineale1web.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Iridaceae_-_Allium_neapolitanum_%288303567329%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Wild_Onions_in_Oklahoma.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alnus_glutinosa",
    "rusName": "Ольха чёрная",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Alnus_glutinosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/20120904Alnus_glutinosa01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/25/20120904Alnus_glutinosa14.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/20120904Alnus_glutinosa16.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Alder_nodules2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/04/Alkottar.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e1/Alnus_glutinosa%2C_Munkholmen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/28/Alnus_glutinosa_MHNT.BOT.2004.0.10a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Alnus_glutinosa_R0015198.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Alnus_glutinosa_R0015202.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alnus_incana",
    "rusName": "Ольха серая",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Alnus_incana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/Alnus_incana_rugosa_leaves.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Alnus_incana_var._tenuifolia_4.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alnus_incana",
    "rusName": "Ольха серая",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Alnus_incana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/Alnus_incana_rugosa_leaves.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Alnus_incana_var._tenuifolia_4.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alopecurus_aequalis",
    "rusName": "Лисохвост равный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Alopecurus_aequalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/AlopecurusAequalis1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alopecurus_geniculatus",
    "rusName": "Лисохвост коленчатый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Alopecurus_geniculatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/47/Geknikte_vossestaart_aar_%28Alopecurus_geniculatus_inflorescens%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alopecurus_pratensis",
    "rusName": "Лисохвост тростниковый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Alopecurus_pratensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Alopecurus_pratensis_Grote_vossenstaart.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Alopecurus_pratensis_ligula.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alyssum_desertorum",
    "rusName": "Бурачок пустынный",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Alyssum_desertorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Alyssum_desertorum_var_desertorum_3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Alyssum_gmelinii",
    "rusName": "Бурачок Гмелина",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Alyssum_gmelinii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Alyssum_gmelinii_habitat.jpg/266px-Alyssum_gmelinii_habitat.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Amaranthus_albus",
    "rusName": "Амарант белый",
    "family": "Amaranthaceae",
    "url": "https://ru.wikipedia.org/wiki/Amaranthus_albus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Amaranthus_albus_%288186658004%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Amaranthus_cruentus_Foxtail_2.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "приготовленные листья"
  },
  {
    "latName": "Amaranthus_blitum",
    "rusName": "Амарант жминда",
    "family": "Amaranthaceae",
    "url": "https://ru.wikipedia.org/wiki/Amaranthus_blitum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/Amaranthus_lividus_%286896677687%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Amaranthus_caudatus",
    "rusName": "Амарант хвостатый",
    "family": "Amaranthaceae",
    "url": "https://ru.wikipedia.org/wiki/Amaranthus_caudatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/2006-10-22Amaranthus03.jpg/275px-2006-10-22Amaranthus03.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Многие части растения, включая листья и семена"
  },
  {
    "latName": "Amaranthus_retroflexus",
    "rusName": "Амарант запрокинутый",
    "family": "Amaranthaceae",
    "url": "https://ru.wikipedia.org/wiki/Amaranthus_retroflexus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Amaranthus_retroflexus_flower1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Travancore_Cheera_Thoran.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые отваренные листья"
  },
  {
    "latName": "Ambrosia_artemisiifolia",
    "rusName": "Амброзия полыннолистная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Ambrosia_artemisiifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Ambrosia_artem1-5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Ambrosia_artemisiifolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Ambrosia_artemisiifolia004.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/Ambrosia_artemisiifolia_-_female_flowers_RHu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Ambrosia_artemisiifolia_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Ambrosia_artemisiifolia_male_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Ambrosia_artemisiifolia_plant7_%2811741895306%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Common_ragweed_at_distance.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anagallis_arvensis",
    "rusName": "Очный цвет полевой",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Anagallis_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/49/Anagallis-arvensis-Horashim2014-Zachi-Evenor01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Anagallis_arvensis_azurea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Anagallis_arvensis_f_azurea0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Anagallis_arvensis_f_azurea_eF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ce/Flowers_March_2008-19.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anchusa_officinalis",
    "rusName": "Воловик лекарственный",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Anchusa_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/Anchusa-officinalis-and-bee.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Anchusa_officinalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Anchusa_officinalis1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Anchusa_officinalis_inflorescence_-_Kulna.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Common_Bugloss_in_early_June.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Ipomoea_tricolor-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Andromeda_polifolia",
    "rusName": "Подбел",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Andromeda_polifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Andromeda_polifolia%2C_Pancake_Bay_PP.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Andromeda_polifolia_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Andromeda_polifolia_bloom.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Cleaned-Illustration_Andromeda_polifolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Linnaeus_Andromeda.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Androsace_filiformis",
    "rusName": "Проломник нитевидный",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Androsace_filiformis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bb/Androsace_filiformis_44512918.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Primula_vulgaris_Mezzolombardo_03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Androsace_maxima",
    "rusName": "Проломник большой",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Androsace_maxima",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Androsace_maxima_sl1.jpg/275px-Androsace_maxima_sl1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Androsace_maxima_sl4.jpg/250px-Androsace_maxima_sl4.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Androsace_septentrionalis",
    "rusName": "Проломник северный",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Androsace_septentrionalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Androsace_septentrionalis_ssp_subumbellata_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Primula_vulgaris_Mezzolombardo_03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anemone_sylvestris",
    "rusName": "Ветреница лесная",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Anemone_sylvestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Anemone_sylvestris_sl5.jpg/275px-Anemone_sylvestris_sl5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Anemone_sylvestris_with_seeds_-_seemnetega_mets%C3%BClane.jpg/220px-Anemone_sylvestris_with_seeds_-_seemnetega_mets%C3%BClane.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anemonoides_nemorosa",
    "rusName": "Ветреница дубравная",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Anemonoides_nemorosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Anemone_Nemorosa_6-9.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Anemone_nemorosa_001.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/e1/Anemone_nemorosa_LC0256.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Anemone_nemorosa_map1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Anemone_nemorosa_pink_240406.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Apis_mellifera_-_Anemone_nemorosa_-_Keila2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Bosanemoon_%28Anemone_nemorosa%29_%28d.j.b.%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/DoubleWoodAnemone.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anemonoides_ranunculoides",
    "rusName": "Ветреница лютичная",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Anemonoides_ranunculoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Anemone_ranunculoides_bgiu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Anemone_ranunculoides_flower_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Anemone_ranunculoides_bgiu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/Symrer1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Angelica_archangelica",
    "rusName": "Дягиль лекарственный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Angelica_archangelica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Absinthe-glass.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Angelica_archangelica_%281118596627%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Koehler1887-GardenAngelica.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Все части растения можно употреблять в качестве пряности. Из свежих корней и побегов делают цукаты, варенье, повидло, пастилу, суррогат чая. Листья растения, выросшие на первом году, употребляют в качестве салатов и гарниров"
  },
  {
    "latName": "Angelica_palustris",
    "rusName": "Дудник болотный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Angelica_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Angelica_palustris_Sturm24.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Angelica_sylvestris",
    "rusName": "Дудник лесной",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Angelica_sylvestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Angelica_sylvestris_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Angelica_sylvestris_flowers_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Gewone_engelwortel_R0012884_blad.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Gewone_engelwortel_R0012890_stengel.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Gewone_engwortel_R0012880_Plant.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/13/Illustration_Angelica_silvestris0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Wild_angelica_seeds_-_Flickr_-_S._Rae.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги, Молодые стебли и черешки листьев"
  },
  {
    "latName": "Anisantha_sterilis",
    "rusName": "Костёр бесплодный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Anisantha_sterilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Bromus_sterilis_%28Ruderal-Trespe%29_IMG_0477.JPG/275px-Bromus_sterilis_%28Ruderal-Trespe%29_IMG_0477.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/445_Bromus_sterilis%2C_B._tectorum.jpg/250px-445_Bromus_sterilis%2C_B._tectorum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anisantha_tectorum",
    "rusName": "Костёр кровельный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Anisantha_tectorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/20180426Bromus_tectorum2.jpg/275px-20180426Bromus_tectorum2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Bromus_tectorum_sl13.jpg/220px-Bromus_tectorum_sl13.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Bromus_tectorum_llustration_%2801%29.jpg/220px-Bromus_tectorum_llustration_%2801%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Antennaria_dioica",
    "rusName": "Кошачья лапка двудомная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Antennaria_dioica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Antennaria_dioica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Antennariadioeca.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anthemis_arvensis",
    "rusName": "Пупавка полевая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Anthemis_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Anthemis_arvensis02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Anthemis_arvensis_001.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anthemis_cotula",
    "rusName": "Пупавка собачья",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Anthemis_cotula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Anthemis_cotula_2.jpg/275px-Anthemis_cotula_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Anthemis_cotula_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-160.jpg/220px-Anthemis_cotula_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-160.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anthemis_subtinctoria",
    "rusName": "Пупавка красильная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Anthemis_subtinctoria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Anthemis_tinctoria_Oulu%2C_Finland_29.06.2013.jpg/275px-Anthemis_tinctoria_Oulu%2C_Finland_29.06.2013.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Anthemis_tinctoria.jpg/220px-Anthemis_tinctoria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Anthemis_April_2010-1.jpg/220px-Anthemis_April_2010-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anthemis_tinctoria",
    "rusName": "Пупавка красильная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Anthemis_tinctoria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Anthemis_tinctoria_Oulu%2C_Finland_29.06.2013.jpg/275px-Anthemis_tinctoria_Oulu%2C_Finland_29.06.2013.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Anthemis_tinctoria.jpg/220px-Anthemis_tinctoria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Anthemis_April_2010-1.jpg/220px-Anthemis_April_2010-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anthericum_ramosum",
    "rusName": "Венечник ветвистый",
    "family": "Anthericaceae",
    "url": "https://ru.wikipedia.org/wiki/Anthericum_ramosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Liliaceae_-_Anthericum_ramosum-5.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/37/Liliaceae_-_Anthericum_ramosum.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anthoxanthum_odoratum",
    "rusName": "Душистый колосок обыкновенный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Anthoxanthum_odoratum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Anthoxanthum.odoratum.2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Anthoxanthum.odoratum.3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Anthoxanthum.odoratum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/AnthoxanthumOdoratum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Anthoxanthum_odoratum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Anthoxanthum_odoratum_%28Billeder_af_nordens_flora_1917ff.%2C_v2_0427%29_clean%2C_no-description.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Anthoxanthum_odoratum_aehrchen_detail.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Anthoxanthum_odoratum_aehre.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/Anthoxanthum_odoratum_blatt.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Anthoxanthum_odoratum_scheidenhaare.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anthriscus_sylvestris",
    "rusName": "Купырь лесной",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Anthriscus_sylvestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Anthriscus_sylvestris_%28K%C3%B6hler%27s_Medizinal-Pflanzen%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Anthriscus_sylvestris_TK_2021-05-16_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Anthriscus_sylvestris_W.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2e/Warming-Skudbygning-Fig5-Anthriscus-sylvestris.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Стебли и листья. Варёные корни"
  },
  {
    "latName": "Anthyllis_arenaria",
    "rusName": "Язвенник песчаный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Anthyllis_arenaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Anthyllis_vulneraria_subsp._polyphylla_sl3.jpg/275px-Anthyllis_vulneraria_subsp._polyphylla_sl3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Anthyllis_vulneraria",
    "rusName": "Язвенник ранозаживляющий",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Anthyllis_vulneraria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Anthyllis_vulneraria_-_Bombus_sylvarum_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Fabaceae_-_Anthyllis_vulneraria-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Fabaceae_-_Anthyllis_vulneraria-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Illustration_Anthyllis_vulneraria0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Apera_spica-venti",
    "rusName": "Метлица обыкновенная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Apera_spica-venti",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Apera_spica-venti.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aquilegia_vulgaris",
    "rusName": "Водосбор обыкновенный",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Aquilegia_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Aquilegia_nora_barlow.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Aquilegia_vulgaris_-_Harilik_kurekell.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Aquilegia_vulgaris_100503a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Aquilegia_vulgaris_100503c.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Aquilegia_vulgaris_MHNT.BOT.2009.13.42.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Arabic_herbal_medicine_guidebook.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Arabidopsis_thaliana",
    "rusName": "Резуховидка Таля",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Arabidopsis_thaliana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/194_Arabidopsis_thaliana%2C_Turritis_glabra.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/ArabidopsisPlantPathology.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Arabidopsis_mutants.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Arabidopsis_thaliana.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Arabis_pendula",
    "rusName": "Резуха повислая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Arabis_pendula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/%D0%A0%D0%B5%D0%B7%D1%83%D1%85%D0%B0_%D0%BF%D0%BE%D0%B2%D0%B8%D1%81%D0%BB%D0%B0%D1%8F.JPG/275px-%D0%A0%D0%B5%D0%B7%D1%83%D1%85%D0%B0_%D0%BF%D0%BE%D0%B2%D0%B8%D1%81%D0%BB%D0%B0%D1%8F.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Arabis_sagittata",
    "rusName": "Резуха стреловидная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Arabis_sagittata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Arabis_sagittata_sl3.jpg/275px-Arabis_sagittata_sl3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Archangelica_officinalis",
    "rusName": "Дягиль лекарственный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Archangelica_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/AngelicaArchangelica1.jpg/247px-AngelicaArchangelica1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Angelica_archangelica_flower.JPG/220px-Angelica_archangelica_flower.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Illustration_Angelica_archangelica0_clean.jpg/220px-Illustration_Angelica_archangelica0_clean.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Archangelica_officinalis_a2.jpg/220px-Archangelica_officinalis_a2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/AngelicaArchangelica.jpg/220px-AngelicaArchangelica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Angelica_archangelica_litoralic_kz.jpg/220px-Angelica_archangelica_litoralic_kz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Angelikalikoer.jpg/220px-Angelikalikoer.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Все части растения можно употреблять в качестве пряности. Из свежих корней и побегов делают цукаты, варенье, повидло, пастилу, суррогат чая. Листья растения, выросшие на первом году, употребляют в качестве салатов и гарниров"
  },
  {
    "latName": "Arctium_lappa",
    "rusName": "Лопух большой",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Arctium_lappa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/ArctiumLappa1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Arctium_lappa02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/30/Arctium_lappa_MHNT.BOT.2004.0.16.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Burdockgobo.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Japanese_Gobo_Salad.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Корень. Побеги и стебли до начала цветения. "
  },
  {
    "latName": "Arctium_minus",
    "rusName": "Лопух малый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Arctium_minus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Arctium_minus_corimbo.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/CDC_artichoke.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Arctium_nemorosum",
    "rusName": "Лопух дубравный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Arctium_nemorosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a1/Arctium-nemorosum_Bl%C3%BCtenstand_011.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Arctium_tomentosum",
    "rusName": "Лопух паутинистый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Arctium_tomentosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Arctium_tomentosum_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Arctium_tomentosum_-_Kulna.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/48/Asteraceae_-_Arctium_tomentosum-000.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Asteraceae_-_Arctium_tomentosum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/CDC_artichoke.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые стебли"
  },
  {
    "latName": "Arctostaphylos_uva-ursi",
    "rusName": "Толокнянка обыкновенная",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Arctostaphylos_uva-ursi",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Arctostaphylos_uva-ursi_25924.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Baerentraube_ML0002.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Bearberry_Flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Common_bearberry_%28%22Kinnikinnick%22%2C_Arctostaphylos_uva-ursi%29_-_fruits_and_leaves.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Arenaria_serpyllifolia",
    "rusName": "Песчанка тимьянолистная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Arenaria_serpyllifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fd/Arenaria.serpyllifolia1web.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Arenaria_serpyllifolia_MHNT.BOT.2011.3.81.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aristolochia_clematitis",
    "rusName": "Кирказон ломоносовидный",
    "family": "Aristolochiaceae",
    "url": "https://ru.wikipedia.org/wiki/Aristolochia_clematitis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Arabic_herbal_medicine_guidebook.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/9/95/Aristolochia_clematitis_%287263875922%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Aristolochia_clematitis_MHNT.BOT.2013.22.25.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Starr_010704-0011_Peperomia_blanda.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Armeniaca_vulgaris",
    "rusName": "Абрикос обыкновенный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Armeniaca_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Prunus_armeniaca_Nubra_Valley.jpg/275px-Prunus_armeniaca_Nubra_Valley.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Turkey_apricot_RHS.jpeg/250px-Turkey_apricot_RHS.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Prunus_armeniaca_flowers_in_Kharkov.jpg/250px-Prunus_armeniaca_flowers_in_Kharkov.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/ArmenianStamps-407.jpg/220px-ArmenianStamps-407.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/%D0%91%D0%B0%D0%BB%D1%8B%D0%BA%D1%87%D1%8B_-_street_market_Balykchy.jpg/370px-%D0%91%D0%B0%D0%BB%D1%8B%D0%BA%D1%87%D1%8B_-_street_market_Balykchy.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Turkey.Pasa_Baglari005.jpg/220px-Turkey.Pasa_Baglari005.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Abricot_march%C3%A9_de_la_casbah_d%27Alger.JPG/200px-Abricot_march%C3%A9_de_la_casbah_d%27Alger.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Apricot_seeds.jpg/200px-Apricot_seeds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Apricots_one_open.jpg/220px-Apricots_one_open.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/%D0%90%D0%B1%D1%80%D0%B8%D0%BA%D0%BE%D1%81_%D1%81%D0%B8%D0%B1%D0%B8%D1%80%D1%81%D0%BA%D0%B8%D0%B9.jpg/220px-%D0%90%D0%B1%D1%80%D0%B8%D0%BA%D0%BE%D1%81_%D1%81%D0%B8%D0%B1%D0%B8%D1%80%D1%81%D0%BA%D0%B8%D0%B9.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Armeria_vulgaris",
    "rusName": "Армерия приморская",
    "family": "Limoniaceae",
    "url": "https://ru.wikipedia.org/wiki/Armeria_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/ArmeriaMaritimaElongata%281%29.jpg/275px-ArmeriaMaritimaElongata%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Armeria_maritima_IMG_0963.JPG/240px-Armeria_maritima_IMG_0963.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Zawciag_nadmorski_Ameria_maritima_maritima.jpg/240px-Zawciag_nadmorski_Ameria_maritima_maritima.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Armeria_maritima_fruits_arma6_003_php.jpg/240px-Armeria_maritima_fruits_arma6_003_php.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/140_Statice_armeria.jpg/220px-140_Statice_armeria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Armeria_maritima_%28buds%29.jpg/250px-Armeria_maritima_%28buds%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Armeria_maritima_%27koster%27_2.JPG/250px-Armeria_maritima_%27koster%27_2.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Armoracia_rusticana",
    "rusName": "Хрен",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Armoracia_rusticana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Armoracia_rusticana_kz03.jpg/275px-Armoracia_rusticana_kz03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Cochlearia_armoracia_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v4.jpg/220px-Cochlearia_armoracia_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Armoracia_rusticana_03_ies.jpg/120px-Armoracia_rusticana_03_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Armoracia_rusticana_05_ies.jpg/120px-Armoracia_rusticana_05_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Armoracia_rusticana_002.JPG/120px-Armoracia_rusticana_002.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Lip%C3%AD_%28Man%C4%9Bt%C3%ADn%29%2C_rostlina.jpg/220px-Lip%C3%AD_%28Man%C4%9Bt%C3%ADn%29%2C_rostlina.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Пряность из корней и листьев"
  },
  {
    "latName": "Arnica_montana",
    "rusName": "Арника горная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Arnica_montana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Arnica_montana_180605.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Arnica_montana_4436.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Arnica_montana_MHNT.BOT.2011.18.5.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aronia_melanocarpa",
    "rusName": "Арония черноплодная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Aronia_melanocarpa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Amelanchier_vulgaris_Sturm10.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9e/Aronia_melanocarpa_cv_Rubina_P1020524.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Aronia_mitschurinii",
    "rusName": "Арония Мичурина",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Aronia_mitschurinii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Aronia_mitschurinii_berries.JPG/275px-Aronia_mitschurinii_berries.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Aronia_mitschurinii_september.JPG/250px-Aronia_mitschurinii_september.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/%D0%A7%D0%B5%D1%80%D0%BD%D0%BE%D0%BF%D0%BB%D0%BE%D0%B4%D0%BD%D0%B0%D1%8F_%D1%80%D1%8F%D0%B1%D0%B8%D0%BD%D0%B0_%D0%B2_%D0%9A%D0%BE%D1%80%D0%B5%D0%BD%D1%8C%D1%81%D0%BA%D0%B8%D1%85_%D1%80%D0%BE%D0%B4%D0%BD%D0%B8%D0%BA%D0%B0%D1%85_2.jpg/121px-%D0%A7%D0%B5%D1%80%D0%BD%D0%BE%D0%BF%D0%BB%D0%BE%D0%B4%D0%BD%D0%B0%D1%8F_%D1%80%D1%8F%D0%B1%D0%B8%D0%BD%D0%B0_%D0%B2_%D0%9A%D0%BE%D1%80%D0%B5%D0%BD%D1%8C%D1%81%D0%BA%D0%B8%D1%85_%D1%80%D0%BE%D0%B4%D0%BD%D0%B8%D0%BA%D0%B0%D1%85_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/%D0%90%D1%80%D0%BE%D0%BD%D0%B8%D1%8F_%D0%9C%D0%B8%D1%87%D1%83%D1%80%D0%B8%D0%BD%D0%B0_%28%D0%BF%D0%BE%D1%81_%D0%96%D0%B0%D1%81%D0%BC%D0%B8%D0%BD%D0%BD%D1%8B%D0%B9_%D0%A1%D0%B0%D1%80%D0%B0%D1%82%D0%BE%D0%B2%29.jpg/250px-%D0%90%D1%80%D0%BE%D0%BD%D0%B8%D1%8F_%D0%9C%D0%B8%D1%87%D1%83%D1%80%D0%B8%D0%BD%D0%B0_%28%D0%BF%D0%BE%D1%81_%D0%96%D0%B0%D1%81%D0%BC%D0%B8%D0%BD%D0%BD%D1%8B%D0%B9_%D0%A1%D0%B0%D1%80%D0%B0%D1%82%D0%BE%D0%B2%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Arrhenatherum_elatius",
    "rusName": "Райграс высокий",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Arrhenatherum_elatius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Arrhenaterum-elatius-flowers.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Glanshaver_bloeiwijze_Arrhenatherum_elatius.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Glanshaver_bloempje_Arrhenatherum_elatius.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Glanshaver_ligula_Arrhenatherum_elatius.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Glanshaver_plant_Arrhenatherum_elatius.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Poaceae_spp_Sturm24.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Starr_010721-9002_Arrhenatherum_elatius.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Artemisia_abrotanum",
    "rusName": "Полынь лечебная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_abrotanum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Artemisia_abrotanum0.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги, но только в незначительных количествах"
  },
  {
    "latName": "Artemisia_absinthium",
    "rusName": "Полынь горькая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_absinthium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Absinthe-glass.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Artemisia_absinthium_0002.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Artemisia_absinthium_P1210748.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Приправа"
  },
  {
    "latName": "Artemisia_annua",
    "rusName": "Полынь однолетняя",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_annua",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Artemisia_annua.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Artemisia_annua.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Artemisiaannua.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Приправа"
  },
  {
    "latName": "Artemisia_austriaca",
    "rusName": "Полынь австрийская",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_austriaca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/55/Artemisia_austriaca_%288102713787%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Artemisia_campestris",
    "rusName": "Полынь полевая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_campestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Artemisia_campestris_Ypey37.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Mrzezyno_Artemisia_campestris_littoral_sprout_2010.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Artemisia_dracunculus",
    "rusName": "Эстрагон",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_dracunculus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Deutscher_Estragon_Artemisia_dracunculus_01.jpg/275px-Deutscher_Estragon_Artemisia_dracunculus_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Starr_080117-2173_Artemisia_dracunculus.jpg/220px-Starr_080117-2173_Artemisia_dracunculus.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Пряность"
  },
  {
    "latName": "Artemisia_scoparia",
    "rusName": "Полынь веничная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_scoparia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Arabic_herbal_medicine_guidebook.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Redstem_wormwood_%28Artemisia_scoparia%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Artemisia_sieversiana",
    "rusName": "Полынь Сиверса",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_sieversiana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2e/Artemisia_sieversiana_52312937.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Artemisia_vulgaris",
    "rusName": "Полынь обыкновенная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Artemisia_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/59/Artemia_vulgaris_leaf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/ArtemisiaVulgaris.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Artemisia_vulgaris_lower_side_of_leaf.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Надземная часть - Пряность"
  },
  {
    "latName": "Aruncus_vulgaris",
    "rusName": "Волжанка обыкновенная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Aruncus_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Aruncus_dioicus_%28Walter%29_Fernald%2C_1939.jpg/275px-Aruncus_dioicus_%28Walter%29_Fernald%2C_1939.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Spiraea_aruncus_Sturm11.jpg/273px-Spiraea_aruncus_Sturm11.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Asarum_europaeum",
    "rusName": "Копытень европейский",
    "family": "Aristolochiaceae",
    "url": "https://ru.wikipedia.org/wiki/Asarum_europaeum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/81/2020_year._Herbarium._Asarum_europaeum._img-005.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/2020_year._Herbarium._Asarum_europaeum._img-006.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Asarum_europaeum20100610_226.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Asarum_europaeum_170406.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Asarum_europaeum_in_early_June.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/European_Wildginger_Asarum_eropaeum_1976px.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Asclepias_syriaca",
    "rusName": "Ваточник сирийский",
    "family": "Asclepiadaceae",
    "url": "https://ru.wikipedia.org/wiki/Asclepias_syriaca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/Asclepias_syriaca_004.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Asclepias_syriaca_2019-04-21_1738.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fd/Asclepias_syriaca_close_up_of_the_seed_copy.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Common_Milkweed_%281035856056%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Common_Milkweed_Asclepias_syriaca_Unopened_Flower_Head_2223px.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e4/Milk3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Milkweedseeds.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Asparagus_officinalis",
    "rusName": "Спаржа лекарственная",
    "family": "Asparagaceae",
    "url": "https://ru.wikipedia.org/wiki/Asparagus_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Spargelbeeren.jpg/275px-Spargelbeeren.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Illustration_Asparagus_officinalis0_clean.jpg/220px-Illustration_Asparagus_officinalis0_clean.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Asparagus_officinalis_ies.jpg/200px-Asparagus_officinalis_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Asparagus_officinalis_subsp._oficinalis_flower%2C_asperge_bloem.jpg/130px-Asparagus_officinalis_subsp._oficinalis_flower%2C_asperge_bloem.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Asparagus_officinalis_004.JPG/132px-Asparagus_officinalis_004.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Spargeldenkmal_Schwetzingen_01.JPG/220px-Spargeldenkmal_Schwetzingen_01.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Asparagus_officinalis_001.JPG/250px-Asparagus_officinalis_001.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Darmstadt-Arheilgen_Spargelfeld_Luftbild_111.jpg/280px-Darmstadt-Arheilgen_Spargelfeld_Luftbild_111.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Steam-boiling_green_asparagus.jpg/220px-Steam-boiling_green_asparagus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Gemuesespargel_Klasse_II_2008-05.jpg/220px-Gemuesespargel_Klasse_II_2008-05.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Fran%C3%A7ois_Bonvin_001.jpg/220px-Fran%C3%A7ois_Bonvin_001.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Не вышедшие из земли побеги (длиной 18—20 см) с ещё не распустившейся головкой"
  },
  {
    "latName": "Asperugo_procumbens",
    "rusName": "Острица",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Asperugo_procumbens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Asperugo_procumbens1.JPG/275px-Asperugo_procumbens1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/581_Asperugo%21_procumbens.jpg/220px-581_Asperugo%21_procumbens.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Asperugo_procumbens_%28Scharfkraut%29_IMG_5334.JPG/220px-Asperugo_procumbens_%28Scharfkraut%29_IMG_5334.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Asperula_arvensis",
    "rusName": "Ясменник полевой",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Asperula_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/56/Asperula_arvensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Asperula_arvensis1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Asplenium_adiantum-nigrum",
    "rusName": "Костенец чёрный",
    "family": "Aspleniaceae",
    "url": "https://ru.wikipedia.org/wiki/Asplenium_adiantum-nigrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Asplenium_adiantum-nigrum_030208.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Asplenium_ruta-muraria",
    "rusName": "Костенец постенный",
    "family": "Aspleniaceae",
    "url": "https://ru.wikipedia.org/wiki/Asplenium_ruta-muraria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Aspenium_ruta-muraria2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Asplenium_ruta_muraria2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Silver-fern.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Asplenium_trichomanes",
    "rusName": "Костенец волосовидный",
    "family": "Aspleniaceae",
    "url": "https://ru.wikipedia.org/wiki/Asplenium_trichomanes",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Asplenium_trichomanes2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Asplenium_trichomanes_subsp_quadrivalens.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aster_amellus",
    "rusName": "Астра ромашковая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Aster_amellus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/72/Aster_amellus_-_plants_%28aka%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Aster_amellus_003.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/e4/Asteraceae_-_Aster_amellus.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Berg-Aster%2C_Bl%C3%BCten.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Illustration_Aster_amellus0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aster_novae-angliae",
    "rusName": "Астра новоанглийская",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Aster_novae-angliae",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Symphyotrichum_novae-angliae_flower_%2824%29.jpg/275px-Symphyotrichum_novae-angliae_flower_%2824%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Aster_novae-angliae_%27Barr%27s_Pink%27%2C_Elfenau.jpg/273px-Aster_novae-angliae_%27Barr%27s_Pink%27%2C_Elfenau.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Aster_novae-angliae_%27alma_potschke%27_02.jpg/273px-Aster_novae-angliae_%27alma_potschke%27_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/New_England_Aster_%27Purple_Dome%27_%28Symphyotrichum_novae-angliae%29_%285651136965%29.jpg/273px-New_England_Aster_%27Purple_Dome%27_%28Symphyotrichum_novae-angliae%29_%285651136965%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Aster_novi-belgii",
    "rusName": "Астра новобельгийская",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Aster_novi-belgii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Symphyotrichum_novi-belgii_138399587_%28cropped%29.jpg/275px-Symphyotrichum_novi-belgii_138399587_%28cropped%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Astragalus_arenarius",
    "rusName": "Астрагал песчаный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Astragalus_arenarius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/322_Astragalus_arenarius.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Astragalus_arenarius_kz1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Astragalus_cicer",
    "rusName": "Астрагал нутовый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Astragalus_cicer",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Astragalus_cicer0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/37/Astragalus_cicer1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Astragalus_danicus",
    "rusName": "Астрагал датский",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Astragalus_danicus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Astragalus_danicus_-_aas-hundihammas.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Astragalus_danicus_in_Cambridgeshire%2C_2018%2C_close-up_flower.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Astragalus_glycyphyllos",
    "rusName": "Астрагал солодколистный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Astragalus_glycyphyllos",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e6/Astragalus_dasyanthus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Astragalus_glycyphyllos_MHNT.BOT.2011.3.62.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Astragalus_glycyphyllos_fruit.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Astrantia_major",
    "rusName": "Астранция крупная",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Astrantia_major",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Apiaceae_-_Astrantia_major-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Apiaceae_-_Astrantia_major-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Astrantia_major_%27Star_of_Beauty%27_J1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Astrantia_major_ENBLA01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Astrantia_major_MHNT.BOT.2007.43.8.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Closeup_of_Astrantia_Major_flower.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Athyrium_filix-femina",
    "rusName": "Кочедыжник женский",
    "family": "Athyriaceae",
    "url": "https://ru.wikipedia.org/wiki/Athyrium_filix-femina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Athyrium_filix-femina0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/Athyrium_filix-femina_Sori_Tannwald1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Illustration_Athyrium_filix-femina0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Unidentified_fern%2C_Cambridge_University_Botanic_Garden.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Во время вегетации до момента разворачивания молодых побегов в листья их можно использовать в пищу как овощ"
  },
  {
    "latName": "Atriplex_hortensis",
    "rusName": "Лебеда садовая",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Atriplex_hortensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Atriplex_hortensis_MHNT.BOT.2013.22.60.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Atriplex_hortensis_sl20.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/72/Atriplexhortensis1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Neuch%C3%A2tel_Herbarium_-_Atriplex_hortensis_-_NEU000004323.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "съедобна"
  },
  {
    "latName": "Atriplex_nitens",
    "rusName": "Лебеда стреловидная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Atriplex_nitens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Atriplex_nitens3pl.jpg/275px-Atriplex_nitens3pl.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Atriplex_sagittata_cleaned_Sturm.jpg/150px-Atriplex_sagittata_cleaned_Sturm.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Atriplex_patula",
    "rusName": "Лебеда раскидистая",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Atriplex_patula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Amaranthus_cruentus_Foxtail_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Atriplex_patula_%285129939806%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/UitstaandeMeldeSorinnesDSCN4516.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Atriplex_prostrata",
    "rusName": "Лебеда простёртая",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Atriplex_prostrata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Atriplex_prostrata_2017-09-16_4066.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c1/Billeder_af_nordens_flora_%281917%29_%2820344834396%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Atriplex_tatarica",
    "rusName": "Лебеда татарская",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Atriplex_tatarica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Atriplex_tatarica_Halle_habitus_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Avena_fatua",
    "rusName": "Овсюг",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Avena_fatua",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Avena_May_2010-3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Avena_fatua_%283886284130%29.jpg/1280px-Avena_fatua_%283886284130%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена как суррогат хлеба"
  },
  {
    "latName": "Avena_strigosa",
    "rusName": "Овёс щетинистый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Avena_strigosa",
    "images": [],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Axyris_amaranthoides",
    "rusName": "Безвкусница щирицевидная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Axyris_amaranthoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/Axyris_amaranthoides_151411077.jpg/225px-Axyris_amaranthoides_151411077.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Baeothryon_alpinum",
    "rusName": "Пухонос альпийский",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Baeothryon_alpinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Trichophorum_alpinum.JPG/330px-Trichophorum_alpinum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Eriophorum_apinum.jpg/275px-Eriophorum_apinum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ballota_nigra",
    "rusName": "Белокудренник чёрный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Ballota_nigra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Ballota_nigra_2005.06.19_10.13.06-p6190007.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Ballotanigra.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Barbarea_stricta",
    "rusName": "Сурепка сжатая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Barbarea_stricta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Barbarea_stricta_sl3.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодые листья"
  },
  {
    "latName": "Barbarea_vulgaris",
    "rusName": "Сурепка обыкновенная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Barbarea_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/88/%28MHNT%29_Barbarea_vulgaris_-_Habit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/31_Barbarea_vulgaris_R.Br.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Barbarea_vulgaris_ENBLA06.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Brassicaceae_-_Barbarea_vulgaris-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Brassicaceae_-_Barbarea_vulgaris-2_%282%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Brassicaceae_-_Barbarea_vulgaris.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Melitaea_athalia_-_Keila.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодые листья и нераспустившиеся соцветия. Содержит вещества, которые в больших количествах могут вызвать отравление."
  },
  {
    "latName": "Batrachium_circinatum",
    "rusName": "Шелковник жестколистный",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Batrachium_circinatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Ranunculus_circinatus_LC0078.jpg/275px-Ranunculus_circinatus_LC0078.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Batrachium_kauffmannii",
    "rusName": "Лютик Кауфмана",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Batrachium_kauffmannii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Ranunculus_kauffmannii_147439839.jpg/275px-Ranunculus_kauffmannii_147439839.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Batrachium_trichophyllum",
    "rusName": "Шелковник волосолистный",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Batrachium_trichophyllum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/RanunculusTrichophyllus.jpg/275px-RanunculusTrichophyllus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Ranunculus_trichophyllus_070811b.JPG/220px-Ranunculus_trichophyllus_070811b.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Beckmannia_eruciformis",
    "rusName": "Бекмания обыкновенная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Beckmannia_eruciformis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Beckmannia_eruciformis_-_Berlin_Botanical_Garden_-_IMG_8543.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bellis_perennis",
    "rusName": "Маргаритка многолетняя",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Bellis_perennis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/20190319_Bellis_perennis_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Belis_peremnis_-_panoramio.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Bellis_perennis-fully_bloomed_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Bellis_perennis_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Bellis_perennis_%288580127027%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/13/Bellis_perennis_ENBLA03.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/ce/Daisy_G%C3%A4nsebl%C3%BCmchen_Bellis_perennis_01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Berberis_vulgaris",
    "rusName": "Барбарис обыкновенный",
    "family": "Berberidaceae",
    "url": "https://ru.wikipedia.org/wiki/Berberis_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Berberis-vulgaris-flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Berberis_vulgaris_.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Dried_barberries_on_a_plate.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Illustration_Berberis_vulgaris0.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды, молодые листья"
  },
  {
    "latName": "Berteroa_incana",
    "rusName": "Икотник серый",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Berteroa_incana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/19/20120922Berteroa_incana3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Berteroa_incana.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Bombus_lapidarius_-_Berteroa_incana_-_Tallinn.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Berula_erecta",
    "rusName": "Берула прямая",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Berula_erecta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Illustration_Berula_erecta0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Betonica_officinalis",
    "rusName": "Буквица лекарственная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Betonica_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/18/2020_year._Herbarium._Herbaceous_plant._img-130.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/2020_year._Herbarium._Herbaceous_plant._img-131.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Betonica_officinalis_%287775493816%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Betonica_officinalis_Munich_Botanic_garden.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/De_Matiera_Medica_Abbasid_periode_Bagdad_1224_kestron_%3D_Betonica_officinalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2a/Der_Heilziest%2C_die_Echte_Betonie%2C_Betonica_officinalis_oder_Stachys_betonica_04.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Der_Heilziest%2C_die_Echte_Betonie%2C_Betonica_officinalis_oder_Stachys_betonica_05.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Stachys_officinalis_%2844558415335%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Betula_humilis",
    "rusName": "Берёза низкая",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Betula_humilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Betula_humilis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Corylus_avellana.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Betula_nana",
    "rusName": "Берёза карликовая",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Betula_nana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Betukananabereza.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Betula_nana0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Betula_nana_upernavik_kujalleq_2007-07-25_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Betula_pendula",
    "rusName": "Берёза повислая",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Betula_pendula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/2014-10-30_09_58_18_Birch_foliage_during_autumn_along_Terrace_Boulevard_in_Ewing%2C_New_Jersey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/B._pendula%2C_Koivu_Birch%2C_end_of_August_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Betula_pendula_Finland.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Betula_pendula_laciniata0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Betula_pendula_winter.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Birch-Betula-pendula-Troms%C3%B8.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Breza_na_jesen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/HAZELIUS%281881%29_Vol.1%2C_Abb.9%2C_p037.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Illustration_Betula_pendula0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Betula_pendula",
    "rusName": "Берёза повислая",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Betula_pendula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/2014-10-30_09_58_18_Birch_foliage_during_autumn_along_Terrace_Boulevard_in_Ewing%2C_New_Jersey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/B._pendula%2C_Koivu_Birch%2C_end_of_August_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Betula_pendula_Finland.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Betula_pendula_laciniata0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Betula_pendula_winter.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Birch-Betula-pendula-Troms%C3%B8.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Breza_na_jesen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/HAZELIUS%281881%29_Vol.1%2C_Abb.9%2C_p037.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Illustration_Betula_pendula0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Betula_pendula",
    "rusName": "Берёза повислая",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Betula_pendula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/2014-10-30_09_58_18_Birch_foliage_during_autumn_along_Terrace_Boulevard_in_Ewing%2C_New_Jersey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/B._pendula%2C_Koivu_Birch%2C_end_of_August_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Betula_pendula_Finland.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Betula_pendula_laciniata0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Betula_pendula_winter.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Birch-Betula-pendula-Troms%C3%B8.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Breza_na_jesen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/HAZELIUS%281881%29_Vol.1%2C_Abb.9%2C_p037.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Illustration_Betula_pendula0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Betula_pubescens",
    "rusName": "Берёза пушистая",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Betula_pubescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Acronicta_auricoma_-_Betula_pubescens_-_Niitv%C3%A4lja_bog.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Betula_pubescens_-_Burgwald_002.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/Betula_pubescens_subsp._tortuosa_Saana.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Niitv%C3%A4lja_soo_sookask_31-12-2017.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/Prestvannet-parkvei.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bidens_cernua",
    "rusName": "Череда поникшая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Bidens_cernua",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Bidens_cernua.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/Bidens_cernua_Herbar.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Bidens_cernua_Sturm18.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bidens_frondosa",
    "rusName": "Череда облиственная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Bidens_frondosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Bidens_frondosa.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bidens_radiata",
    "rusName": "Череда лучистая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Bidens_radiata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/21/BidensRadiata2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bidens_tripartita",
    "rusName": "Череда трёхраздельная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Bidens_tripartita",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Bidens_tripartita1_eF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Bidens_tripartita_MHNT.BOT.2004.0.774.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bifora_radians",
    "rusName": "Бифора лучистая",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Bifora_radians",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/74/QALace2675.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Bistorta_major",
    "rusName": "Змеевик большой",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Bistorta_major",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Polygonum_bistorta_a3.jpg/275px-Polygonum_bistorta_a3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Illustration_Polygonum_bistorta0.jpg/220px-Illustration_Polygonum_bistorta0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Mal%C3%A1_Studen%C3%A1_dolina%2C_Vysok%C3%A9_Tatry%2C_Bistorta_major_%283%29.JPG/220px-Mal%C3%A1_Studen%C3%A1_dolina%2C_Vysok%C3%A9_Tatry%2C_Bistorta_major_%283%29.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и молодые побеги"
  },
  {
    "latName": "Blysmus_compressus",
    "rusName": "Поточник сжатый",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Blysmus_compressus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ad/Blysmus_compressus.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Borago_officinalis",
    "rusName": "Огуречная трава",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Borago_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Blue_borage_flowers_2526205868_6b35bbac29_b.jpg/275px-Blue_borage_flowers_2526205868_6b35bbac29_b.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Illustration_Borago_officinalis0.jpg/220px-Illustration_Borago_officinalis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Borago_officinalis_Alba1SHSU.jpg/220px-Borago_officinalis_Alba1SHSU.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья, цветки, молодые растения целиком"
  },
  {
    "latName": "Botrychium_lanceolatum",
    "rusName": "Гроздовник ланцетный",
    "family": "Botrychiaceae",
    "url": "https://ru.wikipedia.org/wiki/Botrychium_lanceolatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Lance-Leaf_Moonwort_%283816257528%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Botrychium_lunaria",
    "rusName": "Гроздовник полулунный",
    "family": "Botrychiaceae",
    "url": "https://ru.wikipedia.org/wiki/Botrychium_lunaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Botrychium_lunaria_%28Vanoise%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Botrychium_lunaria_frond.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Botrychium_matricariifolium",
    "rusName": "Гроздовник ромашколистный",
    "family": "Botrychiaceae",
    "url": "https://ru.wikipedia.org/wiki/Botrychium_matricariifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Moonwort_%284752770708%29.jpg/266px-Moonwort_%284752770708%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Botrychium_multifidum",
    "rusName": "Гроздовник многораздельный",
    "family": "Botrychiaceae",
    "url": "https://ru.wikipedia.org/wiki/Botrychium_multifidum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Botrychium_multifidum.jpg/275px-Botrychium_multifidum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Botrychium_multifidum_nf.jpg/220px-Botrychium_multifidum_nf.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Botrychium_simplex",
    "rusName": "Гроздовник простой",
    "family": "Botrychiaceae",
    "url": "https://ru.wikipedia.org/wiki/Botrychium_simplex",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/89/Botrychium_simplex_%283599528767%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Silver-fern.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Brachypodium_pinnatum",
    "rusName": "Коротконожка перистая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Brachypodium_pinnatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7e/Brachypodium_pinnatum-%28dkrb%29-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Nordens_flora_Brachypodium_pinnatum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Brachypodium_sylvaticum",
    "rusName": "Коротконожка лесная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Brachypodium_sylvaticum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Brachypodium_sylvaticum_espiguetes_seques.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Brachypodium_sylvaticum_hivern.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/51/False_brome_2.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Brassica_campestris",
    "rusName": "Капуста полевая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Brassica_campestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/BrassicaCampestris3.jpg/275px-BrassicaCampestris3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Illustration_sinapis_arvensis.jpg/220px-Illustration_sinapis_arvensis.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Brassica_juncea",
    "rusName": "Горчица сарептская",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Brassica_juncea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Brassica_juncea_var._juncea_3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Chinese_vegetable_026.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Chinesemustardgreens.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Curly_mustard_leaves.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Gai_Choi_Mustard_Greens_-_J_K_Asian_Grocery_%285050526668%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Gat-gimchi.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Lai_Xaak%27or_Bhaji.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Lai_shak%2C_mashed_potato_%26_fried_egg_plant.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Lai_shak%2C_mashed_potato_%26_fried_egg_plant.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья, молодые побеги, семена"
  },
  {
    "latName": "Brassica_nigra",
    "rusName": "Горчица чёрная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Brassica_nigra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/20120704Brassica_nigra1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Black_mustard.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/90/Brassica_nigra_silique.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Brassica_nigra_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-170.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья, молодые побеги, семена"
  },
  {
    "latName": "Briza_media",
    "rusName": "Трясунка средняя",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Briza_media",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Briza_media.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Briza_media_-_keskmine_v%C3%A4rihein.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/73/PNBT_Briza_media_wiecha_03.07.10_pl.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bromopsis_benekenii",
    "rusName": "Костёр Бенекена",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Bromopsis_benekenii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Dactylis_glomerata_-_Botanical_Garden%2C_University_of_Frankfurt_-_DSC02420.jpg/275px-Dactylis_glomerata_-_Botanical_Garden%2C_University_of_Frankfurt_-_DSC02420.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bromopsis_inermis",
    "rusName": "Костёр безостый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Bromopsis_inermis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/BromusInermisFlowering.jpg/266px-BromusInermisFlowering.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Bromus_inermis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v19.jpg/220px-Bromus_inermis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v19.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bromus_arvensis",
    "rusName": "Костёр полевой",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Bromus_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Bromus_arvensis_plant_%2801%29.jpg/275px-Bromus_arvensis_plant_%2801%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Bromus_arvensis_inflorescence_%2821%29.jpg/220px-Bromus_arvensis_inflorescence_%2821%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Bromus_arvensis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v15.jpg/220px-Bromus_arvensis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v15.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bromus_japonicus",
    "rusName": "Костёр японский",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Bromus_japonicus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Bromus_japonicus_%2826982243684%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bromus_mollis",
    "rusName": "Костёр мягкий",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Bromus_mollis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Zachte_dravik_Bromus_mollis_ligula.jpg/220px-Zachte_dravik_Bromus_mollis_ligula.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/BROMUS_HORDEACEUS_-_MORROCURT_-_IB-937_%28Cua_de_guilla%29.JPG/220px-BROMUS_HORDEACEUS_-_MORROCURT_-_IB-937_%28Cua_de_guilla%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Bromus_hordeaceus_head6_%286920608934%29.jpg/220px-Bromus_hordeaceus_head6_%286920608934%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bromus_secalinus",
    "rusName": "Костёр ржаной",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Bromus_secalinus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Bromus_secalinus0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bromus_squarrosus",
    "rusName": "Костёр растопыренный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Bromus_squarrosus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Bromus_squarrosus_NRCS-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Brunnera_macrophylla",
    "rusName": "Бруннера крупнолистная",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Brunnera_macrophylla",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/79/BlueFlowerWade.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Ipomoea_tricolor-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bryonia_alba",
    "rusName": "Переступень белый",
    "family": "Cucurbitaceae",
    "url": "https://ru.wikipedia.org/wiki/Bryonia_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Bryonia_alba1pl.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Illustration_Bryonia_alba0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Buglossoides_arvensis",
    "rusName": "Буглоссоидес полевой",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Buglossoides_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Buglossoides_arvensis_300406a.jpg/275px-Buglossoides_arvensis_300406a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Illustration_Lithospermum_arvense0.jpg/220px-Illustration_Lithospermum_arvense0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Bunias_orientalis",
    "rusName": "Свербига восточная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Bunias_orientalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d9/Bunias_orientalis1.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья, корни, Молодые стебли (очищенные от кожицы)"
  },
  {
    "latName": "Butomus_umbellatus",
    "rusName": "Сусак зонтичный",
    "family": "Butomaceae",
    "url": "https://ru.wikipedia.org/wiki/Butomus_umbellatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Butomus_umbellatus_-_harilik_luigelill_Keilas.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "корневища"
  },
  {
    "latName": "Cakile_euxina",
    "rusName": "Морская горчица черноморская",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Cakile_euxina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Cakile_maritima03.jpg/275px-Cakile_maritima03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Calamagrostis_arundinacea",
    "rusName": "Вейник тростниковидный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Calamagrostis_arundinacea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Calamagrostis.arundinacea.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Calamagrostis_canescens",
    "rusName": "Вейник седоватый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Calamagrostis_canescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/38/CalamagrostisCanescens.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Calamagrostis_epigeios",
    "rusName": "Вейник наземный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Calamagrostis_epigeios",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/CalamagrostisEpigejos.jpg/266px-CalamagrostisEpigejos.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Calamagrostis_neglecta",
    "rusName": "Вейник незамеченный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Calamagrostis_neglecta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Calamagrostis_stricta_01.jpg/275px-Calamagrostis_stricta_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Billeder_af_nordens_flora_%281917%29_%2820371423455%29.jpg/220px-Billeder_af_nordens_flora_%281917%29_%2820371423455%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Atlas_roslin_pl_Trzcinnik_prosty_3353_8437.jpg/220px-Atlas_roslin_pl_Trzcinnik_prosty_3353_8437.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Caldesia_parnassifolia",
    "rusName": "Кальдезия белозоролистная",
    "family": "Alismataceae",
    "url": "https://ru.wikipedia.org/wiki/Caldesia_parnassifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Caldesia_parnassiifolia3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Elodea_canadensis.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Calla_palustris",
    "rusName": "Белокрыльник",
    "family": "Araceae",
    "url": "https://ru.wikipedia.org/wiki/Calla_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Calla_palustris2.jpg/275px-Calla_palustris2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Calla_palustris_water_arum_VI08_C_H5478.jpg/260px-Calla_palustris_water_arum_VI08_C_H5478.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Calla_palustris_in_natural_monument_Stribrna_Hut_in_summer_2011_%2812%29.JPG/260px-Calla_palustris_in_natural_monument_Stribrna_Hut_in_summer_2011_%2812%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/PNBT_Calla_palustris_li%C5%9B%C4%873_03.07.10_p.jpg/260px-PNBT_Calla_palustris_li%C5%9B%C4%873_03.07.10_p.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Calla_palustris1.jpg/260px-Calla_palustris1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Calla_palustris_Eestis.JPG/260px-Calla_palustris_Eestis.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Illustration_Calla_palustris0.jpg/220px-Illustration_Calla_palustris0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Calla1657.JPG/220px-Calla1657.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Calla_palustris_L._Fuchs_1543.jpg/210px-Calla_palustris_L._Fuchs_1543.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Calla_palustris_Sturm64.jpg/207px-Calla_palustris_Sturm64.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Alois_Lunzer01.jpg/212px-Alois_Lunzer01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/420_Calla_palustris.jpg/185px-420_Calla_palustris.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Callitriche_cophocarpa",
    "rusName": "Болотник короткоплодный",
    "family": "Callitrichaceae",
    "url": "https://ru.wikipedia.org/wiki/Callitriche_cophocarpa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Callitriche_cophocarpa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Callitriche_cophocarpa_kz02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Callitriche_hermaphroditica",
    "rusName": "Болотник обоеполый",
    "family": "Callitrichaceae",
    "url": "https://ru.wikipedia.org/wiki/Callitriche_hermaphroditica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Callitriche_hermaphroditica_NRCS-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Callitriche_palustris",
    "rusName": "Болотник болотный",
    "family": "Callitrichaceae",
    "url": "https://ru.wikipedia.org/wiki/Callitriche_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Illustration_Callitriche_palustris0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Callitriche_stagnalis",
    "rusName": "Болотник прудовой",
    "family": "Callitrichaceae",
    "url": "https://ru.wikipedia.org/wiki/Callitriche_stagnalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Callitriche_stagnalis_Scop._%28AM_AK309216-1%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Callitriche_stagnalis_leaf1_%2812079443754%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Callitrichestagnalis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Calluna_vulgaris",
    "rusName": "Вереск",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Calluna_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Heather_%28Highlands%29.jpg/275px-Heather_%28Highlands%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Illustration_Calluna_vulgaris0.jpg/220px-Illustration_Calluna_vulgaris0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Calluna_vulgaris_-_Heather_-_S%C3%BCp%C3%BCrge_%C3%87al%C4%B1s%C4%B1_1.jpg/220px-Calluna_vulgaris_-_Heather_-_S%C3%BCp%C3%BCrge_%C3%87al%C4%B1s%C4%B1_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/HeathHabitat.jpg/220px-HeathHabitat.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Calluna_vulgaris_%27Arabella%27_kz1.jpg/180px-Calluna_vulgaris_%27Arabella%27_kz1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Calluna_vulgaris_%27Hammondii%27.jpg/180px-Calluna_vulgaris_%27Hammondii%27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Calluna_vulgaris_%27Alba_Plena%27_kz01.jpg/180px-Calluna_vulgaris_%27Alba_Plena%27_kz01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Calluna_vulgaris_%27Gold_Haze%27_kz02.jpg/180px-Calluna_vulgaris_%27Gold_Haze%27_kz02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Calluna_vulgaris_%27Dark_Beauty%27_kz02.jpg/180px-Calluna_vulgaris_%27Dark_Beauty%27_kz02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Calluna_vulgaris_cultivars_kz04.jpg/356px-Calluna_vulgaris_cultivars_kz04.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Calluna_vulgaris_%27Bonita%27_kz02.jpg/300px-Calluna_vulgaris_%27Bonita%27_kz02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Calluna_vulgaris_%27Marleen%27_kz03.jpg/300px-Calluna_vulgaris_%27Marleen%27_kz03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Pow%C4%85zki_Cemetery%2C_Warsaw%2C_Poland%2C_01.jpg/292px-Pow%C4%85zki_Cemetery%2C_Warsaw%2C_Poland%2C_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Haltern_am_See%2C_Westruper_Heide_--_2015_--_8236-40.jpg/368px-Haltern_am_See%2C_Westruper_Heide_--_2015_--_8236-40.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Mountain_Laurel_Kalmia_latifolia_Flowers_2448px.jpg/60px-Mountain_Laurel_Kalmia_latifolia_Flowers_2448px.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Caltha_palustris",
    "rusName": "Калужница болотная",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Caltha_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Caltha_palustris_MHNT.BOT.2005.0.967.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/Caltha_palustris_alba_03.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/Caltha_palustris_plant.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Caltha_palustris_seeds_USDA.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/Caltha_palustris_var_himalensis_W_IMG_7242.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Caltha_sinogracilis_rubriflora.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Calystegia_sepium",
    "rusName": "Повой заборный",
    "family": "Convolvulaceae",
    "url": "https://ru.wikipedia.org/wiki/Calystegia_sepium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Calystegia_April_2008-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/81/Calystegia_sepium_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/25/Calystegia_sepium_05.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/59/Calystegia_sepium_MHNT.BOT.2009.7.11.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Calystegia_sepium_bracteoles.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Calystegia_sepium_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Calystegia_silvatica_bracteoles.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Camelina_sativa",
    "rusName": "Рыжик посевной",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Camelina_sativa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Brassicaceae_spp_Sturm30.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Camelina_Sativa_-_Camelina_False_Flax.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Camelina_sativa_MHNT.BOT.2018.6.15.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Camelina_sativa_eF.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Масло считалось годным"
  },
  {
    "latName": "Campanula_bononiensis",
    "rusName": "Колокольчик болонский",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_bononiensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Campanula_bononiensis_Sturm59.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Campanula_cervicaria",
    "rusName": "Колокольчик жестковолосый",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_cervicaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Campanula_cervicaria_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Campanula_glomerata",
    "rusName": "Колокольчик сборный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_glomerata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Bombus_soroeensis_-_Campanula_glomerata_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Campanula_glomerata.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Campanula_glomerata_at_Ludwika%2C_Sweden.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Campanulaceae_-_Campanula_glomerata-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Campanulaceae_-_Campanula_glomerata-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Campanulaceae_-_Campanula_glomerata-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Campanulaceae_-_Campanula_glomerata-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Campanulaceae_-_Campanula_glomerata-5.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Campanulaceae_-_Campanula_glomerata.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Campanula_latifolia",
    "rusName": "Колокольчик широколистный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_latifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Campanula_latifolia1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Campanula_latifolia_5.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/The_Soviet_Union_1988_CPA_5965_stamp_%28Deciduous_forest_flowers._Giant_bellflower%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и корни"
  },
  {
    "latName": "Campanula_patula",
    "rusName": "Колокольчик раскидистый",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_patula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/CampanulaPatula.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/13/Campanula_patula_-_Harilik_kellukas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Campanula_persicifolia",
    "rusName": "Колокольчик персиколистный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_persicifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d2/Stor_bl%C3%A5klocka.Campanula_persicifolia.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и корни"
  },
  {
    "latName": "Campanula_rapunculoides",
    "rusName": "Колокольчик рапунцелевидный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_rapunculoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Campanula_rapunculoides_HRM2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Campanula_rapunculoides_Sturm60.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/81/Campanulaceae_-_Campanula_rapunculoides-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Campanulaceae_-_Campanula_rapunculoides-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Campanulaceae_-_Campanula_rapunculoides-5.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Campanulaceae_-_Campanula_rapunculoides.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Creeping_Bellflower%2C_Ottawa.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Campanula_rapunculus",
    "rusName": "Колокольчик рапунцель",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_rapunculus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/82/Campanula_rapunculus_Closeup_SierraMadrona.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/09/Campanula_rapunculus_W.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/Campanulaceae_-_Campanula_rapunculus-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/Campanulaceae_-_Campanula_rapunculus-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Campanulaceae_-_Campanula_rapunculus-5.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Campanulaceae_-_Campanula_rapunculus-6.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Campanulaceae_-_Campanula_rapunculus.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Корни и молодые листья"
  },
  {
    "latName": "Campanula_rotundifolia",
    "rusName": "Колокольчик круглолистный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_rotundifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Blaaklokke.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4b/Campanula_rotondifolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Campanula_rotundifolia_21739.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Campanula_rotundifolia_Aachen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/WP_20140712_22_27_19_Raw.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Campanula_sibirica",
    "rusName": "Колокольчик сибирский",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_sibirica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Campanula_sibirica.jpg/275px-Campanula_sibirica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Campanula_sibirica_kz.jpg/150px-Campanula_sibirica_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Campanula_trachelium",
    "rusName": "Колокольчик крапиволистный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Campanula_trachelium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Campanula_trachelium.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/37/Campanula_trachelium_Charmauvillers.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и корни"
  },
  {
    "latName": "Cannabis_sativa",
    "rusName": "Конопля посевная",
    "family": "Cannabaceae",
    "url": "https://ru.wikipedia.org/wiki/Cannabis_sativa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Cannabis_Sativa_tree.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Cannabis_sativa_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Cannabis_sativa_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Cannabis_sativa_Koehler_drawing.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Масло из семян"
  },
  {
    "latName": "Capsella_bursa-pastoris",
    "rusName": "Пастушья сумка обыкновенная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Capsella_bursa-pastoris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Capsella_bursa-pastoris_Sturm23.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/47/Naengi-doenjang-guk.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Nanakusa_gayu_on_Nanakusa_no_sekku.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья. Размолотые семена можно применять вместо горчицы"
  },
  {
    "latName": "Caragana_arborescens",
    "rusName": "Карагана древовидная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Caragana_arborescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/Caragana_arborescens.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Cardamine_amara",
    "rusName": "Сердечник горький",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Cardamine_amara",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Cardamine_amara_IP0305015.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Cardamine_flexuosa",
    "rusName": "Сердечник извилистый",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Cardamine_flexuosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Cardamine_flexuosa_eF.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Гарнир"
  },
  {
    "latName": "Cardamine_impatiens",
    "rusName": "Сердечник недотрога",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Cardamine_impatiens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Cardamine_impatiens_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cardamine_parviflora",
    "rusName": "Сердечник мелкоцветковый",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Cardamine_parviflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Cardamine_spp_Sturm11.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cardamine_pratensis",
    "rusName": "Сердечник луговой",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Cardamine_pratensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Blume_in_Wildbad_07.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Cardamine_pratensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Cardamine_pratensis_10.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Cardamine_pratensis_2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/89/Cuckoo_flower_Wiltshire.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Illustration_Cardamine_pratensis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Pinksterbloem.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Carduus_acanthoides",
    "rusName": "Чертополох колючий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Carduus_acanthoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/df/2009-08-19_%288%29_Thistle%2C_Distel%2C_Carduus_acanthoides.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/20140209Carduus_acanthoides.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/2020_year._Herbarium._Carduus_acanthoides._img-009.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/2020_year._Herbarium._Carduus_acanthoides._img-010.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/29121022Wegdistel_Hockenheimer_Rheinbogen2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carduus_crispus",
    "rusName": "Чертополох курчавый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Carduus_crispus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Arabic_herbal_medicine_guidebook.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/4/48/Bombus_lapidarius_drone_-_Carduus_crispus_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/CDC_artichoke.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Carduus_02.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Carduus_crispus_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Carduus_crispus_002a.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carduus_nutans",
    "rusName": "Чертополох поникающий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Carduus_nutans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Carduus_nutans_180807.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Carduus_nutans_habitus.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Musk_thistle.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_acuta",
    "rusName": "Осока острая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_acuta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Carex_acuta1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Cleaned-Illustration_Carex_acuta.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_acutiformis",
    "rusName": "Осока островидная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_acutiformis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Carex_acutiformis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_appropinquata",
    "rusName": "Осока сближенная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_appropinquata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Carex_appropinquata_-_Berlin_Botanical_Garden_-_IMG_8471.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_aquatilis",
    "rusName": "Осока водяная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_aquatilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Carex_aquatilis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_atherodes",
    "rusName": "Осока прямоколосая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_atherodes",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Carexatherodes.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_brizoides",
    "rusName": "Осока трясунковидная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_brizoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e4/Carex_brizoides3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Carex_brizoides_-_Botanischer_Garten_M%C3%BCnchen-Nymphenburg_-_DSC07826.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Carex_brizoides_kz01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_brunnescens",
    "rusName": "Осока буроватая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_brunnescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Carex_brunnescens.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Carex_brunnescens_var_sphaerostachya.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_buxbaumii",
    "rusName": "Осока Буксбаума",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_buxbaumii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7e/Carex_buxbaumii_kz1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_capillaris",
    "rusName": "Осока волосовидная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_capillaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Carex_capillaris_Atlas_Alpenflora.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_caryophyllea",
    "rusName": "Осока гвоздичная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_caryophyllea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Carex_caryophyllea_%28Ankstyvoji_viksva%29001.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_cespitosa",
    "rusName": "Осока дернистая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_cespitosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2e/Carex_cespitosa_Kiiminki%2C_Finland_16.06.2013_img_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/Carex_cespitosa_area.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_chordorrhiza",
    "rusName": "Осока плетевидная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_chordorrhiza",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Carex_spp_Sturm26.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_colchica",
    "rusName": "Осока колхидская",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_colchica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Carex_colchica_-_Botanischer_Garten%2C_Dresden%2C_Germany_-_DSC08670.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_contigua",
    "rusName": "Осока колосистая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_contigua",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Carex_spicata.jpeg/275px-Carex_spicata.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/433_Carex_contigua%2C_Carex_disticha%2C_Carex_leporina.jpg/220px-433_Carex_contigua%2C_Carex_disticha%2C_Carex_leporina.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_davalliana",
    "rusName": "Осока Дэвелла",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_davalliana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Carex_davalliana_%28Davall-Segge%29_IMG_21240.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_diandra",
    "rusName": "Осока двутычинковая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_diandra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/00/Carex_diandra.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_digitata",
    "rusName": "Осока пальчатая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_digitata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Carex_digitata_kz02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_dioica",
    "rusName": "Осока двудомная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_dioica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f3/Carex_dioica_maennlein.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Carex_dioica_weiblein.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_disperma",
    "rusName": "Осока двусеменная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_disperma",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Carex_disperma_NRCS-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_distans",
    "rusName": "Осока расставленная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_distans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Carex_distans_maennl.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_disticha",
    "rusName": "Осока двурядная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_disticha",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Carex_disticha.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_echinata",
    "rusName": "Осока ежисто-колючая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_echinata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/25/Carex.echinata.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_elata",
    "rusName": "Осока высокая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_elata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ea/Carex_elata.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_elata",
    "rusName": "Осока высокая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_elata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ea/Carex_elata.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_elongata",
    "rusName": "Осока удлинённая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_elongata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Carex_elongata_illustration_%2801%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Carex_elongata_kz07.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_ericetorum",
    "rusName": "Осока верещатниковая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_ericetorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Cleaned-Illustration_Carex_ericetorum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_flacca",
    "rusName": "Осока повислая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_flacca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/51/Carex_flacca.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_flava",
    "rusName": "Осока жёлтая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_flava",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Carex_flava_-_Niitv%C3%A4lja.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Cleaned-Illustration_Carex_flava.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_hartmanii",
    "rusName": "Осока Гартмана",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_hartmanii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Carex_hartmanii.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_heleonastes",
    "rusName": "Осока болотолюбивая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_heleonastes",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Carex_spp_Sturm34.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_hirta",
    "rusName": "Осока мохнатая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_hirta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Carex_hirta_stamens.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Carex_hirta_utricles.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_juncella",
    "rusName": "Осока ситничковая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_juncella",
    "images": [],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_lasiocarpa",
    "rusName": "Осока волосистоплодная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_lasiocarpa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Carex_lasiocarpa_female_spike_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_limosa",
    "rusName": "Осока топяная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_limosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Carex_limosa_inflorescence_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_loliacea",
    "rusName": "Осока плевельная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_loliacea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Carex_spp_Sturm33.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_melanostachya",
    "rusName": "Осока чёрноколосая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_melanostachya",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Carex_melanostachya_sl11.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_montana",
    "rusName": "Осока горная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_montana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4f/Carex_montana_%28Berg-Segge%29_IMG_7730.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_muricata",
    "rusName": "Осока колючковатая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_muricata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Carex_muricata_-_siiltarn_Keilas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_nigra",
    "rusName": "Осока чёрная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_nigra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Carex_nigra.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_ornithopoda",
    "rusName": "Осока птиценожковая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_ornithopoda",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/49/Carex_ornithopoda_Variegata_2020-06-23_0732.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/58/Illustration_Carex_ornithopoda0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_otrubae",
    "rusName": "Осока Отрубы",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_otrubae",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4f/Carex_otrubae.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_ovalis",
    "rusName": "Осока заячья",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_ovalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Carex.ovalis.2.jpg/265px-Carex.ovalis.2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Carex_spp_Sturm31.jpg/220px-Carex_spp_Sturm31.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Carex_ovalis_habitus.jpeg/220px-Carex_ovalis_habitus.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_pallescens",
    "rusName": "Осока бледноватая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_pallescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Carex_pallescens_2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_panicea",
    "rusName": "Осока просяная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_panicea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Carex_panicea_fruchtstand.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_paniculata",
    "rusName": "Осока метельчатая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_paniculata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Carex_paniculata_fruchtstand.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_pauciflora",
    "rusName": "Осока малоцветковая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_pauciflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Carex_pauciflora_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_paupercula",
    "rusName": "Осока магелланская",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_paupercula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Carex_spp_Sturm42.jpg/275px-Carex_spp_Sturm42.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_pilosa",
    "rusName": "Осока волосистая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_pilosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d2/Carex_pilosa6.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Carex_pilosa_%27Copenhagen_Select%27_kz02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_pilulifera",
    "rusName": "Осока шариконосная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_pilulifera",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Carex.pilulifera2.-.lindsey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Carex_pilulifera.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_praecox",
    "rusName": "Осока ранняя",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_praecox",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Carex_praecox_sl4.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_pseudocyperus",
    "rusName": "Осока ложносытевая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_pseudocyperus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Carex_pseudocyperus.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_remota",
    "rusName": "Осока раздвинутая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_remota",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/Carex.remota4.-.lindsey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Carex_remota_-_Hillier_Gardens_-_Romsey%2C_Hampshire%2C_England_-_DSC04603.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/49/Carex_remota_community_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_rhizina",
    "rusName": "Осока корневищная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_rhizina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Carex_rhizina_-_Botanical_Garden_in_Kaisaniemi%2C_Helsinki_-_DSC03532.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_riparia",
    "rusName": "Осока береговая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_riparia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/de/CarexRiparia1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Carex_riparia_%28Ufer-Segge%29_IMG_22812.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_rostrata",
    "rusName": "Осока носиковая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_rostrata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Carex_rostrata.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_serotina",
    "rusName": "Осока поздняя",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_serotina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Carex_serotina_-_Berlin_Botanical_Garden_-_IMG_8648.JPG/275px-Carex_serotina_-_Berlin_Botanical_Garden_-_IMG_8648.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_supina",
    "rusName": "Осока приземистая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_supina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e5/Carex_supina_kz2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_sylvatica",
    "rusName": "Осока лесная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_sylvatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Carex_sylvatica.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_umbrosa",
    "rusName": "Осока теневая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_umbrosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Carex_umbrosa1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_vaginata",
    "rusName": "Осока влагалищная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_vaginata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Carex_vaginata1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_vesicaria",
    "rusName": "Осока пузырчатая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_vesicaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Carex_vesicaria_inflorescense_%2821%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Carex_vesicaria_inflorescense_%2825%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c9/Carex_vesicaria_kz02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e4/Harvesting_Carex_vesicaria_in_F%C3%B8rstevann.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carex_vulpina",
    "rusName": "Осока лисья",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Carex_vulpina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Carex_vulpina_-_Botanischer_Garten_Braunschweig_-_Braunschweig%2C_Germany_-_DSC04388.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/47/Cleaned-Illustration_Carex_vulpina.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carlina_acaulis",
    "rusName": "Колючник бесстебельный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Carlina_acaulis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Carlina_acaulis_160907.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carlina_biebersteinii",
    "rusName": "Колючник Биберштейна",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Carlina_biebersteinii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Carlina_biebersteinii4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Carlina_biebersteinii_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Carlina_biebersteinii_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Carlina_biebersteinii_3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carpinus_betulus",
    "rusName": "Граб обыкновенный",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Carpinus_betulus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/94/CarpinusBetulusBark.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Carpinus_betulus_%27Fastigiata%27_Dublin_P1310089.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Carpinus_betulus_%27Fastigiata%27_Eindhoven_21_MG_3470.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Carpinus_betulus_%27Fastigiata%27_Poundbury_20_P1310846.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Carpinus_betulus_-_Hunsr%C3%BCck_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Carpinus_betulus_MHNT.BOT.2004.0.494.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Carpinus_betulus_bud.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Carpinus_betulus_trunk.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Carpinus_fruit.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Carum_carvi",
    "rusName": "Тмин обыкновенный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Carum_carvi",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Caraway_Carum_carvi_Kerava_VI08_H5457_C.jpg/275px-Caraway_Carum_carvi_Kerava_VI08_H5457_C.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_91%29_%288231711713%29.jpg/220px-K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_91%29_%288231711713%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Carum_carvi_2.JPG/175px-Carum_carvi_2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Carum_carvi.JPG/175px-Carum_carvi.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/K%C3%BCmmelsamen_f%C3%BCr_WP.jpg/175px-K%C3%BCmmelsamen_f%C3%BCr_WP.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена, листья и молодые побеги"
  },
  {
    "latName": "Catabrosa_aquatica",
    "rusName": "Поручейница водяная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Catabrosa_aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e5/Catabrosa_aquatica_%286134822744%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Caulinia_flexilis",
    "rusName": "Наяда гибкая",
    "family": "Najadaceae",
    "url": "https://ru.wikipedia.org/wiki/Caulinia_flexilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Najas_flexilis_NRCS-1.jpg/275px-Najas_flexilis_NRCS-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Nafl_001_lvd.jpg/220px-Nafl_001_lvd.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Caulinia_minor",
    "rusName": "Наяда малая",
    "family": "Najadaceae",
    "url": "https://ru.wikipedia.org/wiki/Caulinia_minor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Najas_minor_NRCS_%28original%29.jpg/275px-Najas_minor_NRCS_%28original%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cenolophium_denudatum",
    "rusName": "Пусторёберник обнажённый",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Cenolophium_denudatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Cenolophium_denudatum06.jpg/275px-Cenolophium_denudatum06.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Cenolophium_denudatum.JPG/250px-Cenolophium_denudatum.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centaurea_cyanus",
    "rusName": "Василёк синий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Centaurea_cyanus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/005_Cornflower_petals_-_edible_flower_on_ice_cream.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Bachelor%27s_button%2C_Basket_flower%2C_Boutonniere_flower%2C_Cornflower_-_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Bee_on_cornflower_in_Aspen_%2891223%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Centaurea_cyanus_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Group_portrait_with_cornflowers_by_Igor_Grabar%2C_1914.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centaurea_diffusa",
    "rusName": "Василёк раскидистый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Centaurea_diffusa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Centaurea_diffusa1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Centaurea_diffusa_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Centaurea_diffusa_5.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centaurea_jacea",
    "rusName": "Василёк луговой",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Centaurea_jacea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Bombus_lapidarius_-_Centaurea_jacea_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/CDC_artichoke.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Centaurea_jacea_Knoopkruid.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centaurea_phrygia",
    "rusName": "Василёк фригийский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Centaurea_phrygia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/CDC_artichoke.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Centaurea_phrygia_-_Bombus_pascuorum_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Centaurea_phrygia_bgiu.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centaurea_scabiosa",
    "rusName": "Василёк шероховатый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Centaurea_scabiosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/de/20150723Centaurea_scabiosa2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Bombus_lucorum_-_Centaurea_scabiosa_-_Keila.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Bombus_veteranus_-_Centaurea_scabiosa_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Centaurea_scabiosa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Centaurea_scabiosa_-_p%C3%B5ldjumikas_Valingu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ea/Centaurea_scabiosa_MichaD_2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centaurium_erythraea",
    "rusName": "Золототысячник обыкновенный",
    "family": "Gentianaceae",
    "url": "https://ru.wikipedia.org/wiki/Centaurium_erythraea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/Centaurium_erythraea_220603.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Leiden_Centauria_minor.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centaurium_littorale",
    "rusName": "Золототысячник приморский",
    "family": "Gentianaceae",
    "url": "https://ru.wikipedia.org/wiki/Centaurium_littorale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Centaurium_littorale_%28Salz-Tausendguldenkraut%29_IMG_2323.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centaurium_pulchellum",
    "rusName": "Золототысячник красивый",
    "family": "Gentianaceae",
    "url": "https://ru.wikipedia.org/wiki/Centaurium_pulchellum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bb/CentauriumPulchellum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Centunculus_minimus",
    "rusName": "Низмянка малая",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Centunculus_minimus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Lysimachia_minima.jpg/275px-Lysimachia_minima.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Centunculus_minimus_kz02.jpg/239px-Centunculus_minimus_kz02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cephalanthera_longifolia",
    "rusName": "Пыльцеголовник длиннолистный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Cephalanthera_longifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Cephalanthera_longifolia_-_identifi%C3%A9e7.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/Cephalanthera_longifolia_seglea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Orchidaceae_-_Cephalanthera_longifolia-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Orchidaceae_-_Cephalanthera_longifolia-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Orchidaceae_-_Cephalanthera_longifolia-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/Orchidaceae_-_Cephalanthera_longifolia-6.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Orchidaceae_-_Cephalanthera_longifolia-7.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cephalanthera_rubra",
    "rusName": "Пыльцеголовник красный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Cephalanthera_rubra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Cephalanthera_rubra_catena-rangeval_55_170602_2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Cephalanthera_rubra_flower_150604.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cerastium_holosteoides",
    "rusName": "Ясколка обыкновенная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Cerastium_holosteoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Cerastium_vulgatum_closeup_002.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cerasus_avium",
    "rusName": "Черешня",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Cerasus_avium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Prunus_avium_fruit.jpg/275px-Prunus_avium_fruit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/314_Prunus_avium.jpg/220px-314_Prunus_avium.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/%D0%91%D0%B5%D0%BB%D0%B0%D1%8F_%D1%87%D0%B5%D1%80%D0%B5%D1%88%D0%BD%D1%8F.jpg/220px-%D0%91%D0%B5%D0%BB%D0%B0%D1%8F_%D1%87%D0%B5%D1%80%D0%B5%D1%88%D0%BD%D1%8F.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/S%C3%BC%C3%9Fkirsche_Prunus_avium.jpg/153px-S%C3%BC%C3%9Fkirsche_Prunus_avium.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Wild_Cherry_Leaf_600.jpg/150px-Wild_Cherry_Leaf_600.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Kirschblueten_2048.jpg/132px-Kirschblueten_2048.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Prunus_avium_fruit1.jpg/272px-Prunus_avium_fruit1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/OzvfYDLYY9Q.jpg/310px-OzvfYDLYY9Q.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Cerasus_vulgaris",
    "rusName": "Вишня обыкновенная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Cerasus_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Prunus_cerasus_LC0017.jpg/275px-Prunus_cerasus_LC0017.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_36%29_%287118319535%29.jpg/220px-K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_36%29_%287118319535%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Fr%C3%BChling_bl%C3%BChender_Kirschenbaum.jpg/200px-Fr%C3%BChling_bl%C3%BChender_Kirschenbaum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Prunus_cerasus.jpg/200px-Prunus_cerasus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Prunus_cerasus_blossom_1b.jpg/180px-Prunus_cerasus_blossom_1b.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Owoce_wisni.jpg/200px-Owoce_wisni.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Ceratophyllum_demersum",
    "rusName": "Роголистник погружённый",
    "family": "Ceratophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Ceratophyllum_demersum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/05/Ceratophyllum_demersum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ceratophyllum_submersum",
    "rusName": "Роголистник полупогружённый",
    "family": "Ceratophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Ceratophyllum_submersum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/CeratophyllumSubmersum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Ceratophyllum_submersum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chaerophyllum_aromaticum",
    "rusName": "Бутень ароматный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chaerophyllum_aromaticum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Chaerophyllum_aromaticum_kz4.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги"
  },
  {
    "latName": "Chaerophyllum_aureum",
    "rusName": "Бутень золотистый",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chaerophyllum_aureum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Chaerophyllum_aureum1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chaerophyllum_bulbosum",
    "rusName": "Бутень клубненосный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chaerophyllum_bulbosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/60/20130620Kaelberkropf_Hockenheim3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/81/20140630Chaerophyllum_bulbosum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Chaerophyllum_bulbosum3_W.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Chaerophyllum_bulbosum_%28cropped%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Chaerophyllum_bulbosum_1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Клубни без верхней кожицы. Корни, молодые побеги"
  },
  {
    "latName": "Chaerophyllum_hirsutum",
    "rusName": "Бутень жёстковолосистый",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chaerophyllum_hirsutum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Chaerophyllum_hirsutum_%28Wimper-K%C3%A4lberkropf%29_IMG_20806.JPG/275px-Chaerophyllum_hirsutum_%28Wimper-K%C3%A4lberkropf%29_IMG_20806.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Chaerophyllum_hirsutum_%28Wimper-K%C3%A4lberkropf%29_IMG_20793.JPG/220px-Chaerophyllum_hirsutum_%28Wimper-K%C3%A4lberkropf%29_IMG_20793.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chaerophyllum_temulum",
    "rusName": "Бутень одуряющий",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chaerophyllum_temulum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c1/Illustration_Chaerophyllum_temulentum0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chaiturus_marrubiastrum",
    "rusName": "Щетинохвост",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chaiturus_marrubiastrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Marrubium_vulgare_KZ.jpg/275px-Marrubium_vulgare_KZ.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Leonurus_marrubiastrum_Sturm42.jpg/220px-Leonurus_marrubiastrum_Sturm42.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chamaecytisus_ruthenicus",
    "rusName": "Ракитник русский",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Chamaecytisus_ruthenicus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Chamaecytisus_ruthenicus_%28in_bloom%29.jpg/275px-Chamaecytisus_ruthenicus_%28in_bloom%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Chamaecytisus_ruthenicus_flowers_big.jpg/120px-Chamaecytisus_ruthenicus_flowers_big.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chamaedaphne_calyculata",
    "rusName": "Хамедафне",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Chamaedaphne_calyculata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Chamaedaphne_calyculata_flower.jpg/241px-Chamaedaphne_calyculata_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Chamaedaphne_calyculata_flower.jpg/200px-Chamaedaphne_calyculata_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Chamaedaphne_ausschnitt_Kopie.jpg/200px-Chamaedaphne_ausschnitt_Kopie.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Mountain_Laurel_Kalmia_latifolia_Flowers_2448px.jpg/60px-Mountain_Laurel_Kalmia_latifolia_Flowers_2448px.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chamaenerion_angustifolium",
    "rusName": "Иван-чай узколистный",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Chamaenerion_angustifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Bombus_lucorum_-_Epilobium_angustifolium_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Chamerion_angustifolium_%28inflorescense%29_fermented_tea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4f/Epilobium_angustifolium_Fireweed_seed_stage_tall.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Fireweed_Epilobium_angustifolium_on_rock_sky.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Fireweed_Epilobium_angustifolium_one_flower_close.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Fireweed_after_Swan_Lake_fire.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1d/Maitohorsma_%28Epilobium_angustifolium%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Porcupine-8474.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги, корни и листья"
  },
  {
    "latName": "Chelidonium_majus",
    "rusName": "Чистотел большой",
    "family": "Papaveraceae",
    "url": "https://ru.wikipedia.org/wiki/Chelidonium_majus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Chelidonium_majus20100511_10.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Chelidonium_majus_by_Danny_S._-_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Chelidonium_majus_sap_3_AB.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d6/Flower_October_2008-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8c/Chelidonium_majus_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-033.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chenopodium_acerifolium",
    "rusName": "Марь кленолистная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_acerifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Chenopodium_acerifolium-MW0201116-live.jpg/275px-Chenopodium_acerifolium-MW0201116-live.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chenopodium_album",
    "rusName": "Марь белая",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_album",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/6H-Lambs-quarter.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Lamb%27s_Quarter.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/Melganzenvoet_bloeiwijze_Chenopodium_album.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Melganzenvoet_bloemen_Chenopodium_album.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья и побеги. Семена могут вызвать заболевание нервной системы и пищеварительных органов"
  },
  {
    "latName": "Chenopodium_bonus-henricus",
    "rusName": "Марь цельнолистная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_bonus-henricus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chenopodium_bonus-henricus.JPG/275px-Chenopodium_bonus-henricus.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Illustration_Chenopodium_bonus-henricus0_clean.JPG/220px-Illustration_Chenopodium_bonus-henricus0_clean.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги, соцветия, листья"
  },
  {
    "latName": "Chenopodium_botrys",
    "rusName": "Марь душистая",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_botrys",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Chenopodiumbotrys.jpg/266px-Chenopodiumbotrys.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Chenopodium_botrys_seeds_chbo2_002_php.jpg/150px-Chenopodium_botrys_seeds_chbo2_002_php.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chenopodium_capitatum",
    "rusName": "Блитум головчатый",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_capitatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Chenopodium_capitatum_Strawberry_Blite_2048px.jpg/275px-Chenopodium_capitatum_Strawberry_Blite_2048px.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Illustration_Blitum_capitatum0.jpg/220px-Illustration_Blitum_capitatum0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chenopodium_foliosum",
    "rusName": "Марь многолистная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_foliosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Chenopodium_foliosum.JPG/275px-Chenopodium_foliosum.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья, \"ягоды\""
  },
  {
    "latName": "Chenopodium_glaucum",
    "rusName": "Марь сизая",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_glaucum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Chenopodium_glaucum_kz.jpg/275px-Chenopodium_glaucum_kz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Chenopodium_glaucum_seeds_chgl3_002_php.jpg/150px-Chenopodium_glaucum_seeds_chgl3_002_php.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chenopodium_hybridum",
    "rusName": "Марь гибридная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_hybridum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Chenopodium_hybridum_%28Sautod-G%C3%A4nsefu%C3%9F%29_IMG_1529.jpg/275px-Chenopodium_hybridum_%28Sautod-G%C3%A4nsefu%C3%9F%29_IMG_1529.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Chenopodium_hybridum_Sturm26.JPG/200px-Chenopodium_hybridum_Sturm26.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Chenopodium_hybridum_%28Sautod-G%C3%A4nsefu%C3%9F%29_IMG_1544.jpg/220px-Chenopodium_hybridum_%28Sautod-G%C3%A4nsefu%C3%9F%29_IMG_1544.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chenopodium_murale",
    "rusName": "Марь постенная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_murale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Starr_030202-0092_Chenopodium_murale.jpg/275px-Starr_030202-0092_Chenopodium_murale.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Chenopodium_murale_%28La_Fajana%29_02_ies.jpg/180px-Chenopodium_murale_%28La_Fajana%29_02_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Chenopodium_murale_%28La_Fajana%29_03_ies.jpg/180px-Chenopodium_murale_%28La_Fajana%29_03_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Chenopodium_murale_seeds_chmu2_002_php.jpg/200px-Chenopodium_murale_seeds_chmu2_002_php.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена, Побеги, листья и стебли"
  },
  {
    "latName": "Chenopodium_polyspermum",
    "rusName": "Марь многосеменная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_polyspermum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Chenopodium_polyspermum_2005.08.21_15.18.04-p8210049.jpg/275px-Chenopodium_polyspermum_2005.08.21_15.18.04-p8210049.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Chenopodium_polyspermum_Sturm28.jpg/220px-Chenopodium_polyspermum_Sturm28.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги"
  },
  {
    "latName": "Chenopodium_rubrum",
    "rusName": "Марь красная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_rubrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Chenopodium_rubrum.jpg/275px-Chenopodium_rubrum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Chenopodium_rubrum_eF.jpg/375px-Chenopodium_rubrum_eF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/Rode_ganzenvoet_plant_%28Chenopodium_rubrum%29.jpg/330px-Rode_ganzenvoet_plant_%28Chenopodium_rubrum%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Rode_ganzenvoet_%28Chenopodium_rubrum%29_bloeiwijze.jpg/330px-Rode_ganzenvoet_%28Chenopodium_rubrum%29_bloeiwijze.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Побеги, листья и стебли"
  },
  {
    "latName": "Chenopodium_urbicum",
    "rusName": "Марь городская",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_urbicum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Chenopodium_urbicum_Sturm26.JPG/203px-Chenopodium_urbicum_Sturm26.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья, плоды, семена"
  },
  {
    "latName": "Chenopodium_vulvaria",
    "rusName": "Марь вонючая",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Chenopodium_vulvaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Chenopodium_vulvaria_-_Woodville.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/World_Distribution_of_Chenopodium_vulvaria.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chimaphila_umbellata",
    "rusName": "Зимолюбка зонтичная",
    "family": "Pyrolaceae",
    "url": "https://ru.wikipedia.org/wiki/Chimaphila_umbellata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Chimaphila2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Chimaphila_umbellata_26031.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Chimaphila_umbellata_fleur.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chloris_gayana",
    "rusName": "Родсова трава",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Chloris_gayana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Starr_030418-0014_Chloris_gayana.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chrysanthemum_segetum",
    "rusName": "Златоцвет посевной",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Chrysanthemum_segetum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/Glebionis_segetum_1.jpg/275px-Glebionis_segetum_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Chrysosplenium_alternifolium",
    "rusName": "Селезёночник очерёднолистный",
    "family": "Saxifragaceae",
    "url": "https://ru.wikipedia.org/wiki/Chrysosplenium_alternifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Chrysosplenium_alternifolium_%28Retournemer%29.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и стебли"
  },
  {
    "latName": "Cichorium_intybus",
    "rusName": "Цикорий обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cichorium_intybus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Cykoria_podroznik_pokroj.jpg/275px-Cykoria_podroznik_pokroj.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Illustration_Cichorium_intybus0.jpg/220px-Illustration_Cichorium_intybus0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Witlof_wortels_kuil.jpg/170px-Witlof_wortels_kuil.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Melta_mixture.jpg/170px-Melta_mixture.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Cup_of_melta.jpg/170px-Cup_of_melta.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Корень, зелень"
  },
  {
    "latName": "Cicuta_virosa",
    "rusName": "Вёх ядовитый",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Cicuta_virosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Cicuta_virosa_001.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cimicifuga_europaea",
    "rusName": "Клопогон европейский",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Cimicifuga_europaea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Cimicifuga_europaea_a1.jpg/275px-Cimicifuga_europaea_a1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cinna_latifolia",
    "rusName": "Цинна широколистная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Cinna_latifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Cinna_latifolia_NRCS-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Circaea_alpina",
    "rusName": "Двулепестник альпийский",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Circaea_alpina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Circaea_alpina_6758.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Circaea_lutetiana",
    "rusName": "Двулепестник парижский",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Circaea_lutetiana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/CircaeaLutetiana2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Illustration_Circaea_lutetiana0_clean.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cirsium_arvense",
    "rusName": "Бодяк полевой",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cirsium_arvense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Carduelis_carduelis2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Cirsium_arvense_-_pappus_%28aka%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Cirsium_arvense_-_p%C3%B5ldohakas.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/Thistle_with_cuckoo_spit.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cirsium_heterophyllum",
    "rusName": "Бодяк разнолистный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cirsium_heterophyllum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Bombus_pascuorum_-_Bombus_bohemicus_-_Cirsium_heterophyllum_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Cirsium_heterophyllum_%28Sturm14%2C_plate_004%29_clean%2C_no-description.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Cirsium_heterophyllum_-_villohakas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cirsium_oleraceum",
    "rusName": "Бодяк огородный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cirsium_oleraceum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Cirsium_oleraceum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Cirsium_oleraceum_-_seaohakas.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и молодые стебли"
  },
  {
    "latName": "Cirsium_palustre",
    "rusName": "Бодяк болотный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cirsium_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Cirsium_palustre.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cirsium_pannonicum",
    "rusName": "Бодяк венгерский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cirsium_pannonicum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Cirsium_pannonicum_03.jpg/251px-Cirsium_pannonicum_03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cirsium_setosum",
    "rusName": "Бодяк щетинистый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cirsium_setosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/%D0%91%D0%BE%D0%B4%D1%8F%D0%BA_%D0%BF%D0%BE%D0%BB%D0%B5%D0%B2%D0%BE%D0%B9_KR_01.JPG/275px-%D0%91%D0%BE%D0%B4%D1%8F%D0%BA_%D0%BF%D0%BE%D0%BB%D0%B5%D0%B2%D0%BE%D0%B9_KR_01.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cirsium_vulgare",
    "rusName": "Бодяк обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cirsium_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/99/20160729Cirsium_vulgare5.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодые побеги, донца нераскрывшихся соцветий"
  },
  {
    "latName": "Cladium_mariscus",
    "rusName": "Меч-трава обыкновенная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Cladium_mariscus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/37/Cladium_mariscus.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Cottonsedge_2531125282.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Clematis_recta",
    "rusName": "Ломонос прямой",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Clematis_recta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fd/Erect_Clematis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Clinopodium_vulgare",
    "rusName": "Пахучка обыкновенная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Clinopodium_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Clinopodium_vulgare_inflorescence.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Coeloglossum_viride",
    "rusName": "Пололепестник",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Coeloglossum_viride",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Coeloglossum_viride_01_mg-k.jpg/201px-Coeloglossum_viride_01_mg-k.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Coeloglossum_viridis_Atlas_Alpenflora.jpg/220px-Coeloglossum_viridis_Atlas_Alpenflora.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Colchicum_autumnale",
    "rusName": "Безвременник осенний",
    "family": "Colchicaceae",
    "url": "https://ru.wikipedia.org/wiki/Colchicum_autumnale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Colchicum_autumnale00.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Colchicum_autumnale_01.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Colchicum_autumnale_050505.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Colchicum_autumnale_2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Colchicum_autumnale_ENBLA02.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Colchicum_autumnale_Luc_Viatour.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/95/Colchicum_autumnale_at_the_U.S._Botanic_Gardens_-_Sarah_Stierch.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Colchiques_dans_les_pr%C3%A9s.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/Illustration_Colchicum_autumnale0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Comarum_palustre",
    "rusName": "Сабельник болотный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Comarum_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d9/Apis_mellifera_on_Potentilla_palustris.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Comarum_palustre_-_Niitv%C3%A4lja_bog.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Comarum_palustre_MHNT.BOT.2018.28.25.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Commelina_communis",
    "rusName": "Коммелина обыкновенная",
    "family": "Commelinaceae",
    "url": "https://ru.wikipedia.org/wiki/Commelina_communis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/28/Commelina_communis_003.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Commelina_communis_bgiu_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/Hoverfly_April_2008-3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Suzuki_Harunobu_001.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Conium_maculatum",
    "rusName": "Болиголов пятнистый",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Conium_maculatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/CMchinoCa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Conium.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Gefleckterschierling.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5f/Hemlockseeds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Poison_Hemlock.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Conringia_orientalis",
    "rusName": "Конрингия восточная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Conringia_orientalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Conringia_orientalis_eF.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодые розеточные листья, масло из семян"
  },
  {
    "latName": "Consolida_regalis",
    "rusName": "Сокирки полевые",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Consolida_regalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Consolida_regalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2a/Consolida_regalis_030705.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Consolida_regalis_MHNT.BOT.2009.17.5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Consolida_regalis_bgiu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Ranuncolaceae_-_Consolida_regalis-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/51/Ranuncolaceae_-_Consolida_regalis.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Ranunculaceae_-_Consolida_regalis-2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Convallaria_majalis",
    "rusName": "Ландыш майский",
    "family": "Convallariaceae",
    "url": "https://ru.wikipedia.org/wiki/Convallaria_majalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Convallaria-oliv-r2.jpg/275px-Convallaria-oliv-r2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/390_Convallaria_majalis.jpg/220px-390_Convallaria_majalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Convallaria_majalis20090914_305.jpg/220px-Convallaria_majalis20090914_305.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Convallaria_majalis_IP0404087.jpg/250px-Convallaria_majalis_IP0404087.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/MuguetDesBois.jpg/300px-MuguetDesBois.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Still_red_2086358562.jpg/350px-Still_red_2086358562.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Lilium_convallium_P_315.jpg/310px-Lilium_convallium_P_315.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/Rhizome_%28254_28%29_Lily_of_the_Valley_%28Convallaria_majalis%29.jpg/220px-Rhizome_%28254_28%29_Lily_of_the_Valley_%28Convallaria_majalis%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/C._m._cv._Plena.JPG/120px-C._m._cv._Plena.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/C.m._cv._Rosea.JPG/115px-C.m._cv._Rosea.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Mikolaj_Kopernik.jpg/120px-Mikolaj_Kopernik.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Albert_Sarraut%2C_1er_mai_1936.jpg/220px-Albert_Sarraut%2C_1er_mai_1936.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Penn%C3%A4_reverse_Anu_1990.jpg/220px-Penn%C3%A4_reverse_Anu_1990.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Convolvulus_arvensis",
    "rusName": "Вьюнок полевой",
    "family": "Convolvulaceae",
    "url": "https://ru.wikipedia.org/wiki/Convolvulus_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Bindweed_%28Convolvulus_Arvensis%29_%283105112704%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Convolvulus_arvensis_-_flower_view.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e1/Convolvulus_arvensis_MHNT.BOT.2005.0.960.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Convolvulus_arvenvis_with_mites.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Conyza_canadensis",
    "rusName": "Мелколепестник канадский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Conyza_canadensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Conyza_canadense_002.JPG/275px-Conyza_canadense_002.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Coreopsis_grandiflora",
    "rusName": "Кореопсис крупноцветковый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Coreopsis_grandiflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/Alois_Lunzer04.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/47/CoreopsisGrandiflora001.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c1/Tickseed_--_Coreopsis_grandiflora_sunburst.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Coreopsis_lanceolata",
    "rusName": "Кореопсис ланцетовидный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Coreopsis_lanceolata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2e/C._lanceolata-seeds-USDA_-_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/58/Coreopsis_lanceolata_Arkansas.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Coreopsis_lanceolata_Sterntaler.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Coriandrum_sativum",
    "rusName": "Кориандр",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Coriandrum_sativum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Coriandrum_sativum_001.JPG/275px-Coriandrum_sativum_001.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_145%29_%288232800542%29.jpg/220px-K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_145%29_%288232800542%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/%28MHNT%29_Coriandrum_sativum_-_inflorescence.jpg/220px-%28MHNT%29_Coriandrum_sativum_-_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Sa-cilantro_seeds.jpg/220px-Sa-cilantro_seeds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Coriander_fresh.JPG/220px-Coriander_fresh.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "листья молодых растений. Семена и плоды как пряность"
  },
  {
    "latName": "Cornus_alba",
    "rusName": "Дёрен белый",
    "family": "Cornaceae",
    "url": "https://ru.wikipedia.org/wiki/Cornus_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Cornus9389.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Cornus_alba_%27Ivory_Halo%C2%AE%27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/Cornus_alba_-_WWT_London_Wetland_Centre_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Cornus_alba_10_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Cornus_alba_a2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Cornus_alba_after_freezing_rain.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/00/Cornus_suecica_Ume_River.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Coronaria_flos-cuculi",
    "rusName": "Кукушкин цвет обыкновенный",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Coronaria_flos-cuculi",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Silene_flos-cuculi_flower_-_Niitv%C3%A4lja.jpg/266px-Silene_flos-cuculi_flower_-_Niitv%C3%A4lja.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Illustration_Silene_flos-cuculi0.jpg/220px-Illustration_Silene_flos-cuculi0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Coronilla_varia",
    "rusName": "Секироплодник пёстрый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Coronilla_varia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Ab_plant_2077.jpg/275px-Ab_plant_2077.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Coronilla_varia.jpg/220px-Coronilla_varia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Cleaned-Illustration_Coronilla_varia.jpg/220px-Cleaned-Illustration_Coronilla_varia.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Corydalis_cava",
    "rusName": "Хохлатка полая",
    "family": "Fumariaceae",
    "url": "https://ru.wikipedia.org/wiki/Corydalis_cava",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/20150322Corydalis_cava3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Compilation_of_seeds_with_elaiosomes_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Corydalis_cava_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Corydalis_cava_Bulb.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/05/Corydalis_cava_MichaD_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Corydalis_cava_MichaD_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Corydalis_cava_MichaD_3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Corydalis_intermedia",
    "rusName": "Хохлатка промежуточная",
    "family": "Fumariaceae",
    "url": "https://ru.wikipedia.org/wiki/Corydalis_intermedia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Corydalis_intermedia1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Corydalis_solida",
    "rusName": "Хохлатка плотная",
    "family": "Fumariaceae",
    "url": "https://ru.wikipedia.org/wiki/Corydalis_solida",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Corydalis_solida_%27George_Baker%27_090325.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2e/Corydalis_solida_240406.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Corylus_avellana",
    "rusName": "Лещина обыкновенная",
    "family": "Betulaceae",
    "url": "https://ru.wikipedia.org/wiki/Corylus_avellana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Corylus_avellana.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Corylus_avellana_%27heterophylla%27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Corylus_avellana_female_flower_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Hazelnut_in_blossom.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Katjes_van_een_hazelaar_%28Corylus_avellana%29._26-01-2021._%28actm.%29_02.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Орехи"
  },
  {
    "latName": "Corynephorus_canescens",
    "rusName": "Булавоносец седоватый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Corynephorus_canescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Silbergras2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cotoneaster_lucidus",
    "rusName": "Кизильник блестящий",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Cotoneaster_lucidus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Iceland_Plants_4939.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Crataegus_crus-galli",
    "rusName": "Боярышник петушья шпора",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Crataegus_crus-galli",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Cockspur_Thorn3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Crataegus_crus-galli_flowers_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Crataegus_crus-galli_fruit_USDA-ARS.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Crataegus_crus_galli_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Crataegus_crus_galli_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Crataegus_crus_galli_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Crataegus_crus_galli_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Crataeguscrusgalli.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Crataegus_curvisepala",
    "rusName": "Боярышник отогнуточашелистиковый",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Crataegus_curvisepala",
    "images": [],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Crataegus_mollis",
    "rusName": "Боярышник мягкий",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Crataegus_mollis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Crataegus-mollis-bud-break.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Crataegus-mollis-flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Crataegus-mollis-fruit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/00/Crataegus-mollis-trunk.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Crataegus_monogyna",
    "rusName": "Боярышник однопестичный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Crataegus_monogyna",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/47/%28MHNT%29_Crataegus_monogyna_-_flowers_and_buds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/2013-05-23_07_24_06_Crataegus_monogyna_%27Crimson_Cloud%27_blossoms_in_Elko_Nevada.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Common_hawthorn.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Crataegus_monogyna_%28subsp._monogyna%29_sl30.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Eenstijlige_meidoorn_%28Crataegus_monogyna_branch%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Hawthorn_St_Mars.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Hawthorn_St_Mars_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/Hawthorn_fruit.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Hethel_Thorn%2C_Hethel_-_geograph.org.uk_-_44746.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Crataegus_sanguinea",
    "rusName": "Боярышник кроваво-красный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Crataegus_sanguinea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Crataegus_pedicellata.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Crataegus_sanguinea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/56/Pair_of_lemons.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Crepis_biennis",
    "rusName": "Скерда двулетняя",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Crepis_biennis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Chicory_flower_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Crepis_biennis_%28Wiesen-Pippau%29_IMG_22243.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Crepis_paludosa",
    "rusName": "Скерда болотная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Crepis_paludosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Chicory_flower_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Crepis_paludosa_Sturm55.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Crepis_tectorum",
    "rusName": "Скерда кровельная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Crepis_tectorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Bombus_soroeensis_-_Crepis_tectorum_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Illustration_Crepis_tectorum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Liiv-koeratubakas_%28Crepis_tectorum%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Crypsis_alopecuroides",
    "rusName": "Скрытница лисохвостовидная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Crypsis_alopecuroides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Crypsis_alopecuroides.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cucubalus_baccifer",
    "rusName": "Волдырник ягодный",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Cucubalus_baccifer",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Silene_baccifera_002.jpg/275px-Silene_baccifera_002.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cuscuta_epithymum",
    "rusName": "Повилика тимьяновая",
    "family": "Cuscutaceae",
    "url": "https://ru.wikipedia.org/wiki/Cuscuta_epithymum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/28/Cuscuta_epithymum1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Ipomoea_purpurea_2400px.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/82/VIscum_album_fruit_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cuscuta_europaea",
    "rusName": "Повилика европейская",
    "family": "Cuscutaceae",
    "url": "https://ru.wikipedia.org/wiki/Cuscuta_europaea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/Cuscuta_europaea_%28on_Urtica_dioica%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cyclachaena_xanthiifolia",
    "rusName": "Циклахена",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Cyclachaena_xanthiifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Cyclachaena_xanthiifolia.JPG/275px-Cyclachaena_xanthiifolia.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cynodon_dactylon",
    "rusName": "Свинорой пальчатый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Cynodon_dactylon",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Bermuda_Grass_growing_out_of_a_curb.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Cynodon_dactylon.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Cynodon_dactylon_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Cynodon_dactylon_at_Peradeniya_Royal_Botanical_Garden.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cynoglossum_officinale",
    "rusName": "Чернокорень лекарственный",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Cynoglossum_officinale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/Cynoglossum_officinale_190506a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Cynoglossum_officinale_W.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cynosurus_cristatus",
    "rusName": "Гребенник обыкновенный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Cynosurus_cristatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Anthers_and_stamen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Cynosurus.cristatus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Cynosurus_cristatus_MHNT.BOT.2007.43.5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Kamgras_%28Cynosurus_cristatus%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Ligule_of_crested_dogstail.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d9/Lower_side.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cyperus_fuscus",
    "rusName": "Сыть бурая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Cyperus_fuscus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Cyperus_Fuscus_crop.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Cyperus_diffusus1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cypripedium_calceolus",
    "rusName": "Башмачок настоящий",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Cypripedium_calceolus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Cypripedium_calceolus_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/Cypripedium_calceolus_wiki_mg-k01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Guckusko_Jamtland_jun2004_UlfSundberg.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Cystopteris_fragilis",
    "rusName": "Пузырник ломкий",
    "family": "Athyriaceae",
    "url": "https://ru.wikipedia.org/wiki/Cystopteris_fragilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Cystopteris_fragilis_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Silver-fern.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dactylis_glomerata",
    "rusName": "Ежа сборная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Dactylis_glomerata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Dactylis_glomerata_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Dactylis_glomerata_Habitus_Valderrepisa_2012-5-31_SierraMadrona.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Dactylis_glomerata_bluete1.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Dactylis_glomerata_stems_and_ligules.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dactylorhiza_fuchsii",
    "rusName": "Пальчатокоренник Фукса",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Dactylorhiza_fuchsii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Dactylorhiza_fuchsii_alba_-_K%C3%A4esalu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Dactylorhiza_fuchsii_hebridensis_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/42/Dactylorhiza_fuchsii_okellyi_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Mg-k_d1002355_dactylorhiza_fuchsii_x_gymnadenia_conopsea.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dactylorhiza_incarnata",
    "rusName": "Пальчатокоренник мясо-красный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Dactylorhiza_incarnata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6a/Black-veined_white_%28Aporia_crataegi%29_male_underside.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Dactylorhiza_incarnata_-_Kahkjaspunane_s%C3%B5rmk%C3%A4pp_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Dactylorhiza_incarnata_-_kahkjaspunane_s%C3%B5rmk%C3%A4pp_Pakri.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a1/Dactylorhiza_incarnata_ochroleuca_-_Niitv%C3%A4lja_bog.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Dactylorhiza_incarnata_ssp_cruenta_LC0305.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Mg-k_d1002037_dactylorhiza_coccinea.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dactylorhiza_longifolia",
    "rusName": "Пальчатокоренник балтийский",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Dactylorhiza_longifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Dactylorhiza_longifolia1.JPG/274px-Dactylorhiza_longifolia1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/%D0%9F%D0%B0%D0%BB%D1%8C%D1%87%D0%B0%D1%82%D0%BE%D0%BA%D0%BE%D1%80%D0%B5%D0%BD%D0%BD%D0%B8%D0%BA_%D0%B1%D0%B0%D0%BB%D1%82%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9.jpg/220px-%D0%9F%D0%B0%D0%BB%D1%8C%D1%87%D0%B0%D1%82%D0%BE%D0%BA%D0%BE%D1%80%D0%B5%D0%BD%D0%BD%D0%B8%D0%BA_%D0%B1%D0%B0%D0%BB%D1%82%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/%D0%9F%D0%B0%D0%BB%D1%8C%D1%87%D0%B0%D1%82%D0%BE%D0%BA%D0%BE%D1%80%D0%B5%D0%BD%D0%BD%D0%B8%D0%BA_%D0%B1%D0%B0%D0%BB%D1%82%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9_%28%D1%84%D1%80%D0%B0%D0%B3%D0%BC%D0%B5%D0%BD%D1%82%29.jpg/220px-%D0%9F%D0%B0%D0%BB%D1%8C%D1%87%D0%B0%D1%82%D0%BE%D0%BA%D0%BE%D1%80%D0%B5%D0%BD%D0%BD%D0%B8%D0%BA_%D0%B1%D0%B0%D0%BB%D1%82%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9_%28%D1%84%D1%80%D0%B0%D0%B3%D0%BC%D0%B5%D0%BD%D1%82%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dactylorhiza_maculata",
    "rusName": "Пальчатокоренник пятнистый",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Dactylorhiza_maculata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Dactylorhiza_maculata20090812_084.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/55/Dactylorhiza_maculata20090914_071.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Dactylorhiza_maculata_LC0232.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Dactylorhiza_maculata_LC0233.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Dactylorhiza_maculata_in_a_garden.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Orchidaceae_-_Dactylorhiza_maculata-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3a/Orchidaceae_-_Dactylorhiza_maculata-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Orchidaceae_-_Dactylorhiza_maculata-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/81/Orchidaceae_-_Dactylorhiza_maculata.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dactylorhiza_majalis",
    "rusName": "Пальчатокоренник майский",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Dactylorhiza_majalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/06/Dactylorhiza_majalis_LC0269.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Dactylorhiza_majalis_flowers140503.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Dactylorhiza_majalis_habitat200504.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Dactylorhiza_x_aschersoniana_190505.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Dactylorhiza_x_braunii_030606.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dactylorhiza_sambucina",
    "rusName": "Пальчатокоренник бузинный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Dactylorhiza_sambucina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Dactylorhiza_sambucina_-_Flickr_003.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Dactylorhiza_sambucina_040531Bw.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Dactylorhiza_sambucina_Rheinland-Pfalz_03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Orchidaceae_-_Dactylorhiza_sambucina-1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dactylorhiza_traunsteineri",
    "rusName": "Пальчатокоренник Траунштейнера",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Dactylorhiza_traunsteineri",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Dactylorhiza_traunsteineri_12.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Flower_of_Rossioglossum_ampliatum_-_Rspb.2013.0960-F1.large-right.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Daphne_cneorum",
    "rusName": "Волчеягодник боровой",
    "family": "Thymelaeaceae",
    "url": "https://ru.wikipedia.org/wiki/Daphne_cneorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/59/DAPHNE_CNEORUM_-_B%C3%92FIA_-_IB-407_%28Flor_de_pastor%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Daphne_cneorum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Daphne_mezereum_ENBLA03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Daphne_mezereum",
    "rusName": "Волчеягодник обыкновенный",
    "family": "Thymelaeaceae",
    "url": "https://ru.wikipedia.org/wiki/Daphne_mezereum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Daphne-mezereum-habit2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Daphne_mezereum2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Daphne_mezereum_004.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Daphne_mezereum_Kouvervaara_Kuusamo_29.7.2005.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Datura_stramonium",
    "rusName": "Дурман обыкновенный",
    "family": "Solanaceae",
    "url": "https://ru.wikipedia.org/wiki/Datura_stramonium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Datura_str_tat5.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Datura_stramonium_2_%282005_07_07%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Datura_stramonium_MHNT.BOT.2004.0.263a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/75/Thorn-apple.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Daucus_carota",
    "rusName": "Морковь дикая",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Daucus_carota",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Carrot-fb.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Daucus_carota_%28Queen_Anne%27s_lace%29_flower_umbell_down_view.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Daucus_carota_May_2008-1_edit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Daucus_carota_seed_pod.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Queen_Anns_Lace_--_Daucus_carota.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Корнеплод"
  },
  {
    "latName": "Delphinium_elatum",
    "rusName": "Живокость высокая",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Delphinium_elatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a3/Delphinium_elatum_-_panoramio.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dentaria_bulbifera",
    "rusName": "Сердечник луковичный",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Dentaria_bulbifera",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Dentaria_spp_Sturm14.jpg/273px-Dentaria_spp_Sturm14.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Deschampsia_cespitosa",
    "rusName": "Луговик дернистый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Deschampsia_cespitosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/DeschampsiaCesp.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Deschampsia_cespitosa_ligula.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Deschampsia_cespitosa_seed.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Descurainia_sophia",
    "rusName": "Дескурайния Софии",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Descurainia_sophia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8c/Descurainia_sophia_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dianthus_arenarius",
    "rusName": "Гвоздика песчаная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Dianthus_arenarius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Dianthus_arenarius_030720085691.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/09/Dianthus_arenarius_arenarius_-_n%C3%B5mmnelk_Pakri2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dianthus_armeria",
    "rusName": "Гвоздика армериевидная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Dianthus_armeria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/77/%28MHNT%29_Dianthus_armeria_-_Plant_habit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Deptford_Pink_%28Dianthus_armeria%29_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/Dianthus_armeria_seeds.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dianthus_barbatus",
    "rusName": "Гвоздика турецкая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Dianthus_barbatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Bartnelke_2008.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Bund_Bartnelken.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/06/Dianthus_barbatus1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Dianthus_barbatus_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Dianthus_barbatus_flowers_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Dianthus_barbatus_flowers_03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Gerard_John_1545-1612.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/June_Summer_Flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Pink_Sweet_William_flowers.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Цветки"
  },
  {
    "latName": "Dianthus_campestris",
    "rusName": "Гвоздика полевая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Dianthus_campestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/%D0%93%D0%B2%D0%BE%D0%B7%D0%B4%D0%B8%D0%BA%D0%B0_%D0%BF%D0%BE%D0%BB%D0%B5%D0%B2%D0%B0%D1%8F_%28_Dianthus_campestris%29.JPG/275px-%D0%93%D0%B2%D0%BE%D0%B7%D0%B4%D0%B8%D0%BA%D0%B0_%D0%BF%D0%BE%D0%BB%D0%B5%D0%B2%D0%B0%D1%8F_%28_Dianthus_campestris%29.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dianthus_deltoides",
    "rusName": "Гвоздика травянка",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Dianthus_deltoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/0_Dianthus_deltoides_alpinus_-_Yvoire.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dianthus_superbus",
    "rusName": "Гвоздика пышная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Dianthus_superbus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Dianthus_%27Tatra_Ghost%27_%28SG%29_%2830389744122%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d9/Dianthus_superbus.MHNT.BOT.2012.10.8.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Dianthus_superbus_140805a.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dichostylis_micheliana",
    "rusName": "Сыть Микели",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Dichostylis_micheliana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Dichostylis_micheliana%2C_Kuban%27_River%2C_Ust%27-Labinsk_02.jpg/275px-Dichostylis_micheliana%2C_Kuban%27_River%2C_Ust%27-Labinsk_02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Digitalis_grandiflora",
    "rusName": "Наперстянка крупноцветковая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Digitalis_grandiflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/09/Digitalis_grandiflora02072006.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Digitalis_grandiflora_BotGardBln1105FruitsA.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Digitalis_grandioflora_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Gro%C3%9Fbl%C3%BCtiger_Fingerhut-Bl%C3%BCten.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Digitalis_purpurea",
    "rusName": "Наперстянка пурпурная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Digitalis_purpurea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Bysedd_y_cwn_ger_Solfach_-_Foxgloves_near_Solva%2C_Sir_Benfro_Pembrokeshire%2C_Wales_10.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Digitalis_Purpurea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2e/Digitalis_purpurea_%27Primrose_Carousel%27_Flower_Closeup_1200px.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/91/Digitalis_purpurea_%27Primrose_Carousel%27_Leaves_1100px.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a1/Digitalis_purpurea_-_flower_view01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Digitalis_purpurea_004.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/Digitalis_purpurea_LC0101.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Digitalis_purpurea_alba_01-Juni.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Digitaria_ischaemum",
    "rusName": "Росичка обыкновенная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Digitaria_ischaemum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Digitaria_ischaemum_10058273.jpg/413px-Digitaria_ischaemum_10058273.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Diphasiastrum_complanatum",
    "rusName": "Дифазиаструм сплюснутый",
    "family": "Lycopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Diphasiastrum_complanatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Diphasiastrum_complanatum_151207c.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Lycopodium_saururus.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Diphasiastrum_tristachyum",
    "rusName": "Дифазиаструм трёхколосковый",
    "family": "Lycopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Diphasiastrum_tristachyum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Diphasiastrum_tristachyum3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Diphasiastrum_tristachyum_240813a.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Diphasiastrum_tristachyum_240813d.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Draba_nemorosa",
    "rusName": "Крупка дубравная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Draba_nemorosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Draba_nemorosa.JPG/275px-Draba_nemorosa.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Draba_nemorosa_BB-1913.jpg/220px-Draba_nemorosa_BB-1913.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dracocephalum_ruyschiana",
    "rusName": "Змееголовник Рюйша",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Dracocephalum_ruyschiana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1d/Dracocephalum_ruyschiana_-_Sile_tondipea_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dracocephalum_thymiflorum",
    "rusName": "Змееголовник тимьяноцветковый",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Dracocephalum_thymiflorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/20395598-Dracocephalum_thymiflorum.jpg/249px-20395598-Dracocephalum_thymiflorum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Drosera_anglica",
    "rusName": "Росянка английская",
    "family": "Droseraceae",
    "url": "https://ru.wikipedia.org/wiki/Drosera_anglica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Drosera_anglica_Kauai.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Drosera_anglica_dw.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Drosera_anglica_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Drosera_anglica_habit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Drosera_anglica_habitat.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/Drosera_anglica_ne1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Drosera_anglica_ne3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Drosera_intermedia",
    "rusName": "Росянка промежуточная",
    "family": "Droseraceae",
    "url": "https://ru.wikipedia.org/wiki/Drosera_intermedia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4b/Drosera_intermedia0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Drosera_intermedia1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Drosera_intermedia_Kentucky.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Drosera_intermedia_ne1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Mittlerer_Sonnentau_i_d_Bl%C3%BCte.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Drosera_rotundifolia",
    "rusName": "Росянка круглолистная",
    "family": "Droseraceae",
    "url": "https://ru.wikipedia.org/wiki/Drosera_rotundifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/ce/Drosera-rotundifolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Drosera_rotundifolia_Brown%27s_Lake_Bog.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Drosera_rotundifolia_habitat.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Drosera_rotundifolia_in_Minnesota.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/06/Drosera_rotundifolia_leaf1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Drosera_rotundifolia_ne1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7e/Drosera_rotundifolia_ne2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Drosera_rotundifolia_ne3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dryopteris_carthusiana",
    "rusName": "Щитовник картузианский",
    "family": "Dryopteridaceae",
    "url": "https://ru.wikipedia.org/wiki/Dryopteris_carthusiana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Dryopteris_carthusiana1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/aa/Dryopteris_carthusiana6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Silver-fern.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Жаренные корневища. Однако, есть указания на ядовитость"
  },
  {
    "latName": "Dryopteris_cristata",
    "rusName": "Щитовник гребенчатый",
    "family": "Dryopteridaceae",
    "url": "https://ru.wikipedia.org/wiki/Dryopteris_cristata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Dryopteris_cristata.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Silver-fern.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dryopteris_expansa",
    "rusName": "Щитовник распростёртый",
    "family": "Dryopteridaceae",
    "url": "https://ru.wikipedia.org/wiki/Dryopteris_expansa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/75/Dryopteris_expansa.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Dryopteris_filix-mas",
    "rusName": "Щитовник мужской",
    "family": "Dryopteridaceae",
    "url": "https://ru.wikipedia.org/wiki/Dryopteris_filix-mas",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Dryopteris_filix_mas_nf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Dryopteris_filix-mas_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-202.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Echinochloa_crusgalli",
    "rusName": "Ежовник обыкновенный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Echinochloa_crusgalli",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Echinochloa_crus-galli_2006.08.27_14.59.37-p8270051.jpg/275px-Echinochloa_crus-galli_2006.08.27_14.59.37-p8270051.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Echinocystis_lobata",
    "rusName": "Эхиноцистис",
    "family": "Cucurbitaceae",
    "url": "https://ru.wikipedia.org/wiki/Echinocystis_lobata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Echinocystis_lobata_001.jpg/275px-Echinocystis_lobata_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Echinocystis_lobata6pl.jpg/144px-Echinocystis_lobata6pl.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Echinocystis_lobata7pl.jpg/220px-Echinocystis_lobata7pl.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Echinocystis_lobata_004.jpg/180px-Echinocystis_lobata_004.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Echinocystis_lobata_003.jpg/180px-Echinocystis_lobata_003.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Echinocystis_lobata_002.jpg/180px-Echinocystis_lobata_002.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Echinops_sphaerocephalus",
    "rusName": "Мордовник шароголовый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Echinops_sphaerocephalus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/76/Asteraceae_-_Echinops_sphaerocephalus-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/e6/Asteraceae_-_Echinops_sphaerocephalus-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Asteraceae_-_Echinops_sphaerocephalus.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a3/Illustration_Echinops_sphaerocephalus0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Kugeldistel_20050705_554.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Kugeldistel_20050705_565.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "используется в пищу подобно артишоку"
  },
  {
    "latName": "Echium_plantagineum",
    "rusName": "Синяк подорожниковый",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Echium_plantagineum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Echium_April_2010-2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Echium_plantagineum_flowers.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Echium_vulgare",
    "rusName": "Синяк обыкновенный",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Echium_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Blueweed.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/Bombus_lucorum_queen_-_Echium_vulgare_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Echium_vulgare_L.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Echium_vulgare_bl%C3%A5eld.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Viper%27s_Bugloss_colonizing_the_banks_of_a_city_highway..jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Elatine_alsinastrum",
    "rusName": "Повойничек мокричный",
    "family": "Elatinaceae",
    "url": "https://ru.wikipedia.org/wiki/Elatine_alsinastrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Fig._10_Elatine_alsinastrum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Starr_010309-0546_Calophyllum_inophyllum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Elatine_hydropiper",
    "rusName": "Повойничек перечный",
    "family": "Elatinaceae",
    "url": "https://ru.wikipedia.org/wiki/Elatine_hydropiper",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/Elatine_hydropiper.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eleocharis_acicularis",
    "rusName": "Болотница игольчатая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Eleocharis_acicularis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Cottonsedge_2531125282.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Eleocharis_acicularis.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eleocharis_palustris",
    "rusName": "Болотница болотная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Eleocharis_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Cottonsedge_2531125282.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/EleocharisPalustrisAspekt.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Eleocharis_palustris_in_Rehovot_vernal_pool.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eleocharis_quinqueflora",
    "rusName": "Болотница пятицветковая",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Eleocharis_quinqueflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Eleocharis_quinqueflora.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Elodea_canadensis",
    "rusName": "Элодея канадская",
    "family": "Hydrocharitaceae",
    "url": "https://ru.wikipedia.org/wiki/Elodea_canadensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/ElodeaCanadensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/ElodeaCanadensisFlowering.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/ElodeaLeaffaceupcells.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/82/Elodea_canadensis_nf.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Elsholtzia_ciliata",
    "rusName": "Эльсгольция реснитчатая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Elsholtzia_ciliata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/Elsholtzia_ciliata-2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Elymus_caninus",
    "rusName": "Пырейник собачий",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Elymus_caninus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Elymus_caninus.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Elymus_sibiricus",
    "rusName": "Пырейник сибирский",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Elymus_sibiricus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Plantarum_indigenarum_et_exoticarum_icones_ad_vivum_coloratae%2C_oder%2C_Sammlung_nach_der_Natur_gemalter_Abbildungen_inn-_und_ausl%C3%A4ndlischer_Pflanzen%2C_f%C3%BCr_Liebhaber_und_Beflissene_der_Botanik_%2815903860549%29.jpg/237px-thumbnail.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Elytrigia_intermedia",
    "rusName": "Пырей средний",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Elytrigia_intermedia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Elymus_hispidus_var._villosus_sl1.jpg/275px-Elymus_hispidus_var._villosus_sl1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Elytrigia_repens",
    "rusName": "Пырей ползучий",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Elytrigia_repens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Elymus_repens_%283900638448%29.jpg/275px-Elymus_repens_%283900638448%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Illustration_Elytrigia_repens1.jpg/220px-Illustration_Elytrigia_repens1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Ilu_mullas.JPG/250px-Ilu_mullas.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Kweek_ligula_Elytrigia_repens.jpg/255px-Kweek_ligula_Elytrigia_repens.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Elymus_repens_sl4.jpg/160px-Elymus_repens_sl4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Elymus_repens_%28L.%29_Gould_-_quackgrass_-_ELRE4_-_Steve_Hurst_%40_USDA-NRCS_PLANTS_Database.jpg/250px-Elymus_repens_%28L.%29_Gould_-_quackgrass_-_ELRE4_-_Steve_Hurst_%40_USDA-NRCS_PLANTS_Database.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Elymus_repens_and_potato.jpg/220px-Elymus_repens_and_potato.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Empetrum_nigrum",
    "rusName": "Водяника чёрная",
    "family": "Empetraceae",
    "url": "https://ru.wikipedia.org/wiki/Empetrum_nigrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Alaskan_Crowberry_from_alpine-tundra_regions.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Empetrum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Empetrum_nigrum_by_Maseltov_2.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Epilobium_adenocaulon",
    "rusName": "Кипрей железистостебельный",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Epilobium_adenocaulon",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Epilobium_ciliatum_eu_76_14062008_2.jpg/268px-Epilobium_ciliatum_eu_76_14062008_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Epilobium_adenocaulon_Hauskn._-_H._Sharp.jpg/220px-Epilobium_adenocaulon_Hauskn._-_H._Sharp.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Epilobium_hirsutum",
    "rusName": "Кипрей волосистый",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Epilobium_hirsutum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/13/Epilobium_hirsutum_-_karvane_pajulill_Keilas.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Epilobium_hirsutum_jfg.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Epilobium_montanum",
    "rusName": "Кипрей горный",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Epilobium_montanum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Blue_Eyes_Fuchsia.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Epilobium_%2814783507140%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Epilobium_palustre",
    "rusName": "Кипрей болотный",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Epilobium_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Blue_Eyes_Fuchsia.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Epilobium_palustre_bluete.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Epilobium_roseum",
    "rusName": "Кипрей розовый",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Epilobium_roseum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/aa/Epilobium_roseum_%287914520828%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Epipactis_atrorubens",
    "rusName": "Дремлик тёмно-красный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Epipactis_atrorubens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Epipactis_atrorubens_%28f._pallens%29_-_M%C3%A4nniku.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Epipactis_atrorubens_-_M%C3%A4nniku.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Epipactis_atrorubens_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Epipactis_atrorubens_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Epipactis_atrorubens_pink_flower_-_M%C3%A4nniku.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Kruszczyk_rdzawoczerwony_%28Epipactis_atrorubens%29_gimped.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Epipactis_helleborine",
    "rusName": "Дремлик широколистный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Epipactis_helleborine",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Epipactis_helleborine_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Epipactis_helleborine_flowers1_220703.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bb/Epipactis_helleborine_range_map.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7e/Hoverfly_depositing_egg.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Epipactis_palustris",
    "rusName": "Дремлик болотный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Epipactis_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Epipactis_palustris_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Epipactis_palustris_-_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/28/Epipactis_palustris_-_fruits.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Epipactis_palustris_230705.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Epipactis_palustris_var._ochroleuca_-_Niitv%C3%A4lja2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Epipactis_palustris_var._ochroleuca_-_Niitv%C3%A4lja_bog.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Soo-neiuvaip.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Soo-neiuvaip.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Epipogium_aphyllum",
    "rusName": "Надбородник безлистный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Epipogium_aphyllum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Epipogium_aphyllum_-_Alutaguse.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Epipogium_aphyllum_-_J%C3%B5hvi.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Epipogium_aphyllum_plants.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Equisetum_arvense",
    "rusName": "Хвощ полевой",
    "family": "Equisetaceae",
    "url": "https://ru.wikipedia.org/wiki/Equisetum_arvense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Equisetum_arvense_Iceland_20070707.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Equisetum_arvense_foliage.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/27/Equisetum_arvense_fr.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Весенние (генеративные) побеги (x)"
  },
  {
    "latName": "Equisetum_fluviatile",
    "rusName": "Хвощ приречный",
    "family": "Equisetaceae",
    "url": "https://ru.wikipedia.org/wiki/Equisetum_fluviatile",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Equisetum_fluviatile_Luc_Viatour.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/Equisetum_fluviatile_central_hollow.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Equisetum_hyemale",
    "rusName": "Хвощ зимующий",
    "family": "Equisetaceae",
    "url": "https://ru.wikipedia.org/wiki/Equisetum_hyemale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Dried_Equisetum_hyemale.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Equisetum_hyemale.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ce/Equisetum_hyemale_subsp._affine_-_Cap_Tourmente_Qu%C3%A9bec.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Schuurbies_Leblanc.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Equisetum_palustre",
    "rusName": "Хвощ болотный",
    "family": "Equisetaceae",
    "url": "https://ru.wikipedia.org/wiki/Equisetum_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/48/Equisetum_palustre_detail.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Equisetum_pratense",
    "rusName": "Хвощ луговой",
    "family": "Equisetaceae",
    "url": "https://ru.wikipedia.org/wiki/Equisetum_pratense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Equisetum_pratense.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/a/aa/Meadow_Horsetail_II_2496723699.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "сырые корни, Молодые спороносные побеги (x)"
  },
  {
    "latName": "Equisetum_sylvaticum",
    "rusName": "Хвощ лесной",
    "family": "Equisetaceae",
    "url": "https://ru.wikipedia.org/wiki/Equisetum_sylvaticum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/89/Equisetum_sylvaticum_240405.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые спороносные побеги (x)"
  },
  {
    "latName": "Equisetum_telmateia",
    "rusName": "Хвощ большой",
    "family": "Equisetaceae",
    "url": "https://ru.wikipedia.org/wiki/Equisetum_telmateia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Equisetopsida.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Equisetum_telmateia_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Equisetum_telmateia_5.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые спороносные побеги (x)"
  },
  {
    "latName": "Equisetum_variegatum",
    "rusName": "Хвощ пёстрый",
    "family": "Equisetaceae",
    "url": "https://ru.wikipedia.org/wiki/Equisetum_variegatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Flower-center130935.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eragrostis_cilianensis",
    "rusName": "Полевичка крупноколосковая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Eragrostis_cilianensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Starr_020124-8001_Eragrostis_cilianensis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eragrostis_minor",
    "rusName": "Полевичка малая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Eragrostis_minor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Eragrostis_1ziarnek.jpg/275px-Eragrostis_1ziarnek.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Eragrostis_minor_illustration_%2801%29.jpg/220px-Eragrostis_minor_illustration_%2801%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/20171004Eragrostis_minor1.jpg/220px-20171004Eragrostis_minor1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eragrostis_pilosa",
    "rusName": "Полевичка волосистая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Eragrostis_pilosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Eragrostis_pilosa_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eremogone_saxatilis",
    "rusName": "Пустынница скальная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Eremogone_saxatilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Arenaria_procera_glabra3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eremopyrum_orientale",
    "rusName": "Мортук восточный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Eremopyrum_orientale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Eremopyrum_orientale_211736509.jpg/275px-Eremopyrum_orientale_211736509.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Eremopyrum_orientale_192900779.jpg/220px-Eremopyrum_orientale_192900779.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eremopyrum_triticeum",
    "rusName": "Мортук пшеничный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Eremopyrum_triticeum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/2015.05.08_17.37.19_IMG_2050_-_Flickr_-_andrey_zharkikh.jpg/275px-2015.05.08_17.37.19_IMG_2050_-_Flickr_-_andrey_zharkikh.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Eremopyrum_triticeum_%286180952581%29.jpg/220px-Eremopyrum_triticeum_%286180952581%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Erigeron_acris",
    "rusName": "Мелколепестник едкий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Erigeron_acris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Erigeron_acris_270805.jpg/275px-Erigeron_acris_270805.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eriophorum_gracile",
    "rusName": "Пушица стройная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Eriophorum_gracile",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Cottonsedge_2531125282.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Eriophorum_gracile_NRCS-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eriophorum_latifolium",
    "rusName": "Пушица широколистная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Eriophorum_latifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e1/Eriophorum_latifolium1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eriophorum_polystachion",
    "rusName": "Пушица узколистная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Eriophorum_polystachion",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Eriophorum_angustifolium_1_%28Picos%29.JPG/275px-Eriophorum_angustifolium_1_%28Picos%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/425_Eriophorum_polystachyum%2C_E._vaginatum.jpg/220px-425_Eriophorum_polystachyum%2C_E._vaginatum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eriophorum_vaginatum",
    "rusName": "Пушица влагалищная",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Eriophorum_vaginatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Eriophorum_vaginatum_close.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/28/Hare%27s-tail_cottongrass_Eriophorum_vaginatum_Puszcza_Piska_Forest_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Wollgras_Eriophorum_vaginatum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Erodium_cicutarium",
    "rusName": "Аистник обыкновенный",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Erodium_cicutarium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/27/Erodium_cicutarium_3246.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Erodium_cicutarium_MHNT.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d2/Erodium_cicutarium_flowers_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Illustration_Erodium_cicutarium0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eruca_sativa",
    "rusName": "Рукола",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Eruca_sativa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Eruca_sativa_1_IP0206101.jpg/255px-Eruca_sativa_1_IP0206101.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Arugula.jpg/180px-Arugula.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Eruca_February_2008-1.jpg/180px-Eruca_February_2008-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Rocket_Salad%2C_Arugula%2C_Roquette%2C_Rucola%2C_Rugula_%28Eruca_vesicaria_subsp._sativa%29.jpg/180px-Rocket_Salad%2C_Arugula%2C_Roquette%2C_Rucola%2C_Rugula_%28Eruca_vesicaria_subsp._sativa%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Rocket_%26radicchio_salad.jpg/220px-Rocket_%26radicchio_salad.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодые побеги и листья"
  },
  {
    "latName": "Eryngium_campestre",
    "rusName": "Синеголовник полевой",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Eryngium_campestre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/75/Eryngium_campestre_%28inflorescences%29_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Eryngium_campestre_310705b.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Eryngium_campestre_MHNT.BOT.2007.40.36.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eryngium_planum",
    "rusName": "Синеголовник плосколистный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Eryngium_planum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Eryngium_planum_bgiu.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Erysimum_canescens",
    "rusName": "Желтушник раскидистый",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Erysimum_canescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Erysimum_diffusum_agg._sl2.jpg/275px-Erysimum_diffusum_agg._sl2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Erysimum_canescens_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v19.jpg/220px-Erysimum_canescens_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v19.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Erysimum_cheiranthoides",
    "rusName": "Желтушник левкойный",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Erysimum_cheiranthoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Erysimum_cheiranthoides_variety_Elbtalaue.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Gewone_steenraket_bloemen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c2/Gewone_steenraket_plant.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euonymus_europaea",
    "rusName": "Бересклет европейский",
    "family": "Celastraceae",
    "url": "https://ru.wikipedia.org/wiki/Euonymus_europaea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/20131018Pfaffenhuetchen4.jpg/275px-20131018Pfaffenhuetchen4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Illustration_Euonymus_europaea0.jpg/220px-Illustration_Euonymus_europaea0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Euonymus_europaeus_03_ies.jpg/200px-Euonymus_europaeus_03_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Euonymus_eur_kz.jpg/115px-Euonymus_eur_kz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Euonymus_europaeus_02_ies.jpg/200px-Euonymus_europaeus_02_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Euonymus_europaeus_04_ies.jpg/200px-Euonymus_europaeus_04_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Euonymus_europaea0.jpg/232px-Euonymus_europaea0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Euonymus_europaeus_flower.jpg/216px-Euonymus_europaeus_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Bonetero.jpg/172px-Bonetero.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Euonymus_europaeus_07_ies.jpg/200px-Euonymus_europaeus_07_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Euonymus_europaeus_12_ies.jpg/200px-Euonymus_europaeus_12_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Euonymus_europaeus_semina.jpg/250px-Euonymus_europaeus_semina.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euonymus_verrucosa",
    "rusName": "Бересклет бородавчатый",
    "family": "Celastraceae",
    "url": "https://ru.wikipedia.org/wiki/Euonymus_verrucosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Euonymus_verrucosus20100910_079.jpg/275px-Euonymus_verrucosus20100910_079.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Euonymus_verrucosus20090506_05.jpg/220px-Euonymus_verrucosus20090506_05.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Euonymus_verrucosus20200930_17131.jpg/220px-Euonymus_verrucosus20200930_17131.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Euonymus_verrucosa070515e.jpg/220px-Euonymus_verrucosa070515e.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Euonymus_verrucosus20110704_059-2.jpg/108px-Euonymus_verrucosus20110704_059-2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Euonymus_verrucosus20100925_092.jpg/383px-Euonymus_verrucosus20100925_092.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Euonymus-verrucosa.jpg/194px-Euonymus-verrucosa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Euonymus_verrucosus_20080907b.jpg/155px-Euonymus_verrucosus_20080907b.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Euonymus_verrucosa070515c.jpg/159px-Euonymus_verrucosa070515c.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Euonymus_verrucosa_2.jpg/187px-Euonymus_verrucosa_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Euonymus_verrucosus_20080907a.jpg/194px-Euonymus_verrucosus_20080907a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Euonymus_verrucosus_7112.jpg/194px-Euonymus_verrucosus_7112.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Eupatorium_cannabinum",
    "rusName": "Посконник коноплёвый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Eupatorium_cannabinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Butterflies_on_Hemp_Agrimony_-_geograph.org.uk_-_1281656.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Eupatorium_cannabinum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/49/Eupatorium_cannabinum_%28xndr%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Eupatorium_cannabinum_bluete.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_cyparissias",
    "rusName": "Молочай кипарисовый",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_cyparissias",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Euphorbe_FR_2012.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Euphorbia_cyparissias_02_bgiu.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_esula",
    "rusName": "Молочай острый",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_esula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Euphorbia_esula_-_kibe_piimalill.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Euphorbia_esula_bluete.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_exigua",
    "rusName": "Молочай малый",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_exigua",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Euphorbia_exigua2_closeup.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Euphorbia_exigua_Sturm34.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_falcata",
    "rusName": "Молочай серповидный",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_falcata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Euphorbia_falcata_Sturm32.jpg/264px-Euphorbia_falcata_Sturm32.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_helioscopia",
    "rusName": "Молочай-солнцегляд",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_helioscopia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d6/Euphorbia_helioscopia2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_humifusa",
    "rusName": "Молочай приземистый",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_humifusa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Euphorbia_humifusa_sl10.jpg/275px-Euphorbia_humifusa_sl10.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_lucida",
    "rusName": "Молочай глянцевитый",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_lucida",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Euphorbia_lucida1.JPG/275px-Euphorbia_lucida1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Euphorbia_lucida2.JPG/220px-Euphorbia_lucida2.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_maculata",
    "rusName": "Молочай пятнистый",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_maculata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Chamaesyce.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/25/Prostrate_spurge.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_palustris",
    "rusName": "Молочай болотный",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Dr%C3%B6singer_Wald_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Euphorbia_amygdaloides_cv_Purpurea5_ies.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_peplus",
    "rusName": "Молочай огородный",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_peplus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/E_peplus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Euphorbia_peplus_cyathium.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_platyphyllos",
    "rusName": "Молочай плосколистный",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_platyphyllos",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Euphorbia_platyphyllos2_W.jpg/275px-Euphorbia_platyphyllos2_W.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Euphorbia_platyphyllos_Sturm27.jpg/220px-Euphorbia_platyphyllos_Sturm27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Euphorbia_platyphyllos_W.jpg/220px-Euphorbia_platyphyllos_W.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_uralensis",
    "rusName": "Молочай лозный",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_uralensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Euphorbia_virgata_%28Ruten-Wolfsmilch%29_IMG_7367.JPG/275px-Euphorbia_virgata_%28Ruten-Wolfsmilch%29_IMG_7367.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphorbia_virgata",
    "rusName": "Молочай лозный",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphorbia_virgata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bb/Euphorbia_virgata_%28Ruten-Wolfsmilch%29_IMG_9813.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Euphrasia_stricta",
    "rusName": "Очанка прямая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Euphrasia_stricta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Euphrasia_stricta_190807.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Fagopyrum_esculentum",
    "rusName": "Гречиха посевная",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Fagopyrum_esculentum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/%EB%A9%94%EB%B0%80.JPG/275px-%EB%A9%94%EB%B0%80.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Illustration_Fagopyrum_esculentum0.jpg/220px-Illustration_Fagopyrum_esculentum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Fagopyrum_%D0%B3%D1%80%D0%B5%D1%87%D0%BA%D0%B0.jpg/200px-Fagopyrum_%D0%B3%D1%80%D0%B5%D1%87%D0%BA%D0%B0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Common_buckwheat_grains.jpg/200px-Common_buckwheat_grains.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Grechiha_pole.jpg/267px-Grechiha_pole.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Гречневая крупа"
  },
  {
    "latName": "Fagopyrum_tataricum",
    "rusName": "Гречиха татарская",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Fagopyrum_tataricum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Fagopyrum_tataricum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Fagopyrum_tataricum_-_Kops.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Grains_de_fagopyrum_tataricum.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Крупа"
  },
  {
    "latName": "Fagus_sylvatica",
    "rusName": "Бук европейский",
    "family": "Fagaceae",
    "url": "https://ru.wikipedia.org/wiki/Fagus_sylvatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c2/Beuk_%28Fagus_sylvatica%29%2C_zwellende_bladknop._24-04-2022_%28d.j.b.%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/BoisDeMadame.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Brussels_Zonienwoud.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Detail_of_Biscogniauxia_nummularia.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/49/Entzia_-_Brotes_de_haya_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/European_Beech.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e4/Fagus-sylvatica-cansiglio-forest-italy.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Fagus_sylvatica_MHNT.BOT.2004.0.312.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Fagus_sylvatica_MHNT.BOT.2010.6.81.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Орехи в большом количестве вредны, лучше их употреблять поджаренными. Молодые листья и почки"
  },
  {
    "latName": "Falcaria_vulgaris",
    "rusName": "Резак (растение)",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Falcaria_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Falcaria_vulgaris_eF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/QALace2675.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Варенные не позеленевшие розетки листьев весенних всходов второго года"
  },
  {
    "latName": "Fallopia_convolvulus",
    "rusName": "Горец вьюнковый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Fallopia_convolvulus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Fallopia_convolvulus1pl.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Fallopia_dumetorum",
    "rusName": "Горец кустарниковый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Fallopia_dumetorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Heggeduizendknoop.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Muehlenbeckia_adpressa_-_Curtis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Festuca_altissima",
    "rusName": "Овсяница высокая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Festuca_altissima",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Festuca_altissima_kz03.jpg/275px-Festuca_altissima_kz03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Festuca_altissima_illustration_%2802%29.jpg/220px-Festuca_altissima_illustration_%2802%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Festuca_altissima.JPG/220px-Festuca_altissima.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Festuca_arundinacea",
    "rusName": "Овсяница тростниковая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Festuca_arundinacea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Schedonorus_arundinaceus_-_Flickr_-_Matt_Lavin.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Starr_030603-0006_Festuca_arundinacea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Starr_040523-9001_Festuca_arundinacea.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Festuca_gigantea",
    "rusName": "Овсяница гигантская",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Festuca_gigantea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Giant_fescue_base.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Giant_fescue_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/75/Giant_fescue_ligule.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e1/Giant_fescue_node.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Festuca_ovina",
    "rusName": "Овсяница овечья",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Festuca_ovina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Festuca_spp_Sturm42.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Illustration_Festuca_ovina0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/Ruig_schapengras_aartjes_%28Festuca_ovina_subsp._hirtula%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Ruig_schapengras_bloeiwijze_%28Festuca_ovina_subsp._hirtula%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Ruig_schapengras_ligula_%28Festuca_ovina_subsp._hirtula%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4b/Ruig_schapengras_plant_%28Festuca_ovina_subsp._hirtula%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Sheep_fescue.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Festuca_pratensis",
    "rusName": "Овсяница луговая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Festuca_pratensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Fescue_9029.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/Fescue_9029_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Festuca_pratensis.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Festuca_pratensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ce/Festuca_pratensis_blatt.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Festuca_rubra",
    "rusName": "Овсяница красная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Festuca_rubra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Festuca.rubra.2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Festuca_rubra_detail.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Gewoon_roodzwenkgras_tongetje_%28Festuca_rubra_var._commutata_ligula%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Festuca_valesiaca",
    "rusName": "Типчак",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Festuca_valesiaca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Festuca_valesiaca1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ficaria_verna",
    "rusName": "Чистяк весенний",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ficaria_verna",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/62/20150315Ficaria_verna3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/BrazenHussy.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Celandine%2C_Killynether_Wood_-_geograph.org.uk_-_748879.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Coppernob.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Double-flowered_lesser_celandine.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/06/Ficaria_Collarette.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Ficaria_verna_bud.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Flowers_%282425723494%29_cropped.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Lesser-Celandine_Bulbils_Close-Up1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Корневые шишки в варёном виде (Как и другие лютики, содержит ядовитые вещества)"
  },
  {
    "latName": "Filago_arvensis",
    "rusName": "Жабник полевой",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Filago_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/49/Filago_arvensis3_W.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Filipendula_ulmaria",
    "rusName": "Таволга вязолистная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Filipendula_ulmaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/37/20140715Filipendula_ulmaria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Filipendula_ulmaria_Sturm12.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Sweet_curve.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Triphragmium_ulmariae%2C_Meadowsweet_Rust.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги"
  },
  {
    "latName": "Fragaria_moschata",
    "rusName": "Земляника мускусная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Fragaria_moschata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Fragaria_moschata1a.UME.JPG/275px-Fragaria_moschata1a.UME.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Fragaria_moschata_detail.JPG/200px-Fragaria_moschata_detail.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Fragaria_elatior_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v17.jpg/220px-Fragaria_elatior_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v17.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Fragaria_moschata_fructus1.JPG/300px-Fragaria_moschata_fructus1.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Fragaria_vesca",
    "rusName": "Земляника лесная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Fragaria_vesca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c1/Fragaria_vesca_-_metsmaasikas.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/aa/Fragaria_vesca_5044.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Fragaria_vesca_Jahodn%C3%ADk_obecn%C3%BD_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Fragaria_vesca_LC0389.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/09/Fragaria_vesca_seeds_USDA-ARS.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Illustration_Fragaria_vesca0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/PerfectStrawberry.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Fragaria_viridis",
    "rusName": "Земляника зелёная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Fragaria_viridis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Fragaria-viridis.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Fragaria_viridis_fruit_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/PerfectStrawberry.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Frangula_alnus",
    "rusName": "Крушина ломкая",
    "family": "Rhamnaceae",
    "url": "https://ru.wikipedia.org/wiki/Frangula_alnus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Frangula-alnus-asplenifolia-autumn.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Frangula-alnus-buds-2010-02-11.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Frangula-alnus-fruits.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Rhamnus_frangula_07_ies.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Fraxinus_excelsior",
    "rusName": "Ясень обыкновенный",
    "family": "Oleaceae",
    "url": "https://ru.wikipedia.org/wiki/Fraxinus_excelsior",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Ash_%28Fraxinus_excelsior%29_Wales_160730_%28Tony_Holkham%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Ash_%28Fraxinus_excelsior%29_Wales_190310_%28Tony_Holkham%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Ash_flower.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Ash_mast_at_Eglinton.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Buds_of_Fraxinus_excelsior_03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Esche_gemeine_Holz.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/73/EurAshLeaf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Fraxinus_excelsior%2801%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4f/Fraxinus_excelsior.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Fraxinus_pennsylvanica",
    "rusName": "Ясень пенсильванский",
    "family": "Oleaceae",
    "url": "https://ru.wikipedia.org/wiki/Fraxinus_pennsylvanica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/2014-10-29_13_12_58_Green_Ash_foliage_during_autumn_coloration_in_Ewing%2C_New_Jersey.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/ad/Fraxinus_pennsylvanica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Fraxinus_pensylvanica_a1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Green_Ash_Fraxinus_pennsylvanica_Bark_2000px.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Fumaria_officinalis",
    "rusName": "Дымянка лекарственная",
    "family": "Fumariaceae",
    "url": "https://ru.wikipedia.org/wiki/Fumaria_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/%28MHNT%29_Fumaria_officinalis_-_Habit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/70/%28MHNT%29_Fumaria_officinalis_-_Plant_habit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Illustration_Fumaria_officinalis0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gagea_granulosa",
    "rusName": "Гусиный лук зернистый",
    "family": "Liliaceae",
    "url": "https://ru.wikipedia.org/wiki/Gagea_granulosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Gagea_granulosa_38027339.jpg/275px-Gagea_granulosa_38027339.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gagea_lutea",
    "rusName": "Гусиный лук жёлтый",
    "family": "Liliaceae",
    "url": "https://ru.wikipedia.org/wiki/Gagea_lutea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a3/Illustration_Gagea_lutea0.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Мелкие луковицы, листья"
  },
  {
    "latName": "Gagea_minima",
    "rusName": "Гусиный лук малый",
    "family": "Liliaceae",
    "url": "https://ru.wikipedia.org/wiki/Gagea_minima",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/aa/Gagea_minima_flower_-_Keila.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Gagea_spathacea",
    "rusName": "Гусиный лук покрывальцевый",
    "family": "Liliaceae",
    "url": "https://ru.wikipedia.org/wiki/Gagea_spathacea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Gagea_spathacea_blatt.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ad/Gagea_spathacea_kz1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ad/Gagea_spathacea_kz2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galeobdolon_luteum",
    "rusName": "Яснотка зеленчуковая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galeobdolon_luteum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Yellow_archangel_in_Haugh_Wood_-_geograph.org.uk_-_1270909.jpg/275px-Yellow_archangel_in_Haugh_Wood_-_geograph.org.uk_-_1270909.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/94_Lamium_galeobdolon.jpg/220px-94_Lamium_galeobdolon.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Lamium_galeobdolon20160530_4923.jpg/220px-Lamium_galeobdolon20160530_4923.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galeopsis_angustifolia",
    "rusName": "Пикульник узколистный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galeopsis_angustifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Galeopsis_angustifolia1_eF.jpg/275px-Galeopsis_angustifolia1_eF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/Galeopsis_angustifolia_Sturm33.jpg/220px-Galeopsis_angustifolia_Sturm33.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Galeopsis_angustifolia_ENBLA01.jpg/220px-Galeopsis_angustifolia_ENBLA01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galeopsis_bifida",
    "rusName": "Пикульник двунадрезанный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galeopsis_bifida",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Galeopsis_bifida.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/2/25/Galeopsis_bifida1_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galeopsis_ladanum",
    "rusName": "Пикульник ладанниковый",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galeopsis_ladanum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Galeopsis_ladanum_Sturm32.jpg/271px-Galeopsis_ladanum_Sturm32.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galeopsis_pubescens",
    "rusName": "Пикульник пушистый",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galeopsis_pubescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ad/Galeopsis_pubescens_040910.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Galeopsis_pubescens_060805a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Galeopsis_pubescens_250807.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Galeopsis_pubescens_250807a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Galeopsis_pubescens_Sturm_11038.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Galeopsis_pubescens_eF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/Galeopsis_pubescens_subsp._pubescens_sl30.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Galeopsis_pubescens_subsp._pubescens_sl31.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Galeopsis_pubescens_subsp._pubescens_sl9.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galeopsis_speciosa",
    "rusName": "Пикульник красивый",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galeopsis_speciosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/GaleopsisSpeciosa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Horminum_pyrenaicum_-_Saint-Hilaire.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galeopsis_tetrahit",
    "rusName": "Пикульник обыкновенный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galeopsis_tetrahit",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/00/Galeopsis_tetrahit_-_Bombus_veteranus_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Galeopsis_tetrahit_-_kare_k%C3%B5rvik_Keilas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galinsoga_ciliata",
    "rusName": "Галинзога четырёхлучевая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Galinsoga_ciliata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Smulkiaziede_galinsoga_resize.JPG/275px-Smulkiaziede_galinsoga_resize.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galinsoga_parviflora",
    "rusName": "Галинзога мелкоцветковая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Galinsoga_parviflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Galinsoga_parviflora_bluete.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galinsoga_quadriradiata",
    "rusName": "Галинзога четырёхлучевая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Galinsoga_quadriradiata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Galinsoga_quadriradiata_macro.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_articulatum",
    "rusName": "Подмаренник мареновидный",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_articulatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Galium_rubioides_kz6.jpg/275px-Galium_rubioides_kz6.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_boreale",
    "rusName": "Подмаренник северный",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_boreale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Galium_boreale_-_v%C3%A4rvmadar_Keilas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_humifusum",
    "rusName": "Подмаренник распростёртый",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_humifusum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Galium_humifusum_20160621%2C_Volgograd_obl.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_mollugo",
    "rusName": "Подмаренник мягкий",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_mollugo",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Rubiaceae_-_Galium_mollugo.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_odoratum",
    "rusName": "Подмаренник душистый",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_odoratum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Waldmeister%28Mai%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Waldmeisterfr%C3%BCchte.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_palustre",
    "rusName": "Подмаренник болотный",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ea/Galium_palustre-Sumpf_Labkraut.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Galium_palustre.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Galium_palustre_Oulu%2C_Finland_18.07.2013.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Galium_palustre_habitus.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_physocarpum",
    "rusName": "Подмаренник мареновидный",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_physocarpum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Galium_rubioides_kz6.jpg/275px-Galium_rubioides_kz6.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_rivale",
    "rusName": "Подмаренник приручейный",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_rivale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Galium_uliginosum_Rokach1.JPG/275px-Galium_uliginosum_Rokach1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_rubioides",
    "rusName": "Подмаренник мареновидный",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_rubioides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Galium_rubioides_kz02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_septentrionale",
    "rusName": "Подмаренник северный",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_septentrionale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Galium_boreale_-_v%C3%A4rvmadar_Keilas.jpg/275px-Galium_boreale_-_v%C3%A4rvmadar_Keilas.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/69_Galium_boreale.jpg/220px-69_Galium_boreale.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_tinctorium",
    "rusName": "Ясменник красильный",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_tinctorium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Galium_tinctorium_NRCS-1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_uliginosum",
    "rusName": "Подмаренник топяной",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_uliginosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Galium_uliginosum_-_lodumadar_Keilas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Galium_verum",
    "rusName": "Подмаренник настоящий",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Galium_verum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/CublesuSJ2012_%2891%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Galium_verum01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Ladys_bedstraw_1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Genista_germanica",
    "rusName": "Дрок германский",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Genista_germanica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Fabaceae_-_Genista_germanica_.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Genista-hispanica%2C_Aarhus_Botanisk_Have_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Genista_germanica20080528.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Illustration_Genista_germanica0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Genista_tinctoria",
    "rusName": "Дрок красильный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Genista_tinctoria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Arabic_herbal_medicine_guidebook.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Crotalaria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Genista_tinctoria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/90/Genista_tinctoria_001.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Genista_tinctoria_003.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Genista_tinctoria_250807.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодые побеги и цветочные бутоны"
  },
  {
    "latName": "Gentiana_cruciata",
    "rusName": "Горечавка крестовидная",
    "family": "Gentianaceae",
    "url": "https://ru.wikipedia.org/wiki/Gentiana_cruciata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Gentiana-clusii-ClusiusEnzian.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/42/Gentiana_cruciata_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Gentianaceae_-_Gentiana_cruciata-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Gentianaceae_-_Gentiana_cruciata-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Gentianaceae_-_Gentiana_cruciata-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/Gentianaceae_-_Gentiana_cruciata-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Gentianaceae_-_Gentiana_cruciata.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gentiana_pneumonanthe",
    "rusName": "Горечавка лёгочная",
    "family": "Gentianaceae",
    "url": "https://ru.wikipedia.org/wiki/Gentiana_pneumonanthe",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Gentiana-clusii-ClusiusEnzian.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Gentiana_pneumonanthe_130805a.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gentiana_verna",
    "rusName": "Горечавка весенняя",
    "family": "Gentianaceae",
    "url": "https://ru.wikipedia.org/wiki/Gentiana_verna",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Gentiana_verna.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Gentiana_verna_%281%29.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gentianella_amarella",
    "rusName": "Горечавочка горьковатая",
    "family": "Gentianaceae",
    "url": "https://ru.wikipedia.org/wiki/Gentianella_amarella",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Gentiana_amarella_L_ag1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Gentianella_amarella.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Gentianella_amarella_ssp._acuta_kz01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_columbinum",
    "rusName": "Герань голубиная",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_columbinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/2013-05-09_15-28-19-fleur.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Geraniaceae_-_Geranium_columbinum-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Geraniaceae_-_Geranium_columbinum-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Geraniaceae_-_Geranium_columbinum-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Geraniaceae_-_Geranium_columbinum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Geranium_columbinum_20100522_a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Geranium_columbinum_20100522_b.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Geranium_columbinum_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_palustre",
    "rusName": "Герань болотная",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fd/Geranium_palustre_flowering.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_phaeum",
    "rusName": "Герань тёмно-бурая",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_phaeum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/2013-05-09_15-28-19-fleur.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_pratense",
    "rusName": "Герань луговая",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_pratense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/%27Mrs._Kendall_Clark%27_detail.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/2013-05-09_15-28-19-fleur.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Meadow_Cranesbill.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_pusillum",
    "rusName": "Герань маленькая",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_pusillum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/2013-05-09_15-28-19-fleur.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Geranium_pusillum.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_robertianum",
    "rusName": "Герань Роберта",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_robertianum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/13/%28MHNT%29_Geranium_robertianum_-_Leafs_and_buds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/%28MHNT%29_Geranium_robertianum_-_blossom_and_bud.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/%28MHNT%29_Geranium_robertianum_-_buds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/91/%28MHNT%29_Geranium_robertianum_-_fruit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/Herb-Robert_800.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_sanguineum",
    "rusName": "Герань кроваво-красная",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_sanguineum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/0_Geranium_sanguineum_%27Striatum%27-_Samo%C3%ABns.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Geraniaceae_-_Geranium_sanguineum-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/Geraniaceae_-_Geranium_sanguineum-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/05/Geraniaceae_-_Geranium_sanguineum-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Geraniaceae_-_Geranium_sanguineum-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/91/Geraniaceae_-_Geranium_sanguineum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Geranium_sanguineum02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Geranium_sanguineum_-_verev_kurereha.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Geraniumsangred2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_sibiricum",
    "rusName": "Герань сибирская",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_sibiricum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e5/Geranium_sibiricum_flower.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geranium_sylvaticum",
    "rusName": "Герань лесная",
    "family": "Geraniaceae",
    "url": "https://ru.wikipedia.org/wiki/Geranium_sylvaticum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/2013-05-09_15-28-19-fleur.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Geranium_sylvaticum_%281%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Geranium_sylvaticum_%282%29.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geum_aleppicum",
    "rusName": "Гравилат алеппский",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Geum_aleppicum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Geum_aleppicum_CaledonJun2017.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Свежие листья"
  },
  {
    "latName": "Geum_macrophyllum",
    "rusName": "Гравилат крупнолистный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Geum_macrophyllum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Addisonia_-_colored_illustrations_and_popular_descriptions_of_plants_%281916-%281964%29%29_%2816585422840%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Geum_macrophyllum_10101.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Geum_macrophyllum_large-leaved_avens_in_fruit.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Geum_rivale",
    "rusName": "Гравилат речной",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Geum_rivale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Geum_rivale_-_Niitv%C3%A4lja.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Geum_rivale_clump.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Geum_rivale_flower_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Geum_rivale_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Geum_rivale_with_pseudopeloria.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья. Корневища - пряность"
  },
  {
    "latName": "Gladiolus_imbricatus",
    "rusName": "Шпажник черепитчатый",
    "family": "Iridaceae",
    "url": "https://ru.wikipedia.org/wiki/Gladiolus_imbricatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Gladiolus_imbricatus_a1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/IrisPink.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gladiolus_palustris",
    "rusName": "Шпажник болотный",
    "family": "Iridaceae",
    "url": "https://ru.wikipedia.org/wiki/Gladiolus_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Iridaceae_-_Gladiolus_palustris-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Iridaceae_-_Gladiolus_palustris-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Iridaceae_-_Gladiolus_palustris-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Iridaceae_-_Gladiolus_palustris-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Iridaceae_-_Gladiolus_palustris.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/IrisPink.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Glaucium_corniculatum",
    "rusName": "Глауциум рогатый",
    "family": "Papaveraceae",
    "url": "https://ru.wikipedia.org/wiki/Glaucium_corniculatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Glaucium_corniculatum_flor.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Glechoma_hederacea",
    "rusName": "Будра плющевидная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Glechoma_hederacea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Glechoma_hederacea%2C_Hondsdraf_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Glechoma_hederacea_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/13/Ground_Ivy_%28Glechoma_hederacea%29_Spectral_comparison_Vis_UV_IR.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e6/Illustration_Glechoma_hederacea0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Warming-Skudbygning-Fig9-Glechoma-hederacea.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Glechoma_hirsuta",
    "rusName": "Будра волосистая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Glechoma_hirsuta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Glechoma_hirsuta_Prague_2012_1.jpg/275px-Glechoma_hirsuta_Prague_2012_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Glyceria_fluitans",
    "rusName": "Манник плавающий",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Glyceria_fluitans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Glyceria.fluitans.2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Glyceria.fluitans.3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Glyceria.fluitans.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c1/GlyceriaFluitans1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Glyceria_fluitans.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Glyceria_fluitans.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Glyceria_fluitans_detail.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Glyceria_spp_Sturm38.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Glyceria_lithuanica",
    "rusName": "Манник литовский",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Glyceria_lithuanica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Glyceria_lithuanica_78558674.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Glyceria_maxima",
    "rusName": "Манник большой",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Glyceria_maxima",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Liesgras_bloeiwijze_Glyceria_maxima.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Зерно"
  },
  {
    "latName": "Glyceria_plicata",
    "rusName": "Манник замеченный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Glyceria_plicata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Glyceria_notata1.JPG/220px-Glyceria_notata1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gnaphalium_uliginosum",
    "rusName": "Сушеница топяная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Gnaphalium_uliginosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c1/Moerasdroogbloem_plant_Gnaphalium_uliginosum_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Gnaphalium_uliginosum_Sturm26.jpg/330px-Gnaphalium_uliginosum_Sturm26.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Goodyera_repens",
    "rusName": "Гудайера ползучая",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Goodyera_repens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Goodyera_repens_%28habitus%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Goodyera_repens_-_Alutaguse.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gratiola_officinalis",
    "rusName": "Авран лекарственный",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Gratiola_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Arabic_herbal_medicine_guidebook.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/9/90/Gratiola_officinalis3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Grossularia_reclinata",
    "rusName": "Крыжовник обыкновенный",
    "family": "Grossulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Grossularia_reclinata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Wilde_kruisbes_%28Ribes_uva-crispa_wild_plant%29.jpg/275px-Wilde_kruisbes_%28Ribes_uva-crispa_wild_plant%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/279_Ribes_grossularia.jpg/220px-279_Ribes_grossularia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Ribes_uva-crispa20090512_042.jpg/158px-Ribes_uva-crispa20090512_042.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Ribes_uva-crispa3_ies.jpg/180px-Ribes_uva-crispa3_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Ribes_uva-crispa4_ies.jpg/180px-Ribes_uva-crispa4_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Gooseberries.JPG/180px-Gooseberries.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Stachelbeeren-WJP-1.jpg/170px-Stachelbeeren-WJP-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Stachelbeere_%28Ribes_uva-crispa%29.jpg/170px-Stachelbeere_%28Ribes_uva-crispa%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Gymnadenia_odoratissima",
    "rusName": "Кокушник ароматнейший",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Gymnadenia_odoratissima",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Flower_of_Rossioglossum_ampliatum_-_Rspb.2013.0960-F1.large-right.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Gymnadenia_odoratissima_-_Niitv%C3%A4lja2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Gymnadenia_odoratissima_albiflora_-_Niitv%C3%A4lja2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gymnocarpium_dryopteris",
    "rusName": "Голокучник обыкновенный",
    "family": "Athyriaceae",
    "url": "https://ru.wikipedia.org/wiki/Gymnocarpium_dryopteris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Gymnocarpium_dryopteris_0318.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Silver-fern.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Gypsophila_paniculata",
    "rusName": "Качим метельчатый",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Gypsophila_paniculata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Close-up_of_2_baby%27s_breath_%28Gypsophila_paniculata%29_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Gypsophila_paniculata_sl17.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Illustration_Gypsophila_paniculata0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Schleierkraut_und_rote_Rose.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hammarbya_paludosa",
    "rusName": "Гаммарбия",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Hammarbya_paludosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Hammarbya_paludosa_plant_%2802%29.jpg/275px-Hammarbya_paludosa_plant_%2802%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Illustration_Malaxis_monophyllos0.jpg/220px-Illustration_Malaxis_monophyllos0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hedera_helix",
    "rusName": "Плющ обыкновенный",
    "family": "Araliaceae",
    "url": "https://ru.wikipedia.org/wiki/Hedera_helix",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Entrance_to_High_Castle_in_Malbork.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Hedera_helix1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Hedera_helix3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Hedera_helix_2_beentree_bialowieza_2005.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Hedera_helix_clinging_on_Platanus_x_hispanica%2C_Agde%2C_H%C3%A9rault.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/IvyWall.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Ivy_in_Hyde_Park.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Helianthemum_nummularium",
    "rusName": "Солнцецвет монетолистный",
    "family": "Cistaceae",
    "url": "https://ru.wikipedia.org/wiki/Helianthemum_nummularium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Helianthemum_alpestre0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c9/Helianthemum_grandiflorum_a1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Helianthemum_grandiflorum_a2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c2/Helianthemum_nummularifolium0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/88/Helianthemum_nummularifolium1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Helianthemum_nummularium1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Helianthemum_nummularium2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Helianthemum_nummularium2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Helianthemum_nummularium_-_leafs_%28aka%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d6/Helianthemum_nummularium_MHNT.BOT.2007.43.43.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Helianthus_annuus",
    "rusName": "Подсолнечник однолетний",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Helianthus_annuus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Sunflower_Taleghan.jpg/275px-Sunflower_Taleghan.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Gc4_helianthus_annuus.jpg/220px-Gc4_helianthus_annuus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Helianthus_annuus_3.jpg/120px-Helianthus_annuus_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Starr_080914-9913_Helianthus_annuus.jpg/120px-Starr_080914-9913_Helianthus_annuus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Helianthus_annuus_0.R_%286%29.jpg/210px-Helianthus_annuus_0.R_%286%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Sonnenblume_geschlossene_Bl%C3%BCtenknospe.JPG/210px-Sonnenblume_geschlossene_Bl%C3%BCtenknospe.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Helianthus_annuus_pollen_1.jpg/205px-Helianthus_annuus_pollen_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Pflanze-Sonnenblume1-Asio.JPG/220px-Pflanze-Sonnenblume1-Asio.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Sunflower_field_in_Bashkortostan%2C_Russia.jpg/220px-Sunflower_field_in_Bashkortostan%2C_Russia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Solrosfr%C3%B6n_-_Sunflowerseeds_-_Ystad-2022.jpg/220px-Solrosfr%C3%B6n_-_Sunflowerseeds_-_Ystad-2022.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Sunflowerseed_oil.jpg/200px-Sunflowerseed_oil.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Black_Helianthus_annuus_seeds.jpg/180px-Black_Helianthus_annuus_seeds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Sunflower_Seeds_Kaldari.jpg/180px-Sunflower_Seeds_Kaldari.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Sunflower-vanilla_ice.jpg/180px-Sunflower-vanilla_ice.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Sunflower-chianti.jpg/160px-Sunflower-chianti.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Helianthus_annuus_-_Teddy_Bear.jpg/160px-Helianthus_annuus_-_Teddy_Bear.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семечки"
  },
  {
    "latName": "Helianthus_tuberosus",
    "rusName": "Топинамбур",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Helianthus_tuberosus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/20130827Helianthus_tuberosus5.jpg/275px-20130827Helianthus_tuberosus5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Helianthus_tuberosus_kz7.jpg/155px-Helianthus_tuberosus_kz7.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Helianthus_tuberosus_kz4.jpg/170px-Helianthus_tuberosus_kz4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/HelianthusTuberosus.jpg/165px-HelianthusTuberosus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Helianthus_tuberosus_kz3.jpg/165px-Helianthus_tuberosus_kz3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Helianth_tuberosus_012_php.jpg/175px-Helianth_tuberosus_012_php.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Root_of_Helianthus_tuberosus.JPG/155px-Root_of_Helianthus_tuberosus.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Jerusalem_artichokes.jpg/220px-Jerusalem_artichokes.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/TopinamburFlowers.jpg/220px-TopinamburFlowers.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Клубни"
  },
  {
    "latName": "Helictotrichon_pubescens",
    "rusName": "Овсец пушистый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Helictotrichon_pubescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/20150514Avenula_pubescens2.jpg/275px-20150514Avenula_pubescens2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Avena_pubescens_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v13.jpg/220px-Avena_pubescens_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v13.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hepatica_nobilis",
    "rusName": "Печёночница благородная",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Hepatica_nobilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Hepatica_nobilis_plant.JPG/275px-Hepatica_nobilis_plant.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Illustration_Hepatica_nobilis0_clean.jpg/220px-Illustration_Hepatica_nobilis0_clean.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Hepatica_nobilis_flowers.JPG/113px-Hepatica_nobilis_flowers.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Hepatica_nobilis_kz1.jpg/175px-Hepatica_nobilis_kz1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Hepatica_noblis_20060501_004.jpg/200px-Hepatica_noblis_20060501_004.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Hepatica_nobilis_kz2.jpg/191px-Hepatica_nobilis_kz2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/%D0%9F%D0%BE%D1%87%D1%82%D0%BE%D0%B2%D0%B0%D1%8F_%D0%BC%D0%B0%D1%80%D0%BA%D0%B0_%D0%A1%D0%A1%D0%A1%D0%A0_%E2%84%96_5400._1983._%D0%92%D0%B5%D1%81%D0%B5%D0%BD%D0%BD%D0%B8%D0%B5_%D1%86%D0%B2%D0%B5%D1%82%D1%8B.jpg/200px-%D0%9F%D0%BE%D1%87%D1%82%D0%BE%D0%B2%D0%B0%D1%8F_%D0%BC%D0%B0%D1%80%D0%BA%D0%B0_%D0%A1%D0%A1%D0%A1%D0%A0_%E2%84%96_5400._1983._%D0%92%D0%B5%D1%81%D0%B5%D0%BD%D0%BD%D0%B8%D0%B5_%D1%86%D0%B2%D0%B5%D1%82%D1%8B.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/%D0%9F%D1%87%D0%B5%D0%BB%D0%B0_%D0%BD%D0%B0_%D0%BF%D0%B5%D1%87%D1%91%D0%BD%D0%BE%D1%87%D0%BD%D0%B8%D1%86%D0%B5_2.jpg/220px-%D0%9F%D1%87%D0%B5%D0%BB%D0%B0_%D0%BD%D0%B0_%D0%BF%D0%B5%D1%87%D1%91%D0%BD%D0%BE%D1%87%D0%BD%D0%B8%D1%86%D0%B5_2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Heracleum_sibiricum",
    "rusName": "Борщевик сибирский",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Heracleum_sibiricum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Heracleum_sibiricum_kz1.jpg/275px-Heracleum_sibiricum_kz1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/251_Heracleum_sibiricum.jpg/220px-251_Heracleum_sibiricum.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги, очищенные от коры, но сок может вызвать воспаление на коже. Молодые листья, черешки стеблей"
  },
  {
    "latName": "Heracleum_sosnowskyi",
    "rusName": "Борщевик Сосновского",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Heracleum_sosnowskyi",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Contact_with_Heracleum_sosnowskyi.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/HeracleumSosnowskyi_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Heracleum_sosnowskyi_Inflorescences.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Herminium_monorchis",
    "rusName": "Бровник одноклубневый",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Herminium_monorchis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Flower_of_Rossioglossum_ampliatum_-_Rspb.2013.0960-F1.large-right.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Herminium_monorchis_%28flowers%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Herminium_monorchis_%28plants%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Herniaria_glabra",
    "rusName": "Грыжник гладкий",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Herniaria_glabra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Herniaria_glabra_2005.06.12_14.50.22.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Herniaria_polygama",
    "rusName": "Грыжник многобрачный",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Herniaria_polygama",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Herniaria_polygama_43042514.jpg/275px-Herniaria_polygama_43042514.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hesperis_matronalis",
    "rusName": "Вечерница ночная фиалка",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Hesperis_matronalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Damerocket.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Gardenology.org-IMG_2785_rbgs11jan.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1d/Sweet_Rocket_Whitelands.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hibiscus_trionum",
    "rusName": "Гибискус тройчатый",
    "family": "Malvaceae",
    "url": "https://ru.wikipedia.org/wiki/Hibiscus_trionum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2e/Hibiscus_trionum_%28Flower-of-an-hour%2C_or_Bladder_Hibiscus%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Hibiscus_trionum_MHNT.BOT.2007.43.29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hieracium_umbellatum",
    "rusName": "Ястребинка зонтичная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Hieracium_umbellatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Canada_Hawkweed.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Hieracium_umbellatum_-_Schirm-Habichtskraut_-L%27Epervi%C3%A8re_en_ombelle_-_Schermhavikskruid_-_Hawkweed_%28cropped%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Hieracium_umbellatum_Sarjakeltano_H8005.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Hieracium_umbellatum_japan3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Megachile_%28male%29_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hierochloe_australis",
    "rusName": "Зубровка южная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Hierochloe_australis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Hierochloe_spp_Sturm7.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Надземная часть до цветения - пряность"
  },
  {
    "latName": "Hierochloe_odorata",
    "rusName": "Зубровка душистая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Hierochloe_odorata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/81/Hierochloe_odorata_%28USDA%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Sweet_Grass.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Надземная часть до цветения - пряность"
  },
  {
    "latName": "Hippophae_rhamnoides",
    "rusName": "Облепиха крушиновидная",
    "family": "Elaeagnaceae",
    "url": "https://ru.wikipedia.org/wiki/Hippophae_rhamnoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/243_Hippophae_rhamnoides.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3a/Hippophae_rhamnoides-01_%28xndr%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Seabuckthorn_%28Hippophae_rhamonides%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Seabuckthorn_berries%2C_Nubra_valley%2C_Ladakh.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Hippuris_vulgaris",
    "rusName": "Хвостник обыкновенный",
    "family": "Hippuridaceae",
    "url": "https://ru.wikipedia.org/wiki/Hippuris_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/09/Hippuris_vulgaris_%28aka%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hirschfeldia_incana",
    "rusName": "Гиршфельдия",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Hirschfeldia_incana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Hirschfeldia_incana.jpg/275px-Hirschfeldia_incana.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Hirschfeldia_incana_flower_%2803%29.jpg/150px-Hirschfeldia_incana_flower_%2803%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Holcus_lanatus",
    "rusName": "Бухарник шерстистый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Holcus_lanatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Gestreepte_witbol_bloei_Holcus_lanatus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Holcus.lanatus.3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Holcus_lanatus_Gestreepte_witbol_%282%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Holcus_mollis",
    "rusName": "Бухарник мягкий",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Holcus_mollis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Gladde_witbol_bloeiwijze_Holcus_mollis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/Holcus.mollis.2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Holoschoenus_vulgaris",
    "rusName": "Камышевидник обыкновенный",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Holoschoenus_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Holoschoenus_romanus1.jpg/275px-Holoschoenus_romanus1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Cyperaceae_spp_Sturm12.jpg/220px-Cyperaceae_spp_Sturm12.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hordeum_distichon",
    "rusName": "Ячмень двурядный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Hordeum_distichon",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/140726_Farm_Tomita_Nakafurano_Hokkaido_Japan02n.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8c/Illustration_Hordeum_distichon0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hordeum_jubatum",
    "rusName": "Ячмень гривастый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Hordeum_jubatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Hordeum_jubatum_-_close-up_%28aka%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hordeum_vulgare",
    "rusName": "Ячмень обыкновенный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Hordeum_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Barley.jpg/275px-Barley.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Illustration_Hordeum_vulgare1.jpg/220px-Illustration_Hordeum_vulgare1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Wintergerste-2008.jpg/120px-Wintergerste-2008.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Gerst_oortjes_Hordeum_vulgare_auricles.jpg/90px-Gerst_oortjes_Hordeum_vulgare_auricles.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Gerstenkorrels_Hordeum_vulgare.jpg/120px-Gerstenkorrels_Hordeum_vulgare.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/ChampsOrge.jpg/113px-ChampsOrge.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Hordeum_vulgare_MHNT.BOT.2015.2.39.jpg/120px-Hordeum_vulgare_MHNT.BOT.2015.2.39.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Зерно"
  },
  {
    "latName": "Hottonia_palustris",
    "rusName": "Турча болотная",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Hottonia_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/HottoniaPalustrisInflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Illustration_Hottonia_palustris0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Primula_vulgaris_Mezzolombardo_03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Humulus_lupulus",
    "rusName": "Хмель обыкновенный",
    "family": "Cannabaceae",
    "url": "https://ru.wikipedia.org/wiki/Humulus_lupulus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Hopfen1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Humle_%28Humulus_lupulus%29-2017-Ystad.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Humulus_lupulus_%27Aurea%27_-_Golden_Hop.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые подземные побеги, только что вышедшие на поверхность"
  },
  {
    "latName": "Huperzia_selago",
    "rusName": "Баранец обыкновенный",
    "family": "Huperziaceae",
    "url": "https://ru.wikipedia.org/wiki/Huperzia_selago",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Huperzia_selago.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Huperzia_selago_MHNT.BOT.2005.0.999.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Lycopodium_saururus.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hydrilla_verticillata",
    "rusName": "Гидрилла",
    "family": "Hydrocharitaceae",
    "url": "https://ru.wikipedia.org/wiki/Hydrilla_verticillata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Hydrilla_Verticillata_3.jpg/275px-Hydrilla_Verticillata_3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hydrocharis_morsus-ranae",
    "rusName": "Водокрас лягушачий",
    "family": "Hydrocharitaceae",
    "url": "https://ru.wikipedia.org/wiki/Hydrocharis_morsus-ranae",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Elodea_canadensis.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/HydrocharisBaby.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ad/HydrocharisMorsus-ranae2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/HydrocharisMorsusRanae.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hydrocotyle_vulgaris",
    "rusName": "Щитолистник обыкновенный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Hydrocotyle_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Gew%C3%B6hnlicher_Wassernabel_PICT8937.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Gew%C3%B6hnlicher_Wassernabel_PICT8939.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/HydrocotyleVulgaris.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Hydrocotyle_vulgaris_flower_kz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Hydrocotyle_vulgaris_inflorescence_kz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/df/Illustration_Hydrocotyle_vulgaris0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hylotelephium_triphyllum",
    "rusName": "Очиток обыкновенный",
    "family": "Crassulaceae",
    "url": "https://ru.wikipedia.org/wiki/Hylotelephium_triphyllum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Sedum_telephium_210805.jpg/275px-Sedum_telephium_210805.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Sedum_telephium_Herbstfreude20170507_7457.jpg/220px-Sedum_telephium_Herbstfreude20170507_7457.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/Sedum_telephium_150805.jpg/220px-Sedum_telephium_150805.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Hylotelephium_telephium20090914_097.jpg/220px-Hylotelephium_telephium20090914_097.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Sedum_telephium_Sturm07043.jpg/220px-Sedum_telephium_Sturm07043.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Sedum_Purple_emperor%2C_RBGE_2008.jpg/220px-Sedum_Purple_emperor%2C_RBGE_2008.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hyoscyamus_bohemicus",
    "rusName": "Белена чешская",
    "family": "Solanaceae",
    "url": "https://ru.wikipedia.org/wiki/Hyoscyamus_bohemicus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Hyoscyamus_niger_var._pallidus_Lulek_czarny_odm._blada_2009-07-20_01.jpg/275px-Hyoscyamus_niger_var._pallidus_Lulek_czarny_odm._blada_2009-07-20_01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hyoscyamus_niger",
    "rusName": "Белена чёрная",
    "family": "Solanaceae",
    "url": "https://ru.wikipedia.org/wiki/Hyoscyamus_niger",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Apothecary_vessels_Hyoscyamus_19th_century.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Henbane2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Hyoscyamus_niger%2C_Solanaceae.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Hyoscyamus_niger_seeds.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hypericum_hirsutum",
    "rusName": "Зверобой жестковолосый",
    "family": "Hypericaceae",
    "url": "https://ru.wikipedia.org/wiki/Hypericum_hirsutum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/76/Hypericum_hirsutum_coteau-charteves_02_23062007_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Hypericum_hirsutum_i01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hypericum_humifusum",
    "rusName": "Зверобой распростёртый",
    "family": "Hypericaceae",
    "url": "https://ru.wikipedia.org/wiki/Hypericum_humifusum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Hypericum_humifusum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Hypericum_humifusum_W.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hypericum_maculatum",
    "rusName": "Зверобой пятнистый",
    "family": "Hypericaceae",
    "url": "https://ru.wikipedia.org/wiki/Hypericum_maculatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Bombus_soroeensis_-_Hypericum_maculatum_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Geflecktes_Johanniskraut.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hypericum_montanum",
    "rusName": "Зверобой горный",
    "family": "Hypericaceae",
    "url": "https://ru.wikipedia.org/wiki/Hypericum_montanum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Hypericum_montanum06.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hypericum_tetrapterum",
    "rusName": "Зверобой четырёхкрылый",
    "family": "Hypericaceae",
    "url": "https://ru.wikipedia.org/wiki/Hypericum_tetrapterum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Hypericum_spp_Sturm59.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Hypopitys_monotropa",
    "rusName": "Подъельник обыкновенный",
    "family": "Monotropaceae",
    "url": "https://ru.wikipedia.org/wiki/Hypopitys_monotropa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Monotropa_hypopitys_5857.JPG/266px-Monotropa_hypopitys_5857.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/157_Monotropa_hypopitys.jpg/220px-157_Monotropa_hypopitys.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Impatiens_glandulifera",
    "rusName": "Недотрога желёзконосная",
    "family": "Balsaminaceae",
    "url": "https://ru.wikipedia.org/wiki/Impatiens_glandulifera",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a1/Impatiens_Glandulifera%2C_leaves.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/55/Impatiens_glandulifera_0004.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/North_West_Wing.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Springkraut_fg04.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Springkruid.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Impatiens_noli-tangere",
    "rusName": "Недотрога обыкновенная",
    "family": "Balsaminaceae",
    "url": "https://ru.wikipedia.org/wiki/Impatiens_noli-tangere",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Gro%C3%9Fe_Springkraut_%28Impatiens_noli-tangere%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Illustration_Impatiens_noli-tangere0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2e/NoliTangere_Pflanze.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/RhododendronSimsiiFlowers2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Inula_britannica",
    "rusName": "Девясил британский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Inula_britannica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Inula_britannica.jpeg/275px-Inula_britannica.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Inula_britannica_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v16.jpg/220px-Inula_britannica_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v16.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Inula_hirta",
    "rusName": "Девясил шершавый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Inula_hirta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Inula_hirta_PID1983-4.jpg/275px-Inula_hirta_PID1983-4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Inula_hirta_120507.jpg/220px-Inula_hirta_120507.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Inula_salicina",
    "rusName": "Девясил иволистный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Inula_salicina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Inula_salicina_240606.jpg/275px-Inula_salicina_240606.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/20140630Inula_britannica.jpg/220px-20140630Inula_britannica.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Iris_aphylla",
    "rusName": "Ирис безлистный",
    "family": "Iridaceae",
    "url": "https://ru.wikipedia.org/wiki/Iris_aphylla",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Iris_aphylla_Orchi_145.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Kosaciec_bezlistny_Iris_aphylla_RB2.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Iris_pseudacorus",
    "rusName": "Ирис ложноаировый",
    "family": "Iridaceae",
    "url": "https://ru.wikipedia.org/wiki/Iris_pseudacorus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6a/20140504Iris_pseudacorus2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Illustration_Iris_pseudacorus0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Iris_pseudacorus_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Iris_pseudacorus_LC0339.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/81/Iris_pseudacorus_MHNT.BOT.2004.0.115a.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Iris_pseudacorus_bud_-_Niitv%C3%A4lja.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/Iris_pseudacorus_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Iris_pseudacorus_from_sweden.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/Iris_pseudacorus_fruit.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Iris_pseudacorus_iris_des_marais.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Iris_sibirica",
    "rusName": "Ирис сибирский",
    "family": "Iridaceae",
    "url": "https://ru.wikipedia.org/wiki/Iris_sibirica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/37/Eriskirch-EriskircherRied3-Asio.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/75/Eriskirch-IrisSibirica1-Asio.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4b/Iris_Purple_Top_View_1788px.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Iris_sibirica_%27Tropic-night%27_flower_Multispectral_comparison_Vis_UV_IR.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Iris_sibirica_05.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Iris_sibirica_060603.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Iris_sibirica_180605.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Iris_sibirica_MHNT.BOT.2009.7.17.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Iris_sibirica_in_natural_monument_Novoveska_draha_in_2011_%2818%29.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Isatis_costata",
    "rusName": "Вайда ребристая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Isatis_costata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Isatis_costata_43626106.jpg/266px-Isatis_costata_43626106.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Isatis_tinctoria",
    "rusName": "Вайда красильная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Isatis_tinctoria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Bachelier_-_H%C3%B4tel_d%27Ass%C3%A9zat_-_Toulouse_-_La_cour_d%27honneur.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Isatis_tinctoria02.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Isatis_tinctoria_%28s._str.%29_sl5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Isatis_tinctoria_003.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Isatis_tinctoria_MHNT.BOT.2011.3.12.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Pastel_pigment_cocagnes_et_feuilles_-_Mus%C3%A9um_du_pastel.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Schreber_woad_mill_1752.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Isoetes_lacustris",
    "rusName": "Полушник озёрный",
    "family": "Isoetaceae",
    "url": "https://ru.wikipedia.org/wiki/Isoetes_lacustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Illustration_Isoetes_lacustris0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Jasione_montana",
    "rusName": "Букашник горный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Jasione_montana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d6/Berg-Sandgl%C3%B6ckchen_Jasione_montana.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Bombus_soroeensis_-_Jasione_montana_-_Tallinn.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Sininukk.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Jovibarba_sobolifera",
    "rusName": "Молодило шароносное",
    "family": "Crassulaceae",
    "url": "https://ru.wikipedia.org/wiki/Jovibarba_sobolifera",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Sempervirum_soboliferum-2.jpg/275px-Sempervirum_soboliferum-2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Jovibarba_globifera_globifera-3.JPG/220px-Jovibarba_globifera_globifera-3.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Juglans_cinerea",
    "rusName": "Орех серый",
    "family": "Juglandaceae",
    "url": "https://ru.wikipedia.org/wiki/Juglans_cinerea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/58/Butternut_nut.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Butternut_sections.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/95/Dead_Butternuts.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Juglans_cinerea_%28fossil_white_walnut%29_in_glacial_sediments%2C_Michigan.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Juglans_cinerea_002.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Орехи"
  },
  {
    "latName": "Juglans_mandshurica",
    "rusName": "Орех маньчжурский",
    "family": "Juglandaceae",
    "url": "https://ru.wikipedia.org/wiki/Juglans_mandshurica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Juglans_mandshurica_Walnut_JPG.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Rendeux_AR3bJPG.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Орехи"
  },
  {
    "latName": "Juglans_regia",
    "rusName": "Орех грецкий",
    "family": "Juglandaceae",
    "url": "https://ru.wikipedia.org/wiki/Juglans_regia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Bark_of_Juglans_regia_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e1/Blanzac-Porcheresse_16_Noyers_2008.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Juglans-regia-buds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Juglans-regia-total.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/75/Juglans-regia.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Орехи"
  },
  {
    "latName": "Juncus_articulatus",
    "rusName": "Ситник членистый",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Juncus_articulatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/JuncusArticulatus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Juncus_articulatus_-_l%C3%A4ikviljaline_luga_Keilas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Juncus_bufonius",
    "rusName": "Ситник жабий",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Juncus_bufonius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Juncus_bufonius.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Juncus_effusus",
    "rusName": "Ситник развесистый",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Juncus_effusus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Juncus_effuses.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Soft_Rush_with_cocoons_of_Coleophora_caespitiella.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Juncus_filiformis",
    "rusName": "Ситник нитевидный",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Juncus_filiformis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Juncus_filiformis_Sturm8.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Juncus_filiformis_kz1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Juncus_geniculatus",
    "rusName": "Ситник членистый",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Juncus_geniculatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/JuncusArticulatus.jpg/275px-JuncusArticulatus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Juncus_articulatus_-_l%C3%A4ikviljaline_luga_Keilas.jpg/250px-Juncus_articulatus_-_l%C3%A4ikviljaline_luga_Keilas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Juncus_gerardii",
    "rusName": "Ситник Жерара",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Juncus_gerardii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Juncus_gerardii.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Juncus_tenuis",
    "rusName": "Ситник тонкий",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Juncus_tenuis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Jute_002_lvp.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Juniperus_communis",
    "rusName": "Можжевельник обыкновенный",
    "family": "Cupressaceae",
    "url": "https://ru.wikipedia.org/wiki/Juniperus_communis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Einebusk_Juniperus_communis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a1/Jeneverbes.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Juniper_wood_pieces_and_1_cent_coin.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d6/JuniperusCommunisAlpina.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Juniperus_communis_MF.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Juniperus_communis_at_Valjala_on_2005-08-11.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Juniperus_communis_fruits_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Juniperus_communis_pollen_cones_TK_2021-05-01_1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Шишкоягоды - пряность, добывают сахар (можжевёловый)"
  },
  {
    "latName": "Kadenia_dubia",
    "rusName": "Кадения сомнительная",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Kadenia_dubia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Cnidium_dubium_001.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Knautia_arvensis",
    "rusName": "Короставник полевой",
    "family": "Dipsacaceae",
    "url": "https://ru.wikipedia.org/wiki/Knautia_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Acker-Witwenblume_Knautia_arvensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Aphantopus_hyperantus_knautia_arvensis_pl.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Bombus_sylvarum_%28male%29_-_Knautia_arvensis_-_Keila2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Scabiosa_ochroleuca_0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Knauti_arvensis_blatt.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d3/Knautia_arvensis_-_harilik_%C3%A4iatar.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Knautia_arvensis_W.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Knautia_arvensis_flower_%28side_view%29_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Knautia_arvensis_inflorescence_%28top_view%29_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Kochia_scoparia",
    "rusName": "Бассия веничная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Kochia_scoparia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Bassia_scoparia_in_Jardin_des_Plantes_001.jpg/275px-Bassia_scoparia_in_Jardin_des_Plantes_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Kochia_scoparia_02.JPG/220px-Kochia_scoparia_02.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Kochia_scoparia",
    "rusName": "Бассия веничная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Kochia_scoparia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Bassia_scoparia_in_Jardin_des_Plantes_001.jpg/275px-Bassia_scoparia_in_Jardin_des_Plantes_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Kochia_scoparia_02.JPG/220px-Kochia_scoparia_02.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Kochia_sieversiana",
    "rusName": "Бассия веничная",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Kochia_sieversiana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Bassia_scoparia_in_Jardin_des_Plantes_001.jpg/275px-Bassia_scoparia_in_Jardin_des_Plantes_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Kochia_scoparia_02.JPG/220px-Kochia_scoparia_02.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Koeleria_delavignei",
    "rusName": "Тонконог Делявиня",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Koeleria_delavignei",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Koeleria_delavignei_40153251.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Koeleria_glauca",
    "rusName": "Тонконог сизый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Koeleria_glauca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Koeleria_glauca_%28habitus%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Laburnum_anagyroides",
    "rusName": "Бобовник анагировидный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Laburnum_anagyroides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Laburnum_anagyroides.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Laburnum_anagyroides2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Laburnum_anagyroides_MHNT.BOT.2004.0.269.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lactuca_sativa",
    "rusName": "Латук посевной",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Lactuca_sativa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/ARS_romaine_lettuce.jpg/275px-ARS_romaine_lettuce.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "листья, кочан, утолщённый стебель"
  },
  {
    "latName": "Lactuca_serriola",
    "rusName": "Латук дикий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Lactuca_serriola",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/91/Kompassla_08-07-2006_9.39.08.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Lactuca_serriola.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/90/Lactuca_serriola_clump.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Lactucaserriola2web.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lactuca_tatarica",
    "rusName": "Латук татарский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Lactuca_tatarica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Chicory_flower_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Lactuca_tatarica.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lamium_album",
    "rusName": "Яснотка белая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Lamium_album",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Bombus_hortorum_-_Laminum_album.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Lamium_album_2_BOGA.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d9/Ortie_blanche_05.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья"
  },
  {
    "latName": "Lamium_amplexicaule",
    "rusName": "Яснотка стеблеобъемлющая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Lamium_amplexicaule",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/82/Lamium_amplexicaule_Kaldari_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Lamium_purpureum_Oklahoma.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lamium_maculatum",
    "rusName": "Яснотка крапчатая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Lamium_maculatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Gefleckte_Taubnessel.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Lamium_maculatum_%27Beacon_Silver%27_Flowers.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/05/Lamium_maculatum_%27Pink_Nancy%27_a1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Lamium_maculatum_%27Shell_Pink%27_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Lamium_maculatum_%27White_Nancy%27_a1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ce/Lamium_maculatum_%27roseum%27_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Lamium_maculatum_MdE.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/Lamium_maculatum_cm01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Lamium_maculatum_leaves_J1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lamium_purpureum",
    "rusName": "Яснотка пурпурная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Lamium_purpureum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Field_of_red_dead-nettle_%28Lamium_purpureum%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Illustration_Lamium_purpureum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Lamium_purpureum_%28red_dead-nettle%29_in_Ohio%2C_United_States.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Lamium_purpureum_-_Tutermaa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Lamium_purpureum_in_the_spring2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/28/Purple_DeathNettle_March_26%2C_2018.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Red_Dead_nettle_close_700.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lappula_squarrosa",
    "rusName": "Липучка оттопыренная",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Lappula_squarrosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Ipomoea_tricolor-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/90/Lappula_squarrosa2.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lapsana_communis",
    "rusName": "Бородавник обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Lapsana_communis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/LapsanaCommunisHabitus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5f/Lapsana_communis_003.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Larix_leptolepis",
    "rusName": "Лиственница тонкочешуйчатая",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Larix_leptolepis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Larix_leptolepis2.JPG/275px-Larix_leptolepis2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Karamatsu-hyouhi.JPG/160px-Karamatsu-hyouhi.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Japanese_Larch_Larix_kaempferi_Needles_3008px.jpg/160px-Japanese_Larch_Larix_kaempferi_Needles_3008px.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Larix_kaempferi_%28Trondheim%29.jpg/160px-Larix_kaempferi_%28Trondheim%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Japanese_Larch_bonsai.jpg/160px-Japanese_Larch_bonsai.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lathraea_squamaria",
    "rusName": "Петров крест чешуйчатый",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Lathraea_squamaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Illustration_Lathraea_squamaria0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Lathraea_squamaria_LC0126.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lathyrus_palustris",
    "rusName": "Чина болотная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lathyrus_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/%28MHNT%29_Vicia_angustifolia_-_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/72/Lathyrus_palustris_flowers_-_Kulna.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lathyrus_pisiformis",
    "rusName": "Чина гороховидная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lathyrus_pisiformis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Lathyrus_pisiformis_in_Estonia.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lathyrus_sylvestris",
    "rusName": "Чина лесная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lathyrus_sylvestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/%28MHNT%29_Vicia_angustifolia_-_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/42/Eucera_longicornis_-_Lathyrus_sylvestris_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Lathyrus_sylvestris1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Lathyrus_tuberosus",
    "rusName": "Чина клубненосная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lathyrus_tuberosus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Lathyrus_tuberosus_MHNT.BOT.2017.12.5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Lathyrus_tuberosus_plant.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Lathyrus_tuberosus_sl22.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Клубни величиной с лесной орех, молодые листья"
  },
  {
    "latName": "Lathyrus_vernus",
    "rusName": "Чина весенняя",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lathyrus_vernus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/%28MHNT%29_Vicia_angustifolia_-_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Bombus_pascuorum_-_Lathyrus_vernus_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Lathyrus_vernus-IMG_7284.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Lathyrus_vernus_%27Alboroseus%27_flower.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Pink-flowered_cultivar%2C_Lathyrus_vernus_%27Alboroseus%27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/The_Soviet_Union_1988_CPA_5966_stamp_%28Deciduous_forest_flowers._Spring_pea%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lavatera_thuringiaca",
    "rusName": "Хатьма тюрингенская",
    "family": "Malvaceae",
    "url": "https://ru.wikipedia.org/wiki/Lavatera_thuringiaca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Lavatera_thuringiaca_Uppsala.jpg/275px-Lavatera_thuringiaca_Uppsala.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Curtis%27s_botanical_magazine_%28No._517%29_%288414348244%29.jpg/67px-Curtis%27s_botanical_magazine_%28No._517%29_%288414348244%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/Plantarum_indigenarum_et_exoticarum_icones_ad_vivum_coloratae%2C_oder%2C_Sammlung_nach_der_Natur_gemalter_Abbildungen_inn-_und_ausl%C3%A4ndlischer_Pflanzen%2C_f%C3%BCr_Liebhaber_und_Beflissene_der_Botanik_%2815904665547%29.jpg/71px-thumbnail.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lavatera_trimestris",
    "rusName": "Хатьма трёхмесячная",
    "family": "Malvaceae",
    "url": "https://ru.wikipedia.org/wiki/Lavatera_trimestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Lavatera_April_2010-4.jpg/275px-Lavatera_April_2010-4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Lavatera0665.JPG/284px-Lavatera0665.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Stockrose_%28Alcea_rosea%29_5_August_2007.jpg/300px-Stockrose_%28Alcea_rosea%29_5_August_2007.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Lavaterazaad-open0854.JPG/300px-Lavaterazaad-open0854.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ledum_palustre",
    "rusName": "Багульник болотный",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Ledum_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Rhododendron-palustre.JPG/275px-Rhododendron-palustre.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/%D0%9C%D1%83%D0%B5%D0%B7%D0%B5%D1%80%D1%81%D0%BA%D0%B8%D0%B9_%D1%80%D0%B0%D0%B9%D0%BE%D0%BD%2C_%D0%B1%D0%BE%D0%BB%D0%BE%D1%82%D0%BE_%D0%A2%D0%B8%D0%BA%D1%88%D0%B0%2C_%D0%B1%D0%B0%D0%B3%D1%83%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA_%281%29.jpg/200px-%D0%9C%D1%83%D0%B5%D0%B7%D0%B5%D1%80%D1%81%D0%BA%D0%B8%D0%B9_%D1%80%D0%B0%D0%B9%D0%BE%D0%BD%2C_%D0%B1%D0%BE%D0%BB%D0%BE%D1%82%D0%BE_%D0%A2%D0%B8%D0%BA%D1%88%D0%B0%2C_%D0%B1%D0%B0%D0%B3%D1%83%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/151_Ledum_palustre.jpg/220px-151_Ledum_palustre.jpg",
      "https://upload.wikimedia.org/wikipedia/ru/thumb/1/1c/Ledum-palustre.jpg/200px-Ledum-palustre.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Rhododendron_tomentosum_003.JPG/200px-Rhododendron_tomentosum_003.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Rhododendron_tomentosum_002.JPG/112px-Rhododendron_tomentosum_002.JPG",
      "https://upload.wikimedia.org/wikipedia/ru/thumb/3/3d/Ledum-palustre1.jpg/130px-Ledum-palustre1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leersia_oryzoides",
    "rusName": "Леерсия рисовидная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Leersia_oryzoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Leersia_oryzoides_NRCS-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Starr_010324-9001_Ehrharta_erecta.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lemna_gibba",
    "rusName": "Ряска горбатая",
    "family": "Lemnaceae",
    "url": "https://ru.wikipedia.org/wiki/Lemna_gibba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/L_gibba2.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "целиком"
  },
  {
    "latName": "Lemna_minor",
    "rusName": "Ряска малая",
    "family": "Lemnaceae",
    "url": "https://ru.wikipedia.org/wiki/Lemna_minor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Dam_with_lemna_minor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Eendekroos_dicht_bijeen.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "целиком"
  },
  {
    "latName": "Lemna_trisulca",
    "rusName": "Ряска трёхдольная",
    "family": "Lemnaceae",
    "url": "https://ru.wikipedia.org/wiki/Lemna_trisulca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Arisaema_triphyllum_fruit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/LemnaTrisulca.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/LemnaTrisulca2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leontodon_autumnalis",
    "rusName": "Кульбаба осенняя",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Leontodon_autumnalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Scorzoneroides_autumnalis_%28Schuppenleuenzahn%29_IMG_4446.JPG/275px-Scorzoneroides_autumnalis_%28Schuppenleuenzahn%29_IMG_4446.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Leontodon_autumnalis_Sturm33.jpg/220px-Leontodon_autumnalis_Sturm33.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leontodon_danubialis",
    "rusName": "Кульбаба щетинистая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Leontodon_danubialis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Leontodon_hispidus_-_Tula_obl.%2C_June_2016.jpg/266px-Leontodon_hispidus_-_Tula_obl.%2C_June_2016.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/548_Leontodon_hispidus.jpg/220px-548_Leontodon_hispidus.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leontodon_hispidus",
    "rusName": "Кульбаба щетинистая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Leontodon_hispidus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Chicory_flower_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Leontodon_hispidus_hyoseroides.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leontodon_saxatilis",
    "rusName": "Кульбаба скальная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Leontodon_saxatilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Chicory_flower_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Leontodontaraxacoides.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leonurus_cardiaca",
    "rusName": "Пустырник сердечный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Leonurus_cardiaca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Motherwort.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leonurus_quinquelobatus",
    "rusName": "Пустырник пятилопастный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Leonurus_quinquelobatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/48/Leonurus_cardiaca_170607b.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lepidium_ruderale",
    "rusName": "Клоповник мусорный",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Lepidium_ruderale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Lepidium_ruderale_Sturm21.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lepidium_sativum",
    "rusName": "Кресс-салат",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Lepidium_sativum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Gartenkresse.jpg/275px-Gartenkresse.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Gartenkresse_bl%C3%BChend.JPG/220px-Gartenkresse_bl%C3%BChend.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Lepidium_sativum_01_ies.jpg/220px-Lepidium_sativum_01_ies.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Свежие листья и масло"
  },
  {
    "latName": "Lepidotheca_suaveolens",
    "rusName": "Ромашка пахучая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Lepidotheca_suaveolens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/20120723Matricaria_discoidea2.jpg/275px-20120723Matricaria_discoidea2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Matricaria_discoidea_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v20.jpg/220px-Matricaria_discoidea_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v20.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Matricaria_discoidea_2630.JPG/220px-Matricaria_discoidea_2630.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lerchenfeldia_flexuosa",
    "rusName": "Луговик извилистый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Lerchenfeldia_flexuosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Deschampsia_flexuosa_1.jpg/275px-Deschampsia_flexuosa_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leucanthemum_maximum",
    "rusName": "Нивяник наибольший",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Leucanthemum_maximum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Galinsoga.ciliata.1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/Leucanthemummaximum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Leucanthemum_vulgare",
    "rusName": "Нивяник обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Leucanthemum_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Leucanthemum_vulgare_08.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/47/Leucanthemum_vulgare_ENBLA03.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/59/Leucanthemum_vulgare_infestation.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Levisticum_officinale",
    "rusName": "Любисток",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Levisticum_officinale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Liebst%C3%B6ckel.JPG/275px-Liebst%C3%B6ckel.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_120%29_%288231726693%29.jpg/220px-K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_120%29_%288231726693%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Зелёные части и корни молодых растений - пряность"
  },
  {
    "latName": "Leymus_arenarius",
    "rusName": "Колосняк песчаный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Leymus_arenarius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Leymus_arenarius_habitus.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Strandr%C3%A5g_%28Leymus_arenarius%29_-Ystad2020.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "мука из зёрен"
  },
  {
    "latName": "Leymus_racemosus",
    "rusName": "Волоснец кистистый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Leymus_racemosus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Leymus_racemosus_%28Lam.%29_Tzvel._1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/88/Leymus_racemosus_-_Berlin_Botanical_Garden_-_IMG_8556.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "мука из зёрен"
  },
  {
    "latName": "Ligustrum_vulgare",
    "rusName": "Бирючина обыкновенная",
    "family": "Oleaceae",
    "url": "https://ru.wikipedia.org/wiki/Ligustrum_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/-_Ligustrum_vulgare_-_Berries_-.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Ligustrum_vulgare_fleurs0a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Schurenbachhalde_10_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6a/Schurenbachhalde_11_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Schurenbachhalde_12_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Wilde_liguster_%28Ligustrum_vulgare%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lilium_martagon",
    "rusName": "Лилия кудреватая",
    "family": "Liliaceae",
    "url": "https://ru.wikipedia.org/wiki/Lilium_martagon",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Lilium_martagon_%28flower%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/T%C3%BCrkenbund_Lilie%2C_Lilium_martagon_2.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "луковицы"
  },
  {
    "latName": "Limosella_aquatica",
    "rusName": "Лужница водяная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Limosella_aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/LimosellaAquatica2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/LimosellaAquatica4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Scrophularia_vernalis0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Linaria_genistifolia",
    "rusName": "Льнянка дроколистная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Linaria_genistifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Linaria_genistifolia_%28subsp._genistifolia%29_sl21.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Linaria_vulgaris",
    "rusName": "Льнянка обыкновенная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Linaria_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/25/Bombus_hortorum_-_Linaria_vulgaris_-_Valingu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Linaria_vulgaris.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/Linaria_vulgaris_flowers_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Linnaea_borealis",
    "rusName": "Линнея северная",
    "family": "Caprifoliaceae",
    "url": "https://ru.wikipedia.org/wiki/Linnaea_borealis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Cogne_rue_linnea_borealis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Linnaea_borealis_1190.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Linnaea_borealis_15030.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Linnaea_borealis_8803.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Twinflower_%28Linnaea_borealis%29_leaf.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Linosyris_vulgaris",
    "rusName": "Солонечник обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Linosyris_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Galatella_linosyris_sl_2.jpg/275px-Galatella_linosyris_sl_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Galatella_linosyris_sl_3.jpg/270px-Galatella_linosyris_sl_3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Linum_catharticum",
    "rusName": "Лён слабительный",
    "family": "Linaceae",
    "url": "https://ru.wikipedia.org/wiki/Linum_catharticum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Liunum_catharticum1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Starr_010309-0546_Calophyllum_inophyllum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Linum_flavum",
    "rusName": "Лён жёлтый",
    "family": "Linaceae",
    "url": "https://ru.wikipedia.org/wiki/Linum_flavum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Linum_flavum_01.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Starr_010309-0546_Calophyllum_inophyllum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Liparis_loeselii",
    "rusName": "Лосняк Лёзеля",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Liparis_loeselii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Epidendrum_schomburgkii_-_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Liparis_loeselii_detail.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Listera_cordata",
    "rusName": "Тайник сердцевидный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Listera_cordata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Listera_cordata_snowdonia.jpg/247px-Listera_cordata_snowdonia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/411_Listera_cordata%2C_Listera_ovata.jpg/220px-411_Listera_cordata%2C_Listera_ovata.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Listera_cordata_030905.jpg/250px-Listera_cordata_030905.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Listera_ovata",
    "rusName": "Тайник яйцевидный",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Listera_ovata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/Listera_ovata_050606.jpg/239px-Listera_ovata_050606.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Illustration_Listera_ovata0.jpg/220px-Illustration_Listera_ovata0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lithospermum_officinale",
    "rusName": "Воробейник лекарственный",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Lithospermum_officinale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Lithospermum_officinale.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Lithospermum_officinale_MHNT.BOT.2007.40.103.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Littorella_uniflora",
    "rusName": "Прибрежница одноцветковая",
    "family": "Plantaginaceae",
    "url": "https://ru.wikipedia.org/wiki/Littorella_uniflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Littorella_uniflora.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lobelia_dortmanna",
    "rusName": "Лобелия Дортмана",
    "family": "Lobeliaceae",
    "url": "https://ru.wikipedia.org/wiki/Lobelia_dortmanna",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Illustration_Legousia_speculum-veneris0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Lobelia_dortmanna_flower2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Lobelia_dortmanna_full.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lobularia_maritima",
    "rusName": "Лобулярия приморская",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Lobularia_maritima",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Brassicaceae_-_Lobularia_maritima-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/37/Brassicaceae_-_Lobularia_maritima-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/30/Brassicaceae_-_Lobularia_maritima-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Brassicaceae_-_Lobularia_maritima.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Lobularia_maritima3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Smagliczka_nadmorska_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Sweet_alyssum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lolium_multiflorum",
    "rusName": "Плевел многоцветковый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Lolium_multiflorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Lolium_multiflorum_detail.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lolium_perenne",
    "rusName": "Плевел многолетний",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Lolium_perenne",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Engels_raaigras_%28Lolium_perenne%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Engels_raaigras_spruiten_%28Lolium_perenne_tillers%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Engels_raaigras_vruchten_%28Lolium_perenne_fruits%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/59/Lolium_perenne_Engels_raaigras_doorschietend.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Lolium_perenne_showing_ligule_and_ribbed_leaf.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/30/Perennial_Ryegrass_Nursery.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lolium_temulentum",
    "rusName": "Плевел опьяняющий",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Lolium_temulentum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Illustration_Leymus_arenarius_and_Lolium_temulentum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/76/Lolium_temulentum_001.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lonicera_tatarica",
    "rusName": "Жимолость татарская",
    "family": "Caprifoliaceae",
    "url": "https://ru.wikipedia.org/wiki/Lonicera_tatarica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/98/20140406Lonicera_tatarica01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/20170307Lonicera_tatarica1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Scabiosa_ochroleuca_0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/04/Lonicera_tatarica_-_Tatarian_honeysuckle_-_51892967732.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Rhagoletis_sp._on_Tatarian_Honeysuckle_%28Lonicera_tatarica%29_-_Oslo%2C_Norway_2020-08-04_%2801%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Scabiosa_ochroleuca_0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/Tartarian_Honeysuckle_Flowers_%283598143554%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Tatarian_Honeysuckle_PLT-BR-BH-5.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lonicera_xylosteum",
    "rusName": "Жимолость настоящая",
    "family": "Caprifoliaceae",
    "url": "https://ru.wikipedia.org/wiki/Lonicera_xylosteum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Scabiosa_ochroleuca_0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Lonicera_xylosteum_Sturm45.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Lonicera_xylosteum_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Scabiosa_ochroleuca_0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lotus_corniculatus",
    "rusName": "Лядвенец рогатый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lotus_corniculatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/02/%28MHNT%29_Lotus_corniculatus_-_Plant_habit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Birds-foot_Lotus_corniculatus_flowerheads_leaves.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9e/Birds-foot_trefoil_Lotus_corniculatus_roadside.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Birds-foot_trefoil_new_seedhead.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Lotus_corniculatus_Minnesota.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lunaria_rediviva",
    "rusName": "Лунник оживающий",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Lunaria_rediviva",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Lunaria_rediviva_2015-06-01_OB_120.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Lunaria_rediviva_2015-06-01_OB_122.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Lunaria_rediviva_251202.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Vaste_Judaspenning_plant_Lunaria_rediviva.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lupinus_angustifolius",
    "rusName": "Люпин узколистный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lupinus_angustifolius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f3/Lupinus_angustifolius3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Lupinus_angustifolius_20150331_a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Lupinus_angustifolius_FlowersCloseup_2009Mach28_DehesaBoyaldePuertollano.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Lupinus_angustifolius_MHNT.BOT.2016.24.52.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lupinus_luteus",
    "rusName": "Люпин жёлтый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lupinus_luteus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Campo_no_distrito_de_Beja_-_08.03.2020.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Gelbe_LUPINE_%28Lupinus_luteus%29_Portugal%2C_Algarve.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Illustration_Lupinus_luteus1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Lupinus_luteus_%286698517471%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/55/Lupinus_luteus_MHNT.BOT.2011.3.55.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lupinus_polyphyllus",
    "rusName": "Люпин многолистный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Lupinus_polyphyllus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Blaue.Lupine.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Flores_de_lupino_%28Ushuaia%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/56/Lupina_mnoholist%C3%A1_%28Lupinus_polyphyllus%29_-_Cesta_slobody.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Lupine_on_St._George_by_Laney_White_USFWS.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/da/Lupinus_polyphyllus.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Russell_lupins_Canterbury_New_Zealand.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Luzula_campestris",
    "rusName": "Ожика равнинная",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Luzula_campestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/4/48/Luzula_campestris_IP0605002.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Luzula_campestris_Sturm23.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Luzula_campestris_in_Wales.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Luzula_multiflora",
    "rusName": "Ожика многоцветковая",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Luzula_multiflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Luzula_multiflora_-_Berlin_Botanical_Garden_-_IMG_8554.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Luzula_multiflora_kz2.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Luzula_pilosa",
    "rusName": "Ожика волосистая",
    "family": "Juncaceae",
    "url": "https://ru.wikipedia.org/wiki/Luzula_pilosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Luzula_pilosa_-_Berlin_Botanical_Garden_-_IMG_8652.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lychnis_chalcedonica",
    "rusName": "Зорька обыкновенная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Lychnis_chalcedonica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Lychnis_chalcedonica_B.jpg/263px-Lychnis_chalcedonica_B.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lycium_barbarum",
    "rusName": "Дереза обыкновенная",
    "family": "Solanaceae",
    "url": "https://ru.wikipedia.org/wiki/Lycium_barbarum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Illustration_Lycium_barbarum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Lycium_barbarum_Flower_Closeup_Miguelturra_CampodeCalatrava.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Wolfberries_China_7-05.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lycopersicon_esculentum",
    "rusName": "Томат",
    "family": "Solanaceae",
    "url": "https://ru.wikipedia.org/wiki/Lycopersicon_esculentum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Pomodorini_sulla_pianta.jpg/275px-Pomodorini_sulla_pianta.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Tomato_San_Marzano_flower_01.jpg/279px-Tomato_San_Marzano_flower_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Tomatoes_plain_and_sliced.jpg/220px-Tomatoes_plain_and_sliced.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Tomates_anciennes.jpg/320px-Tomates_anciennes.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/%D0%92%D1%8F%D0%BB%D0%B5%D0%BD%D1%8B%D0%B5_%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D1%8B.jpg/220px-%D0%92%D1%8F%D0%BB%D0%B5%D0%BD%D1%8B%D0%B5_%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D1%8B.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Tomato_P5260299b.jpg/400px-Tomato_P5260299b.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/%D0%92%D1%81%D1%85%D0%BE%D0%B4%D1%8B_%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%BE%D0%B2.jpg/220px-%D0%92%D1%81%D1%85%D0%BE%D0%B4%D1%8B_%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%BE%D0%B2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Tomato-seedlings-3805.jpg/220px-Tomato-seedlings-3805.jpg",
      "https://upload.wikimedia.org/wikipedia/ru/thumb/1/1a/%D0%9C%D0%BE%D0%BD%D1%83%D0%BC%D0%B5%D0%BD%D1%82_%22%D0%A1%D0%BB%D0%B0%D0%B2%D0%B0_%D0%BF%D0%BE%D0%BC%D0%B8%D0%B4%D0%BE%D1%80%D1%83%22.jpg/204px-%D0%9C%D0%BE%D0%BD%D1%83%D0%BC%D0%B5%D0%BD%D1%82_%22%D0%A1%D0%BB%D0%B0%D0%B2%D0%B0_%D0%BF%D0%BE%D0%BC%D0%B8%D0%B4%D0%BE%D1%80%D1%83%22.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Lycopodiella_inundata",
    "rusName": "Ликоподиелла заливаемая",
    "family": "Lycopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Lycopodiella_inundata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Lycopodiella_inundata_002.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Lycopodiella_inundata_001.jpg/375px-Lycopodiella_inundata_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Lycopodiella_inundata_005.jpg/300px-Lycopodiella_inundata_005.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lycopodium_annotinum",
    "rusName": "Плаун годичный",
    "family": "Lycopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Lycopodium_annotinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Lycopodium_annotinum_1127100195.jpg/275px-Lycopodium_annotinum_1127100195.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/517_Lycopodium_clavatum%2C_Lycopodium_annotinum.jpg/220px-517_Lycopodium_clavatum%2C_Lycopodium_annotinum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lycopodium_clavatum",
    "rusName": "Плаун булавовидный",
    "family": "Lycopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Lycopodium_clavatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/LycopodiumClavatum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ea/Lycopodium_clavatum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1d/Lycopodium_clavatum_151207.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Lycopodium_clavatum_clavatum2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lycopsis_arvensis",
    "rusName": "Воловик полевой",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Lycopsis_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Anchusa_arvensis3.jpg/275px-Anchusa_arvensis3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/578_Lycopsis_arvensis.jpg/220px-578_Lycopsis_arvensis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lycopus_europaeus",
    "rusName": "Зюзник европейский",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Lycopus_europaeus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Lycopus_europaeus_Prague_2012_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Wolfspoot_R0012816.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lysimachia_nummularia",
    "rusName": "Вербейник монетный",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Lysimachia_nummularia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Lysimachia_nummularia0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Lysimachia_nummularia1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Primula_vulgaris_Mezzolombardo_03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lysimachia_vulgaris",
    "rusName": "Вербейник обыкновенный",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Lysimachia_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Lysimachia_vulgaris_%28flowers%29_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Lysimachia_vulgaris_MHNT.BOT.2004.0.805.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Lysimachia_vulgaris_Sweden.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Primula_vulgaris_Mezzolombardo_03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Lythrum_salicaria",
    "rusName": "Дербенник иволистный",
    "family": "Lythraceae",
    "url": "https://ru.wikipedia.org/wiki/Lythrum_salicaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Bombus_sylvarum_-_Lythrum_salicaria_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Cooper_Marsh_-_Purple-loosestrife.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Loosestrife_%28Lythrum_salicaria_%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/06/LythrumSalicariaBig.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Lythrum_salicaria_-_harilik_kukesaba.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Purple_Loosestrife_%28Lythrum_salicaria%29_naturalised_in_Pennsylvania.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Цветки"
  },
  {
    "latName": "Lythrum_virgatum",
    "rusName": "Дербенник прутовидный",
    "family": "Lythraceae",
    "url": "https://ru.wikipedia.org/wiki/Lythrum_virgatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Lythrum_virgatum_RB2.JPG/275px-Lythrum_virgatum_RB2.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Maianthemum_bifolium",
    "rusName": "Майник двулистный",
    "family": "Convallariaceae",
    "url": "https://ru.wikipedia.org/wiki/Maianthemum_bifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Dvilapes_medutes_ziedai.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/Illustration_Maianthemum_bifolium0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Malaxis_monophyllos",
    "rusName": "Мякотница однолистная",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Malaxis_monophyllos",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Epidendrum_schomburgkii_-_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Malaxis_monophyllos_140706.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Malus_baccata",
    "rusName": "Яблоня ягодная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Malus_baccata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Crab_apples_by_the_roadside_-_geograph.org.uk_-_978786.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Malus-baccata.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/MalusBaccataTrunk.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Malus_domestica",
    "rusName": "Яблоня домашняя",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Malus_domestica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Cleaned-Illustration_Malus_domestica.jpg/249px-Cleaned-Illustration_Malus_domestica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Yabloko.jpg/220px-Yabloko.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Fuji_apple.jpg/220px-Fuji_apple.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Apple_blossom._Eastern_Siberia.jpg/220px-Apple_blossom._Eastern_Siberia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Albrecht_D%C3%BCrer_-_Adam_and_Eve_%28Prado%29_2.jpg/180px-Albrecht_D%C3%BCrer_-_Adam_and_Eve_%28Prado%29_2.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Malus_praecox",
    "rusName": "Яблоня низкая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Malus_praecox",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Fleur_de_pommier_11.jpg/275px-Fleur_de_pommier_11.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Malus_sylvestris",
    "rusName": "Яблоня лесная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Malus_sylvestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Crab_Apple_Flower.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Crab_apples_by_the_roadside_-_geograph.org.uk_-_978786.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Crabapples.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Flowering_crab_apple_trees_%28Malus_sylvestris%29_-_geograph.org.uk_-_1310191.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Malus_sylvestris_%28Southeast_Michigan%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Malus_sylvestris_003.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Malus_sylvestris_005.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/27/Malus_sylvestris_inflorescence%2C_Vosseslag%2C_Belgium.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Malva_moschata",
    "rusName": "Мальва мускусная",
    "family": "Malvaceae",
    "url": "https://ru.wikipedia.org/wiki/Malva_moschata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/Malva_moschata_20060708163508wp.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Muskuskaasjeskruid_vrucht.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/91/White_musk_mallow_in_Tuntorp_2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Malva_neglecta",
    "rusName": "Мальва незамеченная",
    "family": "Malvaceae",
    "url": "https://ru.wikipedia.org/wiki/Malva_neglecta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Malva_neglecta-flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Malva_silvestris_Sturm63.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Malva_pusilla",
    "rusName": "Мальва приземистая",
    "family": "Malvaceae",
    "url": "https://ru.wikipedia.org/wiki/Malva_pusilla",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Malva_pusilla_at_Shravanabelagola_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Malva_pusilla_sl7.jpg/413px-Malva_pusilla_sl7.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Malva_pusilla_Smith_-_Flora_regni_Borussici_vol._3_-_t._189.png/330px-Malva_pusilla_Smith_-_Flora_regni_Borussici_vol._3_-_t._189.png"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Malva_sylvestris",
    "rusName": "Мальва лесная",
    "family": "Malvaceae",
    "url": "https://ru.wikipedia.org/wiki/Malva_sylvestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/CommonMallowFlower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Common_mallow_closeup.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/89/Firebug_on_fruit_of_Malva_sylvestris.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/09/Mallow_January_2008-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/MalopeTrifida1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Malva_sylvestris%2C_violet_hue.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Malva_sylvestris_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Malva_sylvestris_seeds.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "листья, молодые побеги, корни"
  },
  {
    "latName": "Malva_verticillata",
    "rusName": "Мальва мутовчатая",
    "family": "Malvaceae",
    "url": "https://ru.wikipedia.org/wiki/Malva_verticillata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d9/Malva_verticillata2_WPC.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e5/Malva_verticillata_%28USDA%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Malva_verticillata_3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Malva_verticillata_4.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Marrubium_vulgare",
    "rusName": "Шандра обыкновенная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Marrubium_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Horehound_bug.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Horehound_candy_drops.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e6/Marrubium_vulgare.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Marrubium_vulgare.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Marrubium_vulgare0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Marrubium_vulgare_in_Mexico_II.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Matricaria_perforata",
    "rusName": "Трёхрёберник непахучий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Matricaria_perforata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Tripleurospermum_perforatum_20041012_2572.jpg/275px-Tripleurospermum_perforatum_20041012_2572.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Tripleurospermum_inodorum_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-178.jpg/220px-Tripleurospermum_inodorum_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-178.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Matricaria_recutita",
    "rusName": "Ромашка аптечная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Matricaria_recutita",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Matricaria_recutita_001_cropped.jpg/275px-Matricaria_recutita_001_cropped.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Matricaria_chamomilla_and_Leucanthemum_vulgare.jpg/300px-Matricaria_chamomilla_and_Leucanthemum_vulgare.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Matricaria_recutita_0.3_R.jpg/200px-Matricaria_recutita_0.3_R.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Chamomile%40original_size.jpg/200px-Chamomile%40original_size.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Kamomillasaunio_%28Matricaria_recutita%29.JPG/220px-Kamomillasaunio_%28Matricaria_recutita%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Matricaria_chamomilla_flowers.jpg/220px-Matricaria_chamomilla_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Camomila-vulgar_seca2.jpg/220px-Camomila-vulgar_seca2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Camillen_blumen_Gart_der_Gesundheit.jpg/220px-Camillen_blumen_Gart_der_Gesundheit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Description_of_Matricaria_chamomilla_in_Linnaei_Species_plantarum_1753_vol_2_p_891.jpg/220px-Description_of_Matricaria_chamomilla_in_Linnaei_Species_plantarum_1753_vol_2_p_891.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Matricaria_recutita_Sturm13045.jpg/145px-Matricaria_recutita_Sturm13045.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Matricaria_chamomilla_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v4.jpg/151px-Matricaria_chamomilla_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Illustration_Matricaria_chamomilla0.jpg/129px-Illustration_Matricaria_chamomilla0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Matricaria_recutita_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-091.jpg/165px-Matricaria_recutita_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-091.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/182_Matricaria_chamomilla_L.jpg/145px-182_Matricaria_chamomilla_L.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/12_Matricaria_chamomilla.jpg/151px-12_Matricaria_chamomilla.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Stamps_of_Germany_%28DDR%29_1978%2C_MiNr_2289.jpg/220px-Stamps_of_Germany_%28DDR%29_1978%2C_MiNr_2289.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Надземную часть используют как замену чая, листья - пряность"
  },
  {
    "latName": "Matteuccia_struthiopteris",
    "rusName": "Страусник обыкновенный",
    "family": "Onocleaceae",
    "url": "https://ru.wikipedia.org/wiki/Matteuccia_struthiopteris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Matteuccia_struthiopteris_%284%29.JPG/275px-Matteuccia_struthiopteris_%284%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/%D0%9F%D0%BE%D1%87%D1%82%D0%BE%D0%B2%D0%B0%D1%8F_%D0%BC%D0%B0%D1%80%D0%BA%D0%B0_%D0%A1%D0%A1%D0%A1%D0%A0_%E2%84%96_5849._1987._%D0%A4%D0%BB%D0%BE%D1%80%D0%B0_%D0%A1%D0%A1%D0%A1%D0%A0._%D0%9F%D0%B0%D0%BF%D0%BE%D1%80%D1%82%D0%BD%D0%B8%D0%BA%D0%B8.jpg/220px-%D0%9F%D0%BE%D1%87%D1%82%D0%BE%D0%B2%D0%B0%D1%8F_%D0%BC%D0%B0%D1%80%D0%BA%D0%B0_%D0%A1%D0%A1%D0%A1%D0%A0_%E2%84%96_5849._1987._%D0%A4%D0%BB%D0%BE%D1%80%D0%B0_%D0%A1%D0%A1%D0%A1%D0%A0._%D0%9F%D0%B0%D0%BF%D0%BE%D1%80%D1%82%D0%BD%D0%B8%D0%BA%D0%B8.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Illustration_Matteuccia_struthiopteris0.jpg/220px-Illustration_Matteuccia_struthiopteris0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Matteuccia_struthiopteris_9882.jpg/220px-Matteuccia_struthiopteris_9882.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Matteuccia_struthiopteris_01.JPG/220px-Matteuccia_struthiopteris_01.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги (рахисы)"
  },
  {
    "latName": "Medicago_falcata",
    "rusName": "Люцерна серповидная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Medicago_falcata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/51/Bombus_sylvarum_-_Medicago_falcata_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Medicago_falcata_bgiu.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Medicago_glandulosa",
    "rusName": "Люцерна серповидная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Medicago_glandulosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Medicago_falcata_bgiu.jpg/275px-Medicago_falcata_bgiu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Medicago_falcata1.jpg/220px-Medicago_falcata1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Bombus_sylvarum_-_Medicago_falcata_-_Keila.jpg/220px-Bombus_sylvarum_-_Medicago_falcata_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Medicago_lupulina",
    "rusName": "Люцерна хмелевидная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Medicago_lupulina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Illustration_Medicago_arabica1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/MedicagoLupulina02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Medicago_lupulina.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Medicago_lupulina_02_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/00/Medicago_lupulina_04_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Medicago_lupulina_10_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Medicago_lupulina_11_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Medicago_lupulina_BB-1913.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Medicago_lupulina_NRCS-2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Medicago_romanica",
    "rusName": "Люцерна серповидная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Medicago_romanica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Medicago_falcata_bgiu.jpg/275px-Medicago_falcata_bgiu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Medicago_falcata1.jpg/220px-Medicago_falcata1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Bombus_sylvarum_-_Medicago_falcata_-_Keila.jpg/220px-Bombus_sylvarum_-_Medicago_falcata_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Medicago_sativa",
    "rusName": "Люцерна посевная",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Medicago_sativa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Alfalfa_in_a_Field_%282646191862%29.jpg/275px-Alfalfa_in_a_Field_%282646191862%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Illustration_Medicago_sativa1.jpg/220px-Illustration_Medicago_sativa1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/%D0%9B%D1%83%D1%86%D0%B5%D1%80%D0%BA%D0%B0_%28%D0%94%D0%B5%D1%82%D0%B5%D0%BB%D0%B8%D0%BD%D0%B0%29_%D0%9C%D0%9A.jpg/270px-%D0%9B%D1%83%D1%86%D0%B5%D1%80%D0%BA%D0%B0_%28%D0%94%D0%B5%D1%82%D0%B5%D0%BB%D0%B8%D0%BD%D0%B0%29_%D0%9C%D0%9A.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Medicago_sativa0.jpg/270px-Medicago_sativa0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Medicago_truncatula",
    "rusName": "Люцерна усечённая",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Medicago_truncatula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Medicago_truncatula_A17_branch.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melampyrum_arvense",
    "rusName": "Марьянник полевой",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Melampyrum_arvense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Melampyrum_arvense_brasles_02_19052002_6.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melampyrum_cristatum",
    "rusName": "Марьянник гребенчатый",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Melampyrum_cristatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Melampyrum_cristatum_180605.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melampyrum_nemorosum",
    "rusName": "Марьянник дубравный",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Melampyrum_nemorosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Atlas_roslin_pl_Pszeniec_gajowy_9408_7395.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Melampyrum_nemorosum_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Pszeniec_gajowy_0179.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melampyrum_pratense",
    "rusName": "Марьянник луговой",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Melampyrum_pratense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Bombus_hortorum_-_Melampyrum_pratense_-_Tallinn.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/Melampyrum_pratense.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melampyrum_sylvaticum",
    "rusName": "Марьянник лесной",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Melampyrum_sylvaticum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Wald-Wachtelweizen.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melandrium_album",
    "rusName": "Дрёма белая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Melandrium_album",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/SILENE_LATIFOLIA_-_L%27ALZINA_-_IB-250_%28Melandri_blanc%29.JPG/275px-SILENE_LATIFOLIA_-_L%27ALZINA_-_IB-250_%28Melandri_blanc%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Silene_latifolia_Sturm18.jpg/220px-Silene_latifolia_Sturm18.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Silene_latifolia_subsp._alba_MHNT.BOT.2004.0.310.jpg/220px-Silene_latifolia_subsp._alba_MHNT.BOT.2004.0.310.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые проростки"
  },
  {
    "latName": "Melandrium_dioicum",
    "rusName": "Дрёма двудомная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Melandrium_dioicum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Bayrischer_Wald_9929.JPG/266px-Bayrischer_Wald_9929.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melica_nutans",
    "rusName": "Перловник поникший",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Melica_nutans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Melica_nutans.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/Melica_nutans_MHNT.BOT.2011.18.4.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melilotus_albus",
    "rusName": "Донник белый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Melilotus_albus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Apis_mellifera_-_Melilotus_albus_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Melilotus_albus_MHNT.BOT.2008.1.31.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melilotus_officinalis",
    "rusName": "Донник лекарственный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Melilotus_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/20140613Melilotus_officinalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Bombus_lapidarius_-_Melilotus_officinalis_-_Tallinn.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Melilotus_wolgicus",
    "rusName": "Донник волжский",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Melilotus_wolgicus",
    "images": [],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Mentha_aquatica",
    "rusName": "Мята водная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Mentha_aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Mentha_aquatica01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Mentha_aquatica_%282005_09_18%29_-_uitsnede.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Mentha_aquatica_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Mentha_aquatica_148819609.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1d/Mentha_aquatica_bluete.jpeg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья, свежие побеги"
  },
  {
    "latName": "Mentha_arvensis",
    "rusName": "Мята полевая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Mentha_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8c/Mentha_arvensis_-_p%C3%B5ldm%C3%BCnt_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Mentha_longifolia",
    "rusName": "Мята длиннолистная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Mentha_longifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Mentha_longifolia_2005.08.02_09.53.56.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Menyanthes_trifoliata",
    "rusName": "Вахта (растение)",
    "family": "Menyanthaceae",
    "url": "https://ru.wikipedia.org/wiki/Menyanthes_trifoliata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/MenyanthesTrifoliata.jpg/275px-MenyanthesTrifoliata.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_89%29_%288231710333%29.jpg/220px-K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_89%29_%288231710333%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/%D0%A1%D0%B5%D0%BC%D0%B5%D0%BD%D0%B0_%D0%B2%D0%B0%D1%85%D1%82%D1%8B..jpg/220px-%D0%A1%D0%B5%D0%BC%D0%B5%D0%BD%D0%B0_%D0%B2%D0%B0%D1%85%D1%82%D1%8B..jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/%D0%A1%D0%B5%D0%BC%D0%B5%D0%BD%D0%B0_%D0%92%D0%B0%D1%85%D1%82%D1%8B.jpg/220px-%D0%A1%D0%B5%D0%BC%D0%B5%D0%BD%D0%B0_%D0%92%D0%B0%D1%85%D1%82%D1%8B.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Menyanthes-trifoliata.JPG/220px-Menyanthes-trifoliata.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Menyanthes_trifoliata1_ies.jpg/190px-Menyanthes_trifoliata1_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Waterdrieblad_bloem.jpg/160px-Waterdrieblad_bloem.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Menyanthes_trifoliata_05_ies.jpg/190px-Menyanthes_trifoliata_05_ies.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Mercurialis_perennis",
    "rusName": "Пролесник многолетний",
    "family": "Euphorbiaceae",
    "url": "https://ru.wikipedia.org/wiki/Mercurialis_perennis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Mercuralis_perennis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Middendorfia_borysthenica",
    "rusName": "Миддендорфия",
    "family": "Lythraceae",
    "url": "https://ru.wikipedia.org/wiki/Middendorfia_borysthenica",
    "images": [],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Milium_effusum",
    "rusName": "Бор развесистый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Milium_effusum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Milium.effusum.2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Milium_effusum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Milium_effusum_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Milium_effusum_03.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Moehringia_trinervia",
    "rusName": "Мёрингия трёхжилковая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Moehringia_trinervia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Moehringia_trinervia_2005.04.30_16.27.42.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Molinia_caerulea",
    "rusName": "Молиния голубая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Molinia_caerulea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5f/Claviceps_microcephala.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Illustration_Molinia_caerulea0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Molinia-caerulea-habitat.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Molinia-caerulea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Molinia.caerulea.2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Molinia.caerulea.3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Molinia.caerulea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e5/Molinia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Molinia_caerulea.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Moneses_uniflora",
    "rusName": "Одноцветка",
    "family": "Pyrolaceae",
    "url": "https://ru.wikipedia.org/wiki/Moneses_uniflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Pyrola_uniflora_a1.jpg/275px-Pyrola_uniflora_a1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/155_Pyrola_uniflora.jpg/220px-155_Pyrola_uniflora.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Moneses_uniflora_300606.jpg/160px-Moneses_uniflora_300606.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Pyrola_uniflora_a2.jpg/222px-Pyrola_uniflora_a2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Moneses_uniflora_1043.JPG/250px-Moneses_uniflora_1043.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Moneses_uniflora.jpeg/125px-Moneses_uniflora.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Mountain_Laurel_Kalmia_latifolia_Flowers_2448px.jpg/60px-Mountain_Laurel_Kalmia_latifolia_Flowers_2448px.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Montia_fontana",
    "rusName": "Монтия ключевая",
    "family": "Portulacaceae",
    "url": "https://ru.wikipedia.org/wiki/Montia_fontana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Montia_fontana_%28mezenc%2C_43%2C_France%292.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Starr_030418-0059_Bougainvillea_spectabilis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Morus_alba",
    "rusName": "Шелковица белая",
    "family": "Moraceae",
    "url": "https://ru.wikipedia.org/wiki/Morus_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/FruitlessMulberry-3965.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Morus-alba.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/MorusAlbaChampion.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Morus_alba-leaves.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Morus_alba_-_Tehran.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/Morus_alba_20190516a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Morus_alba_MHNT.BOT.2006.0.1270.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/e4/Morus_alba_flowers_in_India.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Morus_alba_fruits_7th_Brigade_Park_Chermside_P1070826.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Mycelis_muralis",
    "rusName": "Мицелис",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Mycelis_muralis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Mycelis_muralis_%287914491298%29.jpg/266px-Mycelis_muralis_%287914491298%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/557_Lactuca_muralis.jpg/220px-557_Lactuca_muralis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Myosotis_alpestris",
    "rusName": "Незабудка альпийская",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Myosotis_alpestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Ipomoea_tricolor-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Myosotis_alpestris.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Myosotis_arvensis",
    "rusName": "Незабудка полевая",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Myosotis_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Myosotis_arvensis%2C_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Myosotis_arvensis%2C_flower_from_side_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/55/Myosotis_arvensis%2C_flower_head_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Myosotis_arvensis%2C_general_view.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d6/Myosotis_arvensis%2C_leaf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Myosotis_arvensis%2C_mature_calyx_with_fruit_inside_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/Myosotis_arvensis%2C_mature_calyx_with_fruit_inside_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Myosotis_arvensis%2C_mature_fruit_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Myosotis_arvensis%2C_mature_fruit_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Myosotis_arvensis.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Myosotis_scorpioides",
    "rusName": "Незабудка болотная",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Myosotis_scorpioides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Illustration_Myosotis_scorpioides0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Myosotis_scorpioides_-_Niitv%C3%A4lja_bog.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Myosotis_scorpioides_LC0184.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Myosotis_scorpioides_PID1155-3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/48/Myosotis_scorpioides_blatt.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c1/Water_Forget-Me-Not_%28Myosotis_scorpioides%29_in_Pennsylvania.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Myosotis_sparsiflora",
    "rusName": "Незабудка редкоцветковая",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Myosotis_sparsiflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Myosotis_sparsiflora_kz1.JPG/275px-Myosotis_sparsiflora_kz1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Myosotis_sparsiflora_Sturm20.jpg/220px-Myosotis_sparsiflora_Sturm20.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Myosotis_sylvatica",
    "rusName": "Незабудка лесная",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Myosotis_sylvatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Myosotis_sylvatica_03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Myosotis_sylvatica_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Myosotis_sylvatica_Calyx.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Myosotis_sylvatica_Calyx_on_stem.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/Myosotis_sylvatica_Calyx_with_mature_fruit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Myosotis_sylvatica_Flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Myosotis_sylvatica_Flower_head.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Myosotis_sylvatica_Mature_fruit-side_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6a/Myosotis_sylvatica_Mature_fruit-side_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Myosotis_sylvatica_Stem.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Myosoton_aquaticum",
    "rusName": "Мягковолосник",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Myosoton_aquaticum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Myosoton_aquaticum1.jpg/275px-Myosoton_aquaticum1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Myosoton_aquaticum_Sturm9.jpg/220px-Myosoton_aquaticum_Sturm9.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Масло из семян"
  },
  {
    "latName": "Myosurus_minimus",
    "rusName": "Мышехвостник маленький",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Myosurus_minimus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Myosurus_minimus.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Myriophyllum_spicatum",
    "rusName": "Уруть колосистая",
    "family": "Haloragaceae",
    "url": "https://ru.wikipedia.org/wiki/Myriophyllum_spicatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Cleaned-Illustration_Myriophyllum_spicatum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Myriophyllum_verticillatum",
    "rusName": "Уруть мутовчатая",
    "family": "Haloragaceae",
    "url": "https://ru.wikipedia.org/wiki/Myriophyllum_verticillatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Myriophyllum_verticillatum.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Najas_major",
    "rusName": "Наяда морская",
    "family": "Najadaceae",
    "url": "https://ru.wikipedia.org/wiki/Najas_major",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Najas_marina.jpeg/275px-Najas_marina.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Illustration_Najas_marina0.jpg/220px-Illustration_Najas_marina0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Najas_marina",
    "rusName": "Наяда морская",
    "family": "Najadaceae",
    "url": "https://ru.wikipedia.org/wiki/Najas_marina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Najas_marina_060628_Korea.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Nardus_stricta",
    "rusName": "Белоус (растение)",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Nardus_stricta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Nardus_stricta_mountain_medow.JPG/275px-Nardus_stricta_mountain_medow.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Illustration_Nardus_stricta0.jpg/220px-Illustration_Nardus_stricta0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Naumburgia_thyrsiflora",
    "rusName": "Вербейник кистецветный",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Naumburgia_thyrsiflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Lysimachia_thyrsiflora_Oulu%2C_Finland_12.06.2013.jpg/275px-Lysimachia_thyrsiflora_Oulu%2C_Finland_12.06.2013.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Lysimachia_thyrsiflora_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg/220px-Lysimachia_thyrsiflora_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Lysimachia_thyrsiflora_inflore_kz.jpg/150px-Lysimachia_thyrsiflora_inflore_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Neottia_nidus-avis",
    "rusName": "Гнездовка настоящая",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Neottia_nidus-avis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Neottia_nidus-avis_-_Pruunikas_pesajuur_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Neottia_nidus-avis_buds_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Neottianthe_cucullata",
    "rusName": "Гнездоцветка клобучковая",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Neottianthe_cucullata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Neottianthe_cucullata_%28flower%29.JPG/256px-Neottianthe_cucullata_%28flower%29.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Nepeta_cataria",
    "rusName": "Котовник кошачий",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Nepeta_cataria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Nepeta_cataria_3_RF.jpg/275px-Nepeta_cataria_3_RF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Nepeta_cataria_Sturm24.jpg/220px-Nepeta_cataria_Sturm24.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Starr_080117-2158_Nepeta_cataria.jpg/180px-Starr_080117-2158_Nepeta_cataria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Nepeta-catarica1.jpg/180px-Nepeta-catarica1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Catnip-effects.jpg/220px-Catnip-effects.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Neslia_paniculata",
    "rusName": "Неслия",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Neslia_paniculata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Neslia_paniculata_s._str._sl19.jpg/275px-Neslia_paniculata_s._str._sl19.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Neslia_paniculata_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v19.jpg/220px-Neslia_paniculata_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v19.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Nigella_arvensis",
    "rusName": "Чернушка полевая",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Nigella_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Illustration_Nigella_arvensis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/Nigella_arvensis_kz05.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/Nigella_arvensis_sl66.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена - пряность"
  },
  {
    "latName": "Nuphar_lutea",
    "rusName": "Кубышка жёлтая",
    "family": "Nymphaeaceae",
    "url": "https://ru.wikipedia.org/wiki/Nuphar_lutea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/27/1816_German_Plant_Illustrations%2C_F.G._Hayne%2C_353126%2C_Illustration.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Nuphar_lutea_%28habitus%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Nuphar_lutea_1_ms.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Nuphar_lutea_stamen_in_late_stage_of_flower_developmen.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "В сыром виде корневища ядовиты, но съедобны, если их отварить в солёной воде. Поджаренные семена - суррогат кофе"
  },
  {
    "latName": "Nuphar_pumila",
    "rusName": "Кубышка малая",
    "family": "Nymphaeaceae",
    "url": "https://ru.wikipedia.org/wiki/Nuphar_pumila",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Nuphar_pumilum2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Nymphaea_candida",
    "rusName": "Кувшинка снежно-белая",
    "family": "Nymphaeaceae",
    "url": "https://ru.wikipedia.org/wiki/Nymphaea_candida",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Lumme_%28Nymphaea_candida%29.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "корневища"
  },
  {
    "latName": "Nymphaea_tetragona",
    "rusName": "Кувшинка четырёхгранная",
    "family": "Nymphaeaceae",
    "url": "https://ru.wikipedia.org/wiki/Nymphaea_tetragona",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/Nymphaea_tetragona_var._minima.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена и листовые почки"
  },
  {
    "latName": "Nymphoides_peltata",
    "rusName": "Болотноцветник щитолистный",
    "family": "Menyanthaceae",
    "url": "https://ru.wikipedia.org/wiki/Nymphoides_peltata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Fringed_Water-lily.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e6/NymphoidesPeltata-flower2-hr.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Oenanthe_aquatica",
    "rusName": "Омежник водный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Oenanthe_aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/258_Oenanthe_aquatica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/89/Oenanthe_aquatica_002.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Oenanthe_aquatica_at_Willesborough.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Oenanthe_aquatica_reedbed.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Oenanthe_aquatica_sl18.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Oenanthe_aquatica_sl29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Oenothera_biennis",
    "rusName": "Ослинник двулетний",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Oenothera_biennis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Illustration_Oenothera_biennis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Nachtkerze_mit_Samenst%C3%A4nden_und_Bl%C3%BCten.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Oenothera_biennis%2C_Vic-la-Gardiole_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Oenothera_biennis_ENBLA02.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/0/00/Oenothera_biennis_Rosette.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Oenothera_rubricaulis",
    "rusName": "Ослинник красностебельный",
    "family": "Onagraceae",
    "url": "https://ru.wikipedia.org/wiki/Oenothera_rubricaulis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Oenothera_Niemir%C3%B3w_nad_Bugiem2_pl.jpg/231px-Oenothera_Niemir%C3%B3w_nad_Bugiem2_pl.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Omalotheca_sylvatica",
    "rusName": "Сушеница лесная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Omalotheca_sylvatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Gnaphalium_sylvaticum2010_07_25a.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Onobrychis_arenaria",
    "rusName": "Эспарцет песчаный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Onobrychis_arenaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Onobrychis_arenaria_02.JPG/275px-Onobrychis_arenaria_02.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/20040403_30sant_Latvia_Postage_Stamp.jpg/220px-20040403_30sant_Latvia_Postage_Stamp.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Onobrychis_viciifolia",
    "rusName": "Эспарцет виколистный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Onobrychis_viciifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Onobrychis_Viciifolia_in_Behbahan.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Onobrychis_viciifolia_Inflorescence_11April2009_CampoCalatrava.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Onobrychis_viciifolia_MichaD.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Sainfoin_issyk_kul.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ononis_arvensis",
    "rusName": "Стальник полевой",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Ononis_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f3/Ononis_arvensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Rest_Harrow_%28Oronis_arvensis%29_-_William_Catto_-_ABDAG016034.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья отваренные в соленой воде"
  },
  {
    "latName": "Onopordum_acanthium",
    "rusName": "Татарник колючий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Onopordum_acanthium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Cotton_Thistle%2C_Galong%2C_New_South_Wales.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Eselsdistel.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/O._acanthium-Abro%C3%B1igal-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Onopordum_acanthium-7-08-04.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Onopordum_acanthium.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодых побегов и листьев"
  },
  {
    "latName": "Ophioglossum_vulgatum",
    "rusName": "Ужовник обыкновенный",
    "family": "Ophioglossaceae",
    "url": "https://ru.wikipedia.org/wiki/Ophioglossum_vulgatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Ophioglossum_in_sand-dunes.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Ophioglossum_vulgatum_Saarland_01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ophrys_insectifera",
    "rusName": "Офрис насекомоносная",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Ophrys_insectifera",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Ophrys_insectifera_-_Niitv%C3%A4lja2-crop.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Ophrys_insectifera_Saarland_05.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Ophrys_insectifera_habit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/55/Ophrys_insectifera_infructescence_-_Niitv%C3%A4lja_bog.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Orchis_mascula",
    "rusName": "Ятрышник мужской",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Orchis_mascula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/05/Orchidaceae_-_Orchis_mascula-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Orchidaceae_-_Orchis_mascula-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Orchis_langei2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/df/Orchis_mascula.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Orchis_mascula_Saarland_18.04.2011_-_046.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Orchis_mascula_pinetorum_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Orchis_mascula_subsp._speciosa_030710a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Orchis_m%C3%A2le_%28Orchis_mascula%29_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Orchis_m%C3%A2le_%28Orchis_mascula%29_02.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Клубни"
  },
  {
    "latName": "Orchis_ustulata",
    "rusName": "Неотинея обожжённая",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Orchis_ustulata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Orchis_ustulata_Saarland_18.04.2011_-_065.jpg/270px-Orchis_ustulata_Saarland_18.04.2011_-_065.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Sturm04011_clean.jpg/220px-Sturm04011_clean.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Orchis_ustulata_wiki_mg-k02.jpg/220px-Orchis_ustulata_wiki_mg-k02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Origanum_vulgare",
    "rusName": "Душица обыкновенная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Origanum_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Origanum_vulgare_-_harilik_pune.jpg/266px-Origanum_vulgare_-_harilik_pune.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Origanum_vulgare1_ies.jpg/180px-Origanum_vulgare1_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Origanum_vulgare3_ies.jpg/180px-Origanum_vulgare3_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Origanum_vulgare_05_ies.jpg/180px-Origanum_vulgare_05_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Origanum_vulgare_08_ies.jpg/180px-Origanum_vulgare_08_ies.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Пряность, чай"
  },
  {
    "latName": "Ornithogalum_umbellatum",
    "rusName": "Птицемлечник зонтичный",
    "family": "Hyacinthaceae",
    "url": "https://ru.wikipedia.org/wiki/Ornithogalum_umbellatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2a/Leda_and_the_Swan_1508-1515.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Leonardo_botanical_study.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/57/ORNITHOGALUM_UMBELLATUM_-_TOR%C3%80_-_IB-915_%28Llet_de_gallina%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/da/Ornithogalum_umbellatum1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Ornithogalum_umbellatum_%28Dame_de_onze_heures%29_01.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Ornithogalum_umbellatum_003.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Orobanche_pallidiflora",
    "rusName": "Заразиха бледноцветковая",
    "family": "Orobanchaceae",
    "url": "https://ru.wikipedia.org/wiki/Orobanche_pallidiflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Orobanche_pallidiflora_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v20.jpg/275px-Orobanche_pallidiflora_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v20.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Orthilia_secunda",
    "rusName": "Ортилия однобокая",
    "family": "Pyrolaceae",
    "url": "https://ru.wikipedia.org/wiki/Orthilia_secunda",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/OrthiliaSecunda2.jpg/275px-OrthiliaSecunda2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Ericaceae_spp_Sturm50.jpg/200px-Ericaceae_spp_Sturm50.jpg",
      "https://upload.wikimedia.org/wikipedia/ru/thumb/6/63/Orthilia-secunda1.jpg/180px-Orthilia-secunda1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "семена, надземная часть - чай"
  },
  {
    "latName": "Osmunda_regalis",
    "rusName": "Чистоуст величавый",
    "family": "Osmundaceae",
    "url": "https://ru.wikipedia.org/wiki/Osmunda_regalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/32/OsmundaRegalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Royal_fern_closeup.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Oxalis_acetosella",
    "rusName": "Кислица обыкновенная",
    "family": "Oxalidaceae",
    "url": "https://ru.wikipedia.org/wiki/Oxalis_acetosella",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Oxalis_acetosella_1885_crop.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Oxalis_acetosella_LC0190.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Oxalis_sp._leaves_-_20020616.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "в больших количествах слегка ядовитое. листья"
  },
  {
    "latName": "Oxycoccus_microcarpus",
    "rusName": "Клюква мелкоплодная",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Oxycoccus_microcarpus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/20121015-FS-UNK-0009_%288090982330%29.jpg/275px-20121015-FS-UNK-0009_%288090982330%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Vaccinium_microcarpum_%28Turcz._ex_Rupr.%29_Schmalh._%28Rifugio_Falck%29_2.jpg/220px-Vaccinium_microcarpum_%28Turcz._ex_Rupr.%29_Schmalh._%28Rifugio_Falck%29_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Vaccinium_microcarpum_71332008.jpg/220px-Vaccinium_microcarpum_71332008.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Oxycoccus_palustris",
    "rusName": "Клюква",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Oxycoccus_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/%D0%A1ranberries_Mogilev_Belarus.jpg/275px-%D0%A1ranberries_Mogilev_Belarus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Cranberrymap.jpg/275px-Cranberrymap.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Vaccinum_oxycoccos_120604.jpg/220px-Vaccinum_oxycoccos_120604.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/%D0%AF%D0%B3%D0%BE%D0%B4%D1%8B_%D0%BA%D0%BB%D1%8E%D0%BA%D0%B2%D1%8B_%D0%B4%D0%B8%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D0%BE%D0%BC_16_%D0%BC%D0%BC.JPG/220px-%D0%AF%D0%B3%D0%BE%D0%B4%D1%8B_%D0%BA%D0%BB%D1%8E%D0%BA%D0%B2%D1%8B_%D0%B4%D0%B8%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D0%BE%D0%BC_16_%D0%BC%D0%BC.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Cranberries.jpg/220px-Cranberries.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Cranberrys_beim_Ernten.jpeg/220px-Cranberrys_beim_Ernten.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Canneberge_vosges_fragment.jpg/120px-Canneberge_vosges_fragment.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Ягоды, листья - чай"
  },
  {
    "latName": "Oxytropis_pilosa",
    "rusName": "Остролодочник волосистый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Oxytropis_pilosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Crotalaria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Oxytropis_pilosa_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Padus_avium",
    "rusName": "Черёмуха обыкновенная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Padus_avium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Baum_M%C3%A4lardalen.jpg/275px-Baum_M%C3%A4lardalen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Flowering_bird_cherry._Buryatia%2C_Russia.jpg/220px-Flowering_bird_cherry._Buryatia%2C_Russia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/312_Prunus_padus.jpg/220px-312_Prunus_padus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Prunus_padus20090613_095.jpg/150px-Prunus_padus20090613_095.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Ab_plant_1234.jpg/220px-Ab_plant_1234.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/%D0%9F%D0%B5%D1%82%D1%80%D0%BE%D0%B2-%D0%92%D0%BE%D0%B4%D0%BA%D0%B8%D0%BD_-_%D0%A7%D0%B5%D1%80%D1%91%D0%BC%D1%83%D1%85%D0%B0_%D0%B2_%D1%81%D1%82%D0%B0%D0%BA%D0%B0%D0%BD%D0%B5_%281932%29.jpg/220px-%D0%9F%D0%B5%D1%82%D1%80%D0%BE%D0%B2-%D0%92%D0%BE%D0%B4%D0%BA%D0%B8%D0%BD_-_%D0%A7%D0%B5%D1%80%D1%91%D0%BC%D1%83%D1%85%D0%B0_%D0%B2_%D1%81%D1%82%D0%B0%D0%BA%D0%B0%D0%BD%D0%B5_%281932%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/58/Stamp_of_Moldova_md504.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/%D0%A7%D1%91%D1%80%D0%BD%D0%B0%D1%8F_%D0%BC%D1%83%D1%85%D0%B0_%28Bibio_marci%29_%D0%BD%D0%B0_%D1%86%D0%B2%D0%B5%D1%82%D0%BA%D0%B0%D1%85_%D0%A7%D0%B5%D1%80%D1%91%D0%BC%D1%83%D1%85%D0%B8_%D0%BE%D0%B1%D1%8B%D0%BA%D0%BD%D0%BE%D0%B2%D0%B5%D0%BD%D0%BD%D0%BE%D0%B9_%28Prunus_padus%29.jpg/205px-%D0%A7%D1%91%D1%80%D0%BD%D0%B0%D1%8F_%D0%BC%D1%83%D1%85%D0%B0_%28Bibio_marci%29_%D0%BD%D0%B0_%D1%86%D0%B2%D0%B5%D1%82%D0%BA%D0%B0%D1%85_%D0%A7%D0%B5%D1%80%D1%91%D0%BC%D1%83%D1%85%D0%B8_%D0%BE%D0%B1%D1%8B%D0%BA%D0%BD%D0%BE%D0%B2%D0%B5%D0%BD%D0%BD%D0%BE%D0%B9_%28Prunus_padus%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Gemma_of_Prunus_padus.jpg/155px-Gemma_of_Prunus_padus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Prunus_padus_leaf_fall_colour.jpg/260px-Prunus_padus_leaf_fall_colour.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Prunus_padus_Tuomi_marjoja_VII_04_2989_C.JPG/165px-Prunus_padus_Tuomi_marjoja_VII_04_2989_C.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Prunus_padus_crosssection.jpg/168px-Prunus_padus_crosssection.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Padus_maackii",
    "rusName": "Черёмуха Маака",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Padus_maackii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Prunus_maackii_01_by_Line1.jpg/275px-Prunus_maackii_01_by_Line1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Prunus-mackii-flower.JPG/250px-Prunus-mackii-flower.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Padus_mahaleb",
    "rusName": "Антипка",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Padus_mahaleb",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Flors_de_prunus_mahaleb.JPG/275px-Flors_de_prunus_mahaleb.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Prunus_mahaleb_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v14.jpg/220px-Prunus_mahaleb_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v14.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/%D0%A7%D0%B5%D1%80%D0%B5%D0%BC%D1%83%D1%85%D0%B0_%D0%90%D0%BD%D1%82%D0%B8%D0%BF%D0%BA%D0%B0_%D0%B1%D0%BE%D0%BD%D1%81%D0%B0%D0%B9_%D0%B2_2019_%D0%B3%D0%BE%D0%B4%D1%83.jpg/220px-%D0%A7%D0%B5%D1%80%D0%B5%D0%BC%D1%83%D1%85%D0%B0_%D0%90%D0%BD%D1%82%D0%B8%D0%BF%D0%BA%D0%B0_%D0%B1%D0%BE%D0%BD%D1%81%D0%B0%D0%B9_%D0%B2_2019_%D0%B3%D0%BE%D0%B4%D1%83.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды, очищенные косточки"
  },
  {
    "latName": "Padus_pensylvanica",
    "rusName": "Черёмуха пенсильванская",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Padus_pensylvanica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Prunus_pensylvanica_UGA1213079.jpg/275px-Prunus_pensylvanica_UGA1213079.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Prunus_pensylvanica_Cleaned.jpg/220px-Prunus_pensylvanica_Cleaned.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Padus_serotina",
    "rusName": "Черёмуха поздняя",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Padus_serotina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Amerikaanse_vogelkers_Prunus_serotina_closeup.jpg/275px-Amerikaanse_vogelkers_Prunus_serotina_closeup.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Amerikaanse_vogelkers_bloeitros_Prunus_serotina.jpg/220px-Amerikaanse_vogelkers_bloeitros_Prunus_serotina.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Padus_virginiana",
    "rusName": "Черёмуха виргинская",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Padus_virginiana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/2014-06-13_14_48_01_Choke_Cherries_blooming_along_Lamoille_Canyon_Road_in_Lamoille_Canyon%2C_Nevada.JPG/275px-2014-06-13_14_48_01_Choke_Cherries_blooming_along_Lamoille_Canyon_Road_in_Lamoille_Canyon%2C_Nevada.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Prunus_virginiana_%285069721122%29.jpg/200px-Prunus_virginiana_%285069721122%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Prunus_virginiana0.jpg/200px-Prunus_virginiana0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Prunus_virginiana_leaves_%26_fruit_1.jpg/200px-Prunus_virginiana_leaves_%26_fruit_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Prunus_virginiana.jpg/225px-Prunus_virginiana.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Panicum_miliaceum",
    "rusName": "Просо обыкновенное",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Panicum_miliaceum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Panicum_miliaceum.JPG/275px-Panicum_miliaceum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Illustration_Panicum_miliaceum_and_Echinochloa_crus-galli0.jpg/220px-Illustration_Panicum_miliaceum_and_Echinochloa_crus-galli0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Millet.jpg/220px-Millet.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Пшено"
  },
  {
    "latName": "Papaver_argemone",
    "rusName": "Мак аргемона",
    "family": "Papaveraceae",
    "url": "https://ru.wikipedia.org/wiki/Papaver_argemone",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Papaver_argemone_1.jpg/275px-Papaver_argemone_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Papaver_argemone_MHNT.BOT.2016.24.41.jpg/270px-Papaver_argemone_MHNT.BOT.2016.24.41.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/Illustration_Papaver_argemone0.jpg/220px-Illustration_Papaver_argemone0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Papaver_dubium",
    "rusName": "Мак сомнительный",
    "family": "Papaveraceae",
    "url": "https://ru.wikipedia.org/wiki/Papaver_dubium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/28/Bleke_klaproos_bloem_Papaver_dubium.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Papaver_dubium1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Papaver_dubium_and_radicatum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Papaver_dubium_capsules.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Papaver_rhoeas",
    "rusName": "Мак самосейка",
    "family": "Papaveraceae",
    "url": "https://ru.wikipedia.org/wiki/Papaver_rhoeas",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/00MoinaMichael.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Claude_Monet_-_L%27%C3%A9t%C3%A9_-_Champ_de_coquelicots.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Papaver_rhoeas_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-101.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/NZRSA_remembrance_poppy.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Papaver_rhoeas_capsules.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья, молодые весенние побеги (x)."
  },
  {
    "latName": "Papaver_somniferum",
    "rusName": "Мак снотворный",
    "family": "Papaveraceae",
    "url": "https://ru.wikipedia.org/wiki/Papaver_somniferum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/3mohn_z01.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Cesky-modry-mak_detail.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Coquelicots_-_Parc_floral_6.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Opium_pod_cut_to_demonstrate_fluid_extraction1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Paris_quadrifolia",
    "rusName": "Вороний глаз четырёхлистный",
    "family": "Trilliaceae",
    "url": "https://ru.wikipedia.org/wiki/Paris_quadrifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/20170922-paris_quadrifolia-einbeere.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/88/Illustration_Paris_quadrifolia0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Little_Sorn_and_Cessnock_Water.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Little_Sorn_burnside.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Paris_quadrifolia.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c9/Paris_quadrifolia_flower_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/89/Paris_quadrifolia_fruit_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Paris_quadrifolia_mont-saint-pere_02_21042007_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Parnassia_palustris",
    "rusName": "Белозор болотный",
    "family": "Parnassiaceae",
    "url": "https://ru.wikipedia.org/wiki/Parnassia_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Parnassia_palustris_%28Mount_Ontake_s2%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Parnassia_palustris_-_Niitv%C3%A4lja_bog.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Parthenocissus_quinquefolia",
    "rusName": "Девичий виноград пятилисточковый",
    "family": "Vitaceae",
    "url": "https://ru.wikipedia.org/wiki/Parthenocissus_quinquefolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/34/6370VirginiaCreeper.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5f/Bad_D%C3%BCrkheim_Schlossgartenstra%C3%9Fe_2_001_2020_10_20.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Cissus_sicyoides.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Kisus.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Marienthal_%28Ahr%29_-_Ruine_der_Klosterkirche.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Parthenocissus_quinquefolia%2C_hegwortels%2C_Manie_van_der_Schijff_BT%2C_a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Parthenocissus_quinquefolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Parthenocissus_quinquefolia20100610_356.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Parthenocissus_quinquefolia20110704_003.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pedicularis_kaufmannii",
    "rusName": "Мытник Кауфмана",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Pedicularis_kaufmannii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Pedicularis_kaufmannii_Pinzger_-_Tula_obl.%2C_June_2016.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pedicularis_palustris",
    "rusName": "Мытник болотный",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Pedicularis_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/05/PedicularisPalustris.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pedicularis_sceptrum-carolinum",
    "rusName": "Мытник царский скипетр",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Pedicularis_sceptrum-carolinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/30/Pedicularis_sceptrum-carolinum_-_Kuninga-kuuskjalg_Niitv%C3%A4lja2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pedicularis_sylvatica",
    "rusName": "Мытник лесной",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Pedicularis_sylvatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Pedicularis_sylvatica_Sturm54.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Waldl%C3%A4usekraut_%28Pedicularis_sylvatica%29%4004.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Peplis_portula",
    "rusName": "Бутерлак обыкновенный",
    "family": "Lythraceae",
    "url": "https://ru.wikipedia.org/wiki/Peplis_portula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/LythrumPortula3.jpg/275px-LythrumPortula3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Persicaria_amphibia",
    "rusName": "Горец земноводный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Persicaria_amphibia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Persicaria_amphibia-01_%28xndr%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3a/Persicaria_amphibia-emersa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Persicaria_amphibia-emersa_short.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Persicaria_amphibia-stipulacea_terrestrial.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Persicaria_amphibia_sl1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/48/Persicaria_amphibia_stipulacea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Polygonum-coccineum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Persicaria_hydropiper",
    "rusName": "Горец перечный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Persicaria_hydropiper",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/356_Polygonum_tomentosum%2C_Polygonum_hydropiper.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/PersicariaHydropiper.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Persicaria_hydropiper.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Persicaria_hydropiper_leaves.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Polygonum_hydropiper1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/47/Polygonum_hydropiper2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Polygonum_hydropiper3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Polygonum_hydropiper_%281832%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Надземную часть и измельчённые семена - Приправа"
  },
  {
    "latName": "Persicaria_lapathifolia",
    "rusName": "Горец развесистый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Persicaria_lapathifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Ampfer-Kn%C3%B6terich_%28Persicaria_lapathifolia%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Polygonum_lapathifolium3.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Persicaria_minor",
    "rusName": "Горец малый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Persicaria_minor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Persicaria_minor_W.jpg/275px-Persicaria_minor_W.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Petasites_hybridus",
    "rusName": "Белокопытник гибридный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Petasites_hybridus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Petasites_hybridus_leaf.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Peucedanum_cervaria",
    "rusName": "Горичник олений",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Peucedanum_cervaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Apiaceae_-_Peucedanum_cervaria-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/Apiaceae_-_Peucedanum_cervaria-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Apiaceae_-_Peucedanum_cervaria-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/Apiaceae_-_Peucedanum_cervaria.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Peucedanum_cervaria_Sturm27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/30/Peucedanum_cervaria_eF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/QALace2675.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Peucedanum_ostruthium",
    "rusName": "Горичник настурциевый",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Peucedanum_ostruthium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/06/Peucedanum_ostruthium001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Peucedanum_ostruthium_MHNT.BOT.2009.17.2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Peucedanum_palustre",
    "rusName": "Горичник болотный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Peucedanum_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c9/Peucedanum_palustre_bluete.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phacelia_tanacetifolia",
    "rusName": "Фацелия пижмолистная",
    "family": "Hydrophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Phacelia_tanacetifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Ipomoea_tricolor-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Phacelia_tanacetifolia_7738.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Phacelia_tanacetifolia_7864.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Phacelia_tanacetifolia_MHNT.BOT.2017.10.27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Phacelia_tanacetifolia_RHu_001.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phalacroloma_annuum",
    "rusName": "Мелколепестник однолетний",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Phalacroloma_annuum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Erigeron_annuus_flowers1.jpg/275px-Erigeron_annuus_flowers1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phalacroloma_septentrionale",
    "rusName": "Мелколепестник однолетний",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Phalacroloma_septentrionale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Erigeron_annuus_flowers1.jpg/275px-Erigeron_annuus_flowers1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phalaroides_arundinacea",
    "rusName": "Канареечник тростниковидный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Phalaroides_arundinacea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Phalaris.arundinacea.jpg/265px-Phalaris.arundinacea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/473_Typhoides_arundinacea.jpg/220px-473_Typhoides_arundinacea.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phegopteris_connectilis",
    "rusName": "Фегоптерис связывающий",
    "family": "Thelypteridaceae",
    "url": "https://ru.wikipedia.org/wiki/Phegopteris_connectilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/72/Phegopteris_connectilis_AT.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Silver-fern.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phellodendron_amurense",
    "rusName": "Бархат амурский",
    "family": "Rutaceae",
    "url": "https://ru.wikipedia.org/wiki/Phellodendron_amurense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Kanzou2012.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Phellodendron_amurense0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/Phellodendron_amurense_%2812%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Phellodendron_amurense_%2815%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Phellodendron_amurense_%2818%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Phellodendron_amurense_Morton.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Phellodendron_amurense_seeds%2C_by_Omar_Hoftun.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phleum_phleoides",
    "rusName": "Тимофеевка степная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Phleum_phleoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/04/Phleum_phleoides_-_Berlin_Botanical_Garden_-_IMG_8578.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phleum_pratense",
    "rusName": "Тимофеевка луговая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Phleum_pratense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Phleum_pratense_2.jpg/271px-Phleum_pratense_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Illustration_Phleum_pratense0.jpg/220px-Illustration_Phleum_pratense0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Phleum_pratense_%283938664645%29.jpg/240px-Phleum_pratense_%283938664645%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Phleum_pratense_%283882501733%29.jpg/240px-Phleum_pratense_%283882501733%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Phleum.pratense.-.lindsey.jpg/240px-Phleum.pratense.-.lindsey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Phleum.pratense.jpg/240px-Phleum.pratense.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Timothy_awns.jpg/240px-Timothy_awns.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Timothy_stamen_and_stigma.jpg/240px-Timothy_stamen_and_stigma.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Timothy_seeds.jpg/240px-Timothy_seeds.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phragmites_australis",
    "rusName": "Тростник обыкновенный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Phragmites_australis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Phragmites_australis_-_NASA_Tracks_an_Environmental_Invader_%2848049936657%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Schwielowsee-Schilfrohrguertel-01.jpg/413px-Schwielowsee-Schilfrohrguertel-01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Phragmites_australis_rhizome_kz.jpg/465px-Phragmites_australis_rhizome_kz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Riet_ligula_Phragmites_australis.jpg/353px-Riet_ligula_Phragmites_australis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Phragmites_australis_blossom.jpg/413px-Phragmites_australis_blossom.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Illustration_Phragmites_australis0.jpg/330px-Illustration_Phragmites_australis0.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые, ещё не развернувшиеся побеги. корневища - мука"
  },
  {
    "latName": "Physalis_alkekengi",
    "rusName": "Физалис обыкновенный",
    "family": "Solanaceae",
    "url": "https://ru.wikipedia.org/wiki/Physalis_alkekengi",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Illustration_Physalis_alkekengi0_clean.jpg/235px-Illustration_Physalis_alkekengi0_clean.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Physalis_alkekengi_var._franchetii20130606_12.jpg/220px-Physalis_alkekengi_var._franchetii20130606_12.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Wiener_Dioskurides%2C_fol._359_verso.jpg/220px-Wiener_Dioskurides%2C_fol._359_verso.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Physalis_alkekengi_franchetii_0.0_R.jpg/120px-Physalis_alkekengi_franchetii_0.0_R.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Physalis_alkekengi_R0015041.JPG/120px-Physalis_alkekengi_R0015041.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/3849_-_Physalis_alkekengi_%28Wilde_Blasenkirsche%29.JPG/120px-3849_-_Physalis_alkekengi_%28Wilde_Blasenkirsche%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/PhysalisAlkekengi-balloon.jpg/120px-PhysalisAlkekengi-balloon.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Physocarpus_opulifolius",
    "rusName": "Пузыреплодник калинолистный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Physocarpus_opulifolius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Physocarpus_opulifolius.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Physocarpus_opulifolius_Arkansas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phyteuma_nigrum",
    "rusName": "Кольник чёрный",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Phyteuma_nigrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Schwarze_Teufelskralle.jpg/275px-Schwarze_Teufelskralle.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Phyteuma_spicatum",
    "rusName": "Кольник колосистый",
    "family": "Campanulaceae",
    "url": "https://ru.wikipedia.org/wiki/Phyteuma_spicatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Campanula_cespitosa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Phyteuma_spicata_280504.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Phyteuma_spicata_young_flowers_cooking.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Picea_abies",
    "rusName": "Ель обыкновенная",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Picea_abies",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a3/Fichtenkultur.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Kuusk_Keila-Paldiski_rdt_%C3%A4%C3%A4res.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Picea_abies_%27Inversa%27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Picea_abies_%27Virgata%27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Picea_abies_cone.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Picea_obovata",
    "rusName": "Ель сибирская",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Picea_obovata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e5/Picea_obovata_Urals1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Picea_pungens",
    "rusName": "Ель голубая",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Picea_pungens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Picea_pungens_Bryce_Canyon_NP_2.jpg/268px-Picea_pungens_Bryce_Canyon_NP_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/P%C4%ABcea_p%C5%ABngens.jpg/220px-P%C4%ABcea_p%C5%ABngens.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/2015-03-27_16_02_31_Colorado_Spruce_with_trunk_sprouts_at_Great_Basin_College_in_Elko%2C_Nevada.JPG/300px-2015-03-27_16_02_31_Colorado_Spruce_with_trunk_sprouts_at_Great_Basin_College_in_Elko%2C_Nevada.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Picea_pungens1.jpg/180px-Picea_pungens1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Picea_pungens2.jpg/180px-Picea_pungens2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Gmii.jpg/270px-Gmii.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/%D0%A2%D0%B0%D0%B1%D0%BB%D0%B8%D1%87%D0%BA%D0%B0_%C2%AB%D0%95%D0%BB%D1%8C_%D0%BA%D0%BE%D0%BB%D1%8E%D1%87%D0%B0%D1%8F%C2%BB_%D0%BD%D0%B0_%D1%83%D1%87%D0%B0%D1%81%D1%82%D0%BA%D0%B5_51_%D0%91%D0%B8%D1%80%D1%8E%D0%BB%D1%91%D0%B2%D1%81%D0%BA%D0%BE%D0%B3%D0%BE_%D0%B4%D0%B5%D0%BD%D0%B4%D1%80%D0%B0%D1%80%D0%B8%D1%8F.jpg/220px-%D0%A2%D0%B0%D0%B1%D0%BB%D0%B8%D1%87%D0%BA%D0%B0_%C2%AB%D0%95%D0%BB%D1%8C_%D0%BA%D0%BE%D0%BB%D1%8E%D1%87%D0%B0%D1%8F%C2%BB_%D0%BD%D0%B0_%D1%83%D1%87%D0%B0%D1%81%D1%82%D0%BA%D0%B5_51_%D0%91%D0%B8%D1%80%D1%8E%D0%BB%D1%91%D0%B2%D1%81%D0%BA%D0%BE%D0%B3%D0%BE_%D0%B4%D0%B5%D0%BD%D0%B4%D1%80%D0%B0%D1%80%D0%B8%D1%8F.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Picris_hieracioides",
    "rusName": "Горлюха ястребинковая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Picris_hieracioides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Chicory_flower_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Illustration_Picris_hieracioides0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Picris_rigida",
    "rusName": "Горлюха ястребинковая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Picris_rigida",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Picris_hieracioides_PID1775-3.jpg/275px-Picris_hieracioides_PID1775-3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Illustration_Picris_hieracioides0.jpg/220px-Illustration_Picris_hieracioides0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pilosella_officinarum",
    "rusName": "Ястребиночка обыкновенная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Pilosella_officinarum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Hieracium_pilosella_plant.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/P3038214_Pilosella_officinarum_-_Mouse-ear_Hawkweed.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pilosella_vaillantii",
    "rusName": "Ястребиночка зонтиконосная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Pilosella_vaillantii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Pilosella_cymosa_vaillantii_79076594.jpg/275px-Pilosella_cymosa_vaillantii_79076594.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Pilosella_cymosa_vaillantii_79076609.jpg/220px-Pilosella_cymosa_vaillantii_79076609.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pimpinella_major",
    "rusName": "Бедренец большой",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Pimpinella_major",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Apiaceae_-_Pimpinella_major-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Apiaceae_-_Pimpinella_major-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Apiaceae_-_Pimpinella_major-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4b/Apiaceae_-_Pimpinella_major.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Pimpinella_major_Herbar.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Pimpinella_major_Sturm11.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Pimpinella_major_ssp_rubra_eF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/QALace2675.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pimpinella_saxifraga",
    "rusName": "Бедренец камнеломковый",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Pimpinella_saxifraga",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Pimpinella_saxifraga_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-241.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/QALace2675.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья, корни, семена"
  },
  {
    "latName": "Pinguicula_vulgaris",
    "rusName": "Жирянка обыкновенная",
    "family": "Lentibulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Pinguicula_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Alpina_flower.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Pinguicula_vulgaris_-_Iceland_-_2007-07-08.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Pinguicula_vulgaris_LC0331.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Pinguicula_vulgaris_flower_%28side_view%29_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pinus_banksiana",
    "rusName": "Сосна Банкса",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Pinus_banksiana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/2015-09-23_4E-D2_pig_naturel_DDumais.jpg/275px-2015-09-23_4E-D2_pig_naturel_DDumais.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Jackpine.jpg/220px-Jackpine.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Pinus_banksiana_closed_cones.jpg/220px-Pinus_banksiana_closed_cones.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pinus_cembra",
    "rusName": "Сосна кедровая европейская",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Pinus_cembra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/df/Bark_of_an_young_Arolla_pine.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Fresh_Pinus_cembra_cone_in_June.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Pinoli_pino_cembro_sgusciati_e_da_sgusciare.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Pinus_cembra_cone_dried.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Pinus_cembra_cones_in_Gr%C3%B6den_crop.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Pinus_cembra_coupe_MHNT.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Pinus_cembra_seedling_planted_for_pine_nut_production.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pinus_nigra",
    "rusName": "Сосна чёрная",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Pinus_nigra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Austrian_Pine_Pinus_nigra_Bark_Closeup_2000px.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Crepuscular_rays_in_the_woods_of_Kasterlee%2C_Belgium.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Forest_in_Bulgaria_near_Dundukovo_dam.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Pin_laricio_Corse.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Pinus_nigra_MHNT.2022.4.3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pinus_rigida",
    "rusName": "Сосна жёсткая",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Pinus_rigida",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/2013-05-10_09_02_57_Pitch_Pine_new_growth_and_pollen_cones_along_the_Batona_Trail_in_Brendan_T._Byrne_State_Forest%2C_New_Jersey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/2013-05-12_11_23_41_Pitch_Pine_trees_and_view_west_from_the_Hoeferlin_Trail_in_Ramapo_Mountain_State_Forest_in_New_Jersey.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/2014-08-29_11_51_25_View_north-northeast_from_the_fire_tower_on_Apple_Pie_Hill_in_Wharton_State_Forest%2C_Tabernacle_Township%2C_New_Jersey.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/fd/Pinus_rigida.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Pinus_rigida_cone_Poland.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Pitch_pine_cones_exposed_to_fire.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pinus_sylvestris",
    "rusName": "Сосна обыкновенная",
    "family": "Pinaceae",
    "url": "https://ru.wikipedia.org/wiki/Pinus_sylvestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/45/Conif%C3%A8re_Dordogne.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Meenikunno_maastikukaiteala.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Pinar_Sierra_de_Guadarrama_2005-09-13.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Pinus-sylvestris-cone-2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e1/Pinus_Sylvestris_8407.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Pinus_sylvestris_2020_G2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Pinus_sylvestris_MHNT.BOT.2005.0.971.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/56/Pinus_sylvestris_Sturm01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Pinus_sylvestris_cones_pl.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Plantago_lanceolata",
    "rusName": "Подорожник ланцетолистный",
    "family": "Plantaginaceae",
    "url": "https://ru.wikipedia.org/wiki/Plantago_lanceolata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Galled_head_of_a_Plantain.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Ribwort_600.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Plantago_major",
    "rusName": "Подорожник большой",
    "family": "Plantaginaceae",
    "url": "https://ru.wikipedia.org/wiki/Plantago_major",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Broadleaf_Plantain_%28Plantago_major%29_growing_in_crack_in_sidewalk.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Grote_weegbree_bloeiwijze_Plantago_major_subsp._major.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Plantago_major_05_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Purple_Plantago_major.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья"
  },
  {
    "latName": "Plantago_media",
    "rusName": "Подорожник средний",
    "family": "Plantaginaceae",
    "url": "https://ru.wikipedia.org/wiki/Plantago_media",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/06/Keskmise_teelehe_%C3%B5isik_Plantago_media.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Plantago-media.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Platanthera_bifolia",
    "rusName": "Любка двулистная",
    "family": "Orchidaceae",
    "url": "https://ru.wikipedia.org/wiki/Platanthera_bifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Platanthera_bifolia_%28flower%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Клубни"
  },
  {
    "latName": "Pleurospermum_austriacum",
    "rusName": "Реброплодник австрийский",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Pleurospermum_austriacum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Pleurospermum_austriacum_eF.jpg/264px-Pleurospermum_austriacum_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_angustifolia",
    "rusName": "Мятлик узколистный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_angustifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Poa_angustifolia1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_annua",
    "rusName": "Мятлик однолетний",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_annua",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Poa.annua.2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4f/Poa.annua.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Poa_annua.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/7/77/Poa_annua.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Poa_annua_on_a_footpath._24_Oktoberplein%2C_Utrecht%2C_The_Netherlands%2C_May_20%2C_2018.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Straatgras_tongetje_%28Poa_annua_ligula%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_compressa",
    "rusName": "Мятлик сплюснутый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_compressa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Poa.compressa.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_crispa",
    "rusName": "Мятлик луковичный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_crispa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Poa_bulbosa_%283877047537%29.jpg/275px-Poa_bulbosa_%283877047537%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Poa_bulbosa_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v19.jpg/220px-Poa_bulbosa_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v19.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_nemoralis",
    "rusName": "Мятлик дубравный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_nemoralis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Poa.nemoralis.in.may.germany.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_palustris",
    "rusName": "Мятлик болотный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Poa_palustris.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_pratensis",
    "rusName": "Мятлик луговой",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_pratensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/92/PP_Na_Popovickem_kopci_007_cf_Myrmus_miriformis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Poa_pratensis_%283883809159%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4f/Poa_pratensis_%28Billeder_af_nordens_flora_1917ff.%2C_v2_0383%29_clean%2C_no-description.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Poa_pratensis_detail.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3a/Poa_pratensis_plant1_%287398746202%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/59/Poa_pratensis_seeds_20101113.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Poa_pratensis_sl12.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Poa_pratensis_sl19.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_remota",
    "rusName": "Мятлик расставленный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_remota",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Poa_remota.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_supina",
    "rusName": "Мятлик приземистый",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_supina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Poa_supina1.JPG/275px-Poa_supina1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Poa_trivialis",
    "rusName": "Мятлик обыкновенный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Poa_trivialis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Poa.trivialis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Poa_trivialis_Ruwbeembgras_bloeiwijze.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Ruwbeemdgras_Poa_trivialis_ligula.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polemonium_caeruleum",
    "rusName": "Синюха голубая",
    "family": "Polemoniaceae",
    "url": "https://ru.wikipedia.org/wiki/Polemonium_caeruleum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Jacob%27s_Ladder_or_Greek_valerian_%28Polemonium_caeruleum%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/Polemonium.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Polemonium_caeruleum_nf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Polemonium_caerulum_sk2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/Polemonium_coeruleum1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polycnemum_arvense",
    "rusName": "Хруплявник полевой",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Polycnemum_arvense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/Polycnemum_arvense_sl1.jpg/275px-Polycnemum_arvense_sl1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygala_amarella",
    "rusName": "Истод горьковатый",
    "family": "Polygalaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygala_amarella",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Polygala_amarella1a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Starr_080219-9010_Polygala_sp..jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygala_comosa",
    "rusName": "Истод хохлатый",
    "family": "Polygalaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygala_comosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Schopfige_Kreuzblume_in_%C3%96sterreich.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Starr_080219-9010_Polygala_sp..jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygala_vulgaris",
    "rusName": "Истод обыкновенный",
    "family": "Polygalaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygala_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Milkwort_blue.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Milkwort_mauve.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Polygala_vulgaris_290504.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Polygalaceae_-_Polygala_vulgaris-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Polygalaceae_-_Polygala_vulgaris-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Polygalaceae_-_Polygala_vulgaris-3.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Polygalaceae_-_Polygala_vulgaris-4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Polygalaceae_-_Polygala_vulgaris.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygonatum_multiflorum",
    "rusName": "Купена многоцветковая",
    "family": "Convallariaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygonatum_multiflorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Polygonatum_multiflorum_7789.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygonatum_odoratum",
    "rusName": "Купена аптечная",
    "family": "Convallariaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygonatum_odoratum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Angular_solomons-seal_Polygonatum_odoratum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d3/Dunggulle.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygonum_aviculare",
    "rusName": "Горец птичий",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygonum_aviculare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Polygonum_aviculare_4.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygonum_calcatum",
    "rusName": "Горец топотун",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygonum_calcatum",
    "images": [],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygonum_linicola",
    "rusName": "Горец развесистый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygonum_linicola",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Knopige_duizendknoop_closeup.jpg/275px-Knopige_duizendknoop_closeup.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Polygonum_lapathifolium_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg/220px-Polygonum_lapathifolium_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Polygonum_monspeliense",
    "rusName": "Горец птичий",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygonum_monspeliense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Polygonum_aviculare_4.JPG/266px-Polygonum_aviculare_4.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/357_Polygonum_heterophyllum.jpg/220px-357_Polygonum_heterophyllum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polygonum_patulum",
    "rusName": "Горец отклонённый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Polygonum_patulum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Muehlenbeckia_adpressa_-_Curtis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polypodium_vulgare",
    "rusName": "Многоножка обыкновенная",
    "family": "Polypodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Polypodium_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/42/Illustration_Polypodium_vulgare0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Polypodium_vulgare-NF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Polypodium_vulgare0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/de/Polypodium_vulgare_jfg.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Sycamore_and_epiphytic_ferns.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polypogon_monspeliensis",
    "rusName": "Многобородник монпелиенский",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Polypogon_monspeliensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Polypogon_monspeliensis_3881578658.jpg/275px-Polypogon_monspeliensis_3881578658.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Polystichum_braunii",
    "rusName": "Многорядник Брауна",
    "family": "Dryopteridaceae",
    "url": "https://ru.wikipedia.org/wiki/Polystichum_braunii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Polystichum_braunii_1-OB8.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Populus_alba",
    "rusName": "Тополь белый",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Populus_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2a/Lisboa_June_2014-10.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Poplar-lined_road%2C_Khotan.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/PopulusAblaPyramidalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/Populus_alba_branch.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/Populus_alba_coupe_MHNT.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Populus_alba_leaf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Populus_alba_trunk.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Populus_balsamifera",
    "rusName": "Тополь бальзамический",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Populus_balsamifera",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/Populus_balsamifera.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Reykjav%C3%ADk_-_tree_of_the_year_2016.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Populus_deltoides",
    "rusName": "Тополь дельтовидный",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Populus_deltoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Bark_on_bole_of_mature_Eastern_Cottonwood.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Cottonwood_20090521_083459_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Peuplier_delto%C3%AFde_feuillage.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Populus_laurifolia",
    "rusName": "Тополь лавролистный",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Populus_laurifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e1/Populus_laurifolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Salix-catkins.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Populus_nigra",
    "rusName": "Тополь чёрный",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Populus_nigra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Burr_on_Black_poplar.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/aa/Poplar_seed_tufts_2009_G1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Populus_nigra-bekes.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Populus_nigra_kz1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Populus_tremula",
    "rusName": "Осина",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Populus_tremula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Arctic-Norway-aspen-Ofotfjord.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Aspen-leaves.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/PopulusTremula001.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potamogeton_alpinus",
    "rusName": "Рдест альпийский",
    "family": "Potamogetonaceae",
    "url": "https://ru.wikipedia.org/wiki/Potamogeton_alpinus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d9/Potamogeton_alpinus%2C_robust_form_%28the_River._Uftyuga%2C_Vologda_reg.%2C_Russia%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Potamogeton_alpinus.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Potamogeton_alpinus_shoot.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potamogeton_berchtoldii",
    "rusName": "Рдест Бертхольда",
    "family": "Potamogetonaceae",
    "url": "https://ru.wikipedia.org/wiki/Potamogeton_berchtoldii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Potamogeton_berchtoldii_garden_pond.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potamogeton_crispus",
    "rusName": "Рдест курчавый",
    "family": "Potamogetonaceae",
    "url": "https://ru.wikipedia.org/wiki/Potamogeton_crispus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/PotamogetonCrispus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Potamogeton_crispus.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/9/90/Potamogeton_crispus_Dorset.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Potamogeton_crispus_turion.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potamogeton_natans",
    "rusName": "Рдест плавающий",
    "family": "Potamogetonaceae",
    "url": "https://ru.wikipedia.org/wiki/Potamogeton_natans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Elodea_canadensis.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/5/50/PotamogetonNatans.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/PotamogetonNatans2.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Клубнеообразные утолщения на корневище"
  },
  {
    "latName": "Potamogeton_obtusifolius",
    "rusName": "Рдест туполистный",
    "family": "Potamogetonaceae",
    "url": "https://ru.wikipedia.org/wiki/Potamogeton_obtusifolius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b0/Potamogeton_obtusifolius_kz1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potamogeton_pectinatus",
    "rusName": "Рдест гребенчатый",
    "family": "Potamogetonaceae",
    "url": "https://ru.wikipedia.org/wiki/Potamogeton_pectinatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/PotamogetonPectinatus1.jpg/275px-PotamogetonPectinatus1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potentilla_alba",
    "rusName": "Лапчатка белая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Potentilla_alba_close-up.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potentilla_anserina",
    "rusName": "Лапчатка гусиная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_anserina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Zilverschoon_plant_Potentilla_anserina.jpg/275px-Zilverschoon_plant_Potentilla_anserina.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Illustration_Potentilla_anserina0.jpg/220px-Illustration_Potentilla_anserina0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Potentilla_anserina01.jpg/200px-Potentilla_anserina01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Potentilla_anserina_blad.jpg/200px-Potentilla_anserina_blad.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Silverweed_flower_800.jpg/200px-Silverweed_flower_800.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potentilla_argentea",
    "rusName": "Лапчатка серебристая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_argentea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Potentilla_argentea_%28s._lat.%29_sl6_%28without_scale%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Potentilla_argentea_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potentilla_erecta",
    "rusName": "Лапчатка прямостоячая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_erecta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Potentilla_erecta_%28root%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Корневища - пряность"
  },
  {
    "latName": "Potentilla_heptaphylla",
    "rusName": "Лапчатка семилисточковая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_heptaphylla",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Potentilla_heptaphylla_-_Vrem%C5%A1%C4%8Dica.jpg/275px-Potentilla_heptaphylla_-_Vrem%C5%A1%C4%8Dica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Potentilla_sp_Sturm28.jpg/220px-Potentilla_sp_Sturm28.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potentilla_intermedia",
    "rusName": "Лапчатка средняя",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_intermedia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Potentilla_intermedia_herbarium.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potentilla_norvegica",
    "rusName": "Лапчатка норвежская",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_norvegica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Potentilla_norvegica_%283818646878%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potentilla_recta",
    "rusName": "Лапчатка прямая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_recta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/5668-fingerkraut-r-020607.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d2/Cinquefoil_--_Potentilla_recta.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Cinquefoil_--_Potentilla_recta_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Potentilla_erecta0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Potentilla_recta_a1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Potentilla_supina",
    "rusName": "Лапчатка низкая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Potentilla_supina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c2/Potentilla_supina_sl12.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Primula_elatior",
    "rusName": "Первоцвет высокий",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Primula_elatior",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fa/Essex_Oxlip_-_geograph.org.uk_-_403865.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Primula_elatior_Prague_2012_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Vergleich-Primula-elatior-veris.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Primula_veris",
    "rusName": "Первоцвет весенний",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Primula_veris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Albrecht_D%C3%BCrer%2C_Tuft_of_Cowslips%2C_1526%2C_NGA_74162.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/42/Cowslips%2C_Dalgarven.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Primula_veris_230405.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d3/Primula_veris_MHNT.BOT.2011.3.11.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Primula_veris_flowers_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Red_flowered_cowslip.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и стебли"
  },
  {
    "latName": "Prunella_grandiflora",
    "rusName": "Черноголовка крупноцветковая",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Prunella_grandiflora",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Horminum_pyrenaicum_-_Saint-Hilaire.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/Praktbrun%C3%B6rt._Prunella_grandiflora-2982_-_Flickr_-_Ragnhild_%26_Neil_Crawford.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Prunella_vulgaris",
    "rusName": "Черноголовка обыкновенная",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Prunella_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Common_self-heal_%28Prunella_vulgaris%29_--_leaf.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Kleine_Braunelle%2C_Bl%C3%BCte.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Prunella_vulgaris-flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Prunella_vulgaris_-_harilik_k%C3%A4bihein.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Prunus_domestica",
    "rusName": "Слива домашняя",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Prunus_domestica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Fior_di_Prugno_%28bis%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/67/Fruits_Prunus_domestica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Greengages.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Mirabellen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Page_14_plum_-_Imperial_Gage%2C_Shropshire_Damson%2C_Lombard%2C_Maynard%2C_Yellow_Egg.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Pflaumen_SJ_Eda_20210820_165821.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e4/Prunus_domestica%2C_Agde_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Prunus_domestica_44077334.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Prunus_pumila",
    "rusName": "Слива карликовая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Prunus_pumila",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Prunus_pumila%2C_South_Ste._Marys_Island.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Prunus_pumila_%28Sand_Cherry%29_%2809246a38-f43e-4dea-9228-bfa42287be9f%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Prunus_pumila_2-eheep_%285097472729%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Prunus_pumila_4_%285097489635%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Prunus_pumila_var._depressa_kz01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Prunus_pumila_var_pumila%2C_June.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Prunus_spinosa",
    "rusName": "Тёрн",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Prunus_spinosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Blackthorn_in_blossom.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Closeup_of_blackthorn_aka_sloe_aka_prunus_spinosa_sweden_20050924.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Husband_and_wife_trees_-_Blackthorn.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Illustration_Prunus_spinosa1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Schlehe1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды. Листья как чай"
  },
  {
    "latName": "Psammophiliella_muralis",
    "rusName": "Песколюбочка постенная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Psammophiliella_muralis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Gypsophila_muralis.jpg/275px-Gypsophila_muralis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Gypsophila_serotina_Hayne_-_Flora_regni_Borussici_vol._3_-_t._215_%28clean%29.jpg/220px-Gypsophila_serotina_Hayne_-_Flora_regni_Borussici_vol._3_-_t._215_%28clean%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ptarmica_cartilaginea",
    "rusName": "Тысячелистник иволистный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Ptarmica_cartilaginea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Achillea_salicifolia.JPG/275px-Achillea_salicifolia.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Achillea_salicifolia_kz03.jpg/220px-Achillea_salicifolia_kz03.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ptarmica_vulgaris",
    "rusName": "Тысячелистник птармика",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Ptarmica_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Achillea_ptarmica_2005.07.10_09.17.42.jpg/210px-Achillea_ptarmica_2005.07.10_09.17.42.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Achillea_ptarmica_Sturm38.jpg/220px-Achillea_ptarmica_Sturm38.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Achillea_ptarmica_millefolium_040807.jpg/260px-Achillea_ptarmica_millefolium_040807.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Achillea_ptarmica_leaf_kz.jpg/205px-Achillea_ptarmica_leaf_kz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Achillea_ptarmica20140713_085.jpg/210px-Achillea_ptarmica20140713_085.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Achillea_ptarmica_millefolium_040807.jpg/200px-Achillea_ptarmica_millefolium_040807.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Achillea_ptarmica_flore_pleno_kz.jpg/205px-Achillea_ptarmica_flore_pleno_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pteridium_aquilinum",
    "rusName": "Орляк обыкновенный",
    "family": "Hypolepidaceae",
    "url": "https://ru.wikipedia.org/wiki/Pteridium_aquilinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Adelaarsvaren_planten_Pteridium_aquilinum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Dried_Eastern_brakenfern.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Gosari.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Pteridium_aquilinum_nf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Warabi_mochi_1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодые ещё не развернувшиеся листья и побеги. Корневища"
  },
  {
    "latName": "Puccinellia_distans",
    "rusName": "Бескильница расставленная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Puccinellia_distans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Puccinellia_distans.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pulicaria_vulgaris",
    "rusName": "Блошница обыкновенная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Pulicaria_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/PulicariaVulgaris2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pulmonaria_angustifolia",
    "rusName": "Медуница узколистная",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Pulmonaria_angustifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Pulmonaria_angustifolia0_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pulmonaria_mollis",
    "rusName": "Медуница мягкая",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Pulmonaria_mollis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Pulmonaria_mollis_01.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья ранней весной"
  },
  {
    "latName": "Pulmonaria_obscura",
    "rusName": "Медуница неясная",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Pulmonaria_obscura",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Pulmonaria_obscura0_eF.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Pulmonaria_officinalis",
    "rusName": "Медуница лекарственная",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Pulmonaria_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d2/Boraginaceae_-_Pulmonaria_officinalis-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Boraginaceae_-_Pulmonaria_officinalis-2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/82/Boraginaceae_-_Pulmonaria_officinalis.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/35/Illustration_Pulmonaria_officinalis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Pulmonaria_officinalis_5.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Pulmonaria_officinalis_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Pulmonaria_officinalis_Gevlekt_longkruid.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Pulsatilla_patens",
    "rusName": "Прострел раскрытый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Pulsatilla_patens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Pulsatilla_patens01%28js%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pulsatilla_pratensis",
    "rusName": "Прострел луговой",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Pulsatilla_pratensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/Anemone_pratensis_Sturm41.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Pulsatilla_pratensis_ssp_hungarica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Pulsatilla_pratensis_subsp._bohemica2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pycreus_flavescens",
    "rusName": "Ситовник желтоватый",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Pycreus_flavescens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Cyperus_flavescens_kz2.JPG/266px-Cyperus_flavescens_kz2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Illustration_Carex_pilulifera0.jpg/220px-Illustration_Carex_pilulifera0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pyrethrum_corymbosum",
    "rusName": "Пижма щитковая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Pyrethrum_corymbosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Tanacetum_corymbosum_1.jpg/275px-Tanacetum_corymbosum_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pyrola_chlorantha",
    "rusName": "Грушанка зелёноцветковая",
    "family": "Pyrolaceae",
    "url": "https://ru.wikipedia.org/wiki/Pyrola_chlorantha",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Persimmon_0375.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Pyrola_chlorantha_260507.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pyrola_media",
    "rusName": "Грушанка средняя",
    "family": "Pyrolaceae",
    "url": "https://ru.wikipedia.org/wiki/Pyrola_media",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Pyrola_media_RHu_01.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pyrola_minor",
    "rusName": "Грушанка малая",
    "family": "Pyrolaceae",
    "url": "https://ru.wikipedia.org/wiki/Pyrola_minor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Persimmon_0375.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Pyrola_minor_LC0230.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pyrola_rotundifolia",
    "rusName": "Грушанка круглолистная",
    "family": "Pyrolaceae",
    "url": "https://ru.wikipedia.org/wiki/Pyrola_rotundifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Illustration_Pyrola_rotundifolia0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Persimmon_0375.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Pyrus_communis",
    "rusName": "Груша обыкновенная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Pyrus_communis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/35/2015_Kwiatostan_gruszy_pospolitej.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Blake%27s_Pride_pears.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Clairgeau1.poupou.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Common_pear_tree_in_early_June.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Eight_varieties_of_pears.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/Guteluise1.poupou.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды. Поджаренные семена - суррогат кофе"
  },
  {
    "latName": "Quercus_petraea",
    "rusName": "Дуб скальный",
    "family": "Fagaceae",
    "url": "https://ru.wikipedia.org/wiki/Quercus_petraea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c6/Eglinton_fish_pond_island_inosculated_Q._petraea.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Quercus_petraea_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/89/Quercus_petraea_06.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Quercus_robur",
    "rusName": "Дуб черешчатый",
    "family": "Fagaceae",
    "url": "https://ru.wikipedia.org/wiki/Quercus_robur",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5e/Baginton_oak_tree_july06.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Holz_der_Stieleiche.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Juh%C3%A1sz_Gyula_memorial_tree.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Oak_bark.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Oaksprout.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/Q-robur-concordia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Quercus_robur.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Quercus_robur_-_sprouting_acorn.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Мука из желудей - как добавка к хлебу"
  },
  {
    "latName": "Quercus_rubra",
    "rusName": "Дуб красный",
    "family": "Fagaceae",
    "url": "https://ru.wikipedia.org/wiki/Quercus_rubra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/2014-10-30_10_49_37_Red_Oak_foliage_during_autumn_on_Farrell_Avenue_in_Ewing%2C_New_Jersey.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/51/A_large_northern_red_oak_tree_in_Glen_Abbey%2C_Oakville%2C_Ontario.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c9/Jakey_Hollow_Natural_Area_%28Revisit%29_%2827%29_%2817008356190%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/db/Quercus_rubra_%40_Tortworth_Court.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Quercus_rubra_IP0905004.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Quercus_rubra_with_arborist.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Quercusrubra.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Red_Oak_Quercus_Rubra_Bark_Vertical_High_DoF.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Radiola_linoides",
    "rusName": "Радиола льновидная",
    "family": "Linaceae",
    "url": "https://ru.wikipedia.org/wiki/Radiola_linoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/27/Zwerglein%28Radiola_linoides%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_acris",
    "rusName": "Лютик едкий",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_acris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Illustration_Ranunculus_acris0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_auricomus",
    "rusName": "Лютик золотистый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_auricomus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Ranunculus_auricomus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Ranunculus_auricomis.jpg/400px-Ranunculus_auricomis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Ranunculus_auricomus_Sturm52.jpg/330px-Ranunculus_auricomus_Sturm52.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_cassubicus",
    "rusName": "Лютик кашубский",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_cassubicus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Ranunculus_cassubicus1.jpg/275px-Ranunculus_cassubicus1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_flammula",
    "rusName": "Лютик жгучий",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_flammula",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/06/RanunculusFlammula.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/89/RanunculusFlammula4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Ranunculus_flammula.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Ranunculus_flammula_Prague_2012_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/13/Ranunculus_flammula_Sturm46.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_illyricus",
    "rusName": "Лютик иллирийский",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_illyricus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Ranunculus_illyricus_OB10.1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_lingua",
    "rusName": "Лютик длиннолистный",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_lingua",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/36/165_Ranunculus_lingua.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c9/Boterbloem_grote%2C_Zandweteringpark_Deventer.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_polyanthemos",
    "rusName": "Лютик многоцветковый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_polyanthemos",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Ranunculus_polyanthemoides_Herbar.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_repens",
    "rusName": "Лютик ползучий",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_repens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ranunculus_sceleratus",
    "rusName": "Лютик ядовитый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Ranunculus_sceleratus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Ranunculus_sceleratus_-_m%C3%BCrktulikas_Keilas.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Ranunculus_sceleratus_LC0079.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Raphanus_raphanistrum",
    "rusName": "Редька полевая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Raphanus_raphanistrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/63/%28MHNT%29_Raphanus_raphanistrum_-_Habitus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/%28MHNT%29_Raphanus_raphanistrum_-_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/49/Raphanus_raphanistrum-var-raphanistrum_velennes_80_10062008_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bb/Raphanus_raphanistrum_-_Flickr_-_Kevin_Thiele.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3c/Wild_Radish_in_Behbahan.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Raphanus_sativus",
    "rusName": "Редька посевная",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Raphanus_sativus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Rettichschwarzrund.jpg/275px-Rettichschwarzrund.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/210704_radieschen-raphanus-sativus-marktware_1-640x480.jpg/180px-210704_radieschen-raphanus-sativus-marktware_1-640x480.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/210704_rettich-raphanus-sativus-marktware_1-640x480.jpg/180px-210704_rettich-raphanus-sativus-marktware_1-640x480.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Root_z01_schwarzer_rettich.JPG/180px-Root_z01_schwarzer_rettich.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Raphanus_sativus_convar_lobo20100405_19.jpg/180px-Raphanus_sativus_convar_lobo20100405_19.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Bladrammenas_groenbemesting_Raphanus_sativus_subsp._oleiferus.jpg/180px-Bladrammenas_groenbemesting_Raphanus_sativus_subsp._oleiferus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Ravanello_fiore.jpg/180px-Ravanello_fiore.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Bladramenas_vruchten_Raphanus_sativus_subsp._oleiferus.jpg/180px-Bladramenas_vruchten_Raphanus_sativus_subsp._oleiferus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Radieschen.samen.jpg/148px-Radieschen.samen.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Raphanus_sativus_003.jpg/180px-Raphanus_sativus_003.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодая листва и корнеплоды"
  },
  {
    "latName": "Reseda_lutea",
    "rusName": "Резеда жёлтая",
    "family": "Resedaceae",
    "url": "https://ru.wikipedia.org/wiki/Reseda_lutea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/ResedaLutea-overz-kl.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4b/Reseda_lutea_MHNT.BOT.2011.3.17.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "стебли, корни и листья - приправа"
  },
  {
    "latName": "Reynoutria_japonica",
    "rusName": "Рейнутрия японская",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Reynoutria_japonica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Caersws_headg_row1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Fallopia_overgrows_train.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Japanese_knotweed.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Knotweed.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/Knotweed054.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Reynoutria_japonica_in_Brastad_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Variegated_Japanese_Knotweed_Foliage.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rhamnus_cathartica",
    "rusName": "Крушина слабительная",
    "family": "Rhamnaceae",
    "url": "https://ru.wikipedia.org/wiki/Rhamnus_cathartica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Rhamnus_cathartica_%288023754928%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Rhamnus_catharthica_tree_size.jpg/413px-Rhamnus_catharthica_tree_size.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_63%29_%287118329607%29.jpg/330px-K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_63%29_%287118329607%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Rhamnus_cathartica_5456085.jpg/330px-Rhamnus_cathartica_5456085.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rheum_rhabarbarum",
    "rusName": "Ревень волнистый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rheum_rhabarbarum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Rheum_undulatum_-_Bergianska_tr%C3%A4dg%C3%A5rden_-_Stockholm%2C_Sweden_-_DSC00564.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Rheum_undulatum_g1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e6/Rheum_undulatum_in_Jardin_botanique_de_la_Charme.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "черешки листьев"
  },
  {
    "latName": "Rhinanthus_minor",
    "rusName": "Погремок малый",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Rhinanthus_minor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/20170517Rhinanthus_minor6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/82/Rhinanthus_minor_MHNT.BOT.2004.0.396.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Traditional_pasture%2C_Melverley_Farm_-_geograph.org.uk_-_531782.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Yellow_rattle_plants_with_flowers_and_seed_capsules.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rhinanthus_serotinus",
    "rusName": "Погремок узколистный",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Rhinanthus_serotinus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f3/Rhinanthus_serotinus_sl8.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rhododendron_luteum",
    "rusName": "Рододендрон жёлтый",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Rhododendron_luteum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ef/Hachmann_Rhod_Goldtopas.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Rhododendron_luteum_%28flower%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Rhododendron_luteum_BotGardBln1105Autumn.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/Rhododendron_luteum_i_Oslo.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rhus_typhina",
    "rusName": "Сумах оленерогий",
    "family": "Anacardiaceae",
    "url": "https://ru.wikipedia.org/wiki/Rhus_typhina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/30/20150420Rhus_typhina1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rhynchospora_alba",
    "rusName": "Очеретник белый",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Rhynchospora_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Rhynchosporaalba.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ribes_alpinum",
    "rusName": "Смородина альпийская",
    "family": "Grossulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Ribes_alpinum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Myriophyllum_matogrossense.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Ribes_alpinum_-_berries_%28aka%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Ribes_alpinum_MN_2007.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Ribes_lucidum",
    "rusName": "Смородина альпийская",
    "family": "Grossulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Ribes_lucidum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Ribes_alpinum_-_berries_%28aka%29.jpg/275px-Ribes_alpinum_-_berries_%28aka%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/282_Ribes_alpinum.jpg/220px-282_Ribes_alpinum.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Ribes_nigrum",
    "rusName": "Смородина чёрная",
    "family": "Grossulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Ribes_nigrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Blackcurrant_1.jpg/275px-Blackcurrant_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/281_Ribes_nigrum.jpg/220px-281_Ribes_nigrum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Ribes-nigrum-buds.jpg/119px-Ribes-nigrum-buds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Ribes-nigrum.JPG/239px-Ribes-nigrum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Ribes_nigrum_mustaherukka_kukka.jpg/182px-Ribes_nigrum_mustaherukka_kukka.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Owoce_Porzeczka_czarna.jpg/179px-Owoce_Porzeczka_czarna.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Black_currant_in_the_mountains_of_Zakamensky_district_of_Buryatia%2C_Russia.jpg/220px-Black_currant_in_the_mountains_of_Zakamensky_district_of_Buryatia%2C_Russia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Powdery_mildew_on_leaves_of_a_blackcurrant.jpg/220px-Powdery_mildew_on_leaves_of_a_blackcurrant.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Blackcurrant_in_basket_2021_G1.jpg/220px-Blackcurrant_in_basket_2021_G1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды. Листья - пряность. Молодые листья. Сухие листья - чай"
  },
  {
    "latName": "Ribes_spicatum",
    "rusName": "Смородина колосистая",
    "family": "Grossulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Ribes_spicatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/47/Rote_Johannisbeeren_Mannheim.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Robinia_neomexicana",
    "rusName": "Робиния новомексиканская",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Robinia_neomexicana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Robinia_neomexicana1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Robinia_neomexicana_range_map.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Robinia_pseudoacacia",
    "rusName": "Робиния ложноакациевая",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Robinia_pseudoacacia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/ce/20130528Robinie_Hockenheim4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Black_Locust_Leaf_Close_Up.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/75/Locust-railing.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Robina9146.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/d6/Robinia_pseudacacia00.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9e/Robinia_pseudacacia02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Robinia_pseudoacacia_%27Frisia%27.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена - суррогат кофе"
  },
  {
    "latName": "Roemeria_refracta",
    "rusName": "Рёмерия отогнутая",
    "family": "Papaveraceae",
    "url": "https://ru.wikipedia.org/wiki/Roemeria_refracta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Roemeria_refracta_33346129.jpg/275px-Roemeria_refracta_33346129.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Roemeria_refracta_13555444.jpg/220px-Roemeria_refracta_13555444.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/1974_CPA_4409.jpg/220px-1974_CPA_4409.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rosa_canina",
    "rusName": "Шиповник собачий",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_canina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Divlja_ruza_cvijet_270508.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Phramidium_mucrinatum_on_Dog_Rose_Rosa_canina_%2827483546429%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Robin%27s_pincushion_on_dog_rose_-_geograph.org.uk_-_1012403.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rosa_davurica",
    "rusName": "Роза даурская",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_davurica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Rosa_davurica1b.UME.jpg/275px-Rosa_davurica1b.UME.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Rosa_davurica_shrub.JPG/250px-Rosa_davurica_shrub.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Rosa_glabrifolia",
    "rusName": "Шиповник гололистный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_glabrifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Rosa_glabrifolia_79117849.jpg/200px-Rosa_glabrifolia_79117849.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rosa_glauca",
    "rusName": "Роза сизая",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_glauca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Rosa_glauca_img_2050.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Rosa_glauca_img_2726.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/Rosa_glauca_leaves_img_1851.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rosa_majalis",
    "rusName": "Шиповник майский",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_majalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Baron_de_Gonella_Rose.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Illustration_Rosa_majalis0.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Rosa_mollis",
    "rusName": "Шиповник мягкий",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_mollis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/48/Rosa_pomifera_-_wolley_dod%27s_apple_rose_-_desc-open_flower.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rosa_pendulina",
    "rusName": "Шиповник повислый",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_pendulina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Rosa_pendulina_PID1949-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Rosa_pendulina_Sturm08047.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Rosa_pimpinellifolia",
    "rusName": "Шиповник колючейший",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_pimpinellifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/RosaPimpinellifoliaPlena1UME.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Rosa_pimpinellifolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Rosa_pimpinellifolia_hip.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Rosa_pimpinellifolia_young_stem.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2b/Rosa_spinosissima.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Высушенные листья и плоды - суррогат чая"
  },
  {
    "latName": "Rosa_rubiginosa",
    "rusName": "Шиповник красно-бурый",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_rubiginosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Pink_Rose2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/95/Rosa_rubiginosa_sl20.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rosa_rugosa",
    "rusName": "Шиповник морщинистый",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rosa_rugosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Beach_Rose_Rosa_rugosa_Bud.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a3/Rosa_Rugosa_With_Fruit_On_Shoreline.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Rosa_rugosa_2010.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Rosa_rugosa_Tokyo.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/ce/Rosa_rugosa_buds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Rosa_rugosa_hip_Neelix.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды, лепестки"
  },
  {
    "latName": "Rubus_chamaemorus",
    "rusName": "Морошка",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rubus_chamaemorus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Chamaemorus_fruit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/27/Cloudberries.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Homemade_cloudberry_jam.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Rubus_chamaemorus%2C_from_Troms%C3%B8%2C_August_2020.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Rubus_chamaemorus_LC0151.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Rubus_idaeus",
    "rusName": "Малина",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rubus_idaeus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Activity_on_a_leaf_on_a_red_raspberry_bush.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды. Листья - суррогат чая"
  },
  {
    "latName": "Rubus_nessensis",
    "rusName": "Куманика",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rubus_nessensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Rubus_nessensis_-_Botanischer_Garten%2C_Frankfurt_am_Main_-_DSC02459.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Rubus_saxatilis",
    "rusName": "Костяника",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Rubus_saxatilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/73/Rubus_saxatilis_-_Niitv%C3%A4lja_bog.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Rudbeckia_hirta",
    "rusName": "Рудбекия волосистая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Rudbeckia_hirta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/BESusan.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Northern_Crescent_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5f/Rudbeckia_hirta_Indian_Summer.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Rudbeckia_hirta_kz03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8c/Rudbeckiahirta1web.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rumex_acetosella",
    "rusName": "Щавель воробьиный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_acetosella",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Ahosuolahein%C3%A4_%28Rumex_acetosella%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Rumex_aquaticus",
    "rusName": "Щавель водный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_aquaticus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Rumex_aquaticus_Schwalm_GER.jpg/413px-Rumex_aquaticus_Schwalm_GER.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Rumex_aquaticus_Sturm56.jpg/330px-Rumex_aquaticus_Sturm56.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/56/358_Rumex_aquaticus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Muehlenbeckia_adpressa_-_Curtis.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья"
  },
  {
    "latName": "Rumex_confertus",
    "rusName": "Щавель конский",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_confertus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Muehlenbeckia_adpressa_-_Curtis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Rumex_confertus_01.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья"
  },
  {
    "latName": "Rumex_crispus",
    "rusName": "Щавель курчавый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_crispus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d6/Rumex_crispus_vallee-de-grace-amiens_80_12062007_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a3/Rumexcrispus.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Rumex_longifolius",
    "rusName": "Щавель длиннолистный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_longifolius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Hevonhierakka.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Rumex-longifolius-habit2.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья"
  },
  {
    "latName": "Rumex_maritimus",
    "rusName": "Щавель приморский",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_maritimus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a3/RumexMaritimus.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rumex_obtusifolius",
    "rusName": "Щавель туполистный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_obtusifolius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Rumex-obtusifolius-foliage.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Rumex_obtusifolius_Sturm48.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья. Высушенные семена - пряность"
  },
  {
    "latName": "Rumex_obtusifolius",
    "rusName": "Щавель туполистный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_obtusifolius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Rumex-obtusifolius-foliage.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Rumex_obtusifolius_Sturm48.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья. Высушенные семена - пряность"
  },
  {
    "latName": "Rumex_patientia",
    "rusName": "Щавель шпинатный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_patientia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Muehlenbeckia_adpressa_-_Curtis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Rumex_X_patientia_Sturm55.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Rumex_patientia_MHNT.BOT.2015.34.63.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Rumex_patseeds.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Rumex_pseudonatronatus",
    "rusName": "Щавель ложносолончаковый",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_pseudonatronatus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Finnskr%C3%A4ppa-3964_-_Flickr_-_Ragnhild_%26_Neil_Crawford.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rumex_stenophyllus",
    "rusName": "Щавель узколистный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_stenophyllus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Muehlenbeckia_adpressa_-_Curtis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Rumex_stenophyllus.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rumex_thyrsiflorus",
    "rusName": "Щавель пирамидальный",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_thyrsiflorus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Muehlenbeckia_adpressa_-_Curtis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Rumex_thyrsiflorus_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Rumex_ucranicus",
    "rusName": "Щавель украинский",
    "family": "Polygonaceae",
    "url": "https://ru.wikipedia.org/wiki/Rumex_ucranicus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Rumex_ucranicus_162659968.jpg/275px-Rumex_ucranicus_162659968.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sagina_procumbens",
    "rusName": "Мшанка лежачая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Sagina_procumbens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Moos_5770.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sagittaria_sagittifolia",
    "rusName": "Стрелолист обыкновенный",
    "family": "Alismataceae",
    "url": "https://ru.wikipedia.org/wiki/Sagittaria_sagittifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/81/SagittariaSagittifoliaInflorescence2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/SagittariaSagittifoliaLeaves.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_acutifolia",
    "rusName": "Ива остролистная",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_acutifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Salix-catkins.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Salix_acutifolia_Willd..jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_alba",
    "rusName": "Ива белая",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Salix_alba_020.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Salix_alba_leaves.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Salix_alba_022.jpg/240px-Salix_alba_022.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Salix_alba_012.jpg/240px-Salix_alba_012.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Salix_alba_009.jpg/240px-Salix_alba_009.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Thom%C3%A9_Salix_alba_clean.jpg/330px-Thom%C3%A9_Salix_alba_clean.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Salix_alba_004.jpg/413px-Salix_alba_004.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_aurita",
    "rusName": "Ива ушастая",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_aurita",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Salix-catkins.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Salix_aurita_007.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Salix_aurita_Sturm26.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_caprea",
    "rusName": "Ива козья",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_caprea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/51/Salix_caprea_female_TK_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Salix_caprea_male_TK_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Waterwilg_%28DSC_2539%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Willow_whistle.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_cinerea",
    "rusName": "Ива пепельная",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_cinerea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/2015-02-28_Close-ups_of_Salicaceae_flowers%2C_Weinviertel_%28Producer_M._Stich%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/Betula_pubescens_and_Salix_cinerea_seeds.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Salix_cinerea_Habitus_in_spring_Germany.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_dasyclados",
    "rusName": "Ива шерстистопобеговая",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_dasyclados",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Salix_gmelinii_44139182.jpg/275px-Salix_gmelinii_44139182.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_fragilis",
    "rusName": "Ива ломкая",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_fragilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Salix_fragilis_%27Bullata%27.jpg/275px-Salix_fragilis_%27Bullata%27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/381_Salix_fragilis.jpg/220px-381_Salix_fragilis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Salix_fragilis_001.jpg/180px-Salix_fragilis_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Salix_fragilis_005.jpg/180px-Salix_fragilis_005.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/%D0%A2%D0%B0%D0%B1%D0%BB%D0%B8%D1%87%D0%BA%D0%B0_%C2%AB%D0%98%D0%B2%D0%B0_%D0%BB%D0%BE%D0%BC%D0%BA%D0%B0%D1%8F%C2%BB_%D0%B2_%D0%91%D0%B8%D1%80%D1%8E%D0%BB%D1%91%D0%B2%D1%81%D0%BA%D0%BE%D0%BC_%D0%B4%D0%B5%D0%B4%D1%80%D0%BE%D0%BF%D0%B0%D1%80%D0%BA%D0%B5_%D0%BD%D0%B0_%D1%83%D1%87%D0%B0%D1%81%D1%82%D0%BA%D0%B5_5.jpg/220px-%D0%A2%D0%B0%D0%B1%D0%BB%D0%B8%D1%87%D0%BA%D0%B0_%C2%AB%D0%98%D0%B2%D0%B0_%D0%BB%D0%BE%D0%BC%D0%BA%D0%B0%D1%8F%C2%BB_%D0%B2_%D0%91%D0%B8%D1%80%D1%8E%D0%BB%D1%91%D0%B2%D1%81%D0%BA%D0%BE%D0%BC_%D0%B4%D0%B5%D0%B4%D1%80%D0%BE%D0%BF%D0%B0%D1%80%D0%BA%D0%B5_%D0%BD%D0%B0_%D1%83%D1%87%D0%B0%D1%81%D1%82%D0%BA%D0%B5_5.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_lapponum",
    "rusName": "Ива лопарская",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_lapponum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Salix_lapponum_General_view.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Salix_lapponum_Leaf_under_side.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Salix_lapponum_Leaf_upper_side.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/d7/Salix_lapponum_Twig.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_myrsinifolia",
    "rusName": "Ива мирзинолистная",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_myrsinifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/19/Salix-catkins.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Salix_myrsinifolia_%28Schwarz-Weide%29_IMG_4622.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_myrtilloides",
    "rusName": "Ива черничная",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_myrtilloides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Salix_myrtilloides.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_pentandra",
    "rusName": "Ива пятитычинковая",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_pentandra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Salix_pentandra%2802%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Salix_pentandra0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_purpurea",
    "rusName": "Ива пурпурная",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_purpurea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Salix-purpurea-leaves.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Salix_purpurea_003.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_rosmarinifolia",
    "rusName": "Ива розмаринолистная",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_rosmarinifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Salix_rosmarinifolia01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_starkeana",
    "rusName": "Ива Штарке",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_starkeana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b1/Dong_bei_mu_ben_zhi_wu_tu_zhi_%281955%29_%2820805633159%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_triandra",
    "rusName": "Ива трёхтычинковая",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_triandra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Salix_triandra_bark.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Salix_triandra_female_flower.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salix_viminalis",
    "rusName": "Ива прутовидная",
    "family": "Salicaceae",
    "url": "https://ru.wikipedia.org/wiki/Salix_viminalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Salix-viminalis.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salvia_nemorosa",
    "rusName": "Шалфей дубравный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Salvia_nemorosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Salvia_nemorosa-IMG_3624.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Salvia_verticillata",
    "rusName": "Шалфей мутовчатый",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Salvia_verticillata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c7/Salvia_verticillata_240606.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Приправа"
  },
  {
    "latName": "Salvinia_natans",
    "rusName": "Сальвиния плавающая",
    "family": "Salviniaceae",
    "url": "https://ru.wikipedia.org/wiki/Salvinia_natans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Illustration_Salvinia_natans0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e6/Salvinia_natans_%28habitus%29_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/46/Salvinia_natans_01.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Salvinia_natans_Prague_2014_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Salvinia_natans_kz27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Salvinia_natans_kz_spor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Salwinia_natns_w_naturze.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/88/Submerged_leaf_of_salvinia_natans.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sambucus_ebulus",
    "rusName": "Бузина травянистая",
    "family": "Sambucaceae",
    "url": "https://ru.wikipedia.org/wiki/Sambucus_ebulus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/cd/SambucusEdulus-Ripe.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/51/Sambucus_ebulus_bgiu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/51/Sambucus_ebulus_bgiu.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sambucus_nigra",
    "rusName": "Бузина чёрная",
    "family": "Sambucaceae",
    "url": "https://ru.wikipedia.org/wiki/Sambucus_nigra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/%22Godfrey%27s_Extract_of_Elder-Flowers%22_ad_-_Woman%27s_Exhibition%2C_1900%2C_Earl%27s_Court%2C_London%2C_S.W._-_official_fine_art%2C_historical_and_general_catalogue_%28IA_gri_33125013839424%29_%28page_7_crop%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Auricularia_auricula-judae_Eglinton.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Elder_growing_on_Sycamore%2C_Dalry%2C_Scotland.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b5/Elderberry-jam.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8c/Flowers_Black_Elder.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Hyphodontia_sambuci_Eglinton.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Sambuci_flos_dried.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sambucus_racemosa",
    "rusName": "Бузина красная",
    "family": "Sambucaceae",
    "url": "https://ru.wikipedia.org/wiki/Sambucus_racemosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/0_Sambucus_racemosa_-_Vallorcine.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Sambucus_racemosa_1567.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/27/Sambucus_racemosa_6261.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Sambucus_racemosa_6269.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/88/Sambucus_racemosa_ies.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Samolus_valerandi",
    "rusName": "Самолюс Валеранда",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Samolus_valerandi",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d3/SamolusValerandi2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Samolus_parviflorus.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sanguisorba_officinalis",
    "rusName": "Кровохлёбка лекарственная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Sanguisorba_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Addisonia_-_colored_illustrations_and_popular_descriptions_of_plants_%281916-%281964%29%29_%2816585422840%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/Sanguisorba_officinalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/82/Sanguisorba_officinalisseeds.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Замороженные корни"
  },
  {
    "latName": "Sanicula_europaea",
    "rusName": "Подлесник европейский",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Sanicula_europaea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/74/QALace2675.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Sanicle_at_Spier%27s.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Sanicula_europaea_050606.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Saponaria_officinalis",
    "rusName": "Мыльнянка лекарственная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Saponaria_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Saponaria-officinalis-flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f3/Saponaria_officinalis_MHNT.BOT.40.40.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7b/Saponaria_officinalis_Prague_2011_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Saussurea_amara",
    "rusName": "Соссюрея горькая",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Saussurea_amara",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Saussurea_amara_91910542.jpg/275px-Saussurea_amara_91910542.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Saxifraga_granulata",
    "rusName": "Камнеломка зернистая",
    "family": "Saxifragaceae",
    "url": "https://ru.wikipedia.org/wiki/Saxifraga_granulata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/d4/Saxifraga_granulata_140505.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Saxifraga_oppositifolia_upernavik_2006-06-22_2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Saxifraga_hirculus",
    "rusName": "Камнеломка болотная",
    "family": "Saxifragaceae",
    "url": "https://ru.wikipedia.org/wiki/Saxifraga_hirculus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Saxifraga_oppositifolia_upernavik_2006-06-22_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/05/Skaftafell_-_Gelbe_Bl%C3%BCten.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Saxifraga_tridactylites",
    "rusName": "Камнеломка трёхпалая",
    "family": "Saxifragaceae",
    "url": "https://ru.wikipedia.org/wiki/Saxifraga_tridactylites",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Saxifraga_oppositifolia_upernavik_2006-06-22_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Saxifraga_tridactylites.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Saxifraga_tridactylites_Derbyshire.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scabiosa_ochroleuca",
    "rusName": "Скабиоза бледно-жёлтая",
    "family": "Dipsacaceae",
    "url": "https://ru.wikipedia.org/wiki/Scabiosa_ochroleuca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Scabiosa_ochroleuca_0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f3/Scabiosa_ochroleuca.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Scabiosa_ochroleuca_0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scandix_pecten-veneris",
    "rusName": "Скандикс гребенчатый",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Scandix_pecten-veneris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Scandix_pecten-veneris_flowers_and_fruits.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья и побеги - приправа"
  },
  {
    "latName": "Scheuchzeria_palustris",
    "rusName": "Шейхцерия",
    "family": "Scheuchzeriaceae",
    "url": "https://ru.wikipedia.org/wiki/Scheuchzeria_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/ScheuchzeriaPalustrisColl.jpg/275px-ScheuchzeriaPalustrisColl.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Scheuchzeria_palustris_Sturm.jpg/220px-Scheuchzeria_palustris_Sturm.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Scheuchzeria_pal_fruits.jpg/220px-Scheuchzeria_pal_fruits.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Schoenoplectus_lacustris",
    "rusName": "Камыш озёрный",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Schoenoplectus_lacustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/Schoenoplectus_lacustris_260605.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "корневища с клубнями"
  },
  {
    "latName": "Schoenoplectus_tabernaemontani",
    "rusName": "Камыш Табернемонтана",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Schoenoplectus_tabernaemontani",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/d/de/SchoenoplectusTabernaemontani.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Schoenoplectus_tabernaemontani_-_Flickr_-_Kevin_Thiele.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scilla_siberica",
    "rusName": "Пролеска сибирская",
    "family": "Hyacinthaceae",
    "url": "https://ru.wikipedia.org/wiki/Scilla_siberica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/09/20210430_SiberianSquill-SQ.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/20210430_SiberianSquill-SQ1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Park_Klepacza_Lodz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Scilla_siberica_Indiana_University.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/Scilla_siberica_alba_bulbs.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Scilla_siberica_flower_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/17/Scilla_siberica_pods.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scirpus_sylvaticus",
    "rusName": "Камыш лесной",
    "family": "Cyperaceae",
    "url": "https://ru.wikipedia.org/wiki/Scirpus_sylvaticus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Cottonsedge_2531125282.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/11/ScirpusSylvaticus.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scolochloa_festucacea",
    "rusName": "Тростянка (растение)",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Scolochloa_festucacea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Scolochloa_festucacea_-_Berlin_Botanical_Garden_-_IMG_8549.JPG/275px-Scolochloa_festucacea_-_Berlin_Botanical_Garden_-_IMG_8549.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Poaceae_spp_Sturm37.jpg/220px-Poaceae_spp_Sturm37.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scorzonera_glabra",
    "rusName": "Козелец австрийский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Scorzonera_glabra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Scorzonera_austriaca_%28%C3%96sterreich-Schwarzwurzel%29_0412_IMG.JPG/275px-Scorzonera_austriaca_%28%C3%96sterreich-Schwarzwurzel%29_0412_IMG.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scorzonera_humilis",
    "rusName": "Козелец приземистый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Scorzonera_humilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Chicory_flower_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/Scorzonera_humilis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scrophularia_nodosa",
    "rusName": "Норичник узловатый",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Scrophularia_nodosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Scrophularia_nodosa8.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Warming-Skudbygning-Fig12-Scrophularia-nodosa.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scrophularia_umbrosa",
    "rusName": "Норичник теневой",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Scrophularia_umbrosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Scrophularia_umbrosa.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scutellaria_galericulata",
    "rusName": "Шлемник обыкновенный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Scutellaria_galericulata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Illustration_Scutellaria_galericulata0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Scutellaria_hastifolia",
    "rusName": "Шлемник копьелистный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Scutellaria_hastifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Scutellaria_hastifolia.jpeg/275px-Scutellaria_hastifolia.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Scutellaria_hastifolia_blatt.jpeg/220px-Scutellaria_hastifolia_blatt.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Secale_cereale",
    "rusName": "Рожь",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Secale_cereale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Rye_Mature_Grain_Summer.jpg/275px-Rye_Mature_Grain_Summer.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Illustration_Secale_cereale0.jpg/220px-Illustration_Secale_cereale0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Secale_cereale_MHNT.BOT.2015.2.40.jpg/220px-Secale_cereale_MHNT.BOT.2015.2.40.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Oortjes_rogge_Secale_cereale.jpg/100px-Oortjes_rogge_Secale_cereale.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Rogge_aar_Secale_cereale.jpg/200px-Rogge_aar_Secale_cereale.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Rogge_korrels_Secale_cereale.jpg/220px-Rogge_korrels_Secale_cereale.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/%D0%96%D0%98%D0%A2%D0%9E.jpg/255px-%D0%96%D0%98%D0%A2%D0%9E.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Rye_JM.jpg/450px-Rye_JM.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Sedum_acre",
    "rusName": "Очиток едкий",
    "family": "Crassulaceae",
    "url": "https://ru.wikipedia.org/wiki/Sedum_acre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Sedum_acre_062611.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sedum_spurium",
    "rusName": "Очиток ложный",
    "family": "Crassulaceae",
    "url": "https://ru.wikipedia.org/wiki/Sedum_spurium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Phedimus_spurius_IMG_7179.JPG/275px-Phedimus_spurius_IMG_7179.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Naturalis_Biodiversity_Center_-_L.2096715_-_Anoniem_-_Sedum_spurium_von_Bieberstein_-_Artwork_%28cropped%29.jpeg/310px-Naturalis_Biodiversity_Center_-_L.2096715_-_Anoniem_-_Sedum_spurium_von_Bieberstein_-_Artwork_%28cropped%29.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Phedimus_spurius_leaves_HC1.jpg/330px-Phedimus_spurius_leaves_HC1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Phedimus_spurius_HC1.jpg/330px-Phedimus_spurius_HC1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Sedum_spurium_-_Botanischer_Garten_M%C3%BCnchen-Nymphenburg_-_DSC07633.JPG/330px-Sedum_spurium_-_Botanischer_Garten_M%C3%BCnchen-Nymphenburg_-_DSC07633.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Sedum_spurium_%27tricolor%27.jpg/330px-Sedum_spurium_%27tricolor%27.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Sedum_spurium_Bronze_Carpet.jpg/330px-Sedum_spurium_Bronze_Carpet.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Selinum_carvifolia",
    "rusName": "Гирча тминолистная",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Selinum_carvifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Selinum_carvifolia_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sempervivum_ruthenicum",
    "rusName": "Молодило русское",
    "family": "Crassulaceae",
    "url": "https://ru.wikipedia.org/wiki/Sempervivum_ruthenicum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/%D0%9C%D0%BE%D0%BB%D0%BE%D0%B4%D0%B8%D0%BB%D0%BE_%D1%80%D1%83%D1%81%D1%8C%D0%BA%D0%B51.JPG/275px-%D0%9C%D0%BE%D0%BB%D0%BE%D0%B4%D0%B8%D0%BB%D0%BE_%D1%80%D1%83%D1%81%D1%8C%D0%BA%D0%B51.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/%D0%9C%D0%BE%D0%BB%D0%BE%D0%B4%D0%B8%D0%BB%D0%BE_%D1%80%D1%83%D1%81%D1%8C%D0%BA%D0%B53.JPG/220px-%D0%9C%D0%BE%D0%BB%D0%BE%D0%B4%D0%B8%D0%BB%D0%BE_%D1%80%D1%83%D1%81%D1%8C%D0%BA%D0%B53.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sempervivum_tectorum",
    "rusName": "Молодило кровельное",
    "family": "Crassulaceae",
    "url": "https://ru.wikipedia.org/wiki/Sempervivum_tectorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Bicknell_Sempervivuim_June_2020.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Illustration_Sempervivum_tectorum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/75/SempervivumTectorum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Sempervivum_Tectorum_Greenii%2C_Huntington.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Sempervivum_tectorum_%281%29_1_%28ex_Pyrenees%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/51/Sempervivum_tectorum_Almindelig_Husl%C3%B8g.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Sempervivum_tectorum_abl1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/bd/Sempervivum_tectorum_boutignyanum-1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Senecio_borysthenicus",
    "rusName": "Крестовник днепровский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Senecio_borysthenicus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Jacobaea_borysthenica_94748552.jpg/275px-Jacobaea_borysthenica_94748552.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Senecio_erucifolius",
    "rusName": "Якобея эруколистная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Senecio_erucifolius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/20180816Jacobaea_erucifolia3.jpg/275px-20180816Jacobaea_erucifolia3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Jacobaea_erucifolia.jpg/220px-Jacobaea_erucifolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Hoary_ragwort%2C_Jacobaea_erucifolia.jpg/116px-Hoary_ragwort%2C_Jacobaea_erucifolia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Senecio_erucifolius_sl10.jpg/175px-Senecio_erucifolius_sl10.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Senecio_erucifolius_2016-07-19_3168.jpg/175px-Senecio_erucifolius_2016-07-19_3168.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/SENECIO_ERUCIFOLIUS_-_MATELLA.JPG/175px-SENECIO_ERUCIFOLIUS_-_MATELLA.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Senecio_erucifolius_sl61.jpg/129px-Senecio_erucifolius_sl61.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Senecio_inaequidens",
    "rusName": "Крестовник неравнозубчатый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Senecio_inaequidens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/20180726Senecio_inaequidens2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Senecio_inaequidens_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Senecio_jacobaea",
    "rusName": "Якобея обыкновенная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Senecio_jacobaea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Jacobaea_vulgaris_subsp._vulgaris_plant_flowers.jpg/275px-Jacobaea_vulgaris_subsp._vulgaris_plant_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Illustration_Senecio_jacobaea0.jpg/180px-Illustration_Senecio_jacobaea0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Ragwort_flowers.jpg/220px-Ragwort_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Jacobaea_vulgaris-3235.jpg/220px-Jacobaea_vulgaris-3235.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Jacobaea_vulgaris_subsp._vulgaris_seeds%2C_Jakobskruiskruid_vruchten.jpg/220px-Jacobaea_vulgaris_subsp._vulgaris_seeds%2C_Jakobskruiskruid_vruchten.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Tyria_jacobaeae_qtl1.jpg/220px-Tyria_jacobaeae_qtl1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Senecio_vernalis",
    "rusName": "Крестовник весенний",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Senecio_vernalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Senecio_vernalis001.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/34/Senecio_vernalis_-_Iris-Hill-IZE-033b.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Senecio_vernalis_Sturm60.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Senecio_viscosus",
    "rusName": "Крестовник клейкий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Senecio_viscosus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Kleverig_kruiskruid_R0020102.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Senecio_vernalis-Romania.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Senecio_vulgaris",
    "rusName": "Крестовник обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Senecio_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Cinnabar_moth_caterpillar_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Common_Groundsel-first_fruits.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Ochropleura.plecta.7479.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Serratula_tinctoria",
    "rusName": "Серпуха красильная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Serratula_tinctoria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Asteraceae_-_Serratula_tinctoria-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Asteraceae_-_Serratula_tinctoria.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/CDC_artichoke.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Serratula_tinctoria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Serratula_tinctoria_MHNT.BOT.2012.10.41.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Seseli_annuum",
    "rusName": "Жабрица однолетняя",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Seseli_annuum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4f/Seseli_annuum_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Seseli_libanotis",
    "rusName": "Жабрица порезниковая",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Seseli_libanotis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Seseli_libanotis_eF.jpg/220px-Seseli_libanotis_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Setaria_italica",
    "rusName": "Могар",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Setaria_italica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Japanese_Foxtail_millet_02.jpg/275px-Japanese_Foxtail_millet_02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Kolbenhirse.jpg/130px-Kolbenhirse.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Food_grain_foxtail_millet.jpg/220px-Food_grain_foxtail_millet.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/A_closeup_of_Thinai_millet_with_husk..JPG/120px-A_closeup_of_Thinai_millet_with_husk..JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Hulled_foxtail_millet.jpg/120px-Hulled_foxtail_millet.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Japanese_Foxtail_millet_01.jpg/120px-Japanese_Foxtail_millet_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/%E0%A6%95%E0%A6%BE%E0%A6%89%E0%A6%A8_%E0%A6%95%E0%A7%8D%E0%A6%B7%E0%A7%87%E0%A6%A4.jpg/120px-%E0%A6%95%E0%A6%BE%E0%A6%89%E0%A6%A8_%E0%A6%95%E0%A7%8D%E0%A6%B7%E0%A7%87%E0%A6%A4.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Крупа"
  },
  {
    "latName": "Setaria_viridis",
    "rusName": "Щетинник зелёный",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Setaria_viridis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/%E3%82%A8%E3%83%8E%E3%82%B3%E3%83%AD%E3%82%B0%E3%82%B5Setaria_viridis_%28L.%29_P.Beauv.P9130041.JPG/413px-%E3%82%A8%E3%83%8E%E3%82%B3%E3%83%AD%E3%82%B0%E3%82%B5Setaria_viridis_%28L.%29_P.Beauv.P9130041.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Setaria_viridis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v11.jpg/330px-Setaria_viridis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v11.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sherardia_arvensis",
    "rusName": "Жерардия",
    "family": "Rubiaceae",
    "url": "https://ru.wikipedia.org/wiki/Sherardia_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Field_Madder_%28984153920%29.jpg/275px-Field_Madder_%28984153920%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Illustration_Sherardia_arvensis0.jpg/220px-Illustration_Sherardia_arvensis0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Silene_dichotoma",
    "rusName": "Смолёвка вильчатая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Silene_dichotoma",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Silene_dichotoma_sl13.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Silene_dichotoma_sl30.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Silene_lithuanica",
    "rusName": "Зоречка литовская",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Silene_lithuanica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Silene_lithuanica.jpg/262px-Silene_lithuanica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Silene_lithuanica_2017-09-29_6029.jpg/220px-Silene_lithuanica_2017-09-29_6029.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Silene_lithuanica_2017-09-29_6032.jpg/220px-Silene_lithuanica_2017-09-29_6032.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Silene_lithuanica_2017-09-29_6031.jpg/220px-Silene_lithuanica_2017-09-29_6031.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Lietuvos_gamtai_reversas.jpg/220px-Lietuvos_gamtai_reversas.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Silene_nutans",
    "rusName": "Смолёвка поникшая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Silene_nutans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Silene_nutans_220505.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Silene_repens",
    "rusName": "Смолёвка ползучая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Silene_repens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Silene_repens_43698602.jpg/275px-Silene_repens_43698602.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Silene_vulgaris",
    "rusName": "Смолёвка обыкновенная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Silene_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Bladder_Campion.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/80/Silene_vulgaris_in_Aspen_%2891171%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Побеги"
  },
  {
    "latName": "Silphium_perfoliatum",
    "rusName": "Сильфия пронзённолистная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Silphium_perfoliatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Cup_plant.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1d/Cupplants.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Silphium_perfoliatum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/31/Silphium_perfoliatum_%28Slovenia%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Silphium_perfoliatum_MHNT.BOT.2015.34.69.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sinapis_alba",
    "rusName": "Горчица белая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Sinapis_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/ImageMoutarde1.jpg/275px-ImageMoutarde1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Sinapis_alba_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-265.jpg/220px-Sinapis_alba_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-265.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Семена"
  },
  {
    "latName": "Sinapis_arvensis",
    "rusName": "Горчица полевая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Sinapis_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Brassicaceae_-_Sinapis_arvensis_%283%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/7e/Herik_17-10-2005_14.00.34.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/fc/Sinapis_arvensis_s_IP0310010.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "листья, не цветущие стебли без кожицы"
  },
  {
    "latName": "Sisymbrium_altissimum",
    "rusName": "Гулявник высокий",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Sisymbrium_altissimum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Sisymbrium_altissimum_tumblemustard.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1a/Sisymbrium_altissimum_tumblemustard_closeup.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Sisymbriumaltissimum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sium_latifolium",
    "rusName": "Поручейник широколистный",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Sium_latifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Arabic_herbal_medicine_guidebook.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/QALace2675.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Sium_latifolium.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Solanum_dulcamara",
    "rusName": "Паслён сладко-горький",
    "family": "Solanaceae",
    "url": "https://ru.wikipedia.org/wiki/Solanum_dulcamara",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Bittersweet_Nightshade.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Bitterweet_Nightshade_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/72/Illustration_Solanum_dulcamara0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Solanum-dulcamara-Distribution-PhytoKeys-022-001-g036.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/SolanumDulcamara-bloem-sm.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Solanum_dulcamara%2C_Fogner%2C_Oslo..JPG",
      "https://upload.wikimedia.org/wikipedia/commons/0/05/XN_Solanum_dulcamara_01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Solanum_nigrum",
    "rusName": "Паслён чёрный",
    "family": "Solanaceae",
    "url": "https://ru.wikipedia.org/wiki/Solanum_nigrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/53/Black_Nightshade_-_Flickr_-_treegrow_%282%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Makoi_or_Solanum_nigrum_berries.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f4/Ripe_Berries_of_S._Nigrum.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Solanum_Nigrum_Berries.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7e/Solanum_nigrum_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Solanum_nigrum_leafs_flowers_fruits.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Спелые плоды, молодые листья (x)"
  },
  {
    "latName": "Solidago_canadensis",
    "rusName": "Золотарник канадский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Solidago_canadensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Solidago_canadensis_20050815_248.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Solidago_canadensis_flower.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Solidago_gigantea",
    "rusName": "Золотарник гигантский",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Solidago_gigantea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Solidago_gigantea01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8c/Solidago_gigantea_var_serotina.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Solidago_virgaurea",
    "rusName": "Золотарник обыкновенный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Solidago_virgaurea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Bombus_cryptarum_-_Solidago_virgaurea_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Solidago_virgaurea_minuta1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sonchus_arvensis",
    "rusName": "Осот полевой",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Sonchus_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/Illustration_Sonchus_arvensis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Sonchus_arvensis.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Sonchus_arvensis20090912_303.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Sonchus_arvensis20090912_305.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sonchus_asper",
    "rusName": "Осот шероховатый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Sonchus_asper",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Sonchus_asper2.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Sonchus_oleraceus",
    "rusName": "Осот огородный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Sonchus_oleraceus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/42/Amanida_de_lletsons9.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/d1/Flower_of_Sonchus_oleraceus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Sonchus_February_2008-1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья, корни"
  },
  {
    "latName": "Sorbaria_sorbifolia",
    "rusName": "Рябинник рябинолистный",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Sorbaria_sorbifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Almonds02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/SorbariaSorbifolia3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sorbus_aucuparia",
    "rusName": "Рябина обыкновенная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Sorbus_aucuparia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/11/Freshly_cross_cut_Sorbus_aucuparia_with_heart-wood.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cf/Freshly_rip_cut_Sorbus_aucuparia_from_the_island_of_Engeloeya_in_Norway.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Rowan_tree_20081002b.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/02/Sorbus-aucuparia.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/85/Sorbus_aucuparia_fruit_comparison.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ee/Sorbus_aucuparia_in_the_fall.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3e/Sorbus_aucuparia_inflorescence_kz.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Плоды"
  },
  {
    "latName": "Sorghum_halepense",
    "rusName": "Сорго алеппское",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Sorghum_halepense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Sorgho_d%27Alep-Sorghum_halepense-Inflorescence-20140817.jpg/275px-Sorgho_d%27Alep-Sorghum_halepense-Inflorescence-20140817.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Sorghum_halepense_plant2_%287412716058%29.jpg/220px-Sorghum_halepense_plant2_%287412716058%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sorghum_sudanense",
    "rusName": "Суданская трава",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Sorghum_sudanense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Espigues_sorgo_CP2.JPG/275px-Espigues_sorgo_CP2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Sorghum_sudanese.jpg/250px-Sorghum_sudanese.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sparganium_angustifolium",
    "rusName": "Ежеголовник узколистный",
    "family": "Sparganiaceae",
    "url": "https://ru.wikipedia.org/wiki/Sparganium_angustifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/2/23/Sparganium_angustifolium.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sparganium_emersum",
    "rusName": "Ежеголовник всплывающий",
    "family": "Sparganiaceae",
    "url": "https://ru.wikipedia.org/wiki/Sparganium_emersum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cc/Sparganium_emersum_11429.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sparganium_erectum",
    "rusName": "Ежеголовник прямой",
    "family": "Sparganiaceae",
    "url": "https://ru.wikipedia.org/wiki/Sparganium_erectum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/a/aa/Sparganium_erectum%28Fruit%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Vandens_augalas01.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sparganium_glomeratum",
    "rusName": "Ежеголовник скученный",
    "family": "Sparganiaceae",
    "url": "https://ru.wikipedia.org/wiki/Sparganium_glomeratum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Dactylis_glomerata_bluete2.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/2/28/Sparganium_glomeratum_070828_Vladivostok.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Sparganium_gramineum",
    "rusName": "Ежеголовник злаковый",
    "family": "Sparganiaceae",
    "url": "https://ru.wikipedia.org/wiki/Sparganium_gramineum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Sparganium_gramineum_03fi.jpg/275px-Sparganium_gramineum_03fi.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Spergula_arvensis",
    "rusName": "Торица полевая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Spergula_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Gewone_spurrie_plant_Spergula_arvensis.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Масло семян"
  },
  {
    "latName": "Spergula_linicola",
    "rusName": "Торица полевая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Spergula_linicola",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Gewone_spurrie_plant_Spergula_arvensis.jpg/275px-Gewone_spurrie_plant_Spergula_arvensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Spergula_arvensis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg/220px-Spergula_arvensis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Spergula_arvensis_kz1.jpg/150px-Spergula_arvensis_kz1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/MFG_0683-Spergula-arvensis.JPG/295px-MFG_0683-Spergula-arvensis.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Spergula_arvensis%2C_Gewone_spurrie_bloem_%281%29.jpg/163px-Spergula_arvensis%2C_Gewone_spurrie_bloem_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Spergula_arvensis_Habitus_2010-3-7_DehesaBoyalPuertollano.jpg/165px-Spergula_arvensis_Habitus_2010-3-7_DehesaBoyalPuertollano.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Масло семян"
  },
  {
    "latName": "Spergula_maxima",
    "rusName": "Торица полевая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Spergula_maxima",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Gewone_spurrie_plant_Spergula_arvensis.jpg/275px-Gewone_spurrie_plant_Spergula_arvensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Spergula_arvensis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg/220px-Spergula_arvensis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Spergula_arvensis_kz1.jpg/150px-Spergula_arvensis_kz1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/MFG_0683-Spergula-arvensis.JPG/295px-MFG_0683-Spergula-arvensis.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Spergula_arvensis%2C_Gewone_spurrie_bloem_%281%29.jpg/163px-Spergula_arvensis%2C_Gewone_spurrie_bloem_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Spergula_arvensis_Habitus_2010-3-7_DehesaBoyalPuertollano.jpg/165px-Spergula_arvensis_Habitus_2010-3-7_DehesaBoyalPuertollano.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Масло семян"
  },
  {
    "latName": "Spergula_morisonii",
    "rusName": "Торица Морисона",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Spergula_morisonii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Spergula_morisonii_kz18.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Spergula_sativa",
    "rusName": "Торица полевая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Spergula_sativa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Gewone_spurrie_plant_Spergula_arvensis.jpg/275px-Gewone_spurrie_plant_Spergula_arvensis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Spergula_arvensis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg/220px-Spergula_arvensis_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Spergula_arvensis_kz1.jpg/150px-Spergula_arvensis_kz1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/MFG_0683-Spergula-arvensis.JPG/295px-MFG_0683-Spergula-arvensis.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Spergula_arvensis%2C_Gewone_spurrie_bloem_%281%29.jpg/163px-Spergula_arvensis%2C_Gewone_spurrie_bloem_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Spergula_arvensis_Habitus_2010-3-7_DehesaBoyalPuertollano.jpg/165px-Spergula_arvensis_Habitus_2010-3-7_DehesaBoyalPuertollano.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Масло семян"
  },
  {
    "latName": "Spergularia_rubra",
    "rusName": "Торичник красный",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Spergularia_rubra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Spergularia_rubra_Closeup_SierraMadrona.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Spinacia_oleracea",
    "rusName": "Шпинат огородный",
    "family": "Chenopodiaceae",
    "url": "https://ru.wikipedia.org/wiki/Spinacia_oleracea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Spinazie_vrouwelijke_plant_%28Spinacia_oleracea_female_plant%29.jpg/275px-Spinazie_vrouwelijke_plant_%28Spinacia_oleracea_female_plant%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Illustration_Spinacia_oleracea1.jpg/220px-Illustration_Spinacia_oleracea1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Spinach_field_in_Italy_2.jpg/240px-Spinach_field_in_Italy_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Espinac_5nov.JPG/240px-Espinac_5nov.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Jeunes_pousses_d%27%C3%A9pinards.jpg/302px-Jeunes_pousses_d%27%C3%A9pinards.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Spiraea_media",
    "rusName": "Спирея средняя",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Spiraea_media",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Afbeeldingen_der_fraaiste%2C_meest_uitheemsche_boomen_en_heesters_%28Plate_77%29_%287899807482%29.jpg/275px-Afbeeldingen_der_fraaiste%2C_meest_uitheemsche_boomen_en_heesters_%28Plate_77%29_%287899807482%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Spiraea_media_sl6.jpg/120px-Spiraea_media_sl6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Spiraea_media_sl18.jpg/90px-Spiraea_media_sl18.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Spiraea_media_sl17.jpg/120px-Spiraea_media_sl17.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Spiraea_media.2011.04.25.A.JPG/90px-Spiraea_media.2011.04.25.A.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Spiraea_salicifolia",
    "rusName": "Спирея иволистная",
    "family": "Rosaceae",
    "url": "https://ru.wikipedia.org/wiki/Spiraea_salicifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Illustration_Spiraea_salicifolia0.jpg/238px-Illustration_Spiraea_salicifolia0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/%C3%84ggdalen_2015_Spiraea_salicifolia_001.jpg/120px-%C3%84ggdalen_2015_Spiraea_salicifolia_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Waldo_Lake_Wildflower_%28Lane_County%2C_Oregon_scenic_images%29_%28lanDB3719%29.jpg/80px-Waldo_Lake_Wildflower_%28Lane_County%2C_Oregon_scenic_images%29_%28lanDB3719%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Spiraea_salicifolia_sl5.jpg/120px-Spiraea_salicifolia_sl5.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Spirodela_polyrhiza",
    "rusName": "Многокоренник обыкновенный",
    "family": "Lemnaceae",
    "url": "https://ru.wikipedia.org/wiki/Spirodela_polyrhiza",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/Spirodela_polyrrhiza_marais_poitevin.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stachys_annua",
    "rusName": "Чистец однолетний",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Stachys_annua",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Stachys_annua.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/90/Stachys_annua_Sturm47.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stachys_palustris",
    "rusName": "Чистец болотный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Stachys_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/20170620Stachys_palustris1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Stachys_palustris_-_soo-n%C3%B5ian%C3%B5ges.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Подземные побеги и клубнеподобные утолщения. Не следует допускать передозировки, так как растение обладает токсическими свойствами"
  },
  {
    "latName": "Stachys_recta",
    "rusName": "Чистец прямой",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Stachys_recta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2c/Lamiaceae_-_Stachys_officinalis-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Lamiaceae_-_Stachys_recta-1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a1/Lamiaceae_-_Stachys_recta.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/10/Stachys_recta.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7e/Stachys_recta001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Stachys_recta_Sturm48.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stachys_sylvatica",
    "rusName": "Чистец лесной",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Stachys_sylvatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0f/Wald-Ziest_%28Stachys_sylvatica%29.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Соцветия и листья - пряность"
  },
  {
    "latName": "Staphylea_trifolia",
    "rusName": "Клекачка трёхлистная",
    "family": "Staphyleaceae",
    "url": "https://ru.wikipedia.org/wiki/Staphylea_trifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Geranium_sanguineum0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Staphylea_trifolia_SCA-3462.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stellaria_graminea",
    "rusName": "Звездчатка злаковая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Stellaria_graminea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/44/Stellaria_graminea_detail.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stellaria_holostea",
    "rusName": "Звездчатка ланцетовидная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Stellaria_holostea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Stellaria_holostea001.jpg/275px-Stellaria_holostea001.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stellaria_longifolia",
    "rusName": "Звездчатка длиннолистная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Stellaria_longifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Stellaria_longifolia_%283816685972%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stellaria_media",
    "rusName": "Звездчатка средняя",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Stellaria_media",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/05/Kaldari_Stellaria_media_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/StellariaMedia001.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Stellaria_media_MHNT.BOT.2008.1.33.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "надземную, зелёную часть"
  },
  {
    "latName": "Stellaria_nemorum",
    "rusName": "Звездчатка дубравная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Stellaria_nemorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/af/1024_Hain-Sternmiere_%28Stellaria_nemorum%29_am_Naturlehrweg_Seebachtal-2453.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3d/Red_campion_close_700.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stellaria_palustris",
    "rusName": "Звездчатка болотная",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Stellaria_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Stellaria_palustris.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Stratiotes_aloides",
    "rusName": "Телорез",
    "family": "Hydrocharitaceae",
    "url": "https://ru.wikipedia.org/wiki/Stratiotes_aloides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/82/Stratiotes_aloides_LC0258.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Subularia_aquatica",
    "rusName": "Шильник водяной",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Subularia_aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/%28MHNT%29-_Cardamine_flexuosa_inflorescence.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Subularia_aquatica_Sturm30.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Succisa_pratensis",
    "rusName": "Сивец",
    "family": "Dipsacaceae",
    "url": "https://ru.wikipedia.org/wiki/Succisa_pratensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ed/SuccisaPratensis2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e0/Succisa_pratensis.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Succisa_pratensis01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Succisa_pratensis3_W.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/04/Succisa_pratensis4_W.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/92/Succisa_pratensis_-_Apis_mellifera_mellifera_-_Keila2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Swertia_perennis",
    "rusName": "Сверция многолетняя",
    "family": "Gentianaceae",
    "url": "https://ru.wikipedia.org/wiki/Swertia_perennis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Sumpfenzian_01.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Swida_alba",
    "rusName": "Дёрен белый",
    "family": "Cornaceae",
    "url": "https://ru.wikipedia.org/wiki/Swida_alba",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Swida_alba_01.jpg/275px-Swida_alba_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Cornus_Alba_%D0%94%D0%B5%D1%80%D0%B5%D0%BD_%D0%B1%D0%B5%D0%BB%D1%8B%D0%B9_%D0%9F%D0%BB%D0%BE%D0%B4%D1%8B.jpg/260px-Cornus_Alba_%D0%94%D0%B5%D1%80%D0%B5%D0%BD_%D0%B1%D0%B5%D0%BB%D1%8B%D0%B9_%D0%9F%D0%BB%D0%BE%D0%B4%D1%8B.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Cornus_alba_-_WWT_London_Wetland_Centre_4.jpg/120px-Cornus_alba_-_WWT_London_Wetland_Centre_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/20151225Cornus_alba3.jpg/120px-20151225Cornus_alba3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Cornus_alba_a2.jpg/120px-Cornus_alba_a2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Cornus_alba_10_ies.jpg/120px-Cornus_alba_10_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/%D0%94%D0%B5%D1%80%D0%B5%D0%BD_%D0%B1%D0%B5%D0%BB%D1%8B%D0%B9_%2816017605041%29.jpg/120px-%D0%94%D0%B5%D1%80%D0%B5%D0%BD_%D0%B1%D0%B5%D0%BB%D1%8B%D0%B9_%2816017605041%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Cornus_alba_%27sibirica%271.jpg/120px-Cornus_alba_%27sibirica%271.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Cornus_alba_Elegantissima_A.jpg/120px-Cornus_alba_Elegantissima_A.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Cornus_elagantiss_kz.jpg/120px-Cornus_elagantiss_kz.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Symphoricarpos_albus",
    "rusName": "Снежноягодник белый",
    "family": "Caprifoliaceae",
    "url": "https://ru.wikipedia.org/wiki/Symphoricarpos_albus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/59/Korina_2013-06-27_Symphoricarpos_albus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Snowberries_%2850820547861%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Snowberry_%2843537723244%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Symphoricarpos_albus_%2836645954286%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Symphoricarpos_albus_003.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Symphoricarpos_albus_3054.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Symphytum_caucasicum",
    "rusName": "Окопник кавказский",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Symphytum_caucasicum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Ipomoea_tricolor-1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Symphytum_caucasicum_-_Curtis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Symphytum_officinale",
    "rusName": "Окопник лекарственный",
    "family": "Boraginaceae",
    "url": "https://ru.wikipedia.org/wiki/Symphytum_officinale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Symphytum_officinale_01.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья"
  },
  {
    "latName": "Syringa_josikaea",
    "rusName": "Сирень венгерская",
    "family": "Oleaceae",
    "url": "https://ru.wikipedia.org/wiki/Syringa_josikaea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Syringa-josikaea-flowering.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/3b/Syringa_josiflexa_A.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Syringa_vulgaris",
    "rusName": "Сирень обыкновенная",
    "family": "Oleaceae",
    "url": "https://ru.wikipedia.org/wiki/Syringa_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Fasciated_Lilac.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/11/FliederblueteH2a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/FliederbluetreWeissH1c.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Syr.vulg.Charles_Joly.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Syringa.vulgaris%2801%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Tanacetum_millefolium",
    "rusName": "Пижма тысячелистная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Tanacetum_millefolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Tanacetum_millefolium_2015-07-15_4473.JPG/275px-Tanacetum_millefolium_2015-07-15_4473.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Tanacetum_vulgare",
    "rusName": "Пижма обыкновенная",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Tanacetum_vulgare",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Boerenwormkruid_in_berm.jpg/239px-Boerenwormkruid_in_berm.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Tanacetum_vulgare_-_harilik_soolikarohi_Keilas2.jpg/220px-Tanacetum_vulgare_-_harilik_soolikarohi_Keilas2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Tanacetum_vulgare_-_harilik_soolikarohi_Keilas1.jpg/220px-Tanacetum_vulgare_-_harilik_soolikarohi_Keilas1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Rainfarn_mit_Ameisen.jpeg/220px-Rainfarn_mit_Ameisen.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Illustration_Tanacetum_vulgare0.jpg/220px-Illustration_Tanacetum_vulgare0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Taraxacum_officinale",
    "rusName": "Одуванчик лекарственный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Taraxacum_officinale",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a8/Bombus_ruderarius_-_Taraxacum_officinale_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Dandelion12.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Dandelion_flower_head_%282008-05-04_pic02%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Nottuln%2C_Bauerschaft_Stevern_--_2020_--_7049.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Plate_of_Wehani_rice_with_sauteed_dandelion_greens.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья. Цветки на варенье. Поджаренные корни — суррогат кофе."
  },
  {
    "latName": "Teucrium_scordium",
    "rusName": "Дубровник чесночный",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Teucrium_scordium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2a/Teucrium_scordium1_eF.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья - пряность"
  },
  {
    "latName": "Thalictrum_aquilegiifolium",
    "rusName": "Василисник водосборолистный",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Thalictrum_aquilegiifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6a/Thalictrum_aquilegiifolium_2_RF.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Thalictrum_aquilegiifolium_MHNT.BOT.2004.0.487.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thalictrum_flavum",
    "rusName": "Василистник жёлтый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Thalictrum_flavum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Thalictrum_flavum0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thalictrum_lucidum",
    "rusName": "Василистник светлый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Thalictrum_lucidum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/68/Thalictrum_lucidum_sl7.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thalictrum_minus",
    "rusName": "Василистник малый",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Thalictrum_minus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/04/Thalictrum_minus_1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "молодые побеги"
  },
  {
    "latName": "Thalictrum_simplex",
    "rusName": "Василистник простой",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Thalictrum_simplex",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/5058-Thalictrum_simplex-Be%C5%A1e%C5%88ov%C3%A1-7.05.JPG/275px-5058-Thalictrum_simplex-Be%C5%A1e%C5%88ov%C3%A1-7.05.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thelypteris_palustris",
    "rusName": "Телиптерис болотный",
    "family": "Thelypteridaceae",
    "url": "https://ru.wikipedia.org/wiki/Thelypteris_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Silver-fern.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/ff/Thelypteris_palustris_02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thesium_ebracteatum",
    "rusName": "Ленец бесприцветничковый",
    "family": "Santalaceae",
    "url": "https://ru.wikipedia.org/wiki/Thesium_ebracteatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2f/P%C3%BCst-linalehik_%28Thesium_ebracteatum%29_Keila1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thladiantha_dubia",
    "rusName": "Тладианта сомнительная",
    "family": "Cucurbitaceae",
    "url": "https://ru.wikipedia.org/wiki/Thladiantha_dubia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/La_Belgique_horticole_pl26_Thladiantha_dubia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dc/Rouge_vif_d%27%C3%89tampes-Cucurbita_maxima-01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thlaspi_arvense",
    "rusName": "Ярутка полевая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Thlaspi_arvense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Thlaspi_arvense.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/d/da/Thlaspiseed.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thlaspi_perfoliatum",
    "rusName": "Ярутка пронзённая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Thlaspi_perfoliatum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e4/Thlaspi_perfoliatum.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thuja_occidentalis",
    "rusName": "Туя западная",
    "family": "Cupressaceae",
    "url": "https://ru.wikipedia.org/wiki/Thuja_occidentalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/e7/Eastern_White_Cedar_%28Thuja_occidentalis%29_-_Algonquin_Provincial_Park_2019-09-20.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Poland._Warsaw._Powsin._Botanical_Garden_097.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7f/Something_out_of_nothing_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/04/Superior_Hiking_Trail_boardwalk.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Thuja_occidentalis.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Thuja_occidentalis_foliage_Wisconsin.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Thymus_serpyllum",
    "rusName": "Тимьян ползучий",
    "family": "Lamiaceae",
    "url": "https://ru.wikipedia.org/wiki/Thymus_serpyllum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Creeping_red_thyme.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/91/Illustration_Thymus_serpyllum0_clean.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Thymus_aa1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4b/Thymus_serp_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Thymus_serpyllum1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Thymus_serpyllum_Sturm57.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Creeping_red_thyme.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/91/Illustration_Thymus_serpyllum0_clean.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Thymus_aa1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4b/Thymus_serp_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/18/Thymus_serpyllum1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Thymus_serpyllum_Sturm57.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и молодые побеги"
  },
  {
    "latName": "Tilia_cordata",
    "rusName": "Липа сердцевидная",
    "family": "Tiliaceae",
    "url": "https://ru.wikipedia.org/wiki/Tilia_cordata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Bombus_hypnorum_-_Tilia_cordata_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Tilia-cordata2.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Tilia_cordata_MHNT.BOT.2004.0.780.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/54/Tilia_platyphyllos_and_T._cordata_leaf_comparison.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Tilia_platyphyllos",
    "rusName": "Липа крупнолистная",
    "family": "Tiliaceae",
    "url": "https://ru.wikipedia.org/wiki/Tilia_platyphyllos",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Tilia_platyphallos_bud.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Tilia_platyphyllos%2801%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Tilia_platyphyllos%2802%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Tilia_platyphyllos_%28Flor%C3%A9e%29_JPG1R.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Tilia_platyphyllos_02.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Tillaea_aquatica",
    "rusName": "Толстянка водная",
    "family": "Crassulaceae",
    "url": "https://ru.wikipedia.org/wiki/Tillaea_aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Crassula_aquatica.jpg/275px-Crassula_aquatica.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Torilis_japonica",
    "rusName": "Пупырник японский",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Torilis_japonica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f2/Torilis_japonica.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Torilis_japonica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Torilis_japonica_blatt.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Torilis_japonica_frucht.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Tragopogon_dubius",
    "rusName": "Козлобородник сомнительный",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Tragopogon_dubius",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/36/Goats_beard_Tragopogon_dubius_close.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Tragopogon_dubius_seedhead.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги с листьями, корни"
  },
  {
    "latName": "Tragopogon_pratensis",
    "rusName": "Козлобородник луговой",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Tragopogon_pratensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/66/Chicory_flower_001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Tragopogon_pratense_2004-05-30_JOF.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Tragopogon_pratensis_flower_-_Niitv%C3%A4lja.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/86/Tragopogon_pratensis_subsp._pratensis_bgiu.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые побеги с листьями, корни"
  },
  {
    "latName": "Trapa_natans",
    "rusName": "Рогульник плавающий",
    "family": "Trapaceae",
    "url": "https://ru.wikipedia.org/wiki/Trapa_natans",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Water_caltrop_on_lake.JPG/275px-Water_caltrop_on_lake.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Illustration_Trapa_natans1.jpg/220px-Illustration_Trapa_natans1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Kotewka_orzech_wodny.JPG/220px-Kotewka_orzech_wodny.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/%D0%92%D0%BE%D0%B4%D1%8F%D0%BD%D0%BE%D0%B9_%D0%BE%D1%80%D0%B5%D1%85_%D1%81_%D0%BF%D0%BB%D0%BE%D0%B4%D0%BE%D0%BC_%28made_by_Paliienko_Konstantin%29.jpg/220px-%D0%92%D0%BE%D0%B4%D1%8F%D0%BD%D0%BE%D0%B9_%D0%BE%D1%80%D0%B5%D1%85_%D1%81_%D0%BF%D0%BB%D0%BE%D0%B4%D0%BE%D0%BC_%28made_by_Paliienko_Konstantin%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Trapa_natans_nut.jpg/240px-Trapa_natans_nut.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Ядро плода"
  },
  {
    "latName": "Tribulus_terrestris",
    "rusName": "Якорцы стелющиеся",
    "family": "Zygophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Tribulus_terrestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/47/Gokhru_%28Pakhra%29.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a7/Starr_030612-0067_Tribulus_terrestris.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Starr_030612-0070_Tribulus_terrestris.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Tribulus_Terrestris_Germinating.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Tribulus_terrestris_%28Family_Zygophyllaceae%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Tribulus_terrestris_growing_on_a_beach_%28Philippines%29_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trientalis_europaea",
    "rusName": "Седмичник европейский",
    "family": "Primulaceae",
    "url": "https://ru.wikipedia.org/wiki/Trientalis_europaea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/TrientalisEuropaea.jpg/275px-TrientalisEuropaea.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Trientalis_europaea_skogsstj%C3%A4rna.jpg/220px-Trientalis_europaea_skogsstj%C3%A4rna.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_alpestre",
    "rusName": "Клевер альпийский",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_alpestre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/75/Trifolium_alpestre.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_angustifolium",
    "rusName": "Клевер узколистный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_angustifolium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Trifolium_angustifolium_FlowersCloseup_DehesaBoyaldePuertollano.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Trifolium_angustifolium_Hearst.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_arvense",
    "rusName": "Клевер пашенный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_arvense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/63/Trif_arvense042.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Trifolium_arvense_inflorescence_-_Kulna.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_aureum",
    "rusName": "Клевер золотистый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_aureum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/eb/Trifolium_aureum_W.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_campestre",
    "rusName": "Клевер полевой",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_campestre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/39/Flower6.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Trifolium_campestre_-_Flickr_-_Kevin_Thiele.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Trifolium_campestre_300907.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_fragiferum",
    "rusName": "Клевер земляничный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_fragiferum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/TrifoliumFragiferum2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_hirtum",
    "rusName": "Клевер мохнатый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_hirtum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Trifoliumhirtum3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_hybridum",
    "rusName": "Клевер гибридный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_hybridum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Trifolium_hybridum_-_roosa_%28rootsi%29_ristik.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_lupinaster",
    "rusName": "Клевер люпиновый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_lupinaster",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8b/Curtis%27s_botanical_magazine_%28No._879%29_%288469934133%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_medium",
    "rusName": "Клевер средний",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_medium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/08/Bombus_terrestris_-_Trifolium_medium_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Trifolium_medium_-_Niitv%C3%A4lja.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_montanum",
    "rusName": "Клевер горный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_montanum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Irish_clover.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Trifolium_montanum_-_Niitv%C3%A4lja.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_pratense",
    "rusName": "Клевер луговой",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_pratense",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Bombus_schrencki_-_Trifolium_pratense_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9d/Trifolium_pratense_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Trifolium_pratense_-_Keila2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/64/Trifolium_pratense_04.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/b/b6/Trifolium_pratense_albiflorum_-_Keila.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Листья и молодые нераспустившиеся цветочные головки"
  },
  {
    "latName": "Trifolium_repens",
    "rusName": "Клевер ползучий",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_repens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/73/2020_year._Herbarium._Clover._img-001.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/4-leaf_clover.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/8/8d/79_Trifolium_repens_L.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Starr_070313-5645_Trifolium_repens.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Trifolium_repens_-_white_clover_on_way_from_Govindghat_to_Gangria_at_Valley_of_Flowers_National_Park_-_during_LGFC_-_VOF_2019_%281%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Trifolium_repens_White_Clover%2C_Dutch_Clover_at_Thimphu_during_LGFC_-_Bhutan_2019_%281%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_rubens",
    "rusName": "Клевер красный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_rubens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Trifolium_rubens_-_img_25427.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trifolium_spadiceum",
    "rusName": "Клевер тёмно-каштановый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Trifolium_spadiceum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Trifolium_spadiceum_-_Niitv%C3%A4lja.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Triglochin_maritimum",
    "rusName": "Триостренник приморский",
    "family": "Juncaginaceae",
    "url": "https://ru.wikipedia.org/wiki/Triglochin_maritimum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Trma4_003_lvp.jpg/266px-Trma4_003_lvp.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Triglochin_maritima_Sturm4.jpg/220px-Triglochin_maritima_Sturm4.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "в мае-июне. Срезают как спаржу, в пищу идёт как белая подземная часть растения, так и зелёная. Есть предупреждения о ядовитости в сыром виде."
  },
  {
    "latName": "Triglochin_palustre",
    "rusName": "Триостренник болотный",
    "family": "Juncaginaceae",
    "url": "https://ru.wikipedia.org/wiki/Triglochin_palustre",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Triglochium_palustris_BotGartBln310505.JPG/275px-Triglochium_palustris_BotGartBln310505.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Illustration_Triglochin_palustre0.jpg/220px-Illustration_Triglochin_palustre0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Triticum_aestivum",
    "rusName": "Пшеница мягкая",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Triticum_aestivum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Viglaska_slovaque.jpg/275px-Viglaska_slovaque.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Illustration_Triticum_aestivum1.jpg/220px-Illustration_Triticum_aestivum1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Weizenfeld.jpg/220px-Weizenfeld.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Зерно"
  },
  {
    "latName": "Trollius_europaeus",
    "rusName": "Купальница европейская",
    "family": "Ranunculaceae",
    "url": "https://ru.wikipedia.org/wiki/Trollius_europaeus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8a/Creeping_butercup_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/29/Trollius_europaeus_flower_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Trollius_europaeus_habitat_-_wooded_meadow_in_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Trollius_europaeus_seed_head_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Trommsdorfia_maculata",
    "rusName": "Пазник крапчатый",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Trommsdorfia_maculata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Hypochaeris_maculata_%28Flecken-Ferkelkraut%29_3022_IMG.JPG/275px-Hypochaeris_maculata_%28Flecken-Ferkelkraut%29_3022_IMG.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Hypochaeris_maculata_%28Flecken-Ferkelkraut%29_IMG_9921.JPG/220px-Hypochaeris_maculata_%28Flecken-Ferkelkraut%29_IMG_9921.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Tulipa_sylvestris",
    "rusName": "Тюльпан лесной",
    "family": "Liliaceae",
    "url": "https://ru.wikipedia.org/wiki/Tulipa_sylvestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Illustration_Tulipa_sylvestris0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Tulipa_sylvestris_MHNT.BOT.2015.34.30.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Turritis_glabra",
    "rusName": "Вяжечка гладкая",
    "family": "Brassicaceae",
    "url": "https://ru.wikipedia.org/wiki/Turritis_glabra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/2017_05_09_Turritis_glabra_Blueten.jpg/275px-2017_05_09_Turritis_glabra_Blueten.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Turritis_glabra_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v9.jpg/220px-Turritis_glabra_%E2%80%94_Flora_Batava_%E2%80%94_Volume_v9.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Arabis_glabra_5.JPG/220px-Arabis_glabra_5.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Tussilago_farfara",
    "rusName": "Мать-и-мачеха",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Tussilago_farfara",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Coltsfoot.jpg/275px-Coltsfoot.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_6%29_%286972230546%29.jpg/220px-K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_6%29_%286972230546%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Tussilago_farfara20100409_57.jpg/200px-Tussilago_farfara20100409_57.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Tussilago_farfara20100409_07.jpg/256px-Tussilago_farfara20100409_07.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Tussilago-farfara_0018_a.jpg/300px-Tussilago-farfara_0018_a.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Tussilago_farfara_bgiu.jpg/218px-Tussilago_farfara_bgiu.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Typha_angustifolia",
    "rusName": "Рогоз узколистный",
    "family": "Typhaceae",
    "url": "https://ru.wikipedia.org/wiki/Typha_angustifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6d/Typha_angustifolia_%28habitus%29_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Typha_angustifolia_nf.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Корневища. Молодые цветоносные побеги маринуют"
  },
  {
    "latName": "Typha_latifolia",
    "rusName": "Рогоз широколистный",
    "family": "Typhaceae",
    "url": "https://ru.wikipedia.org/wiki/Typha_latifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Grote_lisdoddes_aan_het_water.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0e/Rohrkolben_Typha_1.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/e/e3/Typha_flwrs.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Typha_latifolia_%28Common_bulrush%29_stem_cross_section%2C_Arnhem%2C_the_Netherlands.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Typha_latifolia_02_bgiu.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4d/Typha_latifolia_Finland.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Корневища, Молодые цветоносные побеги, Пыльцу мужских соцветий"
  },
  {
    "latName": "Typha_laxmannii",
    "rusName": "Рогоз Лаксмана",
    "family": "Typhaceae",
    "url": "https://ru.wikipedia.org/wiki/Typha_laxmannii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/ca/Typha_laxmannii_%28fruit%29_1.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Корневища"
  },
  {
    "latName": "Ulmus_laevis",
    "rusName": "Вяз гладкий",
    "family": "Ulmaceae",
    "url": "https://ru.wikipedia.org/wiki/Ulmus_laevis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Barjols2013_007.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/Bole_of_ancient_Ulmus_laevis_in_hedgerow_near_Over_Wallop%2C_England.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/04/Fladderiep_te_Heure_bij_Borculo.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/02/HW_laevis_leaf.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/2a/Hebden_U._laevis_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Kopeci_szil_2011.11.28..jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Lewisham_elm_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/Llandegfan_Elm_tree.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ulmus_minor",
    "rusName": "Вяз малый",
    "family": "Ulmaceae",
    "url": "https://ru.wikipedia.org/wiki/Ulmus_minor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/2d/Barjols_16_Villesequelande_elm_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d3/Biscarrosse_Elm_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/43/Blismes_elm_2007.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d2/East_Coker_elm%2C_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/94/Elm_tree_in_Sliven_Province%2C_Bulgaria.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Flatford_Lock.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5d/Navajas._Plaza_del_Olmo._Fiestas_Mayores_5.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Navajas._Plaza_del_Olmo_1.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ulmus_pumila",
    "rusName": "Вяз приземистый",
    "family": "Ulmaceae",
    "url": "https://ru.wikipedia.org/wiki/Ulmus_pumila",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Elm_in_beijing.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Image-Ulmus_pumila_%28Ulmus_gobicus%29_in_Gobi_Desert.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Marche_Pesaro_Olmo_siberiano_08L76402G479PU11.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1d/RN_Ulmus_pumila.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/a/a4/Sibirski_brest_pionirski_park.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Tree_Sparrow_with_an_elm_seed_in_its_beak%2C_in_Ukraine.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Ulmus_scabra",
    "rusName": "Вяз шершавый",
    "family": "Ulmaceae",
    "url": "https://ru.wikipedia.org/wiki/Ulmus_scabra",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/RN_Ulmus_glabra_%28alnarp_sweden%29.jpg/275px-RN_Ulmus_glabra_%28alnarp_sweden%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Ulmus_glabra-tr.JPG/275px-Ulmus_glabra-tr.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Ulmus_glabra_001.jpg/275px-Ulmus_glabra_001.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Urtica_dioica",
    "rusName": "Крапива двудомная",
    "family": "Urticaceae",
    "url": "https://ru.wikipedia.org/wiki/Urtica_dioica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/01/Br%C3%A4nn%C3%A4ssla_%28Urtica_Dioica%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d2/Illustration_Urtica_dioica0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/13/Kopiva.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Mariasdorf_Burgenland_2021-08-20_Tschabach_2_Tagpfauenauge_Aglais_io_Brennesseln_Urtica.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Stinging_Nettle_Leaf_Detail.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья и побеги"
  },
  {
    "latName": "Urtica_kioviensis",
    "rusName": "Крапива киевская",
    "family": "Urticaceae",
    "url": "https://ru.wikipedia.org/wiki/Urtica_kioviensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Marsh_nettles%2C_Bure_Marshes_nature_reserve_-_geograph.org.uk_-_402616.jpg/275px-Marsh_nettles%2C_Bure_Marshes_nature_reserve_-_geograph.org.uk_-_402616.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Urtica_urens",
    "rusName": "Крапива жгучая",
    "family": "Urticaceae",
    "url": "https://ru.wikipedia.org/wiki/Urtica_urens",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Urtica_urens_002.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "листья и молодые стебли"
  },
  {
    "latName": "Utricularia_australis",
    "rusName": "Пузырчатка южная",
    "family": "Lentibulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Utricularia_australis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0b/Alpina_flower.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/69/Eremophila_abietina.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/87/Kowhai_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Utriculariaaustralis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vaccaria_hispanica",
    "rusName": "Тысячеголов",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Vaccaria_hispanica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Illustration_Vaccaria_hispanica0.jpg/236px-Illustration_Vaccaria_hispanica0.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vaccinium_myrtillus",
    "rusName": "Черника",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Vaccinium_myrtillus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/203_Vaccinum_myrtillus_L.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ad/BilberriesBig.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/52/Hands_scooping_up_fresh_bilberries_picked_in_Tuntorp.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Llusi_duon_bach.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1f/Vaccinium_myrtillus_4858.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Ягоды"
  },
  {
    "latName": "Vaccinium_uliginosum",
    "rusName": "Голубика",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Vaccinium_uliginosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/98/Blueberries_in_Eastern_Siberia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bb/Blueberries_on_the_branches_of_a_bush.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/84/Bog_bilberry_liquor_and_Korean_rhododendron_liquor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Vaccinium_ulig.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Vaccinium_uliginosum_fruit.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Ягоды"
  },
  {
    "latName": "Vaccinium_vitis-idaea",
    "rusName": "Брусника",
    "family": "Ericaceae",
    "url": "https://ru.wikipedia.org/wiki/Vaccinium_vitis-idaea",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Tytteb%C3%A6r.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Vaccinium_vitis-idaea_%28flowering%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Vaccinium_vitis-idaea_20060824_003.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Ягоды"
  },
  {
    "latName": "Valeriana_officinalis",
    "rusName": "Валериана лекарственная",
    "family": "Valerianaceae",
    "url": "https://ru.wikipedia.org/wiki/Valeriana_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/ValerianaOfficinalisAgg.jpg/266px-ValerianaOfficinalisAgg.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_47%29_%286972244348%29.jpg/220px-K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_%28Plate_47%29_%286972244348%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Valeriana_officinalis0.jpg/220px-Valeriana_officinalis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Valeriana_officinalis_-_Niitv%C3%A4lja.jpg/220px-Valeriana_officinalis_-_Niitv%C3%A4lja.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Valerianella_locusta",
    "rusName": "Полевой салат",
    "family": "Valerianaceae",
    "url": "https://ru.wikipedia.org/wiki/Valerianella_locusta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b3/Ackersalat02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/62/Illustration_Valerianella_locusta0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/40/Mache.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/61/Mache_Blooming.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a2/Mache_in_garden.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые прикорневые листья"
  },
  {
    "latName": "Veratrum_lobelianum",
    "rusName": "Чемерица Лобеля",
    "family": "Melanthiaceae",
    "url": "https://ru.wikipedia.org/wiki/Veratrum_lobelianum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f0/Veratrum_lobelianum_-_inflorescence.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Verbascum_blattaria",
    "rusName": "Коровяк тараканий",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Verbascum_blattaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Verbascum_blattaria_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7d/Verbascum_blattaria_3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/8f/Verbascum_blattaria_MHNT.BOT.2005.0.963.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Verbascum_densiflorum",
    "rusName": "Коровяк высокий",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Verbascum_densiflorum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Scrophularia_vernalis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f7/Verbascum_densiflorum_%27dense-flowered_mullein%27_2007-06-02_%28plant%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Verbascum_lychnitis",
    "rusName": "Коровяк мучнистый",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Verbascum_lychnitis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Scrophularia_vernalis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/0a/Verbascum_lychnitis_%28flowers%29_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4f/Verbascum_lychnitis_RF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Verbascum_nigrum",
    "rusName": "Коровяк чёрный",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Verbascum_nigrum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3f/Scrophularia_vernalis0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6a/Verbascum_nigrum_02.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Verbascum_phlomoides",
    "rusName": "Коровяк лекарственный",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Verbascum_phlomoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Verbascum_phlomoides_2018_G1.jpg/255px-Verbascum_phlomoides_2018_G1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Illustration_Verbascum_phlomoides0.jpg/220px-Illustration_Verbascum_phlomoides0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Bouillon_blanc.jpg/220px-Bouillon_blanc.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Verbascum_phoeniceum",
    "rusName": "Коровяк фиолетовый",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Verbascum_phoeniceum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Verbascum_phoeniceum2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Verbascum_thapsus",
    "rusName": "Коровяк обыкновенный",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Verbascum_thapsus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/3a/Starr_040723-0030_Verbascum_thapsus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c4/Starr_040723-0032_Verbascum_thapsus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Starr_040723-0074_Verbascum_thapsus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4e/Starr_040723-0260_Verbascum_thapsus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/26/Starr_040723-0267_Verbascum_thapsus.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e5/Verbasci_flos_dried.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/99/Verbascum_thapsus_6.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Verbena_officinalis",
    "rusName": "Вербена лекарственная",
    "family": "Verbenaceae",
    "url": "https://ru.wikipedia.org/wiki/Verbena_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/38/Eisenkraut%2C_Passau.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/d/df/Hierabotane_Verbenaca.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Verbena_officinalis_sl2.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Надземная часть в период цветения - суррогат чая"
  },
  {
    "latName": "Veronica_agrestis",
    "rusName": "Вероника пашенная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_agrestis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/04/Veronica_agrestis_Sturm48.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Veronica_agrestis_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_anagallis-aquatica",
    "rusName": "Вероника ключевая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_anagallis-aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Veronica_anagallis-aquatica_5.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_arvensis",
    "rusName": "Вероника полевая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0c/Veronica_arvensis_7998.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_beccabunga",
    "rusName": "Вероника поточная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_beccabunga",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Veronica_beccabunga_plant2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_chamaedrys",
    "rusName": "Вероника дубравная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_chamaedrys",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b2/Ehrenpreis-JR-T20-2021-05-29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_dillenii",
    "rusName": "Вероника Дилления",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_dillenii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/72/Veronica_dillenii2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/56/Veronica_dillenii_kz01.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_hederifolia",
    "rusName": "Вероника плющелистная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_hederifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1e/Veronica_hederifolia_Nashville.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_incana",
    "rusName": "Вероника седая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_incana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f3/Veronica_incana.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/83/Veronica_spicata_subsp._incana_Silberteppich_2020-06-23_0836.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_longifolia",
    "rusName": "Вероника длиннолистная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_longifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/0/07/Pseudolysimachion-longifolium-spikes.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_officinalis",
    "rusName": "Вероника лекарственная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_officinalis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/bf/Gypsyweed.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3a/Heath_Speedwell_%28Veronica_officinalis%29_in_Pennsylvania.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/71/Veronica_officinalis-53.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Молодые листья"
  },
  {
    "latName": "Veronica_opaca",
    "rusName": "Вероника тусклая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_opaca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Veronica_opaca_Sturm49.jpg/267px-Veronica_opaca_Sturm49.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_peregrina",
    "rusName": "Вероника иноземная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_peregrina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Starr_080415-4014_Veronica_peregrina.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_persica",
    "rusName": "Вероника персидская",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_persica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/7c/VeronicaPersicaFruit.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/Veronica_persica_060403Fw.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4a/Veronica_persica_183115889.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_polita",
    "rusName": "Вероника скромная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_polita",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Veronica_polita_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_prostrata",
    "rusName": "Вероника простёртая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_prostrata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3a/Veronica_prostrata_subsp._scheereri_%28habitus%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_scutellata",
    "rusName": "Вероника щитковая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_scutellata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b8/Veronica_scutellata_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_serpyllifolia",
    "rusName": "Вероника тимьянолистная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_serpyllifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/be/Tijmereprijs_plant_%28Veronica_serpyllifolia%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/ea/Veronica_serpyllifolia_W.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Veronica_serpyllifolia_pods.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_spicata",
    "rusName": "Вероника колосистая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_spicata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/bb/Bombus_norvegicus_-_Veronica_spicata_-_Keila.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_spuria",
    "rusName": "Вероника ненастоящая",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_spuria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Veronica_spuria_107583210.jpg/275px-Veronica_spuria_107583210.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_teucrium",
    "rusName": "Вероника широколистная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_teucrium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Veronica_teucrium.JPG/275px-Veronica_teucrium.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_triphyllos",
    "rusName": "Вероника трёхлистная",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_triphyllos",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/8/8e/Achetaria_azurea_at_Kadavoor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b9/Veronica_triphyllos_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Veronica_verna",
    "rusName": "Вероника весенняя",
    "family": "Scrophulariaceae",
    "url": "https://ru.wikipedia.org/wiki/Veronica_verna",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Veronica_verna_eF.jpg/275px-Veronica_verna_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viburnum_lantana",
    "rusName": "Калина гордовина",
    "family": "Viburnaceae",
    "url": "https://ru.wikipedia.org/wiki/Viburnum_lantana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/Viburnum_lantana_a1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/9b/Wolliger_Schneeball_mit_zeitgleich_roten_unreifen_und_schwarzen_reifen_Fr%C3%BCchten.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viburnum_opulus",
    "rusName": "Калина обыкновенная",
    "family": "Viburnaceae",
    "url": "https://ru.wikipedia.org/wiki/Viburnum_opulus",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Snowball_flowers_%2813985050634%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/Viburnum-Siberia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/70/Viburnum_01.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Viburnum_opulus.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vicia_angustifolia",
    "rusName": "Горошек узколистный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_angustifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Vicia_sativa_ssp_nigra.jpeg/275px-Vicia_sativa_ssp_nigra.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/%28MHNT%29_Vicia_angustifolia_-_flowers.jpg/220px-%28MHNT%29_Vicia_angustifolia_-_flowers.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vicia_cassubica",
    "rusName": "Горошек кашубский",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_cassubica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Vicia_cassubica_W2.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vicia_cracca",
    "rusName": "Горошек мышиный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_cracca",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9f/Bombus_sylvarum_-_Vicia_cracca_-_Keila.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/Tufted_vetch_close_800.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Vicia_cracca_MHNT.BOT.2005.0.968.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vicia_hirsuta",
    "rusName": "Горошек волосистый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_hirsuta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/%28MHNT%29_Vicia_angustifolia_-_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/b/b7/%28MHNT%29_Vicia_hirsuta_-_Plant_habit.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Во время Первой мировой войны использовалось в пищу в Германии."
  },
  {
    "latName": "Vicia_sepium",
    "rusName": "Горошек заборный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_sepium",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/%28MHNT%29_Vicia_angustifolia_-_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fb/Vicia_sepium1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Vicia_sepium_-_aed-hiirehernes.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vicia_sylvatica",
    "rusName": "Горошек лесной",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_sylvatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/%28MHNT%29_Vicia_angustifolia_-_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Vicia_sylvatica.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vicia_tenuifolia",
    "rusName": "Горошек тонколистный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_tenuifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Vicia_dalmatica_140506.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6f/Vicia_spp_Sturm4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Vicia_tenuifolia_%2827260895200%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/79/Vicia_tenuifolia_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/d8/Vicia_tenuifolia_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e2/Vicia_tenuifolia_4.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e8/Vicia_tenuifolia_Herbar.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vicia_tetrasperma",
    "rusName": "Горошек четырёхсемянный",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_tetrasperma",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/0/0d/%28MHNT%29_Vicia_angustifolia_-_flowers.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/16/Vicia_tetrasperma_W.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vicia_villosa",
    "rusName": "Горошек мохнатый",
    "family": "Fabaceae",
    "url": "https://ru.wikipedia.org/wiki/Vicia_villosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Hairy_Vetch%2C_Vicia_villosa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/33/Vicia_villosa.jpeg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vinca_minor",
    "rusName": "Барвинок малый",
    "family": "Apocynaceae",
    "url": "https://ru.wikipedia.org/wiki/Vinca_minor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/21/Vinca_major-minor_margins.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/af/Vinca_minor_%27Argenteovariegata%27_Barwinek_pospolity_2017-10-15_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5b/Vinca_minor_Nashville.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/8/82/Vinca_minor_in_Latvia.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/74/Vinca_minor_patch_MN_2007.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Vincetoxicum_hirundinaria",
    "rusName": "Ластовень лекарственный",
    "family": "Asclepiadaceae",
    "url": "https://ru.wikipedia.org/wiki/Vincetoxicum_hirundinaria",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/9a/Apocynaceae_-_Vincetoxicum_hirundinaria.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Asclepias_curassavica12.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/e/e9/Vincetoxicum-hirundinaria-seeds.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Vincetoxicum_hirundinaria_MHNT.BOT.2009.17.21.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Vincetoxicum_hirundinaria_sl3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_arvensis",
    "rusName": "Фиалка полевая",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_arvensis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/96/%28MHNT%29_Viola_arvensis_-_flower.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/a5/%28MHNT%29_Viola_arvensis_-_flower_and_leaves.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_canina",
    "rusName": "Фиалка собачья",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_canina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Viola_canina.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_collina",
    "rusName": "Фиалка холмовая",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_collina",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Viola_collina_%281%29.jpg/275px-Viola_collina_%281%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_epipsila",
    "rusName": "Фиалка лысая",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_epipsila",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/96/Viola_epipsila_5773.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_hirta",
    "rusName": "Фиалка опушённая",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_hirta",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Violahirta.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_mirabilis",
    "rusName": "Фиалка удивительная",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_mirabilis",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/Viola_mirabilis1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_montana",
    "rusName": "Фиалка высокая",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_montana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Viola_elatior1.JPG/275px-Viola_elatior1.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_odorata",
    "rusName": "Фиалка душистая",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_odorata",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Viola_odorata4_ies.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/7/7a/Viola_odorata_fg01.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Viole.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/4/41/VioletteBlanche.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_palustris",
    "rusName": "Фиалка болотная",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/7/78/Viola_palustris_6796.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Viola_palustris_6798.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_persicifolia",
    "rusName": "Фиалка персиколистная",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_persicifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/57/Viola_stagnina2_eF.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_reichenbachiana",
    "rusName": "Фиалка Рейхенбаха",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_reichenbachiana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/c/c8/Viola_reichenbachiana_LC0128.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/2/24/Viola_reichenbachiana_MHNT.BOT.2015.34.44.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_riviniana",
    "rusName": "Фиалка Ривинуса",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_riviniana",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ae/Viola_riviniana-01_%28xndr%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_rupestris",
    "rusName": "Фиалка песчаная",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_rupestris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/2/22/Viola_rupestris_%28Sand-Veilchen%29_IMG_7264.JPG"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_selkirkii",
    "rusName": "Фиалка Селькирка",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_selkirkii",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Viola_selkirkii_flower_front_1_AB.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_tricolor",
    "rusName": "Фиалка трёхцветная",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_tricolor",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/14/Viola_tricolor.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/1/1c/Viola_tricolor_%28flower%29.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f8/Viola_tricolor_LC0041.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/a/ab/Viola_tricolor_maritima_kz3.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c0/Violatricolorarvensis.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viola_uliginosa",
    "rusName": "Фиалка топяная",
    "family": "Violaceae",
    "url": "https://ru.wikipedia.org/wiki/Viola_uliginosa",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/a6/Dahlia_redoute.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/3/32/Viola_uliginosa_Sturm53.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viscaria_vulgaris",
    "rusName": "Смолка клейкая",
    "family": "Caryophyllaceae",
    "url": "https://ru.wikipedia.org/wiki/Viscaria_vulgaris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/5/58/Lychnis_viscaria1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/9/93/Silene_viscaria_Seed.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/6/6b/Silene_viscaria_Seed_Capsule_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f9/Silene_viscaria_Seed_Capsule_2.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/5a/Silene_viscaria_Seed_Capsule_Whorl.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Viscum_album",
    "rusName": "Омела белая",
    "family": "Viscaceae",
    "url": "https://ru.wikipedia.org/wiki/Viscum_album",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ea/Each_arrow_overshot_his_head_by_Elmer_Boyd_Smith.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/f6/Mistletoe.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/d/dd/Mistletoe_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c9/Mistletoe_in_France.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/c/c5/Mistletoe_in_White_Poplar_1.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/f/fe/Mistletoe_seed_on_twig.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Visnaga_daucoides",
    "rusName": "Амми зубная",
    "family": "Apiaceae",
    "url": "https://ru.wikipedia.org/wiki/Visnaga_daucoides",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Ammi_Visnaga_%28289632722%29.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Wolffia_arrhiza",
    "rusName": "Вольфия бескорневая",
    "family": "Lemnaceae",
    "url": "https://ru.wikipedia.org/wiki/Wolffia_arrhiza",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/a/ac/Wolffia_arrhiza.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Xanthium_spinosum",
    "rusName": "Дурнишник колючий",
    "family": "Asteraceae",
    "url": "https://ru.wikipedia.org/wiki/Xanthium_spinosum",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/9/97/Xanthium_spinosum.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/6/6e/Xanthium_spinosum_sl18.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/5/50/Xanthium_spinosum_sl25.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Zannichellia_palustris",
    "rusName": "Дзанникеллия болотная",
    "family": "Zannichelliaceae",
    "url": "https://ru.wikipedia.org/wiki/Zannichellia_palustris",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/e/ec/Elodea_canadensis.jpeg",
      "https://upload.wikimedia.org/wikipedia/commons/2/27/ZannichelliaPalustris3.jpg"
    ],
    "isEdible": false,
    "edibleDesc": null
  },
  {
    "latName": "Zea_mays",
    "rusName": "Кукуруза",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Zea_mays",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Maispflanze.jpg/275px-Maispflanze.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Zea_mays_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-283.jpg/220px-Zea_mays_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-283.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/SweetcornFlower.JPG/300px-SweetcornFlower.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Maiskolben_auf_dem_Feld.jpg/150px-Maiskolben_auf_dem_Feld.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/%D0%9A%D1%83%D0%BA%D1%83%D1%80%D1%83%D0%B7%D0%B03.jpg/170px-%D0%9A%D1%83%D0%BA%D1%83%D1%80%D1%83%D0%B7%D0%B03.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Zea_mays_fraise_MHNT.BOT.2011.18.21.jpg/330px-Zea_mays_fraise_MHNT.BOT.2011.18.21.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Uno_de_los_primeros_imagines_europeos_de_maiz.jpg/220px-Uno_de_los_primeros_imagines_europeos_de_maiz.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Cereal_con_yogur.jpg/220px-Cereal_con_yogur.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Flickr_-_cyclonebill_-_Tortilla_med_kylling%2C_guacamole%2C_peberfrugt%2C_agurk%2C_r%C3%B8dl%C3%B8g%2C_tomat%2C_ost_og_salsa.jpg/220px-Flickr_-_cyclonebill_-_Tortilla_med_kylling%2C_guacamole%2C_peberfrugt%2C_agurk%2C_r%C3%B8dl%C3%B8g%2C_tomat%2C_ost_og_salsa.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Popcorn02.jpg/220px-Popcorn02.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Zea_mays.jpg/220px-Zea_mays.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Зёрна"
  },
  {
    "latName": "Zizania_aquatica",
    "rusName": "Цицания водная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Zizania_aquatica",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Zizania_aquatica_01.jpg/199px-Zizania_aquatica_01.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Ziziania_aquatica_-_%D0%B7%D0%B5%D1%80%D0%BD%D0%B0_%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B3%D0%BE_%28%D0%B4%D0%B8%D0%BA%D0%BE%D0%B3%D0%BE%29_%D1%80%D0%B8%D1%81%D0%B0.jpg/220px-Ziziania_aquatica_-_%D0%B7%D0%B5%D1%80%D0%BD%D0%B0_%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B3%D0%BE_%28%D0%B4%D0%B8%D0%BA%D0%BE%D0%B3%D0%BE%29_%D1%80%D0%B8%D1%81%D0%B0.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/%D0%92%D0%B0%D1%80%D0%B5%D0%BD%D1%8B%D0%B9_%D0%B4%D0%B8%D0%BA%D0%B8%D0%B9_%D1%80%D0%B8%D1%81.jpg/212px-%D0%92%D0%B0%D1%80%D0%B5%D0%BD%D1%8B%D0%B9_%D0%B4%D0%B8%D0%BA%D0%B8%D0%B9_%D1%80%D0%B8%D1%81.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/%D0%A0%D0%B0%D0%B7%D0%BD%D0%BE%D0%B2%D0%B8%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%B8_%D1%80%D0%B8%D1%81%D0%BE%D0%B2%D0%BE%D0%B9_%D0%BA%D1%80%D1%83%D0%BF%D1%8B_%D0%B2_%D1%80%D0%BE%D0%B7%D0%BD%D0%B8%D1%87%D0%BD%D0%BE%D0%B9_%D0%BF%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B5_%28%D0%B2_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%29_03.JPG/220px-%D0%A0%D0%B0%D0%B7%D0%BD%D0%BE%D0%B2%D0%B8%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%B8_%D1%80%D0%B8%D1%81%D0%BE%D0%B2%D0%BE%D0%B9_%D0%BA%D1%80%D1%83%D0%BF%D1%8B_%D0%B2_%D1%80%D0%BE%D0%B7%D0%BD%D0%B8%D1%87%D0%BD%D0%BE%D0%B9_%D0%BF%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B5_%28%D0%B2_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%29_03.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/%D0%A0%D0%B0%D0%B7%D0%BD%D0%BE%D0%B2%D0%B8%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%B8_%D1%80%D0%B8%D1%81%D0%BE%D0%B2%D0%BE%D0%B9_%D0%BA%D1%80%D1%83%D0%BF%D1%8B_%D0%B2_%D1%80%D0%BE%D0%B7%D0%BD%D0%B8%D1%87%D0%BD%D0%BE%D0%B9_%D0%BF%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B5_%28%D0%B2_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%29_01.JPG/220px-%D0%A0%D0%B0%D0%B7%D0%BD%D0%BE%D0%B2%D0%B8%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%B8_%D1%80%D0%B8%D1%81%D0%BE%D0%B2%D0%BE%D0%B9_%D0%BA%D1%80%D1%83%D0%BF%D1%8B_%D0%B2_%D1%80%D0%BE%D0%B7%D0%BD%D0%B8%D1%87%D0%BD%D0%BE%D0%B9_%D0%BF%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B5_%28%D0%B2_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%29_01.JPG"
    ],
    "isEdible": true,
    "edibleDesc": "Зёрна, молодые побеги и основания стеблей, корневища"
  },
  {
    "latName": "Zizania_latifolia",
    "rusName": "Цицания широколистная",
    "family": "Poaceae",
    "url": "https://ru.wikipedia.org/wiki/Zizania_latifolia",
    "images": [
      "https://upload.wikimedia.org/wikipedia/commons/6/6c/Water_bamboo.JPG",
      "https://upload.wikimedia.org/wikipedia/commons/f/f5/Wild_rice_stems.jpg",
      "https://upload.wikimedia.org/wikipedia/commons/3/3a/Zizania_latifolia_in_cultivation.jpg"
    ],
    "isEdible": true,
    "edibleDesc": "Зерно и стебли"
  }
]
