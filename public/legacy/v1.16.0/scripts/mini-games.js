const playCombatGame = function(enemyID = 'Dorrator') {

  let music = new Audio('https://s3.envato.com/files/' + getRandomElement(combatMusicIDs) + '/preview.mp3');
  //Example: 'https://s3.envato.com/files/251442876/preview.mp3'
  music.play();


  let enemy = creatures.get(enemyID);

  game.isPaused = true;

  let board = document.createElement('div');
  board.id = 'combatGameBoard';
  board.className = 'mg-combat-board';
  document.querySelector('body').appendChild(board);

  let text1 = document.createElement('p');
  text1.innerHTML = '<b>Defeat the enemy by using right letters in the right time</b>'
  board.appendChild(text1);

  // let mainCard = document.createElement('div');
  // mainCard.id = 'herbsCollectingGameMainCard';
  // mainCard.className = 'mg-combat-main-card';
  // mainCard.setAttribute('data-marker', herb.latName);
  // mainCard.innerHTML = herb.comName;
  // board.appendChild(mainCard);

  board.appendChild(document.createElement('br'));
  board.appendChild(document.createElement('hr'));
  board.appendChild(document.createElement('br'));


  let attackBarContainer = document.createElement('div');
  attackBarContainer.id = 'attackBarContainer';
  attackBarContainer.className = 'mg-attack-bar-container';
  board.appendChild(attackBarContainer);

  let table1 = document.createElement('table');
  attackBarContainer.appendChild(table1);

  let tr1 = document.createElement('tr');
  tr1.id = 'attackRow';
  table1.appendChild(tr1);

  for (let i = 0; i < 10; i++) {
    let td = document.createElement('td');
    td.className = 'mg-combat-cell';
    if (i === 5) {
      td.id = 'attackCell';
      td.className = 'mg-combat-active-cell';
    }
    tr1.appendChild(td);
  }


  let picturesContainer = document.createElement('div');
  picturesContainer.className = 'mg-combat-pictures-container';
  board.appendChild(picturesContainer);

  let playerPic = document.getElementById(dorrator.staysAt).cloneNode(true);
  playerPic.id = 'playerPic';
  playerPic.className = 'mg-combat-picture';
  playerPic.style.transform = 'scale(3)';
  picturesContainer.appendChild(playerPic);

  let enemyPic = document.getElementById(enemy.staysAt).cloneNode(true);
  enemyPic.id = 'enemyPic';
  enemyPic.className = 'mg-combat-picture';
  enemyPic.style.transform = 'scale(3)';
  picturesContainer.appendChild(enemyPic);


  let defenceBarContainer = document.createElement('div');
  defenceBarContainer.id = 'defenceBarContainer';
  defenceBarContainer.className = 'mg-defence-bar-container';
  board.appendChild(defenceBarContainer);

  let table2 = document.createElement('table');
  defenceBarContainer.appendChild(table2);

  let tr2 = document.createElement('tr');
  tr2.id = 'defenceRow';
  table2.appendChild(tr2);

  for (let i = 0; i < 10; i++) {
    let td = document.createElement('td');
    td.className = 'mg-combat-cell';
    if (i === 5) {
      td.id = 'defenceCell';
      td.className = 'mg-combat-active-cell';
    }
    tr2.appendChild(td);
  }

  board.appendChild(document.createElement('br'));

  let howToPlayLink = document.createElement('a');
  howToPlayLink.href = 'https://youtu.be/RrvH7c3w6Q0';
  howToPlayLink.target = '_blank';
  howToPlayLink.innerHTML = 'How to play';
  board.appendChild(howToPlayLink);

  let enemyHPDiv = document.createElement('div');
  enemyHPDiv.style.float = 'right';
  enemyHPDiv.innerHTML = 'Enemy\'s HP: <span id="combatGameEnemyHPCurrent">34</span>/<span id="combatGameEnemyHPInitial">100</span>';
  board.appendChild(enemyHPDiv);
  document.getElementById('combatGameEnemyHPInitial').innerText = enemy.props.health;
  document.getElementById('combatGameEnemyHPCurrent').innerText = enemy.props.health;

  let closeBtn = document.createElement('div');
  closeBtn.id = 'combatGameCloseBtn';
  closeBtn.className = 'mg-combat-close-btn';
  closeBtn.innerHTML = 'X';
  board.appendChild(closeBtn);



  let defenceArr = Array.from(defenceRow.childNodes).filter(element => element.nodeName === 'TD');
  let attackArr = Array.from(attackRow.childNodes).filter(element => element.nodeName === 'TD');
  defenceArr = defenceArr.reverse();

  const letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
  let blockedHits = [];

  let iterate = function(arr, speed, isHurtable = false) {
    let index = 0;
    let randomLetter = letters[Math.floor(Math.random() * (letters.length - 0) + 0)];
    let letter = randomLetter;
    let timerID = setInterval(function() {
      let prevElement = arr[index-1];
      if (prevElement) {
        prevElement.innerHTML = '';
      }
      let currentElement = arr[index++ % arr.length];
      currentElement.innerHTML = letter;
      if (index === arr.length) {
        if (isHurtable) {
          if (!blockedHits.includes(letter)) {
            //hit the player;
            dorrator.receiveDamage(getRandomNumber(1, enemy.props.strength));
            createjs.Sound.play(randomHitSound());
          } else {
            //remove letter from blockedHits;
            blockedHits.splice(blockedHits.indexOf(letter), 1);
          }
        }
        currentElement.innerHTML = '';
        clearInterval(timerID);
      }
    }, speed);
  }

  //iterate(['r2c1', 'r2c2', 'r2c3', 'r3c3', 'r3c4']);
  let defenceInterval = setInterval(function() {
    if (chance('65%')) {
      iterate(defenceArr, enemy.props.reaction, true);
    }
  }, getRandomNumber(2000, 5000));

  let attackInterval = setInterval(function() {
    if (chance('75%')) {
      iterate(attackArr, 1000);
    }
  }, getRandomNumber(2000, 5000));

  let play = function(e) {
    dorrator.reduceStamina(3);
    if (e.key.toUpperCase() === attackCell.innerHTML) {
      //hit the enemy;
      if (creatures.has(enemyID)) {
        // The following line is used to make playing with bare hands more interesting;
        let strength = dorrator.strength > 5 ? dorrator.strength : 5;
        enemy.receiveDamage(getRandomNumber(1, strength));
        document.getElementById('combatGameEnemyHPCurrent').innerText = enemy.props.health;
        createjs.Sound.play(randomHitSound());
        attackCell.innerHTML = '⚔';
      } else {
        blockedHits = letters;
        quitMiniGame();
      }
    }
    if (defenceCell) {
      if (e.key.toUpperCase() === defenceCell.innerHTML) {
        blockedHits.push(defenceCell.innerHTML);
        defenceCell.innerHTML = '🛡';
      }
    }
  }

  document.addEventListener('keydown', play);



  let quitMiniGame = function(e) {
    if (!e) {
      combatGameCloseBtn.removeEventListener('click', quitMiniGame);
      document.removeEventListener('keydown', play);
      clearInterval(defenceInterval);
      clearInterval(attackInterval);
      combatGameBoard.remove();
      music.pause();
      game.isPaused = false;
      return;
    }
    if (e.target.id === 'combatGameCloseBtn') {
      combatGameCloseBtn.removeEventListener('click', quitMiniGame);
      document.removeEventListener('keydown', play);
      clearInterval(defenceInterval);
      clearInterval(attackInterval);
      combatGameBoard.remove();
      music.pause();
      game.isPaused = false;
    }
    // if (e.target.id === 'potionMakingGameOKBtn') {
    //   if (document.querySelector('#potionMakingGameInpt').value === herb.latName) {

    //     print('You make a potion out of <span class="herb-name js-herb-name" title="' + herb.comName + '">' + herb.latName + '</span>');
    //     dorrator.removeHerbFromHerbsStorage(herb.latName);
    //     dorrator.obtainHealthPotion();

    //     combatGameBoard.remove();
    //     document.removeEventListener('click', quitMiniGame);
    //     return;

    //   } else {

    //     print('You <span class="bg-danger">failed</span> to make a potion.');
    //     dorrator.removeHerbFromHerbsStorage(herb.latName);

    //     combatGameBoard.remove();
    //     document.removeEventListener('click', quitMiniGame);
    //     return;

    //   }
    // }
  }
  
  combatGameCloseBtn.addEventListener('click', quitMiniGame);

}

const playHerbsCollectingGame = function() {

  let herb = getAlmostRandomHerb();

  let setPhotoOrLatName = function(element, cssClass, dataMarker) {
    $.ajax({
      url: 'https://en.wikipedia.org/w/api.php',
      data: { action: 'query', titles: dataMarker, prop: 'pageimages', format: 'json', pithumbsize: '300' },
      dataType: 'jsonp',
      success: function(data) {
        let result = JSON.stringify(data.query.pages);
        let imgURL = result.substring(result.indexOf('https://upload.wikimedia.org'), result.indexOf('","width":'));
        if (imgURL) {
          element.innerHTML = `<img class='${cssClass}' data-marker='${dataMarker}' src='${imgURL}' />`;          
        } else {
          element.innerHTML = dataMarker;
          element.className = cssClass;
          if (dataMarker) {
            element.setAttribute('data-marker', dataMarker);  
          }
        }
      }
    });
  }

  // { latName: 'Acacia senegal', comName: 'Gum arabic', medDesc: '', photo: 'Khair.JPG' };
  let props = Object.getOwnPropertyNames(herb);
  if (props.includes('count')) {
    props.splice(props.indexOf('count'), 1);
  }
  if (props.includes('img')) {
    props.splice(props.indexOf('img'), 1);
  }
  if (props.includes('link')) {
    props.splice(props.indexOf('link'), 1);
  }

  let criteria = props[getRandomNumber(0, props.length - 1)];
  props.splice(props.indexOf(criteria), 1);
  if (!herb[criteria]) {
    criteria = 'latName';
  }

  let board = document.createElement('div');
  board.id = 'herbsCollectingGameBoard';
  board.className = 'mg-herbs-collecting-board';
  document.querySelector('body').appendChild(board);

  let text1 = document.createElement('p');
  //text1.className = 'mg-herbs-collecting-text1';
  text1.innerHTML = '<b>Find this herb:</b>'
  board.appendChild(text1);

  let mainCard = document.createElement('div');
  mainCard.id = 'herbsCollectingGameMainCard';
  mainCard.className = 'mg-herbs-collecting-main-card';
  mainCard.setAttribute('data-marker', herb.latName);
  mainCard.innerHTML = herb[criteria];
  if (criteria === 'photo') {
    setPhotoOrLatName(mainCard, mainCard.className, herb.latName);
  }
  board.appendChild(mainCard);

  board.appendChild(document.createElement('br'));
  board.appendChild(document.createElement('hr'));
  board.appendChild(document.createElement('br'));

  let text2 = document.createElement('p');
  //text2.className = 'mg-herbs-collecting-text2';
  text2.innerHTML = '<b>Among these:</b>'
  board.appendChild(text2);

  let numOfOptions = 5;
  let options = [];

  let winnerCardIndex = getRandomNumber(0, numOfOptions);
  console.log('winnerCardIndex', winnerCardIndex);

  for (let i = 0; i < numOfOptions; i++) {
    let optionObj = {};
    if (i === winnerCardIndex) {
      optionObj = herb;
    } else {
      optionObj = getAlmostRandomHerb();
    }
    options.push(optionObj);
    let optionCard = document.createElement('div');
    optionCard.className = 'mg-herbs-collecting-option-card';
    let criteria = props[getRandomNumber(0, props.length - 1)];
    if (!optionObj[criteria]) {
      criteria = props.includes('latName') ? 'latName' : 'comName';
    }
    optionCard.innerHTML = optionObj[criteria];
    if (criteria === 'photo') {
      setPhotoOrLatName(optionCard, optionCard.className, optionObj.latName);
    }
    optionCard.setAttribute('data-marker', optionObj.latName);
    board.appendChild(optionCard);
  }

  let closeBtn = document.createElement('div');
  closeBtn.id = 'herbsCollectingGameCloseBtn';
  closeBtn.className = 'mg-herbs-collecting-close-btn';
  closeBtn.innerHTML = 'X';
  document.querySelector('#herbsCollectingGameBoard').appendChild(closeBtn);

  let quitMiniGame = function(e) {
    if (e.target.id === 'herbsCollectingGameCloseBtn') {
      herbsCollectingGameBoard.remove();
      document.removeEventListener('click', quitMiniGame);
      return;
    }
    if (e.target.getAttribute('data-marker')) {
      if (e.target.getAttribute('data-marker') === herb.latName) {

        print('You found herb <span class="herb-name js-herb-name" title="' + herb.comName + '">' + herb.latName + '</span>');
        //responsiveVoice.speak(herb.latName, 'Latin Male');
        dorrator.obtainHerb(herb);
        $.ajax({
          url: 'https://en.wikipedia.org/w/api.php',
          data: { action: 'query', titles: herb.latName, prop: 'pageimages', format: 'json', pithumbsize: '300' },
          dataType: 'jsonp',
          success: function(data) {
            let result = JSON.stringify(data.query.pages);
            let imgURL = result.substring(result.indexOf('https://upload.wikimedia.org'), result.indexOf('","width":'));
            if (!imgURL) {
              imgURL = './images/herb_placeholder.png';
            }
            let linkToWikipedia = result.substring(result.indexOf('"pageid":'), result.indexOf(',"ns":'));
            linkToWikipedia = linkToWikipedia.split('"pageid":')[1];
            linkToWikipedia = 'http://en.wikipedia.org/?curid=' + linkToWikipedia;
            if (dorrator.herbsStorage.get(herb.latName)) {
              let tempHerb =  dorrator.herbsStorage.get(herb.latName);
              tempHerb.img = imgURL;
              tempHerb.link = linkToWikipedia;
              dorrator.herbsStorage.set(herb.latName, tempHerb);
            }
            $.notify({
                icon: imgURL,
                title: '<br /><b>' + herb.latName + '</b><br />',
                message: '(' + herb.comName + ')<br />' + herb.medDesc + '<br /><br />' + '<a href=' + linkToWikipedia + ' target="_blank">Read More</a>'
              },{
                icon_type: 'image',
                template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<img data-notify="icon" width="100" height="100">' +
                    '<span data-notify="title">{1}</span>' +
                    '<span data-notify="message">{2}</span>' +
                  '</div>',
                type: 'success',
                placement: {from: 'top', align: 'left'},
                mouse_over: 'pause'
            });
          }
        });

        herbsCollectingGameBoard.remove();
        document.removeEventListener('click', quitMiniGame);
        return;

      } else {

        print('You failed to find a herb.');

        herbsCollectingGameBoard.remove();
        document.removeEventListener('click', quitMiniGame);
        return;

      }
    }
  }
  
  document.addEventListener('click', quitMiniGame);

}


const playPotionMakingGame = function(herb) {

  // herb as parameter could be like this: { latName: 'Acacia senegal', comName: 'Gum arabic', medDesc: '', photo: 'Khair.JPG' };

  let board = document.createElement('div');
  board.id = 'herbsCollectingGameBoard';
  board.className = 'mg-herbs-collecting-board';
  document.querySelector('body').appendChild(board);

  let text1 = document.createElement('p');
  text1.innerHTML = '<b>You are going to make a health potion out of this herb:</b>'
  board.appendChild(text1);

  let mainCard = document.createElement('div');
  mainCard.id = 'herbsCollectingGameMainCard';
  mainCard.className = 'mg-herbs-collecting-main-card';
  mainCard.setAttribute('data-marker', herb.latName);
  mainCard.innerHTML = herb.comName;
  board.appendChild(mainCard);

  board.appendChild(document.createElement('br'));
  board.appendChild(document.createElement('hr'));
  board.appendChild(document.createElement('br'));

  let text2 = document.createElement('p');
  text2.innerHTML = '<b>Type its latin name here:</b>'
  board.appendChild(text2);

  let inpt = document.createElement('input');
  inpt.id = 'potionMakingGameInpt';
  board.appendChild(inpt);

  let btn = document.createElement('button');
  btn.id = 'potionMakingGameOKBtn';
  btn.innerText = 'Make';
  board.appendChild(btn);

  let closeBtn = document.createElement('div');
  closeBtn.id = 'herbsCollectingGameCloseBtn';
  closeBtn.className = 'mg-herbs-collecting-close-btn';
  closeBtn.innerHTML = 'X';
  document.querySelector('#herbsCollectingGameBoard').appendChild(closeBtn);

  let quitMiniGame = function(e) {
    if (e.target.id === 'herbsCollectingGameCloseBtn') {
      print('You <span class="bg-danger">failed</span> to make a potion.');
      dorrator.removeHerbFromHerbsStorage(herb.latName);
      herbsCollectingGameBoard.remove();
      document.removeEventListener('click', quitMiniGame);
      return;
    }
    if (e.target.id === 'potionMakingGameOKBtn') {
      if (document.querySelector('#potionMakingGameInpt').value === herb.latName) {

        print('You make a potion out of <span class="herb-name js-herb-name" title="' + herb.comName + '">' + herb.latName + '</span>');
        dorrator.removeHerbFromHerbsStorage(herb.latName);
        dorrator.obtainHealthPotion();

        herbsCollectingGameBoard.remove();
        document.removeEventListener('click', quitMiniGame);
        return;

      } else {

        print('You <span class="bg-danger">failed</span> to make a potion.');
        dorrator.removeHerbFromHerbsStorage(herb.latName);

        herbsCollectingGameBoard.remove();
        document.removeEventListener('click', quitMiniGame);
        return;

      }
    }
  }
  
  document.addEventListener('click', quitMiniGame);

}


let playMathMiniGame = function(numberOfEquations = 1) {

  numberOfEquations = Number(numberOfEquations);

  let board = document.createElement('div');
  board.id = 'mathMiniGameBoard';
  board.className = 'mg-math-board';
  document.querySelector('body').appendChild(board);

  let description = document.createElement('p');
  description.innerHTML = `<b>Solve ${numberOfEquations} equation(s):</b>`;
  board.appendChild(description);

  for (let i = 0; i < numberOfEquations; i++) {

    let equationContainer = document.createElement('div');
    equationContainer.className = 'mg-math-equation-container';
    board.appendChild(equationContainer);

    let operand1 = document.createElement('span');
    operand1.id = 'operand1Value';
    operand1.innerHTML = getRandomNumber(1, 10);
    equationContainer.appendChild(operand1);

    let sign = document.createElement('span');
    sign.id = 'signValue';
    let randomSign = getRandomElement(['+', '-', '*']);
    sign.innerHTML = randomSign;
    equationContainer.appendChild(sign);

    let operand2 = document.createElement('span');
    operand2.id = 'operand2Value';
    operand2.innerHTML = getRandomNumber(1, 10);
    equationContainer.appendChild(operand2);

    let equalSign = document.createElement('span');
    equalSign.innerHTML = '=';
    equationContainer.appendChild(equalSign);

    let inpt = document.createElement('input');
    inpt.className = 'mg-math-input js-mg-math-input';
    inpt.id = 'mathMiniGameAnswerInput';
    board.appendChild(inpt);

    let rightAnswer;
    if (randomSign === '+') {
      rightAnswer = Number(operand1.innerHTML) + Number(operand2.innerHTML);
    }
    if (randomSign === '-') {
      rightAnswer = Number(operand1.innerHTML) - Number(operand2.innerHTML);
    }
    if (randomSign === '*') {
      rightAnswer = Number(operand1.innerHTML) * Number(operand2.innerHTML);
    }
    inpt.setAttribute('data-answer', rightAnswer);

    let br = document.createElement('br');
    board.appendChild(br);

  }

  let btn = document.createElement('button');
  btn.id = 'mathMiniGameOKBtn';
  btn.className = 'mg-math-mini-game-ok-btn';
  btn.innerText = 'OK';
  board.appendChild(btn);

  let closeBtn = document.createElement('div');
  closeBtn.id = 'mathMiniGameCloseBtn';
  closeBtn.className = 'mg-math-close-btn';
  closeBtn.innerHTML = 'X';
  document.querySelector('#mathMiniGameBoard').appendChild(closeBtn);

  let quitMathMiniGame = function(e) {
    if (e.target.id === 'mathMiniGameCloseBtn') {          
      mathMiniGameBoard.remove();
      document.removeEventListener('click', quitMathMiniGame);
      return;
    }
    if (e.target.id === 'mathMiniGameOKBtn') {
      let isEverythingCorrect = function() {
        let result = Array.from(document.querySelectorAll('.js-mg-math-input')).every(element => {
          return Number(element.value) === Number(element.getAttribute('data-answer'));
        });
        return result;
      }
      if (isEverythingCorrect()) {
        let numberOfCoins = numberOfEquations * getRandomNumber(1, 10);
        print(`You found ${numberOfCoins} coins`);
        dorrator.obtainCoins(numberOfCoins);
        mathMiniGameBoard.remove();
        document.removeEventListener('click', quitMathMiniGame);
        return;
      } else {
        print(`You failed to obtain coins`);
        mathMiniGameBoard.remove();
        document.removeEventListener('click', quitMathMiniGame);
        return;
      }
    }
  }  
  document.addEventListener('click', quitMathMiniGame);
}
