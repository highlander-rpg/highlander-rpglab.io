let openHerbalStorage = function() {
  let board = document.createElement('div');
  board.id = 'herbalStorageBoard';
  board.className = 'herbal-storage-board';
  document.querySelector('body').appendChild(board);

  let closeBtn = document.createElement('div');
  closeBtn.id = 'herbalStorageCloseBtn';
  closeBtn.className = 'herbal-storage-close-btn';
  closeBtn.innerHTML = 'X';
  board.appendChild(closeBtn);

  let mainText = document.createElement('h2');
  mainText.innerHTML = 'Herbal Storage';
  board.appendChild(mainText);

  // Example:

  // <div class="card" style="width: 18rem;">
  //   <img class="card-img-top" src="..." alt="Card image cap">
  //   <div class="card-body">
  //     <h5 class="card-title">Card title</h5>
  //     <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
  //     <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  //     <a href="#" class="card-link">Card link</a>
  //     <a href="#" class="btn btn-primary">Go somewhere</a>
  //   </div>
  // </div>

  dorrator.herbsStorage.forEach(function(element, key) {
    // herbObject example: { latName: 'Acacia senegal', comName: 'Gum arabic', medDesc: '', photo: 'Khair.JPG' } + img, count and link props;
    let card = document.createElement('div');
    card.className = 'card';
    card.style.width = '18rem';
    card.style.display = 'inline-block';
    card.style.margin = '5px';
    board.appendChild(card);

    let img = document.createElement('img');
    img.className = 'card-img-top';
    img.src = element.img;
    img.alt = element.latName;
    card.appendChild(img);

    let cardBody = document.createElement('div');
    cardBody.className = 'card-body';
    card.appendChild(cardBody);

    let title = document.createElement('h5');
    title.className = 'card-title';
    title.innerText = element.latName;
    cardBody.appendChild(title);

    let subtitle = document.createElement('h6');
    subtitle.className = 'card-subtitle mb-2 text-muted';
    subtitle.innerText = element.comName;
    cardBody.appendChild(subtitle);

    let text = document.createElement('p');
    text.className = 'card-text';
    text.innerHTML = element.medDesc;
    cardBody.appendChild(text);

    let link = document.createElement('a');
    link.className = 'card-link';
    link.href = element.link;
    link.target = '_blank';
    link.innerText = 'Read More';
    cardBody.appendChild(link);

    let playLatNameBtn = document.createElement('a');
    playLatNameBtn.setAttribute('data-herbLatName', element.latName);
    playLatNameBtn.style.float = 'right';
    playLatNameBtn.className = 'btn btn-warning';
    playLatNameBtn.href = '#';
    playLatNameBtn.innerHTML = `🔊`;
    cardBody.appendChild(playLatNameBtn);

    let playLatName = function(e) {
      let latName = e.target.getAttribute('data-herbLatName');
      responsiveVoice.speak(latName, 'Latin Male'); 
    }

    playLatNameBtn.addEventListener('click', playLatName);

    let btn = document.createElement('a');
    btn.style.float = 'right';
    btn.className = 'btn btn-secondary';
    if (element.count >= 5) {
      btn.className = 'btn btn-warning js-mix-to-potion-btn';
      btn.setAttribute('data-herbLatName', element.latName)
    }
    btn.href = '#';
    btn.innerHTML = `${element.count}/5`;
    cardBody.appendChild(btn);
  });

  let quitHerbalStorage = function(e) {

    if (e.target.id === 'herbalStorageCloseBtn') {
      herbalStorageBoard.remove();
      document.removeEventListener('click', quitHerbalStorage);
    }

    if (e.target.classList.contains('js-mix-to-potion-btn')) {
      playPotionMakingGame(dorrator.herbsStorage.get(e.target.getAttribute('data-herbLatName')));

      herbalStorageBoard.remove();
      document.removeEventListener('click', quitHerbalStorage);
    }

  }
  
  document.addEventListener('click', quitHerbalStorage);
}