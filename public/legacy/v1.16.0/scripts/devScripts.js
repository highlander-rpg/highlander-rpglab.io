if (location.href.split('?')[1] === 'Dev=true') {
  // Dev mode is on
  print('😀🌿');
  appMode = 'Dev';
  console.warn('Prompts are turned off by appMode evaluated to "Dev"');
}