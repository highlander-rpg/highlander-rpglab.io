const db = new Dexie("maps_database");
db.version(1).stores({
    maps: 'name,data'
});
const mapsNet = new Map;

let isInitialInteractionWithPagePerformed = false;

const game = {};
game.isPaused = false;

var currentMapName = 'OS-RPG-Map--Surface--' + Math.random();
var currentMapType = 'Surface';
var currentMapCoordinates = '0;0';
var isCurrentMapNewToPlayer = true;
const visitedMaps = [];
mapsNet.set(currentMapCoordinates, currentMapName);
var playerCameFrom = 'Nowhere';
var cellToPlacePlayer = '';
var futureMapName = '';
const worldWidth = 70;
const worldHeight = 50;
var currentObject = '-256px -1536px';
var tempObject = '';
var obstacleVisibility = false;
const selectedSprites = [];
var inventory = [];
var inventoryState = 'Closed';
var containerState = 'Closed';
var uniqueID = 0;
var dragged;
var containerCells = [];
var currentContainer = '';
var healthIncreasingProcess;
var staminaIncreasingProcess;
var appMode = 'Game';
var userControlIsBlocked = false;
let simultaneousWalkingSessionsCount = 0;

const obstaclesArr = [];
const visArr = [];
const wasSeenVisArr = [];

let isContextMenuOpen = false;

if (location.href.split('?')[1] === 'Editor=true') {
  appMode = 'Editor';
}

let installDefaultMaps = function() {
  db.maps.put({name: 'OS-RPG-Map-Test2', data: test2});
  db.maps.put({name: 'OS-RPG-Map-Interior_lighthouse_first-floor', data: interiorLighthouseFirstFloor});
  db.maps.put({name: 'OS-RPG-Map-Interior_lighthouse_second-floor', data: interiorLighthouseSecondFloor});
  db.maps.put({name: 'OS-RPG-Map-Seashore_woods', data: seashoreWoods});
  db.maps.put({name: 'OS-RPG-Map-Den_of_beast', data: denOfBeast});
  db.maps.put({name: 'OS-RPG-Map-Secluded_hook', data: secludedHook});
  db.maps.put({name: 'OS-RPG-Map-Empty', data: emptyMap});

  // USAGE:
  /*db.maps.get('OS-RPG-Map-Interior_lighthouse_first-floor').then(map => {
    console.log(map.data);
  });*/

  //http://dexie.org/docs/Tutorial/Getting-started
}

installDefaultMaps();

var generateUniqueID = function() {
  uniqueID++;
  return uniqueID;
}

const grassSprites = ['-448px -288px', '-512px -288px', '-576px -288px', '-704px -288px', '-768px -288px', '-832px -288px', '-896px -288px', '-960px -288px', '-1024px -288px', '-1088px -288px', '-1152px -288px', '-1216px -288px'];
const getRandomGrassSprite = function() {
  let max = grassSprites.length - 1;
  let min = 0;
  return grassSprites[Math.round(Math.random() * (max - min) + min)];
}
const fillMapWithGrass = function() {

}

var resetBorder = function(cellId) {
  document.getElementById(cellId).style.border = '1px solid black';
}

var addControls = function(subject) {

  subject.onclick = function(e) {
    if (e.shiftKey) {
      placeObstacle(this.id, 'All');
      return;
    }
    if (e.ctrlKey) {
      placeObstacle(this.id, 'Top');
      return;
    }
    if (e.altKey) {
      placeObstacle(this.id, 'None');
      removeDoor(this.id);
      return;
    }
    if (appMode === 'Editor') {
      place(this.id, currentObject);
    }
  }

  subject.oncontextmenu = function(e) {
    if (e.shiftKey && e.ctrlKey) {
      removeUpper(this.id);
      return false;
    }
    if (e.altKey) {
      pick(this.id);
      return false;
    }
    if (e.shiftKey) {
      removeUpper(this.id);
      return false;
    }
    /*if (e.shiftKey) {
      document.getElementById('F-' + this.id).style.opacity = 0;
      return false;
    }*/
    /*if (e.ctrlKey) {
      document.getElementById('F-' + this.id).style.opacity = .9;
      return false;
    }*/
    if (e.ctrlKey) {
      place(this.id, getRandomGrassSprite());
      return false;
    }
  }

  /*subject.onmouseover = function(e) {
    if (e.shiftKey && e.ctrlKey) {
      place(this.id, getRandomGrassSprite());
      return false;
    }
  }*/
}

var createWorld = function(width, height) {

  var body = document.getElementsByTagName('body')[0];
  var tbl = document.createElement('table');
  tbl.id = 'world';
  tbl.className = 'world';
  var tblBody = document.createElement('tbody');
  
  for (var i = 0; i < height; i++) {
    var row = document.createElement('tr');
    row.id = 'row' + i;

    for (var j = 0; j < width; j++) {
      var cell = document.createElement('td');
      cell.id = 'r' + i + 'c' + j;
      if (appMode === 'Editor') {
        addControls(cell);
      }
      row.appendChild(cell);
    }
  
    tblBody.appendChild(row);
  }
  
  tbl.appendChild(tblBody);
  body.appendChild(tbl);

  if (appMode === 'Game') {
    document.getElementById('world').addEventListener('click', handleClick);
    document.getElementById('world').addEventListener('mousemove', handleMouseMove);
    document.getElementById('world').addEventListener('contextmenu', handleContextMenu);
  }

  document.getElementById('world').style.width = width * 32 + 'px';

}

var createFogOfWar = function(width, height) {

  var body = document.getElementsByTagName('body')[0];
  var tbl = document.createElement('table');
  tbl.id = 'fogOfWar';
  tbl.className = 'fog-of-war';
  var tblBody = document.createElement('tbody');
  
  for (var i = 0; i < height; i++) {
    var row = document.createElement('tr');

    for (var j = 0; j < width; j++) {
      var cell = document.createElement('td');
      cell.id = 'F-r' + i + 'c' + j;
      visArr.push(cell.id);
      cell.style.background = 'rgb(0, 0, 0)';
      cell.style.opacity = 0;
      row.appendChild(cell);
    }
  
    tblBody.appendChild(row);
  }
  
  tbl.appendChild(tblBody);
  body.appendChild(tbl);

  document.getElementById('fogOfWar').style.width = width * 32 + 'px';
}

createWorld(worldWidth, worldHeight);

createFogOfWar(worldWidth, worldHeight);

let isTileVisible = function(cellId) {

  let x0 = Number(dorrator.staysAt.split('c')[1]);
  let y0 = Number(dorrator.staysAt.split('c')[0].split('r')[1]);

  let x1 = Number(cellId.split('c')[1]);
  let y1 = Number(cellId.split('c')[0].split('r')[1]);

  // if one or both points are outside of this map, we discount it from the checks
  if (x0 < 0 || x0 >= worldWidth || x1 < 0 || x1 >= worldWidth || 
    y0 < 0 || y0 >= worldHeight || y1 < 0 || y1 >= worldHeight) {
    return true;
  }

  // get the deltas and steps for both axis
  var dx = Math.abs(x1 - x0);
  var dy = Math.abs(y1 - y0);
  var sx = x0 < x1 ? 1 : -1;
  var sy = y0 < y1 ? 1 : -1;

  // stores an error factor we use to change the axis coordinates
  var err = dx - dy;

  while (x0 != x1 || y0 != y1)
  {
    // check our collision map to see if this tile blocks visibility
    //if (this.collisionMap[y0][x0] == 1)
    if (document.getElementById('r' + y0 + 'c' + x0).hasAttribute('data-obstacles'))
    return false;

    // check our error value against our deltas to see if 
    // we need to move to a new point on either axis
    var e2 = 2 * err;
    if (e2 > -dy)
    {
    err -= dy;
    x0 += sx;
    }
    if (e2 < dx)
    {
    err += dx;
    y0 += sy;
    }
  }

  // if we're here we hit no occluders and therefore can see this tile
  return true;
};


var place = function (cellID, what) {
  if (what.startsWith('door-')) {
    console.log(what + ' is going to be placed');
    var type = what.split('-')[1];
    type = type.charAt(0).toUpperCase() + type.slice(1);
    var destinationMap = prompt('Where this door leads to?');
    if (!destinationMap) {destinationMap = 'Test2'}
    placeDoor(cellID, type, destinationMap, 'r3c3');
  }
  var currentObjects = window.getComputedStyle(document.getElementById(cellID)).backgroundPosition;
  currentObjects = currentObjects.replace(/, /gi, ', url("images/sprites.png")');
  var objects = 'url("images/sprites.png") ' + what + ', url("images/sprites.png")' + currentObjects;
  document.getElementById(cellID).style.background = objects;
}

/*place('r1c2', '-1504px -1280px');*/

const remove = function(cellID, what) {
  let cell = document.getElementById(cellID);
  if (cell.style.background.includes(what)) {
    let objectsOnCell = cell.style.background.split(', ');
    objectsOnCell.splice(objectsOnCell.indexOf('url("images/sprites.png") '+ what), 1);
    objectsOnCell = objectsOnCell.toString();
    document.getElementById(cellID).style.background = objectsOnCell;
  }
}

const placeItem = function(cellID, itemID) {
  if (typeof cellID === 'string' && typeof itemID === 'string') {
    if (!items.has(itemID)) {
      throw 'There is no item with such ID';
    } else {
      let prevItemsOnCell = '';
      let cell = document.getElementById(cellID);
      prevItemsOnCell = document.getElementById(cellID).getAttribute('data-items');
      if (cell.hasAttribute('data-items') && prevItemsOnCell != '') {
        cell.setAttribute('data-items', prevItemsOnCell + ', ' + itemID);
      } else {
        cell.setAttribute('data-items', itemID);
        cell.setAttribute('title', items.get(itemID).name + ' (+' + items.get(itemID).effect + ')');
      }
      place(cellID, items.get(itemID).inventoryAppearance);
    }
  }
}

const removeItem = function(cellID, itemID) {
  if (typeof itemID === 'string') {
    let cell = document.getElementById(cellID);
    let objects = cell.style.background.split(', ');
    objects.splice(objects.indexOf('url("images/sprites.png") '+ items.get(itemID).inventoryAppearance), 1);
    objects = objects.toString();
    document.getElementById(cellID).style.background = objects;
    let itemsOnCell = cell.getAttribute('data-items').split(', ');
    itemsOnCell.splice(itemsOnCell.indexOf(itemID), 1);
    itemsOnCell = itemsOnCell.toString();
    cell.setAttribute('data-items', itemsOnCell);
  }
}

var removeUpper = function (cellId) {
  var objects = document.getElementById(cellId).style.background.split(', ');
  objects.shift();
  objects = objects.toString();
  document.getElementById(cellId).style.background = objects;
};

var pick = function(cellId) {
  var objects = document.getElementById(cellId).style.background.split(', ');
  var object = objects.shift();
  currentObject = object.split(' ')[1] + ' ' + object.split(' ')[2];
  document.getElementById('current-object-cell').style.background = 'url("images/sprites.png") ' + currentObject;
  resetBorder('current-object-cell');
}

var pickToTemp = function(cellId) {
  var objects = document.getElementById(cellId).style.background.split(', ');
  var object = objects.shift();
  tempObject = object.split(' ')[1] + ' ' + object.split(' ')[2];
}

var setObstacleVisibility = function(boolean) {
  var cells = document.getElementById('world').getElementsByTagName('td');
  if (boolean) {
    obstacleVisibility = true;
    for (var i = 0; i < cells.length; i++) {
      if (document.getElementById(cells[i].id).getAttribute('data-obstacles') === 'All') {
        document.getElementById(cells[i].id).style.border = '2px solid brown';
      }
      if (document.getElementById(cells[i].id).getAttribute('data-obstacles') === 'Top') {
        document.getElementById(cells[i].id).style.borderTop = '2px solid brown';
      }
    }
    console.log('Obstacle visibility is on');
    return;
  }
  obstacleVisibility = false;
  for (var i = 0; i < cells.length; i++) {
    if (document.getElementById(cells[i].id).style.border === '2px solid brown') {
      document.getElementById(cells[i].id).style.border = '';
    }
  }
  console.log('Obstacle visibility is off');
}

var placeObstacle = function(cellId, typeOfObstacle) {
  if (typeOfObstacle === 'All') {
    if (obstacleVisibility) {
      document.getElementById(cellId).style.border = '2px solid brown';
    }
    document.getElementById(cellId).setAttribute('data-obstacles', 'All');
    obstaclesArr.push(cellId);
    return;
  }
  if (typeOfObstacle === 'Top') {
    if (obstacleVisibility) {
      document.getElementById(cellId).style.border = '';
      document.getElementById(cellId).style.borderTop = '2px solid brown';
    }
    document.getElementById(cellId).setAttribute('data-obstacles', 'Top');
    return;
  }
  if (typeOfObstacle === 'None') {
    document.getElementById(cellId).style.border = '';
    document.getElementById(cellId).removeAttribute('data-obstacles');
    return;
  }
}

var removeObstacle = function(cellID) {
  placeObstacle(cellID, 'None');
}

var placeDoor = function(cellId, type, destinationMap, destinationCellId) {
  if (!destinationCellId) {
    destinationCellId = cellId;
  }
  if (/*obstacleVisibility && */type === 'Top') {
    document.getElementById(cellId).style.borderTop = '2px solid green';
  }
  if (/*obstacleVisibility && */type === 'Bottom') {
    document.getElementById(cellId).style.borderBottom = '2px solid green';
  }
  if (/*obstacleVisibility && */type === 'Left') {
    document.getElementById(cellId).style.borderLeft = '2px solid green';
  }
  if (/*obstacleVisibility && */type === 'Right') {
    document.getElementById(cellId).style.borderRight = '2px solid green';
  }
  document.getElementById(cellId).setAttribute('data-door', type + ', ' + destinationMap + ', ' + destinationCellId);
  document.getElementById(cellId).setAttribute('title', destinationMap);
}

var removeDoor = function(cellId) {
  document.getElementById(cellId).style.border = '';
  document.getElementById(cellId).removeAttribute('data-door');
}

let transmitSpriteLocation = function(e) {
  let sprite = e.target.style.backgroundPosition;
  if (!selectedSprites.includes(sprite)) {
    selectedSprites.push(e.target.style.backgroundPosition);
    e.target.style.border = '2px solid yellow';
    console.log(sprite + ' is added');
  } else {
    selectedSprites.splice(selectedSprites.indexOf(sprite), 1);
    e.target.style.border = 'none';
    console.log(sprite + ' is excluded');
  }
}

var openEditor = function() {
  var body = document.getElementsByTagName('body')[0];
  var div = document.createElement('div');
  body.appendChild(div);
  div.id = 'editors-panel';
  div.className = 'editors-panel';

  var body = document.getElementById('editors-panel');

  var tbl = document.createElement('table');
  var tblBody = document.createElement('tbody');

  for (var i = 0; i < 95; i++) {
    var row = document.createElement('tr');

    for (var j = 0; j < 64; j++) {
      var cell = document.createElement('td');
      cell.id = 'E-r' + i + 'c' + j;
      cell.style.border = '1px solid black';
      cell.style.background = 'url("images/sprites.png") -' + j * 32 + 'px -' + i * 32 + 'px';
      cell.onclick = function(e) {
        let x = this.id.split('c')[1];
        let y = this.id.split('c')[0].split('r')[1];
        let spriteLocation = '-' + x * 32 + 'px -' + y * 32 + 'px';
        currentObject = spriteLocation;
        document.getElementById('current-object-cell').style.background = 'url("images/sprites.png") ' + spriteLocation;
        resetBorder('current-object-cell');
        if (e.ctrlKey) {
          transmitSpriteLocation(e);
        }
      }
      var cellText = document.createTextNode('E-r' + i + 'c' + j);
      cell.appendChild(cellText);
      row.appendChild(cell);
    }

      tblBody.appendChild(row);
  }

  tbl.appendChild(tblBody);
  body.appendChild(tbl);

  tbl.style.width = '2048px';

  //Current Object panel;
  var body = document.getElementsByTagName('body')[0];
  var div = document.createElement('div');
  body.appendChild(div);
  div.id = 'current-object-panel';
  div.className = 'current-object-panel';

  var body = document.getElementById('current-object-panel');
  var tbl = document.createElement('table');
  var tblBody = document.createElement('tbody');
  var row = document.createElement('tr');
  var cell = document.createElement('td');

  cell.id = 'current-object-cell';
  cell.className = 'current-object-cell';
  row.appendChild(cell);
  tblBody.appendChild(row);
  tbl.appendChild(tblBody);
  body.appendChild(tbl);

  //Doors menu;
  var body = document.getElementsByTagName('body')[0];
  var div = document.createElement('div');
  body.appendChild(div);
  div.id = 'doors-menu-panel';
  div.className = 'doors-menu-panel';

  var body = document.getElementById('doors-menu-panel');
  var tbl = document.createElement('table');
  var tblBody = document.createElement('tbody');
  var row = document.createElement('tr');
  var cell1 = document.createElement('td');
  var cell2 = document.createElement('td');
  var cell3 = document.createElement('td');
  var cell4 = document.createElement('td');

  cell1.id = 'doors-menu-top-door-cell';
  cell1.style.borderTop = '2px solid green';
  cell1.style.backgroundPosition = '-1152px -32px';
  cell1.onclick = function() {
    resetBorder('current-object-cell');
    currentObject = 'door-top';
    document.getElementById('current-object-cell').style.borderTop = cell1.style.borderTop;
  }
  cell2.id = 'doors-menu-right-door-cell';
  cell2.style.borderRight = '2px solid green';
  cell2.style.backgroundPosition = '-1696px -352px';
  cell2.onclick = function() {
    resetBorder('current-object-cell');
    currentObject = 'door-right';
    document.getElementById('current-object-cell').style.borderRight = cell2.style.borderRight;
  }
  cell3.id = 'doors-menu-bottom-door-cell';
  cell3.style.borderBottom = '2px solid green';
  cell3.style.backgroundPosition = '-1728px -384px';
  cell3.onclick = function() {
    resetBorder('current-object-cell');
    currentObject = 'door-bottom';
    document.getElementById('current-object-cell').style.borderBottom = cell3.style.borderBottom;
  }
  cell4.id = 'doors-menu-left-door-cell';
  cell4.style.borderLeft = '2px solid green';
  cell4.style.backgroundPosition = '-1728px -352px'
  cell4.onclick = function() {
    resetBorder('current-object-cell');
    currentObject = 'door-left';
    document.getElementById('current-object-cell').style.borderLeft = cell4.style.borderLeft;
  }
  row.appendChild(cell1);
  row.appendChild(cell2);
  row.appendChild(cell3);
  row.appendChild(cell4);
  tblBody.appendChild(row);
  tbl.appendChild(tblBody);
  body.appendChild(tbl);
  if (appMode === 'Game') {
    document.getElementById('editors-panel').style.display = 'none';
  }
}

if (appMode === 'Editor') {
  openEditor();
}

var closeEditor = function() {
  document.getElementById('editors-panel').parentNode.removeChild(document.getElementById('editors-panel'));
  document.getElementById('current-object-panel').parentNode.removeChild(document.getElementById('current-object-panel'));
  document.getElementById('doors-menu-panel').parentNode.removeChild(document.getElementById('doors-menu-panel'));
}

var createContainer = function(size, appearance, content) {
  new Container(size, appearance, content);
}

var placeContainer = function(cellID, containerID) {
  let container = containers.get(containerID);
  place(cellID, container.appearance);
  document.getElementById(cellID).setAttribute('data-container', container.id);
}

var removeContainer = function(cellID, containerID) {
  let container = containers.get(containerID);
  remove(cellID, container.appearance);
  document.getElementById(cellID).removeAttribute('data-container');
}

var openContainer = function(containerID) {
  currentContainer = containerID;
  let container = containers.get(containerID);
  var body = document.getElementsByTagName('body')[0];
  let div = document.createElement('div');
  let sizes = container.size.split('x');
  let numOfRows = sizes[0];
  let numOfColls = sizes[1];
  body.appendChild(div);
  div.id = 'container-panel';
  div.className = 'container-panel';
  div.style.width = numOfRows * 32 + 'px';
  div.style.height = numOfColls * 32 + 'px';

  var body = document.getElementById('container-panel');
  let tbl = document.createElement('table');
  tbl.id = 'container-cells';
  let tblBody = document.createElement('tbody');
  for (var i = 0; i < numOfColls; i++) {
    var row = document.createElement('tr');
    for (var j = 0; j < numOfRows; j++) {
      var cell = document.createElement('td');
      cell.id = 'C-r' + i + 'c' + j;
      containerCells.push(cell.id);
      cell.addEventListener('click', function(event) {
        let itemID = document.getElementById(event.target.id).getAttribute('data-items');
        removeItemFromContainer(items.get(itemID).id, currentContainer);
        dorrator.obtainItem(itemID);
      }, false);
      cell.addEventListener('drag', function(event) {}, false);
      cell.addEventListener('dragstart', function(event) {
        dragged = event.target;
        event.target.style.opacity = .5;
      }, false);
      cell.addEventListener('dragend', function(event) {
        event.target.style.opacity = "";
      }, false);
      cell.addEventListener('dragover', function(event) {
        event.preventDefault();
      }, false);
      cell.addEventListener('dragenter', function(event) {
        event.target.style.border = '2px solid black';
      }, false);
      cell.addEventListener('dragleave', function(event) {
        event.target.style.border = '1px solid black';
      }, false);
      cell.addEventListener('drop', function(event) {
        event.preventDefault();
        let itemID = document.getElementById(dragged.id).getAttribute('data-items');
        let cellID = containerCells.indexOf(event.target.id);
        if (dragged.id.slice(0, 1) === 'C') { // Taken from container;
          removeItemFromContainer(items.get(itemID).id, currentContainer);
        }
        if (dragged.id.slice(0, 1) === 'I') { // Taken from inventory;
          dorrator.removeItemFromInventoryCell(dragged.id);
        }
        putItemInContainer(items.get(itemID).id, currentContainer, cellID);
      }, false);
      cell.addEventListener("contextmenu", function(event) {
        event.preventDefault();
        let itemID = document.getElementById(event.target.id).getAttribute('data-items');
        removeItemFromContainer(items.get(itemID).id, currentContainer);
        placeItem(dorrator.staysAt, items.get(itemID).id);
      }, false);
      cell.style.border = '1px solid black';
      row.appendChild(cell);
    }
    tblBody.appendChild(row);
  }
  tbl.appendChild(tblBody);
  body.appendChild(tbl);
  tblBody.style.margin = '20px';
  for (let i in container.content) {
    if (container.content[i]) {
      placeItem(containerCells[i], container.content[i]);
    }
  }

  containerState = 'Open';
}

var closeContainer = function() {
  document.getElementById('container-panel').parentNode.removeChild(document.getElementById('container-panel'));
  currentContainer = '';
  containerCells = [];
  containerState = 'Closed';
}

var toggleContainer = function(containerID) {
  if (containerState === 'Open') {
    closeContainer();
  } else {
    openContainer(containerID);
    showPrompt('Click <b>left mouse button</b> on an item in container to take it. Click <b>right mouse button</b> to throw it away.');
  }
}

var refreshContainer = function(containerID) {
  toggleContainer(containerID);
  toggleContainer(containerID);
}

var putItemInContainer = function(itemID, containerID, cellID) {
  let item = items.get(itemID).id;
  let container = containers.get(containerID).content;
  if (cellID && container[cellID] === undefined) {
    container[cellID] = item;
    refreshContainer(containerID);
    return;
  }
  for (let i = 0; i < container.length; i++) {
    if (container[i] === undefined) {
      container[i] = item;
      refreshContainer(containerID);
      break;
    }
  }
}

var removeItemFromContainer = function(itemID, containerID) {
  let item = items.get(itemID).id;
  let container = containers.get(containerID).content;
  for (let i = 0; i < container.length; i++) {
    if (container[i] === itemID) {
      container[i] = undefined;
      break;
    }
  }
  refreshContainer(containerID);
}

const findPlayer = function() {
  // VERSION 1:

  // let x = (dorrator.getCol() * 32) + ( - window.screen.width / 2);
  // if (x < 0) {
  //   world.style.left = Math.abs(x) + 'px';
  //   fogOfWar.style.left = Math.abs(x) + 'px';
  // } else {
  //   world.style.left = '-' + x + 'px';
  //   fogOfWar.style.left = '-' + x + 'px';
  // }
  // let y = (dorrator.getRow() * 32) + ( - window.screen.height / 3);
  // if (y < 0) {
  //   world.style.top = Math.abs(y) + 'px';
  //   fogOfWar.style.top = Math.abs(y) + 'px';
  // } else {
  //   world.style.top = '-' + y + 'px';
  //   fogOfWar.style.top = '-' + y + 'px';
  // }

  // VERSION 2:

  let currentCell = document.getElementById(dorrator.staysAt);
  currentCell.scrollIntoView({behavior: 'smooth'});
  currentCell.classList.add('highlight');
  window.setTimeout(function() {
    currentCell.classList.remove('highlight');
  }, 3000);
}

const getCoordinateX = function(mapCoordinates) {
  return mapCoordinates.split(';')[0];
}

const getCoordinateY = function(mapCoordinates) {
  return mapCoordinates.split(';')[1];
}

let applyMapBorders = function() {
  numOfRows = world.getElementsByTagName('tbody')[0].getElementsByTagName('tr').length;
  numOfCols = world.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[0].getElementsByTagName('td').length;
  for (let r = 0; r < numOfRows; r++) {
    for (let c = 0; c < numOfCols; c++) {
      //pattern for doors: (cellId, type, destinationMap, destinationCellId)
      let thisCell = 'r' + r + 'c' + c;
      if (r === 0) {
        let cellToGo = 'r' + (worldHeight - 1) + 'c' + c;
        let y = Number(getCoordinateY(currentMapCoordinates)) + 1;
        let destinationMapCoordinates = getCoordinateX(currentMapCoordinates) + ';' + y;
        if (mapsNet.get(destinationMapCoordinates)) {
          placeDoor(thisCell, 'Top', mapsNet.get(destinationMapCoordinates), cellToGo);
        } else {
          let random = 'Unvisited-Map--Surface--' + Math.random() + '';
          placeDoor(thisCell, 'Top', random, cellToGo);
        }
        document.getElementById(thisCell).innerText = '↑';
        document.getElementById(thisCell).style.color = 'gold';
        document.getElementById(thisCell).style.textAlign = 'center';
      }
      if (c === (worldWidth - 1)) {
        let cellToGo = 'r' + r + 'c' + 0;
        let x = Number(getCoordinateX(currentMapCoordinates)) + 1;
        let destinationMapCoordinates = x + ';' + getCoordinateY(currentMapCoordinates);
        if (mapsNet.get(destinationMapCoordinates)) {
          placeDoor(thisCell, 'Right', mapsNet.get(destinationMapCoordinates), cellToGo);
        } else {
          let random = 'Unvisited-Map--Surface--' + Math.random() + '';
          placeDoor(thisCell, 'Right', random, cellToGo);
        }
        document.getElementById(thisCell).innerText = '→';
        document.getElementById(thisCell).style.color = 'gold';
        document.getElementById(thisCell).style.textAlign = 'center';
      }
      if (r === (worldHeight - 1)) {
        let cellToGo = 'r' + 0 + 'c' + c;
        let y = Number(getCoordinateY(currentMapCoordinates)) - 1;
        let destinationMapCoordinates = getCoordinateX(currentMapCoordinates) + ';' + y;
        if (mapsNet.get(destinationMapCoordinates)) {
          placeDoor(thisCell, 'Bottom', mapsNet.get(destinationMapCoordinates), cellToGo);
        } else {
          let random = 'Unvisited-Map--Surface--' + Math.random() + '';
          placeDoor(thisCell, 'Bottom', random, cellToGo);
        }
        document.getElementById(thisCell).innerText = '↓';
        document.getElementById(thisCell).style.color = 'gold';
        document.getElementById(thisCell).style.textAlign = 'center';
      }
      if (c === 0) {
        let cellToGo = 'r' + r + 'c' + (worldWidth - 1);
        let x = Number(getCoordinateX(currentMapCoordinates)) - 1;
        let destinationMapCoordinates = x + ';' + getCoordinateY(currentMapCoordinates);
        if (mapsNet.get(destinationMapCoordinates)) {
          placeDoor(thisCell, 'Left', mapsNet.get(destinationMapCoordinates), cellToGo);
        } else {
          let random = 'Unvisited-Map--Surface--' + Math.random() + '';
          placeDoor(thisCell, 'Left', random, cellToGo);
        }
        document.getElementById(thisCell).innerText = '←';
        document.getElementById(thisCell).style.color = 'gold';
        document.getElementById(thisCell).style.textAlign = 'center';
      }
    }
  }
}

var saveMap = function(name) {
  if (!name) {
    name = prompt('Name your map:');
  }
  dorrator.moveOut(dorrator.staysAt);
  /*if (appMode === 'Game') {
    karkafan.moveOut(karkafan.staysAt);
    beast.moveOut(beast.staysAt);
    gazhavana.moveOut(gazhavana.staysAt);
    garlab.moveOut(garlab.staysAt);
    torab.moveOut(torab.staysAt);
    tikul.moveOut(tikul.staysAt);
    sharlock.moveOut(sharlock.staysAt);
    kipan.moveOut(kipan.staysAt);
    spider.moveOut(spider.staysAt);
    magan.moveOut(magan.staysAt);
    kavayop.moveOut(magan.staysAt);
  }*/
  db.maps.put({name: name, data: document.getElementById('world').innerHTML});
}

var loadMap = function(name) {
    
  //saveMap(previousMapName);

  if (appMode === 'Editor') {
    closeEditor();
  }

  db.maps.get(name).then(map => {
    document.getElementById('world').innerHTML = map.data;
 
    var numberOfCells = document.getElementById('world').getElementsByTagName('td').length;
    for (var i = 0; i < numberOfCells; i++) {
      addControls(document.getElementById('world').getElementsByTagName('td')[i]);
    }
  
    if (appMode === 'Editor') {
      openEditor();
    }
  
    setObstacleVisibility(false);

    if (visitedMaps[visitedMaps.length - 1].split('--')[1] === 'Surface') {
      console.log('The condition is met');
      applyMapBorders();
    }

    obstaclesArr.length = 0;
    $('[data-obstacles]').each(function(index, elem) {
      obstaclesArr.push(elem.id);
    });

    drawMiniMap();

  });
}

/*if (appMode === 'Game') {
  loadMap('OS-RPG-Map-Seashore_woods');
} else {
  loadMap('OS-RPG-Map-Empty');
}*/

let changeSoundAmbience = function() {
  if (currentMapType === visitedMaps[visitedMaps.length - 1].split('--')[1]) {
    console.log('No need to change sound ambience: the map type is the same');
    return;
  }
  if (currentMapType === 'Cave' || currentMapType === 'Dungeon') {
    s1.stop();
    s4.stop();
    clearInterval(s2Surface);
    clearInterval(s3Surface);
    clearInterval(s5Surface);
    clearInterval(s6Surface);
    clearInterval(s7Surface);
    clearInterval(s8Surface);

    s6Cave = window.setInterval(playCaveS6, 20000);
    caveS1.play();
    caveS3.play();
    caveS4.play();
    caveS5.play();
    caveS7.play();

  }
  if (currentMapType === 'Surface') {
    clearInterval(s6Cave);
    caveS1.stop();
    caveS3.stop();
    caveS4.stop();
    caveS5.stop();
    caveS7.stop();

    s2Surface = window.setInterval(playS2, 30000);
    s3Surface = window.setInterval(playS3, 85000);
    s5Surface = window.setInterval(playS5, 60000);
    s6Surface = window.setInterval(playS6, 20000);
    s7Surface = window.setInterval(playS7, 20000);
    s8Surface = window.setInterval(playS8, 30000); 
    s1.play();
    s4.play();
  }
}

const bannedPrompts = [];

const showPrompt = function(topic, type = 'info', placement = {from: 'top', align: 'right'}) {
  if (!bannedPrompts.includes(topic) && appMode === 'Game') {

    $.notify({
      message: topic
    },{
      type: type,
      placement: placement,
      delay: 100000,
      onShow: bannedPrompts.push(topic)
    });
  }
}

const hidePrompt = function(topic) {
  $('[data-notify="container"]').each( (index, element) => {
    if (element.querySelector('[data-notify="message"]').innerHTML === topic) {
      element.remove();
    }
  });
}

const createInventoryButton = function() {
  let body = document.getElementsByTagName('body')[0];
  let div = document.createElement('div');
  body.appendChild(div);
  div.id = 'inventoryButton';
  div.className = 'inventory-button';
  div.title = 'Open inventory (' + hotKeysBindings.openInventory + ')';
  place('inventoryButton', '-448px -896px');
  place('inventoryButton', '-1312px -1088px');
  div.onclick = function() {
    dorrator.toggleInventory();
  }
}

const createHealthPotionButton = function() {
  let body = document.getElementsByTagName('body')[0];
  let div = document.createElement('div');
  body.appendChild(div);
  div.id = 'healthPotionButton';
  div.className = 'health-potion-button';
  div.title = 'Drink health potion (' + hotKeysBindings.heal + ')';
  div.innerHTML = '0';
  place('healthPotionButton', '-448px -896px');
  place('healthPotionButton', '-832px -1344px');
  div.onclick = function() {
    dorrator.drinkHealthPotion(dorrator.maxHealth - dorrator.health);
  }
}

const createHerbsStoragenButton = function() {
  let body = document.getElementsByTagName('body')[0];
  let div = document.createElement('div');
  body.appendChild(div);
  div.id = 'herbsStorageButton';
  div.className = 'herbs-storage-button';
  div.title = 'Check herbs storage';
  place('herbsStorageButton', '-448px -896px');
  place('herbsStorageButton', '-224px -2336px');
  div.onclick = function() {
    //dorrator.checkHerbsStorage();
    openHerbalStorage();
  }
}

const createDescConsoleButton = function() {
  let body = document.getElementsByTagName('body')[0];
  let div = document.createElement('div');
  body.appendChild(div);
  div.id = 'descConsoleButton';
  div.className = 'desc-console-button';
  div.title = 'See the history of your activity';
  place('descConsoleButton', '-448px -896px');
  place('descConsoleButton', '-1280px -928px');
  div.onclick = function() {
    $.notify({
        title: '<b>History:</b><br />',
        message: descConsole.innerHTML
      },{
        type: 'pastel-warning',
        placement: {from: 'top', align: 'left'},
        mouse_over: 'pause',
        template: '<div data-notify="container" class="alert alert-{0}" role="alert">' +
          '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
          '<span data-notify="title">{1}</span>' +
          '<span data-notify="message">{2}</span>' +
        '</div>'
    });
  }
}

const createGeneralPurposeButtonsPanel = function() {
  let body = document.getElementsByTagName('body')[0];
  let panel = document.createElement('div');
  body.appendChild(panel);
  panel.id = 'generalPurposeButtonsPanel';
  panel.className = 'general-purpose-buttons-panel';

  let btn1 = document.createElement('div');
  btn1.id = 'findPlayerButton';
  btn1.className = 'find-player-button';
  btn1.title = 'Find the player (' + hotKeysBindings.findPlayer + ')';
  panel.appendChild(btn1);
  place('findPlayerButton', '-448px -896px');
  place('findPlayerButton', '-1952px -1024px');
  btn1.onclick = function() {
    findPlayer();
  }
}

const print = function(text) {
  // TODO: Replace double spaces with 1 space in @text;
  descConsole.innerHTML = text + '<br />' + descConsole.innerHTML;
}

const isInRange = function(startCell, targetCell, distance) {
  if (startCell === targetCell) {
    return true;
  }
  let targetCellX = Number(targetCell.split('c')[1]);
  let targetCellY = Number(targetCell.split('c')[0].split('r')[1]);
  let startCellX = Number(startCell.split('c')[1]);
  let startCellY = Number(startCell.split('c')[0].split('r')[1]);
  distance = Number(distance);
  if (targetCellX - startCellX <= distance) {
    if (targetCellX - startCellX >= distance * -1) {
      if (targetCellY - startCellY <= distance) {
        if (targetCellY - startCellY >= distance * -1) {
          return true;
        }
      }
    }
  }
  return false;
}

const getFreeCells = function(centerCell, radius) {
  let centerCellRow = Number(centerCell.split('c')[0].split('r')[1]);
  let centerCellColl = Number(centerCell.split('c')[1]);

  let isCellFree = function(cellToCheck) {
    if (
      document.getElementById(cellToCheck) &&
      cellToCheck !== centerCell &&
      cellToCheck !== dorrator.staysAt &&
      !document.getElementById(cellToCheck).hasAttribute('data-obstacles') &&
      !document.getElementById(cellToCheck).hasAttribute('data-enemies')
      ) {
      return true;
    } else {
      return false;
    }
  }

  let arr = [];
  //check all cells around by changing initial row
  for (let i = radius * -1; i <= radius; i++) {
    //check all cells around by changing initial column (coll)
    for (let k = radius * -1; k <= radius; k++) {
      let currentCell = 'r' + (centerCellRow + i) + 'c' + (centerCellColl + k);
      if (isCellFree(currentCell)) {
        arr.push(currentCell);
      }
    }
  }

  return arr;
}

const isNextVersionOfTheGameAvailable = function() {
  var currentPath = location.href; //'https://dorrator.github.io/projects/RPG/v1.4.0/game.html'
  if (currentPath.includes('projects/RPG/v1.')) {
    var currentVersion = Number(currentPath.split('RPG/v1.')[1].split('.')[0]);
    var nextVersionPathPart1 = currentPath.split('RPG/v1.')[0];
    var nextVersionPathPart2 = currentPath.split('.0/')[1];
    var nextVersionPath = nextVersionPathPart1 + 'RPG/v1.' + (currentVersion + 1) + '.0/' + nextVersionPathPart2;
    
    function urlExists(testUrl) {
      var http = $.ajax({
          type:"HEAD",
          url: testUrl,
          async: false
      })
      return http.status;
      // this will return 200 on success, and 0, 404 or negative value on error
    }

    if (urlExists(nextVersionPath) === 200) {
      return true;
    } else {
      return false;
    }
  }
}

if (isNextVersionOfTheGameAvailable()) {
  showPrompt('You are playing an outdated version of the game. Check out the updated version <a href="https://dorrator.github.io/projects/RPG/welcome/">here</a>.', 'warning', {from: 'bottom', align: 'right'});
}

if (appMode === 'Game') {
  createInventoryButton();
}

createHealthPotionButton();
createHerbsStoragenButton();
createDescConsoleButton();

createGeneralPurposeButtonsPanel();