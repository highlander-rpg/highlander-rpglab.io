console.log('GUI rules!!!');

/*let msg = React.createElement('p', null, 'Hello, we a REACTive now!');
let pic = React.createElement('div', { className: 'prompt-pic'});
let container = React.createElement('div', null, msg, pic);
ReactDOM.render(container, document.getElementById('root'));*/

if (appMode === 'Editor') {
  
  let Creatures_panel_text = function() {
    return (
      <p>Creatures Panel (Unstable version 0.01):</p>
    );
  }
  
  class Creatures_buttons extends React.Component {
    render() {
      console.log(JSON.stringify(this.props.children));
      return (
        <div>
          {this.props.children}
          <button>{this.props.names[0]}</button>
          <button>{this.props.names[1]}</button>
          <button>{this.props.names[2]}</button>
        </div>
      )
    }
  }

  let names = ['Rageppa', 'Skifshroga', 'Tulup', 'Karatap'];
  let namesList = names.map((name, i) =><div key={'name_' + i}>{name}</div>);
  
  let Creatures_panel = function(props) {
    return (
      <div className="creatures-panel">
        <Creatures_panel_text />
        <div className={props.className}>{props.name}</div>
        <div className={props.className}>{props.name}</div>
        <Creatures_buttons names={["1", "2", "3"]}>Buttons: </Creatures_buttons>
        {namesList}
      </div>
      );
  }
  
  ReactDOM.render(<Creatures_panel
    className="creature-pic"
    name="1"/>,
    document.getElementById('root')
  );

  let transmitSpriteLocation = function(spriteLocation) {
    console.log(spriteLocation);
  }

}