let drawMiniMap = function() {

  let cvs = document.getElementById('miniMapCanvas');

  cvs.width = worldWidth;
  cvs.height = worldHeight;
  
  let cv = cvs.getContext('2d');
  
  $('#world').find('td').each((index, element) => {
    let x = Number(element.id.split('c')[1]);
    let y = Number(element.id.split('c')[0].split('r')[1]);
    //console.log(element.id, x, y, document.getElementById(element.id).hasAttribute('data-obstacles'));
    //cv.fillStyle = 'green';
    if (document.getElementById(element.id).hasAttribute('data-color')) {
      cv.fillStyle = document.getElementById(element.id).getAttribute('data-color');
    } else {
      //console.warn('No attribute data-color found at ' + element.id);
    }
    //cv.fillRect(x*2, y*2, 2, 2);
    cv.fillRect(x, y, 1, 1);
  });

}

drawMiniMap();