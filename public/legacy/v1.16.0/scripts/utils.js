const getRandomElement = function(array) { 
  return array[Math.floor(Math.random() * (array.length - 0) + 0)];
};

const getRandomNumber = function(min, max) {
  return Math.round(Math.random() * (max - min) + min);
};

const chance = function(percent = '100%') {
  percent = Number(percent.split('%')[0]) / 100;
  if (Math.random() < percent) {
    return true;
  } else {
    return false;
  }
};

const isInViewport = function (elem) {
  let bounding = elem.getBoundingClientRect();
  return (
    bounding.top >= 0 &&
    bounding.left >= 0 &&
    bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};
