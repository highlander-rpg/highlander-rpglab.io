var creatures = new Map;
var creaturesNet = new Map;
const deathSprites = ['-480px -1632px', '-512px -1632px', '-544px -1632px', '-576px -1632px', '-608px -1632px', '-640px -1632px', '-672px -1632px', '-704px -1632px', '-736px -1632px', '-768px -1632px', '-800px -1632px'];
const getRandomDeathSprite = function() {
  let max = deathSprites.length - 1;
  let min = 0;
  return deathSprites[Math.round(Math.random() * (max - min) + min)];
}
const promptAboutInventory = 'Press <kbd>' + hotKeysBindings.openInventory + '</kbd> to open inventory';
const promptAboutWSAD = 'Use <kbd>' + hotKeysBindings.moveUp + '</kbd>, <kbd>' + hotKeysBindings.moveDown + '</kbd>, <kbd>' + hotKeysBindings.moveLeft + '</kbd>, <kbd>' + hotKeysBindings.moveRight + '</kbd> to move';

class Player {

  constructor() {
    this.body = 'url("images/sprites.png") -96px -2688px, url("images/sprites.png") -1504px -2976px, url("images/sprites.png") -288px -2560px';
    this.hair = 'url("images/sprites.png") -288px -2720px, ';
    this.clothes = '';
    this.leftHand = '';
    this.rightHand = '';
    this.headGear = '';
    this.gloves = '';
    this.feet = '';
    this.produceAppearance();
    this.health = 100;
    this.maxHealth = 100;
    this.healthRestoreSpeed = 10000;
    this.healthState = 'Fixed';
    this.createHealthBar();
    this.stamina = 100;
    this.maxStamina = 100;
    this.staminaRestoreSpeed = 500;
    this.staminaState = 'Fixed';
    this.createStaminaBar();
    this.strength = 2;
    this.defense = 0;
    this.staysAt = 'r3c8'
    this.visionDistance = 3;
    this.name = 'Dorrator';
    this.inventory = new Map;
    this.coins = 0;
    this.createInventory();
    this.herbsStorage = new Map;
    this.herbsStorageLimit = 5;
    this.numberOfHealthPotions = 0;
    creatures.set(this.name, this);
  }

  produceAppearance() {
    this.appearance = this.rightHand + this.leftHand + this.clothes + this.gloves + this.feet + this.headGear + this.hair + this.body;
    this.appearance = this.appearance.replace('url("images/sprites.png") ', ''); // Cut out first 'url("images/sprites.png") ' - it is added by place() function;
  }

  createHealthBar() {
    let body = document.getElementsByTagName('body')[0];
    let div = document.createElement('div');
    div.id = 'health-bar';
    div.classList.add('health-bar');
    div.style.width = this.health + 'px';
    div.setAttribute('title', 'Health');
    let healthPoints = document.createTextNode(this.health + '/' + this.maxHealth);
    div.appendChild(healthPoints);
    body.appendChild(div);
  }

  removeHealthBar() {
    document.getElementById('health-bar').parentNode.removeChild(document.getElementById('health-bar'));
  }

  refreshHealthBar() {
    this.removeHealthBar();
    this.createHealthBar();
  }

  receiveDamage(points = 1, direction = 'All') {
    let x = points;
    let y = points;
    switch (direction) {
      case 'All':
        break;
      case 'Right':
        x = points * -1;
        y = 0;
        break;
      case 'Bottom':
        x = 0;
        y = points * -1;
        break;
      case 'Left':
        x = points;
        y = 0;
        break;
      case 'Top':
        x = 0;
        y = points;
        break;
      default:
        break;
    }
    let fightingGround = document.getElementById(this.staysAt);
    fightingGround.style.boxShadow = 'inset ' + x + 'px ' + y + 'px 20px 2px red';
    if (playerPic) {
      playerPic.style.boxShadow = 'inset ' + x + 'px ' + y + 'px 20px 2px red';
    }
    let assignPreviousStyle = function() {
      fightingGround.style.boxShadow = '';
      if (playerPic) {
        playerPic.style.boxShadow = ''; 
      }
    }
    setTimeout(assignPreviousStyle, 200);
    points = points - Math.round(this.defense / 10);
    if (points < 0) {
      points = 0;
    }
    this.health = this.health - points;
    print('You got ' + points + ' points of damage');
    this.refreshHealthBar();
    if (this.health <= 0) {
      this.die();
    }
    if (this.healthState === 'Fixed') {
      let increaseHealth = this.increaseHealth.bind(this, 1);
      healthIncreasingProcess = setInterval(increaseHealth, this.healthRestoreSpeed);
      this.healthState = 'Increasing';
    }
    //showPrompt('Push enemy to attack it!', 'danger');
  }

  attack(cellID, points, direction) {
    // cellID = cellID;
    // if (!points) {
    //   points = getRandomNumber(1, this.strength);
    // } else {
    //   points = getRandomNumber(1, points);
    // }
    // direction = direction;
    let enemyName = document.getElementById(cellID).getAttribute('data-enemies');
    playCombatGame(enemyName);
    // let enemy = creatures.get(enemyName);
    // enemy.receiveDamage(points, direction);
    // this.reduceStamina(2);
    // createjs.Sound.play(randomHitSound());
  }

  throwThingAt(cellId) {
    if (rightHandCell.getAttribute('data-items') || leftHandCell.getAttribute('data-items')) {
      let thing = {};
      if (rightHandCell.hasAttribute('data-items')) {
        thing = items.get(rightHandCell.getAttribute('data-items'));
      } else {
        thing = items.get(leftHandCell.getAttribute('data-items'));
      }

      dorrator.unequip(thing.id);

      let flyingThingSprites = ["-1504px -800px", "-1536px -800px", "-1568px -800px", "-1600px -800px", "-1632px -800px", "-1664px -800px", "-1696px -800px", "-1728px -800px"];
      if (thing.isNet === true) {
        flyingThingSprites = ['-992px -832px', '-1024px -832px', '-1056px -832px', '-1088px -832px', '-1120px -832px', '-1152px -832px', '-1184px -832px', '-1216px -832px'];
      }

      let i = 0;

      const destinationCellX = Number(cellId.split('c')[1]);
      const destinationCellY = Number(cellId.split('c')[0].split('r')[1]);
      let cellOfThingX = dorrator.getCol();
      let cellOfThingY = dorrator.getRow();

      let timerID = setInterval(function() {
        let launchCellAppearance = document.getElementById(dorrator.staysAt).style.background;
        let prevThingPlace = 'r' + cellOfThingY + 'c' + cellOfThingX;
        if (destinationCellX < cellOfThingX) {
          //this.moveLeft();
          cellOfThingX = cellOfThingX - 1;
        }
        if (destinationCellX > cellOfThingX) {
          //this.moveRight();
          cellOfThingX = cellOfThingX + 1;
        }
        if (destinationCellY < cellOfThingY) {
          //this.moveUp();
          cellOfThingY = cellOfThingY - 1;
        }
        if (destinationCellY > cellOfThingY) {
          //this.moveDown();
          cellOfThingY = cellOfThingY + 1;
        }

        let prevSprite = flyingThingSprites[i - 1];
        if (i === 0) {
          prevSprite = flyingThingSprites[flyingThingSprites.length - 1];
        }

        remove(prevThingPlace, prevSprite);
        document.getElementById(dorrator.staysAt).style.background = launchCellAppearance;
        place('r' + cellOfThingY + 'c' + cellOfThingX, flyingThingSprites[i]);


        let lastSprite = '';
        if (i === flyingThingSprites.length - 1) {
          remove(prevThingPlace, flyingThingSprites[i]);
          i = 0;
          lastSprite = flyingThingSprites[flyingThingSprites.length-1];
        } else {
          i++;
          lastSprite = flyingThingSprites[i-1];
        }

        if (document.getElementById('r' + cellOfThingY + 'c' + cellOfThingX).hasAttribute('data-enemies')) {
          //dorrator.attack('r' + cellOfThingY + 'c' + cellOfThingX, (dorrator.strength * 10), 'Right');
          let enemy = document.getElementById('r' + cellOfThingY + 'c' + cellOfThingX).getAttribute('data-enemies');
          enemy = creatures.get(enemy);
          if (thing.isNet) {
            enemy.becomeImmobilized();
            place('r' + cellOfThingY + 'c' + cellOfThingX, Traps.spriteOfActivatedNet);
            items.delete(thing.id);
          } else {
            enemy.receiveDamage(dorrator.strength * 10);
            place('r' + cellOfThingY + 'c' + cellOfThingX, thing.inventoryAppearance);
            document.getElementById('r' + cellOfThingY + 'c' + cellOfThingX).setAttribute('data-items', thing.id);
          }
          remove('r' + cellOfThingY + 'c' + cellOfThingX, lastSprite);
          clearInterval(timerID);
        } else if (destinationCellY === cellOfThingY && destinationCellX === cellOfThingX) {
          if (thing.isNet) {
            place('r' + cellOfThingY + 'c' + cellOfThingX, thing.inventoryAppearance);
            document.getElementById('r' + cellOfThingY + 'c' + cellOfThingX).setAttribute('data-items', thing.id);
          } else {
            place('r' + cellOfThingY + 'c' + cellOfThingX, thing.inventoryAppearance);
            document.getElementById('r' + cellOfThingY + 'c' + cellOfThingX).setAttribute('data-items', thing.id);
          }
          remove('r' + cellOfThingY + 'c' + cellOfThingX, lastSprite);
          clearInterval(timerID);
        }

      }, 100);
    }
  }

  throwFireball(cellId) {
    const fireballFlightSprites = ['-1568px -768px', '-1600px -768px', '-1632px -768px'];
    const fireballBoomSprites = ['-1216px -736px', '-1248px -736px', '-1280px -736px', '-1312px -736px'];

    let i = 0;

    const destinationCellX = Number(cellId.split('c')[1]);
    const destinationCellY = Number(cellId.split('c')[0].split('r')[1]);
    let cellOfThingX = dorrator.getCol();
    let cellOfThingY = dorrator.getRow();

    let timerID = setInterval(function() {
      let launchCellAppearance = document.getElementById(dorrator.staysAt).style.background;
      let prevThingPlace = 'r' + cellOfThingY + 'c' + cellOfThingX;
      if (destinationCellX < cellOfThingX) {
        //this.moveLeft();
        cellOfThingX = cellOfThingX - 1;
      }
      if (destinationCellX > cellOfThingX) {
        //this.moveRight();
        cellOfThingX = cellOfThingX + 1;
      }
      if (destinationCellY < cellOfThingY) {
        //this.moveUp();
        cellOfThingY = cellOfThingY - 1;
      }
      if (destinationCellY > cellOfThingY) {
        //this.moveDown();
        cellOfThingY = cellOfThingY + 1;
      }

      let prevSprite = fireballFlightSprites[i - 1];
      if (i === 0) {
        prevSprite = fireballFlightSprites[fireballFlightSprites.length - 1];
      }

      remove(prevThingPlace, prevSprite);
      document.getElementById(dorrator.staysAt).style.background = launchCellAppearance;
      place('r' + cellOfThingY + 'c' + cellOfThingX, fireballFlightSprites[i]);


      let lastSprite = '';
      if (i === fireballFlightSprites.length - 1) {
        remove(prevThingPlace, fireballFlightSprites[i]);
        i = 0;
        lastSprite = fireballFlightSprites[fireballFlightSprites.length-1];
      } else {
        i++;
        lastSprite = fireballFlightSprites[i-1];
      }

      if (document.getElementById('r' + cellOfThingY + 'c' + cellOfThingX).hasAttribute('data-enemies')) {
        //dorrator.attack('r' + cellOfThingY + 'c' + cellOfThingX, (dorrator.strength * 10), 'Right');
        let enemy = document.getElementById('r' + cellOfThingY + 'c' + cellOfThingX).getAttribute('data-enemies');
        enemy = creatures.get(enemy);
        enemy.receiveDamage(dorrator.strength * 10);
        const currentCell = 'r' + cellOfThingY + 'c' + cellOfThingX;
        remove(currentCell, lastSprite);
        Effects.animateSprites(currentCell, Effects.fireExplosionSprites);
        clearInterval(timerID);
      } else if (destinationCellY === cellOfThingY && destinationCellX === cellOfThingX) {
        const currentCell = 'r' + cellOfThingY + 'c' + cellOfThingX;
        remove(currentCell, lastSprite);
        Effects.animateSprites(currentCell, Effects.fireExplosionSprites);
        clearInterval(timerID);
      }

    }, 100);
  }

  increaseHealth(points = 1) {
    if (!game.isPaused) {
      if (this.health >= this.maxHealth || this.stamina < this.maxStamina) {
        clearInterval(healthIncreasingProcess);
        this.healthState = 'Fixed';
        return;
      }
      this.health += points;
      this.refreshHealthBar();
    }
  }

  drinkHealthPotion(points = 10) {
    if (this.numberOfHealthPotions > 0) {
      this.stamina = this.maxStamina;
      this.increaseHealth(points);
      let txtDrank = getRandomElement(['drank', 'used']);
      print(`You ${txtDrank} a health potion. <b>HP ${points}</b>.`);
      print('Your stamina is also refilled.');
      this.removeHealthPotion();
    } else {
      print('You looked for a health potion but found nothing.');
    }
  }

  obtainHealthPotion(bottles = 1) {
    this.numberOfHealthPotions += bottles;
    document.querySelector('#healthPotionButton').innerHTML = this.numberOfHealthPotions;
  }

  removeHealthPotion(bottles = 1) {
    if (this.numberOfHealthPotions > 0) {
      this.numberOfHealthPotions -= bottles;
      document.querySelector('#healthPotionButton').innerHTML = this.numberOfHealthPotions;
    }
  }

  obtainHerb(herbObject) {
    // herbObject example: { latName: 'Acacia senegal', comName: 'Gum arabic', medDesc: '', photo: 'Khair.JPG' }
    let newHerbObject = Object.assign({}, herbObject);
    if (!this.herbsStorage.has(newHerbObject.latName) && this.herbsStorage.size < this.herbsStorageLimit) {
      newHerbObject.count = 1;
      this.herbsStorage.set(newHerbObject.latName, newHerbObject);
      print(`Herb ${newHerbObject.latName} is taken.`);
    } else  if (this.herbsStorage.has(newHerbObject.latName)) {
      let newNumber = Number(this.herbsStorage.get(newHerbObject.latName).count);
      newNumber++;
      newHerbObject.count = newNumber;
      this.herbsStorage.set(newHerbObject.latName, newHerbObject);
      print(`Herb ${newHerbObject.latName} is added.`);
    } else {
      print(`You tried to take ${newHerbObject.latName}, but your herbal storage is full.`);
    }
  }

  removeHerbFromHerbsStorage(latName) {
    if (this.herbsStorage.has(latName)) {
      this.herbsStorage.delete(latName);
    }
  }

  obtainCoins(numberOfCoins) {
    this.coins += Number(numberOfCoins);
    this.refreshStatistics();
  }

  removeCoins(numberOfCoins) {
    this.coins -= Number(numberOfCoins);
    this.refreshStatistics();
  }

  // checkHerbsStorage() {
  //   if (this.herbsStorage.size) {
  //     this.herbsStorage.forEach(function(value, key) {
  //       print(`You have herb ${key} (${value.count}/5)`);
  //       if (value.count >= 5) {
  //         print(`<span class="bg-primary">Great!</span> You've collected enough ${key}. Your ${value.count} plants of ${key} can be converted to 1 health potion.`);
  //         playPotionMakingGame(value);
  //       }
  //     });
  //   } else {
  //     print(`Your herbs storage is empty.`);
  //   }
  // }

  createStaminaBar() {
    let body = document.getElementsByTagName('body')[0];
    let div = document.createElement('div');
    div.id = 'stamina-bar';
    div.classList.add('stamina-bar');
    div.style.width = this.stamina + 'px';
    div.setAttribute('title', 'Stamina');
    let staminaPoints = document.createTextNode(this.stamina + '/' + this.maxStamina);
    div.appendChild(staminaPoints);
    body.appendChild(div);
  }

  removeStaminaBar() {
    document.getElementById('stamina-bar').parentNode.removeChild(document.getElementById('stamina-bar'));
  }

  refreshStaminaBar() {
    this.removeStaminaBar();
    this.createStaminaBar();
  }

  reduceStamina(points = 1) {
    this.stamina = this.stamina - points;
    this.refreshStaminaBar();
    if (this.stamina <= 0) {
      showPrompt('Ragnarr is almost tired to death!', 'danger');
    }
    if (this.staminaState === 'Fixed') {
      let increaseStamina = this.increaseStamina.bind(this, 1)
      staminaIncreasingProcess = setInterval(increaseStamina, this.staminaRestoreSpeed);
      this.staminaState = 'Increasing';
    }
  }

  increaseStamina(points = 2) {
    this.stamina += points;
    this.refreshStaminaBar();
    if (this.stamina >= this.maxStamina) {
      clearInterval(staminaIncreasingProcess);
      this.staminaState = 'Fixed';
      if (this.healthState === 'Fixed' && this.health <= this.maxHealth) {
        let increaseHealth = this.increaseHealth.bind(this, 1)
        healthIncreasingProcess = setInterval(increaseHealth, this.healthRestoreSpeed);
        this.healthState = 'Increasing';
      }
    }
  }

  equip(itemID) {
    let itemCategory = items.get(itemID).equipmentCategory;
    switch (itemCategory) {
      case 'rightHandCell':
        this.equipRightHand(items.get(itemID).onCreatureAppearance);
        this.strength += items.get(itemID).effect;
        break;
      case 'clothesCell':
        this.equipBody(items.get(itemID).onCreatureAppearance);
        this.defense += items.get(itemID).effect;
        break;
      case 'headgearCell':
        this.equipHead(items.get(itemID).onCreatureAppearance);
        this.defense += items.get(itemID).effect;
        break;
      case 'necklaceCell':
        /*Currently unavailable*/
        break;
      case 'glovesCell':
        this.equipGloves(items.get(itemID).onCreatureAppearance);
        this.defense += items.get(itemID).effect;
        break;
      case 'leftHandCell':
        this.equipLeftHand(items.get(itemID).onCreatureAppearance);
        this.defense += items.get(itemID).effect;
        break;
      case 'footgearCell':
        this.equipFeet(items.get(itemID).onCreatureAppearance);
        this.defense += items.get(itemID).effect;
        break;
    }
    this.refreshStatistics();
  }

  unequip(itemID) {
    let itemCategory = items.get(itemID).equipmentCategory;
    switch (itemCategory) {
      case 'rightHandCell':
        this.unequipRightHand(items.get(itemID).onCreatureAppearance);
        this.strength -= items.get(itemID).effect;
        break;
      case 'clothesCell':
        this.unequipBody(items.get(itemID).onCreatureAppearance);
        this.defense -= items.get(itemID).effect;
        break;
      case 'headgearCell':
        this.unequipHead(items.get(itemID).onCreatureAppearance);
        this.defense -= items.get(itemID).effect;
        break;
      case 'necklaceCell':
        /*Currently unavailable*/
        break;
      case 'glovesCell':
        this.unequipGloves(items.get(itemID).onCreatureAppearance);
        this.defense -= items.get(itemID).effect;
        break;
      case 'leftHandCell':
        this.unequipLeftHand(items.get(itemID).onCreatureAppearance);
        this.defense -= items.get(itemID).effect;
        break;
      case 'footgearCell':
        this.unequipFeet(items.get(itemID).onCreatureAppearance);
        this.defense -= items.get(itemID).effect;
        break;
    }
    this.refreshStatistics();
  }

  equipRightHand(onCreatureAppearance) {
    this.moveOut(this.staysAt);
    this.rightHand = 'url("images/sprites.png") ' + onCreatureAppearance + ', ';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  unequipRightHand() {
    this.moveOut(this.staysAt);
    this.rightHand = '';
    this.produceAppearance();
    rightHandCell.title = '';
    removeItem('rightHandCell', rightHandCell.getAttribute('data-items'));
    place(this.staysAt, this.appearance);
  }

  equipLeftHand(onCreatureAppearance) {
    this.moveOut(this.staysAt);
    this.leftHand = 'url("images/sprites.png") ' + onCreatureAppearance + ', ';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  unequipLeftHand() {
    this.moveOut(this.staysAt);
    this.leftHand = '';
    this.produceAppearance();
    leftHandCell.title = '';
    removeItem('leftHandCell', leftHandCell.getAttribute('data-items'));
    place(this.staysAt, this.appearance);
  }

  equipBody(onCreatureAppearance) {
    this.moveOut(this.staysAt);
    this.clothes = 'url("images/sprites.png") ' + onCreatureAppearance + ', ';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  unequipBody() {
    this.moveOut(this.staysAt);
    this.clothes = '';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  equipGloves(onCreatureAppearance) {
    this.moveOut(this.staysAt);
    this.gloves = 'url("images/sprites.png") ' + onCreatureAppearance + ', ';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  unequipGloves() {
    this.moveOut(this.staysAt);
    this.gloves = '';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  equipFeet(onCreatureAppearance) {
    this.moveOut(this.staysAt);
    this.feet = 'url("images/sprites.png") ' + onCreatureAppearance + ', ';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  unequipFeet() {
    this.moveOut(this.staysAt);
    this.feet = '';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  equipHead(onCreatureAppearance) {
    this.moveOut(this.staysAt);
    this.headGear = 'url("images/sprites.png") ' + onCreatureAppearance + ', ';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  unequipHead() {
    this.moveOut(this.staysAt);
    this.headGear = '';
    this.produceAppearance();
    place(this.staysAt, this.appearance);
  }

  moveOut(cellId) {
    if (this.staysAt === cellId) {
      document.getElementById(cellId).style.background = document.getElementById(cellId).style.background.replace('url("images/sprites.png") ' + this.appearance + ', ', '');
    }
  }

  moveTo(cellId) {
    if (game.isPaused || document.getElementById(cellId).getAttribute('data-obstacles') === 'All' || this.stamina <= 0) {
      return;
    }
    this.moveOut(this.staysAt);
    this.closeContainer();
    this.closeInventory();
    place(cellId, this.appearance);
    this.staysAt = cellId;
    this.reduceStamina(1);
    if (document.getElementById(cellId).getAttribute('data-door')) {
      showPrompt('Move to the direction of green line to enter another map');
    }
    if (document.getElementById(cellId).getAttribute('data-container')) {
      showPrompt(promptAboutInventory);
    }
    if (document.getElementById(cellId).getAttribute('data-items')) {
      showPrompt('Press <kbd>' + hotKeysBindings.use + '</kbd> to take items from the ground');
    }
    
    //move camera (variant 1);
    /*let x = (this.getCol() * 32) + ( - window.screen.width / 2);
    if (x < 0) {
      world.style.left = Math.abs(x) + 'px';
      fogOfWar.style.left = Math.abs(x) + 'px';
    } else {
      world.style.left = '-' + x + 'px';
      fogOfWar.style.left = '-' + x + 'px';
    }
    let y = (this.getRow() * 32) + ( - window.screen.height / 3);
    if (y < 0) {
      world.style.top = Math.abs(y) + 'px';
      fogOfWar.style.top = Math.abs(y) + 'px';
    } else {
      world.style.top = '-' + y + 'px';
      fogOfWar.style.top = '-' + y + 'px';
    }*/

 
    //move camera (variant 2);
    let keyCell = document.getElementById(this.staysAt).getClientRects()[0];

    if (keyCell.left < 32 * 5) {
      let x = document.getElementById(cellId).getBoundingClientRect().left + window.scrollX;
      window.scroll({ left: (x + (screen.width / 3) * -1), behavior: 'smooth' });
    }

    if (keyCell.top < 32 * 5) {
      let y = document.getElementById(cellId).getBoundingClientRect().top + window.scrollY;
      window.scroll({ top: (y + (screen.height / 3) * -1), behavior: 'smooth' });
    }

    if (keyCell.left > screen.width - 32 * 5) {
      let x = document.getElementById(cellId).getBoundingClientRect().left + window.scrollX;
      window.scroll({ left: (x + (screen.width / 3) * 1), behavior: 'smooth' });
    }

    if (keyCell.top > screen.height - 32 * 10) {
      let y = document.getElementById(cellId).getBoundingClientRect().top + window.scrollY;
      window.scroll({ top: (y + (screen.height / 8) * -1), behavior: 'smooth' });
    }



    //see around;
    if (currentMapType === 'Dungeon' || currentMapType === 'Cave') {
      visArr.forEach(id => {
        if (isTileVisible(id)) {
          document.getElementById(id).style.opacity = 0;
          if (!wasSeenVisArr.includes(id) && obstaclesArr.includes(id.split('F-')[1])) {
            wasSeenVisArr.push(id); 
          }
        } else {
          if (!wasSeenVisArr.includes(id)) {
            document.getElementById(id).style.opacity = 1;
          } else {
            document.getElementById(id).style.opacity = 0.6;
          }
        }
      });
    }

  }

  getPlace() {
    return this.staysAt;
  }

  getRow() {
    return +this.getPlace().split('c')[0].split('r')[1];
  }

  getCol() {
    return +this.getPlace().split('c')[1];
  }

  moveUp() {
    if (document.getElementById(this.staysAt).hasAttribute('data-door')) {
      if (document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[0] === 'Top') {
        var destinationMapName = document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[1];
        var destinationMapType = destinationMapName.split('--')[1];
        var destinationCellId = document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[2];
        playerCameFrom = 'bottom';
        if (destinationMapName === 'Random--Cave') {
          futureMapName = 'OS-RPG-Map--' + 'Cave' + '--' + Math.random();
          removeDoor(this.staysAt);
          placeDoor(this.staysAt, 'Top', futureMapName);
        }
        if (destinationMapName === 'Random--Dungeon') {
          futureMapName = 'OS-RPG-Map--' + 'Dungeon' + '--' + Math.random();
          removeDoor(this.staysAt);
          placeDoor(this.staysAt, 'Top', futureMapName);
        }
        //$('[data-enemies]').removeAttr('data-enemies');
        saveMap(currentMapName);
        visitedMaps.push(currentMapName);
        currentMapName = destinationMapName;
        if (destinationMapName === 'Random--Cave' || destinationMapName === 'Random--Dungeon') {
          currentMapName = futureMapName;
        }
        currentMapType = destinationMapType;
        cellToPlacePlayer = destinationCellId;
        enterMap();
        return;
      }
    }
    let newRow = this.getRow() - 1;
    let cellToGo = 'r' + newRow + 'c' + this.getCol();
    if (document.getElementById(cellToGo).getAttribute('data-enemies')) {
      this.attack(cellToGo, this.strength, 'Bottom');
      return;
    }
    if (document.getElementById(this.staysAt).getAttribute('data-obstacles') === 'Top') {
      return;
    }
    if (document.getElementById(cellToGo).getAttribute('data-enemies')) {

    }
    this.moveTo(cellToGo);
    showPrompt(promptAboutInventory);
  }

  moveDown() {
    if (document.getElementById(this.staysAt).hasAttribute('data-door')) {
      if (document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[0] === 'Bottom') {
        var destinationMapName = document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[1];
        var destinationMapType = destinationMapName.split('--')[1];
        var destinationCellId = document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[2];
        playerCameFrom = 'top';
        //$('[data-enemies]').removeAttr('data-enemies');
        if (destinationMapName === 'Random--Cave') {
          futureMapName = 'OS-RPG-Map--' + 'Cave' + '--' + Math.random();
          removeDoor(this.staysAt);
          placeDoor(this.staysAt, 'Bottom', futureMapName);
        }
        if (destinationMapName === 'Random--Dungeon') {
          futureMapName = 'OS-RPG-Map--' + 'Dungeon' + '--' + Math.random();
          removeDoor(this.staysAt);
          placeDoor(this.staysAt, 'Bottom', futureMapName);
        }
        saveMap(currentMapName);
        visitedMaps.push(currentMapName);
        currentMapName = destinationMapName;
        if (destinationMapName === 'Random--Cave' || destinationMapName === 'Random--Dungeon') {
          currentMapName = futureMapName;
        }
        currentMapType = destinationMapType;
        cellToPlacePlayer = destinationCellId;
        enterMap();
        return;
      }
    }
    let newRow = this.getRow() + 1;
    let cellToGo = 'r' + newRow + 'c' + this.getCol();
    if (document.getElementById(cellToGo).getAttribute('data-enemies')) {
      this.attack(cellToGo, this.strength, 'Top');
      return;
    }
    if (document.getElementById(cellToGo).getAttribute('data-obstacles') === 'Top') {
      return;
    }
    this.moveTo(cellToGo);
  }

  moveRight() {
    if (document.getElementById(this.staysAt).hasAttribute('data-door')) {
      if (document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[0] === 'Right') {
        var destinationMapName = document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[1];
        var destinationMapType = destinationMapName.split('--')[1];
        var destinationCellId = document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[2];
        playerCameFrom = 'left';
        //$('[data-enemies]').removeAttr('data-enemies');
        if (destinationMapName === 'Random--Cave') {
          futureMapName = 'OS-RPG-Map--' + 'Cave' + '--' + Math.random();
          removeDoor(this.staysAt);
          placeDoor(this.staysAt, 'Right', futureMapName);
        }
        if (destinationMapName === 'Random--Dungeon') {
          futureMapName = 'OS-RPG-Map--' + 'Dungeon' + '--' + Math.random();
          removeDoor(this.staysAt);
          placeDoor(this.staysAt, 'Bottom', futureMapName);
        }
        saveMap(currentMapName);
        visitedMaps.push(currentMapName);
        currentMapName = destinationMapName;
        if (destinationMapName === 'Random--Cave' || destinationMapName === 'Random--Dungeon') {
          currentMapName = futureMapName;
        }
        currentMapType = destinationMapType;
        cellToPlacePlayer = destinationCellId;
        enterMap();
        return;
      }
    }
    let newCol = this.getCol() + 1;
    let cellToGo = 'r' + this.getRow() + 'c' + newCol;
    if (document.getElementById(cellToGo).getAttribute('data-enemies')) {
      this.attack(cellToGo, this.strength, 'Left');
      return;
    }
    this.moveTo(cellToGo);
  }

  moveLeft() {
    if (document.getElementById(this.staysAt).hasAttribute('data-door')) {
      if (document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[0] === 'Left') {
        var destinationMapName = document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[1];
        var destinationMapType = destinationMapName.split('--')[1];
        var destinationCellId = document.getElementById(this.staysAt).getAttribute('data-door').split(', ')[2];
        playerCameFrom = 'right';
        //$('[data-enemies]').removeAttr('data-enemies');
        if (destinationMapName === 'Random--Cave') {
          futureMapName = 'OS-RPG-Map--' + 'Cave' + '--' + Math.random();
          removeDoor(this.staysAt);
          placeDoor(this.staysAt, 'Left', futureMapName);
        }
        if (destinationMapName === 'Random--Dungeon') {
          futureMapName = 'OS-RPG-Map--' + 'Dungeon' + '--' + Math.random();
          removeDoor(this.staysAt);
          placeDoor(this.staysAt, 'Bottom', futureMapName);
        }
        saveMap(currentMapName);
        visitedMaps.push(currentMapName);
        currentMapName = destinationMapName;
        if (destinationMapName === 'Random--Cave' || destinationMapName === 'Random--Dungeon') {
          currentMapName = futureMapName;
        }
        currentMapType = destinationMapType;
        cellToPlacePlayer = destinationCellId;
        enterMap();
        return;
      }
    }
    let newCol = this.getCol() - 1;
    let cellToGo = 'r' + this.getRow() + 'c' + newCol;
    if (document.getElementById(cellToGo).getAttribute('data-enemies')) {
      this.attack(cellToGo, this.strength, 'Right');
      return;
    }
    this.moveTo(cellToGo);
  }

  walkTo(destinationCellId, action) {

    let grid = new PF.Grid(worldWidth, worldHeight);
    $('#world').find('td').each( function(index, element) {
      if ($(element).attr('data-obstacles')) {
        let x = element.id.split('c')[1];
        let y = element.id.split('c')[0].split('r')[1];
        grid.setWalkableAt(x, y, false);
      }
    });

    let finder = new PF.AStarFinder({ allowDiagonal: true });
    
    let startX = this.staysAt.split('c')[1];
    let startY = this.staysAt.split('c')[0].split('r')[1];
    let finishX = destinationCellId.split('c')[1];
    let finishY = destinationCellId.split('c')[0].split('r')[1];
    let path = finder.findPath(startX, startY, finishX, finishY, grid);

    let pathToGo = []
    path.forEach(function(element) {
      let x = element[1];
      let y = element[0];
      pathToGo.push('r' + x + 'c' + y);
      document.getElementById('r' + x + 'c' + y).innerText = 'o';
      document.getElementById('r' + x + 'c' + y).style.color = 'green';
      setTimeout(function() {
        document.getElementById('r' + x + 'c' + y).innerText = '';
      }, 2000);
    });

    this.walk(pathToGo, action);
  }

  walk(path, action) {
    // call should be like so: dorrator.walk(['r2c1', 'r2c2', 'r2c3', 'r3c3', 'r3c4']);
    simultaneousWalkingSessionsCount++;
    let index = 0;
    let timerID = setInterval(function() {

      let enemyNearBy = false;
      let cellToPlacePlayerC = Number(dorrator.staysAt.split('c')[1]);
      let cellToPlacePlayerR = Number(dorrator.staysAt.split('c')[0].split('r')[1]);
      if (dorrator.getCol() !== 0 && dorrator.getCol() !== worldWidth - 1 && dorrator.getRow() !== 0 && dorrator.getRow() !== worldHeight - 1) {
        if (
          document.getElementById('r' + cellToPlacePlayerR + 'c' + (cellToPlacePlayerC+1)).hasAttribute('data-enemies') ||
          document.getElementById('r' + (cellToPlacePlayerR+1) + 'c' + (cellToPlacePlayerC+1)).hasAttribute('data-enemies') ||
          document.getElementById('r' + (cellToPlacePlayerR+1) + 'c' + cellToPlacePlayerC).hasAttribute('data-enemies') ||
          document.getElementById('r' + (cellToPlacePlayerR+1) + 'c' + (cellToPlacePlayerC-1)).hasAttribute('data-enemies') ||
          document.getElementById('r' + cellToPlacePlayerR + 'c' + (cellToPlacePlayerC-1)).hasAttribute('data-enemies') ||
          document.getElementById('r' + (cellToPlacePlayerR-1) + 'c' + (cellToPlacePlayerC-1)).hasAttribute('data-enemies') ||
          document.getElementById('r' + (cellToPlacePlayerR-1) + 'c' + cellToPlacePlayerC).hasAttribute('data-enemies') ||
          document.getElementById('r' + (cellToPlacePlayerR-1) + 'c' + (cellToPlacePlayerC+1)).hasAttribute('data-enemies')
          ) {
          enemyNearBy = true;
        }
      }
      

      if (enemyNearBy) {
        if (document.getElementById(path[index+1]).getAttribute('data-enemies')) {
          dorrator.attack(path[index+1], dorrator.strength, 'Right');
        }
        clearInterval(timerID);
        simultaneousWalkingSessionsCount--;
      } else if (simultaneousWalkingSessionsCount > 1) {
        simultaneousWalkingSessionsCount--;
        clearInterval(timerID);
      } else {
        dorrator.moveTo(path[index++ % path.length]);
        if (index === path.length) {
          clearInterval(timerID);
          simultaneousWalkingSessionsCount--;
          if (action) {
            if (action === 'take') {
              dorrator.take();
            }
            if (action === 'enter') {
              if (document.getElementById(dorrator.staysAt).hasAttribute('data-door')) {
                switch (document.getElementById(dorrator.staysAt).getAttribute('data-door').split(',')[0]) {
                  case 'Top':
                    dorrator.moveUp();
                    break;
                  case 'Right':
                    dorrator.moveRight();
                    break;
                  case 'Bottom':
                    dorrator.moveDown();
                    break;
                  case 'Left':
                    dorrator.moveLeft();
                    break;
                }
              }
            }
            if (action === 'openContainer') {
              dorrator.toggleInventoryAndContainer();
            }
          }
        }
      }

    }, 300);
  }

  lookAround() {
    var revealCell = function(cellId) {
      document.getElementById('F-' + cellId).style.opacity = 0;
    }
    var revealArea = function(centerPoint, radius) {
      var y = centerPoint.split('c')[0].split('r')[1];
      var x = centerPoint.split('c')[1];
      return y + " " + x;
    }
    revealCell(this.staysAt);
  }

  sayName() {
    console.log('My name is ' + this.name);
  }

  obtainItem(itemID, cellToPut) {
    if (typeof itemID === 'string') {
      if (document.getElementById(items.get(itemID).equipmentCategory) && !document.getElementById(items.get(itemID).equipmentCategory).getAttribute('data-items')) {
        //auto-equip;
        this.inventory.set(cellToPut, items.get(itemID));
        this.equip(itemID);
        cellToPut = items.get(itemID).equipmentCategory;
        this.inventory.set(cellToPut, items.get(itemID));
        place(cellToPut, items.get(itemID).inventoryAppearance);
        document.getElementById(cellToPut).setAttribute('data-items', itemID);
        document.getElementById(cellToPut).setAttribute('title', items.get(itemID).name + ' (+' + items.get(itemID).effect + ')');
      } else if (cellToPut && this.inventory.get(cellToPut) === 'Empty') {
        this.inventory.set(cellToPut, items.get(itemID));
        place(cellToPut, items.get(itemID).inventoryAppearance);
        document.getElementById(cellToPut).setAttribute('data-items', itemID);
        document.getElementById(cellToPut).setAttribute('title', items.get(itemID).name + ' (+' + items.get(itemID).effect + ')');
      } else {
        for (let [key, value] of this.inventory) {
          if (key.startsWith('I') && value === 'Empty' && typeof itemID === 'string') {
            this.inventory.set(key, items.get(itemID));
            place(key, items.get(itemID).inventoryAppearance);
            document.getElementById(key).setAttribute('data-items', itemID);
            document.getElementById(key).setAttribute('title', items.get(itemID).name + ' (+' + items.get(itemID).effect + ')');
            break;
          }
        }
      }
    }
  }

  removeItemFromInventoryCell(cellID) {
    let item = this.inventory.get(cellID);
    removeUpper(cellID);
    this.inventory.set(cellID, 'Empty');
    document.getElementById(cellID).removeAttribute('data-items');
    document.getElementById(cellID).removeAttribute('title');
  }

  take() {
    let itemsOnCell = document.getElementById(this.staysAt).getAttribute('data-items').split(', ');
    for (let itemID in itemsOnCell) {
      itemID = itemsOnCell[itemID];
      this.obtainItem(itemID);
      removeItem(this.staysAt, itemID);
    }
    document.getElementById(this.staysAt).removeAttribute('data-items');
    document.getElementById(this.staysAt).removeAttribute('title');
  }

  createInventory() {
    var body = document.getElementsByTagName('body')[0];
    var div = document.createElement('div');
    body.appendChild(div);
    div.id = 'inventory-panel';
    div.className = 'inventory-panel';
  
    var body = document.getElementById('inventory-panel');
  
    //Quick Access Cells;
    var tbl = document.createElement('table');
    tbl.id = 'quick-access-cells';
    var tblBody = document.createElement('tbody');
    var row = document.createElement('tr');
    for (var j = 0; j < 8; j++) {
      var cell = document.createElement('td');
      cell.id = 'Q-r' + '0' + 'c' + j;
      this.inventory.set(cell.id, 'Empty');
      addControls(cell);
      row.appendChild(cell);
    }
    tblBody.appendChild(row);
    tbl.appendChild(tblBody);
    body.appendChild(tbl);
    tbl.className = 'quick-access-cells-container';
    tbl.style.width = 32 * 8 + 'px';
  
    //Inventory Equipped Items Section;
    var div = document.createElement('div');
    div.id = 'inventory-equipped-items-section';
    div.className = 'inventory-equipped-items-section';
    body.appendChild(div);

    //Inventory Statistics;
    let stats = document.createElement('div');
    stats.id = 'player-statistics';
    div.appendChild(stats);
    stats.innerText = 'Attack: ' + this.strength + '\n' + 'Defense: ' + this.defense + '\n' + 'Coins: ' + this.coins;
  
    var addDraggingControl = function() {
      slot.addEventListener("drag", function(event) {}, false);
      slot.addEventListener("dragstart", function(event) {
        dragged = event.target;
        event.target.style.opacity = .5;
        var itemID = document.getElementById(dragged.id).getAttribute('data-items');
        that.unequip(itemID);
      }, false);
      slot.addEventListener("dragend", function(event) {
        event.target.style.opacity = "";
      }, false);
      slot.addEventListener("dragover", function(event) {
        event.preventDefault();
      }, false);
      slot.addEventListener("dragenter", function(event) {
        event.target.style.border = '2px solid black';
      }, false);
      slot.addEventListener("dragleave", function(event) {
        event.target.style.border = '1px solid black';
      }, false);
      slot.addEventListener("drop", function(event) {
        event.preventDefault();
        var itemID = document.getElementById(dragged.id).getAttribute('data-items');
        if (dragged.id.slice(0, 1) === 'I') {
          itemID = that.inventory.get(dragged.id).id;
        } else {
          itemID = items.get(itemID).id;
        }
        if (event.target.id === items.get(itemID).equipmentCategory) {
          if (document.getElementById(event.target.id).getAttribute('data-items')) {
            that.obtainItem(document.getElementById(event.target.id).getAttribute('data-items'));
            that.unequip(items.get(document.getElementById(event.target.id).getAttribute('data-items')).id);
            removeItem(event.target.id, itemID);
            removeUpper(event.target.id);
            console.log('That happend');
          }
          placeItem(event.target.id, itemID);
          that.equip(itemID);
          that.removeItemFromInventoryCell(dragged.id);
          if (dragged.id.slice(0, 1) === 'C') {
            removeItemFromContainer(items.get(itemID).id, currentContainer);
          }
        }
        event.target.style.border = '1px solid black';
      }, false);
      slot.addEventListener("contextmenu", function(event) {
        if (!event.ctrlKey) {
          event.preventDefault();
          let itemID = document.getElementById(event.target.id).getAttribute('data-items');
          that.unequip(itemID);
          placeItem(that.staysAt, itemID);
          removeItem(event.target.id, itemID);
        }
      }, false);
    }

    let slot;
  
    slot = document.createElement('td');
    slot.id = 'headgearCell';
    this.inventory.set(slot.id, 'Empty');
    div.appendChild(slot);
    slot.className = 'equipped-items-slot-default';
    slot.style.left = '20%';
    slot.style.top = '10%';
    var that = this;
    addDraggingControl();
    
  
    slot = document.createElement('td');
    slot.id = 'necklaceCell';
    this.inventory.set(slot.id, 'Empty');
    div.appendChild(slot);
    slot.className = 'equipped-items-slot-default';
    slot.style.left = '20%';
    slot.style.top = '25%';
    var that = this;
    addDraggingControl();

    slot = document.createElement('td');
    slot.id = 'clothesCell';
    this.inventory.set(slot.id, 'Empty');
    div.appendChild(slot);
    slot.className = 'equipped-items-slot-default';
    slot.style.left = '5px';
    slot.style.top = '28%';
    var that = this;
    addDraggingControl();
  
    slot = document.createElement('td');
    slot.id = 'leftHandCell';
    this.inventory.set(slot.id, 'Empty');
    div.appendChild(slot);
    slot.className = 'equipped-items-slot-default';
    slot.style.left = '30%';
    slot.style.top = '50%';
    var that = this;
    addDraggingControl();

    slot = document.createElement('td');
    slot.id = 'glovesCell';
    this.inventory.set(slot.id, 'Empty');
    div.appendChild(slot);
    slot.className = 'equipped-items-slot-default';
    slot.style.left = '5px';
    slot.style.top = '45%';
    var that = this;
    addDraggingControl();
  
    slot = document.createElement('td');
    slot.id = 'rightHandCell';
    this.inventory.set(slot.id, 'Empty');
    div.appendChild(slot);
    slot.className = 'equipped-items-slot-default';
    slot.style.left = '15px';
    slot.style.top = '57%';
    var that = this;
    addDraggingControl();
  
    slot = document.createElement('td');
    slot.id = 'footgearCell';
    this.inventory.set(slot.id, 'Empty');
    div.appendChild(slot);
    slot.className = 'equipped-items-slot-default';
    slot.style.left = '5px';
    slot.style.top = '80%';
    var that = this;
    addDraggingControl();
  
    //Inventory Cells;
    var tbl = document.createElement('table');
    tbl.id = 'inventory-cells';
    tbl.className = 'inventory-cells';
    var tblBody = document.createElement('tbody');
  
    for (var i = 0; i < 6; i++) {
      var row = document.createElement('tr');

      for (var j = 0; j < 8; j++) {
        var cell = document.createElement('td');
        cell.id = 'I-r' + i + 'c' + j;
        this.inventory.set(cell.id, 'Empty');
        var that = this;
        cell.addEventListener("drag", function(event) {}, false);
        cell.addEventListener("dragstart", function(event) {
          dragged = event.target;
          event.target.style.opacity = .5;
          let itemID = document.getElementById(event.target.id).getAttribute('data-items');
          let typeOfItem = items.get(itemID).equipmentCategory;
          document.getElementById(typeOfItem).style.border = '2px solid orange';
        }, false);
        cell.addEventListener("dragend", function(event) {
          event.target.style.opacity = "";
        }, false);
        cell.addEventListener("dragover", function(event) {
          event.preventDefault();
        }, false);
        cell.addEventListener("dragenter", function(event) {
          event.target.style.border = '2px solid black';
        }, false);
        cell.addEventListener("dragleave", function(event) {
          event.target.style.border = '1px solid black';
        }, false);
        cell.addEventListener("drop", function(event) {
          event.preventDefault();
          let itemID = document.getElementById(dragged.id).getAttribute('data-items');
          if (dragged.id.slice(0, 1) === 'I') {
            itemID = that.inventory.get(dragged.id).id;
          } else {
            itemID = items.get(itemID).id;
          }
          that.obtainItem(itemID, event.target.id);
          event.target.style.border = '1px solid black';
          let typeOfItem = items.get(itemID).equipmentCategory;
          document.getElementById(typeOfItem).style.border = '1px solid black';
          that.removeItemFromInventoryCell(dragged.id);
          if (dragged.id.slice(0, 1) === 'C') {
            removeItemFromContainer(items.get(itemID).id, currentContainer);
          }
        }, false);
        cell.addEventListener("contextmenu", function(event) {
          if (!event.ctrlKey) {
            event.preventDefault();
            placeItem(that.staysAt, that.inventory.get(event.target.id).id);
            that.removeItemFromInventoryCell(event.target.id);
          }
        }, false);
        cell.style.border = '1px solid black';
        row.appendChild(cell);
        cell.setAttribute('draggable', 'true');
      }
  
        tblBody.appendChild(row);
    }
  
    tbl.appendChild(tblBody);
    body.appendChild(tbl);
  
    tbl.className = 'inventory-cells';
    tbl.style.width = 32 * 8 + 'px';

    inventoryState = 'Open';
  
  }

  refreshStatistics() {
    document.getElementById('player-statistics').innerText = 'Attack: ' + this.strength + '\n' + 'Defense: ' + this.defense + '\n' + 'Coins: ' + this.coins;
  }

  openInventory() {
    if (inventoryState === 'Closed') {
      document.querySelector('#inventory-panel').style.display = 'block';
      inventoryState = 'Open';
    }
    if (!bannedPrompts.includes(promptAboutInventory) && appMode === 'Game') {
      bannedPrompts.push(promptAboutInventory);
    }
  }

  closeInventory() {
    if (inventoryState === 'Open') {
      document.querySelector('#inventory-panel').style.display = 'none';
      inventoryState = 'Closed';
    }
  }

  toggleInventory() {
    if (inventoryState === 'Open') {
      this.closeInventory();
    } else {
      this.openInventory();
      showPrompt('<b>Drag and drop</b> an item to equip it. Click <b>right mouse button</b> to throw it out.', 'info', {from: 'top', align: 'left'});
    }
  }

  openContainer() {
    if (document.getElementById(dorrator.staysAt).getAttribute('data-container')) {
      let containerID = document.getElementById(dorrator.staysAt).getAttribute('data-container');
      if (containerID) {
        openContainer(containerID);
      }
    }
  }

  closeContainer() {
    if (containerState === 'Open') {
      closeContainer();
    }
  }

  toggleContainer() {
    if (containerState === 'Open') {
      this.closeContainer();
    } else {
      this.openContainer();
    }
  }

  toggleInventoryAndContainer() {
    this.toggleInventory();
    this.toggleContainer();
  }

  die() {
    this.moveOut(this.staysAt);
    place(this.staysAt, getRandomDeathSprite());
    game.isPaused = true;
    creatures.delete('Dorrator');
    let body = document.getElementsByTagName('body')[0];
    let div = document.createElement('div');
    div.className = 'darkness';
    body.appendChild(div);
    document.getElementById('deathMusicPart1').play();
    let openDeathWindow = function() {
      let deathContent = '<audio id="deathMusicPart2" src="audio/death-music-part2.mp3" type="audio/mp3" autoplay></audio>'+
        '  <div id="explode" class="ui--modal explode">'+
        '    <div>'+
        '      <h3 class="huge">Death!!!</h3>'+
        '      <img class="death-pic" src="images/death-pic.jpg" alt="" />'+
        '      <a href="#close" id="playAgainDeathBtn" class="ui--button alt">Play Again</a>'+
        '      <a href="https://docs.google.com/forms/d/e/1FAIpQLSdpsBP4Umcv3z3ywW7RzVqUtaKVpI3ntoTQF9-aJ-8ttTJAng/viewform" class="ui--button alt" target="_blank">Subscribe for news</a>'+
        '    </div>'+
        '    <div class="bg">'+
        '      <img src="images/explode.gif" id="boooom">'+
        '    </div>'+
        '  </div>';
      document.querySelector('body').innerHTML = deathContent;
      location.href = '#explode';
      document.getElementById('playAgainDeathBtn').addEventListener('click', function() {
        location.href = location.origin + location.pathname;
      });
    }
    setTimeout(openDeathWindow, 5000);
  }

}

var dorrator = new Player;

const testNet = new Item({
  name: 'Net',
  isNet: true,
  inventoryAppearance: '-1664px -1568px',
  onCreatureAppearance: '-640px -2912px',
  equipmentCategory: 'rightHandCell',
  weight: 1
});
testNet.addToCollection();
dorrator.obtainItem(testNet.id);


//dorrator.obtainItem('itm-003103a');
//dorrator.obtainItem('itm-002376');
// !!!CAUTION!!! Workaround area starts (to initially place items in player's hands);
//dorrator.equip('itm-003103a');
//dorrator.removeItemFromInventoryCell('I-r0c0');
//placeItem('rightHandCell', 'itm-003103a');
//dorrator.equip('itm-002376');
//dorrator.removeItemFromInventoryCell('I-r0c1');
//placeItem('leftHandCell', 'itm-002376');
// Workaround area finishes;
dorrator.obtainHealthPotion(3);
showPrompt(promptAboutWSAD);

class Creature {

  constructor(passedParams) {
    //passedParams is an object

    const defaultParams = {
      name: '' + Math.random(),
      type: 'general',
      body: 'url("images/sprites.png") ' + randomCreatureSprite(),
      beard: '',
      hair: '',
      clothes: '',
      leftHand: '',
      rightHand: '',
      headGear: '',
      gloves: '',
      feet: '',
      health: 50,
      strength: 10,
      reaction: 1000,
      cellID: 'r5c12',
      visionDistance: 7,
      stateTowardsPlayer: 'Enemy',
      canEquipRightHand: false,
      canEquipLeftHand: false
    };

    if (passedParams.type === 'orc_worker') {
      passedParams.body = 'url("images/sprites.png") ' + getRandomElement(['-864px -1952px', '-896px -1952px']);
      if (chance('30%')) {
        passedParams.beard = 'url("images/sprites.png") ' + getRandomElement(['-192px -2592px', '-224px -2592px', '-256px -2592px', '-288px -2592px', '-320px -2592px', '-352px -2592px', '-384px -2592px', '-416px -2592px', '-448px -2592px', '-480px -2592px', '-512px -2592px']);
      }
      passedParams.canEquipRightHand = true;
      passedParams.canEquipLeftHand = true;
    }

    if (passedParams.type === 'orc_warrior') {
      passedParams.body = 'url("images/sprites.png") ' + getRandomElement(['-832px -1952px', '-1088px -1952px', '-1120px -1952px', '-1472px -2496px', '-1504px -2496px']);
      if (chance('50%')) {
        passedParams.beard = 'url("images/sprites.png") ' + getRandomElement(['-192px -2592px', '-224px -2592px', '-256px -2592px', '-288px -2592px', '-320px -2592px', '-352px -2592px', '-384px -2592px', '-416px -2592px', '-448px -2592px', '-480px -2592px', '-512px -2592px']);
      }
      passedParams.canEquipRightHand = true;
      passedParams.canEquipLeftHand = true;
    }

    // Change default parameters with passed if any
    this.props = $.extend(true, {}, defaultParams, passedParams);
    
    this.produceAppearance();
    this.staysAt = this.props.cellID;
    this.staysAtX = this.getRow();
    this.staysAtY = this.getCol();

    this.inventory = new Map;

    //TODO: Move this block somewhere
    let wpn = new Item(getRandomItemProperties());
    wpn.addToCollection();
    this.inventory.set(wpn.id, wpn);
    let wpn2 = new Item(getRandomItemProperties());
    wpn2.addToCollection();
    this.inventory.set(wpn2.id, wpn2);
    let wpn3 = new Item(getRandomItemProperties());
    wpn3.addToCollection();
    this.inventory.set(wpn3.id, wpn3);

    this.equipItemsFromInventory();
    let lookForPlayer = this.lookForPlayer.bind(this);
    this.isIdle = true;
    this.moveTo(this.staysAt);
    this.attention = setInterval(lookForPlayer, this.props.reaction);
    let receiveDamage = this.receiveDamage.bind(this);
    let performIdleMovement = this.performIdleMovement.bind(this);
    this.idleMovement = setInterval(performIdleMovement, (_.random(1, 10) * 1000));
    creatures.set(this.props.name, this);
  }

  produceAppearance() {
    let arr = [this.props.rightHand, this.props.leftHand, this.props.clothes, this.props.gloves, this.props.feet, this.props.headGear, this.props.hair, this.props.beard, this.props.body];
    let arr2 = [];
    arr.forEach(function(element) {
      if (element) {
        if (!element.startsWith('url("images/sprites.png") ')) {
          element = 'url("images/sprites.png") ' + element;
        }
        arr2.push(element);
      }
    });
    this.appearance = arr2.join(', ').replace('url("images/sprites.png") ', ''); // Cut out first 'url("images/sprites.png") ' - it is added by place() function;
    //Expected output: '-1504px -2976px, url("images/sprites.png") -288px -2560px, url("images/sprites.png") -448px -288px'
  }

  equipItemsFromInventory() {

    let itemsInInventory = [];
    this.inventory.forEach(function(item) {
      itemsInInventory.push(item);
    });

    let thisCreature = this;
    //Example of usage: equip('itm_0.9901292055901245', 'canEquipRightHand', 'rightHand')
    let equip = function(itemID, possibilityChecker, where) {
      if (thisCreature.props[possibilityChecker]) {
        //Remove previous item
        thisCreature.props[where] = '';
        //Put new item
        thisCreature.props[where] = items.get(itemID).onCreatureAppearance;
      }
    }


    if (this.props.canEquipRightHand) {
      //Get first suitable item from inventory
      let rightHandItem = itemsInInventory.find(obj => obj.equipmentCategory === 'rightHandCell');
      if (rightHandItem) {
        equip(rightHandItem.id, 'canEquipRightHand', 'rightHand');
      }
    }

    if (this.props.canEquipLeftHand) {
      //Get first suitable item from inventory
      let leftHandItem = itemsInInventory.find(obj => obj.equipmentCategory === 'leftHandCell');
      if (leftHandItem) {
        equip(leftHandItem.id, 'canEquipLeftHand', 'leftHand');
      }
    }


    this.produceAppearance();
  }

  attack(cellID, points, direction) {
    // cellID = cellID;
    // points = getRandomNumber(1, this.strength);
    // direction = direction;
    // let enemy = creatures.get('Dorrator');
    // enemy.receiveDamage(points, direction);
    // createjs.Sound.play(randomHitSound());
    playCombatGame(this.props.name);
  }

  receiveDamage(points = 1, direction = 'All') {
    let x = points;
    let y = points;
    switch (direction) {
      case 'All':
        break;
      case 'Right':
        x = points * -1;
        y = 0;
        break;
      case 'Bottom':
        x = 0;
        y = points * -1;
        break;
      case 'Left':
        x = points;
        y = 0;
        break;
      case 'Top':
        x = 0;
        y = points;
        break;
      default:
        break;
    }
    let fightingGround = document.getElementById(this.staysAt);
    fightingGround.style.boxShadow = 'inset ' + x + 'px ' + y + 'px 20px 2px red';
    if (document.getElementById('enemyPic')) {
      enemyPic.style.boxShadow = 'inset ' + x + 'px ' + y + 'px 20px 2px red';
    }
    let assignPreviousStyle = function() {
      fightingGround.style.boxShadow = '';
      if (document.getElementById('enemyPic')) {
        enemyPic.style.boxShadow = '';
      }
    }
    setTimeout(assignPreviousStyle, 200);
    this.props.health = this.props.health - points;
    print('Enemy took ' + points + ' points of damage');
    if (this.props.health <= 0) {
      print('Enemy is killed');
      this.die();
    }
  }

  lookForPlayer() {
    if (!game.isPaused) {
      let cellOfPlayerX = dorrator.getCol();
      let cellOfPlayerY = dorrator.getRow();
      let cellOfCreatureX = this.getCol();
      let cellOfCreatureY = this.getRow();
      let distance = this.props.visionDistance;
      let checkDistance = function() {
        if (cellOfPlayerX - cellOfCreatureX <= distance) {
          if (cellOfPlayerX - cellOfCreatureX >= distance * -1) {
            if (cellOfPlayerY - cellOfCreatureY <= distance) {
              if (cellOfPlayerY - cellOfCreatureY >= distance * -1) {
                return true;
              }
            }
          }
        }
      }
      if (checkDistance()) {
        this.isIdle = false;
        this.comeCloser();
      } else {
        this.isIdle = true;
      }
    }
  }

  comeCloser() {
    let cellOfPlayerX = dorrator.getCol();
    let cellOfPlayerY = dorrator.getRow();
    let cellOfCreatureX = this.getCol();
    let cellOfCreatureY = this.getRow();
    let distance = this.props.visionDistance;
    if (cellOfPlayerX < cellOfCreatureX) {
      this.moveLeft();
    }
    if (cellOfPlayerX > cellOfCreatureX) {
      this.moveRight();
    }
    if (cellOfPlayerY < cellOfCreatureY) {
      this.moveUp();
    }
    if (cellOfPlayerY > cellOfCreatureY) {
      this.moveDown();
    }
  }

  performIdleMovement() {
    if (this.isIdle && creatures.get(this.props.name)) {
      let randomNumber = _.random(1, 7);
      if (randomNumber === 1) {
        this.moveRight();
      } else if (randomNumber === 2) {
        this.moveDown();
      } else if (randomNumber === 3) {
        this.moveLeft();
      } else if (randomNumber === 4) {
        this.moveUp();
      }
      // 5, 6, 7 - do nothing;
    }
  }

  moveOut(cellId) {
    document.getElementById(cellId).style.background = document.getElementById(cellId).style.background.replace('url("images/sprites.png") ' + this.appearance + ', ', '');
    document.getElementById(cellId).removeAttribute('data-enemies');
    document.getElementById(cellId).removeAttribute('title');
  }

  moveTo(cellId) {
    if (document.getElementById(cellId)) {
      if (document.getElementById(cellId).hasAttribute('data-obstacles') && document.getElementById(cellId).getAttribute('data-obstacles') === 'All') {
        return;
      };
      this.moveOut(this.staysAt);
      document.getElementById(cellId).setAttribute('data-enemies', this.props.name);
      document.getElementById(cellId).setAttribute('title', this.props.name + ' (' + this.stateTowardsPlayer + ')');
      place(cellId, this.appearance);
      this.staysAt = cellId;
      this.staysAtX = this.getRow();
      this.staysAtY = this.getCol();
    }
  }

  getPlace() {
    return this.staysAt;
  }

  getRow() {
    return +this.getPlace().split('c')[0].split('r')[1];
  }

  getCol() {
    return +this.getPlace().split('c')[1];
  }

  moveUp() {
    if (document.getElementById(this.staysAt).getAttribute('data-obstacles') === 'Top') {
      return;
    };
    let newRow = this.getRow() - 1;
    let cellToGo = 'r' + newRow + 'c' + this.getCol();
    if (dorrator.staysAt === cellToGo) {
      this.attack(cellToGo, this.props.strength, 'Bottom');
      return;
    }
    this.moveTo(cellToGo);
  }

  moveDown() {
    let newRow = this.getRow() + 1;
    let cellToGo = 'r' + newRow + 'c' + this.getCol();
    if (document.getElementById(cellToGo).hasAttribute('data-obstacles') && document.getElementById(cellToGo).getAttribute('data-obstacles') === 'Top') {
      return;
    };
    if (dorrator.staysAt === cellToGo) {
      this.attack(cellToGo, this.props.strength, 'Top');
      return;
    }
    this.moveTo(cellToGo);
  }

  moveRight() {
    let newCol = this.getCol() + 1;
    let cellToGo = 'r' + this.getRow() + 'c' + newCol;
    if (dorrator.staysAt === cellToGo) {
      this.attack(cellToGo, this.props.strength, 'Left');
      return;
    }
    this.moveTo(cellToGo);
  }

  moveLeft() {
    let newCol = this.getCol() - 1;
    let cellToGo = 'r' + this.getRow() + 'c' + newCol;
    if (dorrator.staysAt === cellToGo) {
      this.attack(cellToGo, this.props.strength, 'Right');
      return;
    }
    this.moveTo(cellToGo);
  }

  throwFromInventory(itemID, cellID) {
    if (this.inventory.has(itemID)) {
      let itm = this.inventory.get(itemID);
      this.inventory.delete(itemID);
      let cellToPutItem = cellID;
      place(cellToPutItem, itm.inventoryAppearance);
      document.getElementById(cellToPutItem).setAttribute('data-items', itm.id);
      document.getElementById(cellToPutItem).setAttribute('title', itm.name + ' (+' + itm.effect + ')');
    } else {
      console.warn(`${this.props.name} wanted to throw item ${itemID} from the inventory, but there is no such item in the inventory.`);
    }
  }

  die() {
    this.comeCloser = function() {}; //Better to clearInterval;
    let getFreeCells = function(centerCell, radius) {
      let centerCellRow = Number(centerCell.split('c')[0].split('r')[1]);
      let centerCellColl = Number(centerCell.split('c')[1]);

      let isCellFree = function(cellToCheck) {
        if (
          document.getElementById(cellToCheck) &&
          cellToCheck !== centerCell &&
          cellToCheck !== dorrator.staysAt &&
          !document.getElementById(cellToCheck).hasAttribute('data-obstacles') &&
          !document.getElementById(cellToCheck).hasAttribute('data-items') &&
          !document.getElementById(cellToCheck).hasAttribute('data-enemies')
          ) {
          return true;
        } else {
          return false;
        }
      }

      let arr = [];
      //check all cells around by changing initial row
      for (let i = radius * -1; i <= radius; i++) {
        //check all cells around by changing initial column (coll)
        for (let k = radius * -1; k <= radius; k++) {
          let currentCell = 'r' + (centerCellRow + i) + 'c' + (centerCellColl + k);
          if (isCellFree(currentCell)) {
            arr.push(currentCell);
          }
        }
      }

      return arr;
    }

    let freeCellsArr = getFreeCells(this.staysAt, 2);
    let thisCreature = this;
    this.inventory.forEach(function(value, key) {
      let index = getRandomNumber(0, freeCellsArr.length-1);
      thisCreature.throwFromInventory(key, freeCellsArr[index]);
      freeCellsArr.splice(index, 1);
    });

    this.moveOut(this.staysAt);
    creatures.delete(this.props.name);
    place(this.staysAt, getRandomDeathSprite());
  }

  freeze() {
    clearInterval(this.attention);
  }

  unfreeze() {
    this.attention = setInterval(this.lookForPlayer.bind(this), this.props.reaction);
  }

  becomeImmobilized() {
    this.freeze();
    this.isIdle = false;
    let that = this;
    let attemptsToGetOut = setInterval(function() {
      if (chance('30%')) {
        that.unfreeze();
        that.isIdle = true;
        clearInterval(attemptsToGetOut);
        print('Enemy got out.');

        remove(that.staysAt, Traps.spriteOfActivatedNet);

        const net = new Item({
          name: 'Net',
          isNet: true,
          inventoryAppearance: '-1664px -1568px',
          onCreatureAppearance: '-640px -2912px',
          equipmentCategory: 'rightHandCell',
          weight: 1
        });
        net.addToCollection();
        placeItem(that.staysAt, net.id);

      } else {
        print('Enemy tried to get out.');
      }
    }, this.props.reaction);
  }
}


//Nice, but to many layers of abstraction - it is better to use type property of Creature instead
//Moreover, due to appearance being inherited the bug of sprite duplication is observed
//Or this bug can be illuminated by removing and placing the creature back in the Orc constructor?
class Orc extends Creature {
  constructor(params) {
    super(params);

    const defaultParams = {
      name: 'Orc_' + Math.random(),
      appearance: '-832px -1952px'
    };

    // Change default parameters with passed if any
    this.props = $.extend(true, {}, defaultParams, params);
  }

  sayName() {
    console.log('My name is ' + this.props.name);
  }
}