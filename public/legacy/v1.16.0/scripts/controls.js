let handleClick = function(e) {

  if (isContextMenuOpen) {
    wheel.removeWheel();
    contextMenuHolder.remove();
    isContextMenuOpen = false;
  }

  if (!document.getElementById(e.target.id).hasAttribute('data-obstacles')) {
    if (document.getElementById(e.target.id).hasAttribute('data-items')) {
      dorrator.walkTo(e.target.id, 'take');
    } else if (document.getElementById(e.target.id).hasAttribute('data-door')) {
      dorrator.walkTo(e.target.id, 'enter');
    } else if (document.getElementById(e.target.id).hasAttribute('data-container')) {
      dorrator.walkTo(e.target.id, 'openContainer');
    } else {
      dorrator.walkTo(e.target.id);
    }
  }
}

let body = document.getElementsByTagName('body')[0];
let cursorSatelite = document.createElement('div');
cursorSatelite.id = 'cursorSatelite';
cursorSatelite.className = 'cursor-satelite';
body.appendChild(cursorSatelite);
cursorSatelite = document.getElementById('cursorSatelite');

let handleMouseMove = function(e) {

  // ----Initiate scroll when cursor is pushed to the screen edges --->
  let currentCellRow = Number(e.target.id.split('c')[0].split('r')[1]);
  let currentCellCol = Number(e.target.id.split('c')[1]);
  let topCellId = 'r' + (currentCellRow - 1) + 'c' + currentCellCol;
  let rightCellId = 'r' + currentCellRow + 'c' + (currentCellCol + 1);
  let bottomCellId = 'r' + (currentCellRow + 1) + 'c' + currentCellCol;
  let leftCellId = 'r' + currentCellRow + 'c' + (currentCellCol - 1);
  let arrOfSurroundingCells = [topCellId, rightCellId, bottomCellId, leftCellId];

  arrOfSurroundingCells.forEach(function(cellId) {
    if (document.getElementById(cellId)) {
      if (!isInViewport(document.getElementById(cellId))) {
        document.getElementById(cellId).scrollIntoView({behavior: 'smooth'});
      }
    }
  });
  // <--- Initiate scroll when cursor is pushed to the screen edges -----

  cursorSatelite.style.left = e.pageX + 32 + 'px';
  cursorSatelite.style.top = e.pageY + 32 + 'px';
  if (document.getElementById(e.target.id).hasAttribute('data-enemies')) {
    if (document.getElementById('rightHandCell').style.background) {
      cursorSatelite.style.background = document.getElementById('rightHandCell').style.background;
    } else {
      cursorSatelite.style.background = 'url(images/sprites.png) -864px -928px';
    }
  } else if (document.getElementById(e.target.id).hasAttribute('data-items')) {
    if (document.getElementById('glovesCell').style.background) {
      cursorSatelite.style.background = document.getElementById('glovesCell').style.background;
    } else {
      cursorSatelite.style.background = 'url(images/sprites.png) -768px -928px';
    }
  } else if (document.getElementById(e.target.id).hasAttribute('data-door')) {
    cursorSatelite.style.background = 'url(images/sprites.png) -64px -1728px';
  } else if (document.getElementById(e.target.id).hasAttribute('data-obstacles')) {
    cursorSatelite.style.background = 'url(images/sprites.png) -1248px -1600px';
  } else if (document.getElementById(e.target.id).hasAttribute('data-container')) {
    cursorSatelite.style.background = 'url(images/sprites.png) -1440px -896px';
  } else {
    if (document.getElementById('footgearCell').style.background) {
      cursorSatelite.style.background = document.getElementById('footgearCell').style.background;
    } else {
      cursorSatelite.style.background = 'url(images/sprites.png) -1152px -1120px';
    }
  }
}

let handleContextMenu = function(e) {

  let closeContextMenu = function() {
    wheel.removeWheel();
    contextMenuHolder.remove();
    isContextMenuOpen = false;
    cursorStatusText.innerHTML = '';
  }

  if (isContextMenuOpen) {
    closeContextMenu();
  }

  if (!e.ctrlKey && appMode === 'Game') {

    e.preventDefault();

    isContextMenuOpen = true;

    let actions = [];

    let obj = {};
    obj.icon = 'imgsrc:images/sprites/28-16.png';
    obj.desc = 'Look here';
    obj.action = function() {
      let txtThoughts = getRandomElement(['H-m-m-m, interesting...', 'Let\'s look...', 'Well...', 'OK...', 'All right...']);
      print(`${txtThoughts}`);
      if ($('#' + e.target.id).attr('data-surface')) {
        if ($('#' + e.target.id).attr('data-surface').includes('Water')) {
          let txtItIs = getRandomElement(['It is', 'It\'s', 'Seems like', 'Seems like it is', 'Here you can see']);
          print(`${txtItIs} water.`);
        }
        if ($('#' + e.target.id).attr('data-surface').includes('Grass')) {
          let txtItIs = getRandomElement(['It is', 'It\'s', 'Seems like', 'Seems like it is', 'Here you can see']);
          let txtGrass = getRandomElement(['grass', 'a meadow', 'a grassland', 'a bent', 'a leasow', 'a prairie']);
          print(`${txtItIs} ${txtGrass}`);
        }
      }
      closeContextMenu();
    };
    actions.push(obj);


    let obj2 = {};
    obj2.icon = 'imgsrc:images/sprites/31-19.png';
    obj2.desc = 'Throw Fireball';
    obj2.action = function() {
      dorrator.throwFireball(e.target.id);
      closeContextMenu();
    };
    actions.push(obj2);

    if ($('#' + e.target.id).attr('data-surface')) {
      if ($('#' + e.target.id).attr('data-surface').includes('Grass')) {
        let obj = {};
        obj.icon = 'imgsrc:images/sprites/68-4.png';
        obj.desc = 'Collect herbs';
        obj.action = function() {
          playHerbsCollectingGame();
          closeContextMenu();
        };
        actions.push(obj);
      };
      if ($('#' + e.target.id).attr('data-surface').includes('Tree') /*&& isInRange(dorrator.staysAt, e.target.id, 1)*/) {
        let obj = {};
        obj.icon = 'imgsrc:images/sprites/26-41.png';
        obj.desc = 'Cut the tree';
        obj.action = function() {
          Trees.chopTree(e.target.id);
          closeContextMenu();
        };
        actions.push(obj);
      }
    }

    if (currentMapType === 'Dungeon' && isInRange(dorrator.staysAt, e.target.id, 3)) {
      let obj = {};
      obj.icon = 'imgsrc:images/sprites/40-48.png';
      obj.desc = 'Find coins';
      obj.action = function() {
        playMathMiniGame(getRandomNumber(1, 10));
        closeContextMenu();
      };
      actions.push(obj);
    }

    if (document.getElementById(e.target.id).hasAttribute('data-door')) {
      let obj2 = {};
      obj2.icon = 'imgsrc:images/sprites/28-45.png';
      obj2.desc = 'Enter the door';
      obj2.action = function() {
        dorrator.walkTo(e.target.id, 'enter');
        closeContextMenu();
      };
      actions.push(obj2);
    }

    if (document.getElementById(e.target.id).hasAttribute('data-enemies')) {
      let obj2 = {};
      obj2.icon = 'imgsrc:images/sprites/28-28.png';
      obj2.desc = 'Attack enemy';
      obj2.action = function() {
        dorrator.walkTo(e.target.id);
        closeContextMenu();
      };
      actions.push(obj2);
    }

    if (!document.getElementById(e.target.id).hasAttribute('data-obstacles')) {
      let obj = {};
      obj.icon = 'imgsrc:images/sprites/28-46.png';
      obj.desc = 'Go here';
      obj.action = function() {
        dorrator.walkTo(e.target.id);
        closeContextMenu();
      };
      actions.push(obj);

      if (rightHandCell.getAttribute('data-items') || leftHandCell.getAttribute('data-items')) {
        let obj = {};
        obj.icon = 'imgsrc:images/sprites/28-42.png';
        obj.desc = 'Throw weapon at enemy';
        obj.action = function() {
          dorrator.throwThingAt(e.target.id);
          closeContextMenu();
        };
        actions.push(obj);
      }
    }

    if (document.getElementById(e.target.id).hasAttribute('data-items')) {
      let obj = {};
      obj.icon = 'imgsrc:images/sprites/29-24.png';
      obj.desc = 'Take the item';
      obj.action = function() {
        dorrator.walkTo(e.target.id, 'take');
        closeContextMenu();
      };
      actions.push(obj);
    }

    if (document.getElementById(e.target.id).hasAttribute('data-roomdoor')) {
      let obj = {};
      obj.icon = 'imgsrc:images/sprites/28-45.png';
      obj.desc = 'Interact with door';
      obj.action = function() {
        RoomDoors.toggle(e.target.id);
        closeContextMenu();
      };
      actions.push(obj);
    }

    if (document.getElementById(e.target.id).hasAttribute('data-container')) {
      let obj = {};
      obj.icon = 'imgsrc:images/sprites/28-45.png';
      obj.desc = 'Open container';
      obj.action = function() {
        dorrator.walkTo(e.target.id, 'openContainer');
        closeContextMenu();
      };
      actions.push(obj);
    }

    let options = [];
    actions.forEach(function(element, index) {
      options.push(element.icon);
    });

    let wheelSize = actions.length * 20;

    let contextMenuHolder = document.createElement('div');
    contextMenuHolder.id = 'contextMenuHolder';
    contextMenuHolder.className = 'context-menu-holder';
    contextMenuHolder.style.position = 'absolute';
    contextMenuHolder.style.left = e.pageX - (wheelSize / 2) + 'px';
    contextMenuHolder.style.top = e.pageY - (wheelSize / 2) + 'px';
    // contextMenuHolder.style.width = '120px';
    // contextMenuHolder.style.height = '120px';
    contextMenuHolder.style.width = wheelSize + 'px';
    contextMenuHolder.style.height = wheelSize + 'px';
    contextMenuHolder.style.zIndex = '100';
    //contextMenuHolder.style.opacity = '.5';
    body.appendChild(contextMenuHolder);

    wheel = new wheelnav('contextMenuHolder');
    wheel.clickModeRotate = false;
    wheel.slicePathFunction = slicePath().WheelSlice;
    wheel.colors = ['#EDC951'];

    // options.push('imgsrc:images/sprites/0-30.png');
    
    // options.push('imgsrc:images/sprites/28-16.png');
    
    // options.push('imgsrc:images/sprites/40-37.png');

    // var codepenlogo = 'imgsrc:images/sprites/0-30.png';

    wheel.createWheel(options);
    wheel.navItems[0].selected = false;

    actions.forEach(function(element, index) {
      wheel.navItems[index].navSlice.mousedown(function () { element.action(); });
      wheel.navItems[index].navSlice.mouseover(function () { cursorStatusText.innerHTML = element.desc; });
    });

    // wheel.navItems[0].navSlice.mouseup(function () { console.log('0-30'); });
    // wheel.navItems[1].navSlice.mouseup(function () { console.log('2-30'); });
    // wheel.navItems[2].navSlice.mouseup(function () { console.log('6-30'); });

  }

}

// document.addEventListener('click', function(e) {
//   if (e.target.className.includes('js-herb-name')) {

//     $.notify({
//       title: e.target.innerText,
//       message: 'Text'
//     },{
//       type: 'success',
//       placement: {from: 'top', align: 'left'}
//     });

//   }
// })

document.addEventListener('click', function() {
  isInitialInteractionWithPagePerformed = true;
});

document.addEventListener('contextMenu', function() {
  isInitialInteractionWithPagePerformed = true;
});

document.addEventListener('keypress', function() {
  isInitialInteractionWithPagePerformed = true;
});


const hotKeysBindings = {
  moveUp: 'w',
  moveDown: 's',
  moveRight: 'd',
  moveLeft: 'a',
  openInventory: 'i',
  use: 'e',
  heal: 'h',
  findPlayer: 'f'
};

document.onkeypress = function(e) {
  if (!userControlIsBlocked && !game.isPaused) {
    switch(e.key) {
      case hotKeysBindings.moveUp:
        simultaneousWalkingSessionsCount++;
        dorrator.moveUp();
        hidePrompt(promptAboutWSAD);
        setTimeout(function() {simultaneousWalkingSessionsCount = 0;}, 300);
        break;
      case hotKeysBindings.moveDown:
        simultaneousWalkingSessionsCount++;
        dorrator.moveDown();
        hidePrompt(promptAboutWSAD);
        setTimeout(function() {simultaneousWalkingSessionsCount = 0;}, 300);
        break;
      case hotKeysBindings.moveRight:
        simultaneousWalkingSessionsCount++;
        dorrator.moveRight();
        hidePrompt(promptAboutWSAD);
        setTimeout(function() {simultaneousWalkingSessionsCount = 0;}, 300);
        break;
      case hotKeysBindings.moveLeft:
        simultaneousWalkingSessionsCount++;
        dorrator.moveLeft();
        hidePrompt(promptAboutWSAD);
        setTimeout(function() {simultaneousWalkingSessionsCount = 0;}, 300);
        break;
      case hotKeysBindings.openInventory:
        dorrator.toggleInventoryAndContainer();
        hidePrompt(promptAboutInventory);
        break;
      case hotKeysBindings.use:
        dorrator.take();
        break;
      case hotKeysBindings.heal:
        dorrator.drinkHealthPotion(dorrator.maxHealth - dorrator.health);
        break;
      case hotKeysBindings.findPlayer:
        findPlayer();
        break;
    }
  }
}