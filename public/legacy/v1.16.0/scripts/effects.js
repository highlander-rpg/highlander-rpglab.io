
const launchBoltOfLightning = function(startPointX, startPointY, endPointX, endPointY, duration = 3000) {
  let body = document.querySelector('body');
  let canvas = document.createElement('canvas');
  canvas.id = 'boltOfLightningCanvas_' + Math.random();
  canvas.style.position = 'absolute';
  canvas.style.left = Math.min(startPointX, endPointX) + 'px';
  canvas.style.top = Math.min(startPointY, endPointY) + 'px';
  canvas.style.border = '2px solid orange';
  body.appendChild(canvas);
  canvas.width = Math.abs(Number(startPointX) - Number(endPointX));
  canvas.height = Math.abs(Number(startPointY) - Number(endPointY));

  //get the direction of strike
  let directionOfStrike = '';
  if (startPointX > endPointX && startPointY < endPointY) {
    directionOfStrike = 'fromTopRightToBottomLeft';
  }
  if (startPointX > endPointX && startPointY > endPointY) {
    directionOfStrike = 'fromBottomRightToTopLeft';
  }
  if (startPointX < endPointX && startPointY > endPointY) {
    directionOfStrike = 'fromBottomLeftToTopRight';
  }
  if (startPointX < endPointX && startPointY < endPointY) {
    directionOfStrike = 'fromTopLeftToBottomRight';
  }
  if (!directionOfStrike) {
    console.warn('Could not calculate the direction of strike');
  }

  let lightningParams = [];
  switch (directionOfStrike) {
    case 'fromTopRightToBottomLeft':
      lightningParams = [canvas.width, 0, 0, canvas.height];
      break;
    case 'fromBottomRightToTopLeft':
      lightningParams = [canvas.width, canvas.height, 0, 0];
      break;
    case 'fromBottomLeftToTopRight':
      lightningParams = [0, canvas.height, canvas.width, 0];
      break;
    case 'fromTopLeftToBottomRight':
      lightningParams = [0, 0, canvas.width, canvas.height];
      break;
  }

  let cv = canvas.getContext('2d');
  // the creation of electrical shot that starts from point A and flickers to point B;
  var elec = function(aX, aY, bX, bY) {
    let diagonalLineLength = Math.sqrt(canvas.width * canvas.width + canvas.height * canvas.height);
    // marking point A with red square and A-letter;
    cv.beginPath();
    cv.strokeStyle = "red";
    cv.strokeRect(aX-2, aY-2, 4, 4);
    cv.stroke();
    cv.textBaseline = "hanging";
    cv.fillText("A", aX+2, aY+2);
    cv.closePath();
    // highlightning B-point with red square and B-letter;
    cv.beginPath();
    cv.strokeRect(bX-2, bY-2, 4, 4);
    cv.stroke();
    cv.fillText("B", bX+2, bY+2);
    cv.closePath();
    // drawing the axis of the lightning;
    cv.beginPath();
    cv.strokeStyle = "grey";
    cv.moveTo(aX, aY);
    cv.lineTo(bX, bY);
    cv.stroke();
    cv.closePath();
    // drawing the ligtning;
    cv.beginPath();
    cv.strokeStyle = "white";
    // declare step that the light does before changing direction;
    var stepX = aX;
    var stepY = aY;
    let averageX = aX;
    let averageY = aY;
    // making segmented lightning;
    for (let i = 0; i < diagonalLineLength; i++) {
      cv.moveTo(stepX, stepY);
      switch (directionOfStrike) {
        case 'fromTopRightToBottomLeft':
          averageX -= 10;
          averageY += 10;
          //console.log(averageX, averageY);
          stepX -= Math.random()*10;
          stepY += Math.random()*10;
          break;
        case 'fromBottomRightToTopLeft':
          stepX -= Math.random()*10;
          stepY -= Math.random()*10;
          break;
        case 'fromBottomLeftToTopRight':
          stepX += Math.random()*10;
          stepY -= Math.random()*10;
          break;
        case 'fromTopLeftToBottomRight':
          stepX += Math.random()*10;
          stepY += Math.random()*10;
          break;
      }
      cv.lineTo(stepX, stepY);
    };
    cv.stroke();
   };
  // animating (It's alive!!!);
  var step = function() {
    //clear canvas
    canvas.width = canvas.width;
    //elec(20, 100, 70, 30);
    elec(...lightningParams);
  };
  window.setInterval(step, 1000/25);
};


//Test lightning
let startPointForLightning = '';
document.addEventListener('contextmenu', function(e) {
  if (e.shiftKey) {
    e.preventDefault();
    if (!startPointForLightning) {
      startPointForLightning = {};
      startPointForLightning.x = e.screenX;
      startPointForLightning.y = e.screenY;
      return;
    } else {
      launchBoltOfLightning(startPointForLightning.x, startPointForLightning.y, e.screenX, e.screenY);
      startPointForLightning = '';
    }
  }
});



// let body1 = document.querySelector('body');
// let canvas1 = document.createElement('canvas');
// canvas1.id = 'boltOfLightningCanvas';
// canvas1.style.position = 'absolute';
// canvas1.style.border = '2px solid orange';
// canvas1.style.zIndex = '2';
// body1.appendChild(canvas1);

// var size = 500;
// var c = document.getElementById('boltOfLightningCanvas');
// c.width = size;
// c.height = size;
// var ctx = c.getContext("2d");

// var center = {x: size / 2, y: 20};
// var minSegmentHeight = 5;
// var groundHeight = size - 20;
// var color = 'white';
// var roughness = 2;
// var maxDifference = size / 5;

// ctx.globalCompositeOperation = "lighter";

// ctx.strokeStyle = color;
// ctx.shadowColor = color;

// ctx.fillStyle = color;
// ctx.fillRect(0, 0, size, size);
// ctx.fillStyle = 'white';

// function render() {
//   canvas1.width = canvas1.width;
//   //ctx.shadowBlur = 0;
//   //ctx.globalCompositeOperation = "source-over";
//   //ctx.fillRect(0, 0, size, size);
//   //ctx.globalCompositeOperation = "lighter";
//   //ctx.shadowBlur = 15;
//   var lightning = createLightning();
//   ctx.beginPath();
//   for (var i = 0; i < lightning.length; i++) {
//     ctx.lineTo(lightning[i].x, lightning[i].y);
//   }
//   ctx.stroke();
//   requestAnimationFrame(render);
//   //window.setInterval(render, 1000/25);
// }

// function createLightning() {
//   var segmentHeight = groundHeight - center.y;
//   var lightning = [];
//   lightning.push({x: center.x, y: center.y});
//   lightning.push({x: Math.random() * (size - 100) + 50, y: groundHeight + (Math.random() - 0.9) * 50});
//   var currDiff = maxDifference;
//   while (segmentHeight > minSegmentHeight) {
//     var newSegments = [];
//     for (var i = 0; i < lightning.length - 1; i++) {
//       var start = lightning[i];
//       var end = lightning[i + 1];
//       var midX = (start.x + end.x) / 2;
//       var newX = midX + (Math.random() * 2 - 1) * currDiff;
//       newSegments.push(start, {x: newX, y: (start.y + end.y) / 2});
//     }
    
//     newSegments.push(lightning.pop());
//     lightning = newSegments;
    
//     currDiff /= roughness;
//     segmentHeight /= 2;
//   }
//   return lightning;
// }

// render();


const Effects = {};

Effects.fireExplosionSprites = ['-1216px -736px', '-1248px -736px', '-1280px -736px', '-1312px -736px'];

Effects.animateSprites = function(cellID, arrayOfSprites) {
  let i = 0;
  let timerID = setInterval(function() {
    place(cellID, arrayOfSprites[i]);
    setTimeout(function() {
      arrayOfSprites.forEach(sprite => {
        remove(cellID, sprite);
      });
      if (arrayOfSprites[i] === arrayOfSprites[arrayOfSprites.length-1]) {
        clearInterval(timerID);
      } else {
        i++;
      }
    }, 100);
  }, 100);
}