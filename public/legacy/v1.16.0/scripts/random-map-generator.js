let arrayOfGrassSprites = ['-448px -288px', '-512px -288px', '-576px -288px', '-704px -288px', '-768px -288px', '-832px -288px', '-896px -288px', '-960px -288px', '-1024px -288px', '-1088px -288px', '-1152px -288px', '-1216px -288px'];
let randomGrassSprite = function() {
  let max = arrayOfGrassSprites.length - 1;
  let min = 0;
  return arrayOfGrassSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfSeaSprites = ['-1056px -704px', '-1088px -704px', '-1120px -704px', '-1152px -704px', '-1280px -704px', '-1408px -704px', '-1504px -704px', '-1568px -704px', '-1600px -704px', '-1632px -704px', '-1664px -704px', '-1696px -704px'];
let randomSeaSprite = function() {
  let max = arrayOfSeaSprites.length - 1;
  let min = 0;
  return arrayOfSeaSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfTreesSprites = ['-416px -416px', '-448px -416px', '-480px -416px', '-512px -416px', '-544px -416px', '-576px -416px', '-608px -416px', '-640px -416px', '-672px -416px'];
let randomTreeSprite = function() {
  let max = arrayOfTreesSprites.length - 1;
  let min = 0;
  return arrayOfTreesSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfCreaturesSprites = ['-1952px -1856px', '-1984px -1856px', '-2016px -1856px', '-0px -1888px', '-32px -1888px', '-64px -1888px', '-96px -1888px', '-128px -1888px', '-160px -1888px', '-192px -1888px', '-224px -1888px', '-256px -1888px', '-288px -1888px', '-320px -1888px', '-352px -1888px', '-384px -1888px', '-416px -1888px', '-448px -1888px', '-480px -1888px', '-512px -1888px', '-544px -1888px', '-576px -1888px', '-608px -1888px', '-640px -1888px', '-672px -1888px', '-704px -1888px', '-736px -1888px', '-768px -1888px', '-800px -1888px', '-832px -1888px', '-864px -1888px', '-896px -1888px', '-928px -1888px', '-960px -1888px', '-992px -1888px', '-1024px -1888px', '-1056px -1888px', '-1088px -1888px', '-1120px -1888px', '-1152px -1888px', '-1184px -1888px', '-1216px -1888px', '-1248px -1888px', '-1280px -1888px', '-1312px -1888px', '-1344px -1888px', '-1376px -1888px', '-1408px -1888px', '-1440px -1888px', '-1472px -1888px', '-1504px -1888px', '-1536px -1888px', '-1568px -1888px', '-1600px -1888px', '-1632px -1888px', '-1664px -1888px', '-1696px -1888px', '-1728px -1888px', '-1760px -1888px', '-1792px -1888px', '-1824px -1888px', '-1856px -1888px', '-1888px -1888px', '-1920px -1888px', '-1952px -1888px', '-1984px -1888px', '-2016px -1888px', '-0px -1920px', '-32px -1920px', '-64px -1920px', '-96px -1920px', '-128px -1920px', '-160px -1920px', '-192px -1920px', '-224px -1920px', '-256px -1920px', '-288px -1920px', '-320px -1920px', '-352px -1920px', '-384px -1920px', '-416px -1920px', '-448px -1920px', '-480px -1920px', '-512px -1920px', '-544px -1920px', '-576px -1920px', '-608px -1920px', '-640px -1920px', '-672px -1920px', '-704px -1920px', '-736px -1920px', '-768px -1920px', '-800px -1920px', '-832px -1920px', '-864px -1920px', '-896px -1920px', '-928px -1920px', '-960px -1920px', '-992px -1920px', '-1024px -1920px', '-1056px -1920px', '-1088px -1920px', '-1120px -1920px', '-1152px -1920px', '-1184px -1920px', '-1216px -1920px', '-1248px -1920px', '-1280px -1920px', '-1312px -1920px', '-1344px -1920px', '-1376px -1920px', '-1408px -1920px', '-1440px -1920px', '-1472px -1920px', '-1504px -1920px', '-1536px -1920px', '-1568px -1920px', '-1600px -1920px', '-1632px -1920px', '-1664px -1920px', '-1696px -1920px', '-1728px -1920px', '-1760px -1920px', '-1792px -1920px', '-1824px -1920px', '-1856px -1920px', '-1888px -1920px', '-1920px -1920px', '-1952px -1920px', '-1984px -1920px', '-2016px -1920px', '-0px -1952px', '-32px -1952px', '-64px -1952px', '-96px -1952px', '-128px -1952px', '-160px -1952px', '-192px -1952px', '-224px -1952px', '-256px -1952px', '-288px -1952px', '-320px -1952px', '-352px -1952px', '-384px -1952px', '-416px -1952px', '-448px -1952px', '-480px -1952px', '-512px -1952px', '-544px -1952px', '-576px -1952px', '-608px -1952px', '-640px -1952px', '-672px -1952px', '-704px -1952px', '-736px -1952px', '-768px -1952px', '-800px -1952px', '-832px -1952px', '-864px -1952px', '-896px -1952px', '-928px -1952px', '-960px -1952px', '-992px -1952px', '-1024px -1952px', '-1056px -1952px', '-1088px -1952px', '-1120px -1952px', '-1152px -1952px', '-1184px -1952px', '-1216px -1952px', '-1248px -1952px', '-1280px -1952px', '-1312px -1952px', '-1344px -1952px', '-1376px -1952px', '-1408px -1952px', '-1440px -1952px', '-1472px -1952px', '-1504px -1952px', '-1536px -1952px', '-1568px -1952px', '-1600px -1952px', '-1632px -1952px', '-1664px -1952px', '-1696px -1952px', '-1728px -1952px', '-1760px -1952px', '-1792px -1952px', '-1824px -1952px', '-1856px -1952px', '-1888px -1952px', '-1920px -1952px', '-1952px -1952px', '-1984px -1952px', '-2016px -1952px', '-0px -1984px', '-32px -1984px', '-64px -1984px', '-96px -1984px', '-128px -1984px', '-160px -1984px', '-192px -1984px', '-224px -1984px', '-256px -1984px', '-288px -1984px', '-320px -1984px', '-352px -1984px', '-384px -1984px', '-416px -1984px', '-448px -1984px', '-480px -1984px', '-512px -1984px', '-544px -1984px', '-576px -1984px', '-608px -1984px', '-640px -1984px', '-672px -1984px', '-704px -1984px', '-736px -1984px', '-768px -1984px', '-800px -1984px', '-832px -1984px', '-864px -1984px', '-896px -1984px', '-928px -1984px', '-960px -1984px', '-992px -1984px', '-1024px -1984px', '-1056px -1984px', '-1088px -1984px', '-1120px -1984px', '-1152px -1984px', '-1184px -1984px', '-1216px -1984px', '-1248px -1984px', '-1280px -1984px', '-1312px -1984px', '-1344px -1984px', '-1376px -1984px', '-1408px -1984px', '-1440px -1984px', '-1472px -1984px', '-1504px -1984px', '-1536px -1984px', '-1568px -1984px', '-1600px -1984px', '-1632px -1984px', '-1664px -1984px', '-1696px -1984px', '-1728px -1984px', '-1760px -1984px', '-1792px -1984px', '-1824px -1984px', '-1856px -1984px', '-1888px -1984px', '-1920px -1984px', '-1952px -1984px', '-1984px -1984px', '-2016px -1984px', '-0px -2016px', '-32px -2016px', '-64px -2016px', '-96px -2016px', '-128px -2016px', '-160px -2016px', '-192px -2016px', '-224px -2016px', '-256px -2016px', '-288px -2016px', '-320px -2016px', '-352px -2016px', '-384px -2016px', '-416px -2016px', '-448px -2016px', '-480px -2016px', '-512px -2016px', '-544px -2016px', '-576px -2016px', '-608px -2016px', '-640px -2016px', '-672px -2016px', '-704px -2016px', '-736px -2016px', '-768px -2016px', '-800px -2016px', '-832px -2016px', '-864px -2016px', '-896px -2016px', '-928px -2016px', '-960px -2016px', '-992px -2016px', '-1024px -2016px', '-1056px -2016px', '-1088px -2016px', '-1120px -2016px', '-1152px -2016px', '-1184px -2016px', '-1216px -2016px', '-1248px -2016px', '-1280px -2016px', '-1312px -2016px', '-1344px -2016px', '-1376px -2016px', '-1408px -2016px', '-1440px -2016px', '-1472px -2016px', '-1504px -2016px', '-1536px -2016px', '-1568px -2016px', '-1600px -2016px', '-1632px -2016px', '-1664px -2016px', '-1696px -2016px', '-1728px -2016px', '-1760px -2016px', '-1792px -2016px', '-1824px -2016px', '-1856px -2016px', '-1888px -2016px', '-1920px -2016px', '-1952px -2016px', '-1984px -2016px', '-2016px -2016px', '-0px -2048px', '-32px -2048px', '-64px -2048px', '-96px -2048px', '-128px -2048px', '-160px -2048px', '-192px -2048px', '-224px -2048px', '-256px -2048px', '-288px -2048px', '-320px -2048px', '-352px -2048px', '-384px -2048px', '-416px -2048px', '-448px -2048px', '-480px -2048px', '-512px -2048px', '-544px -2048px', '-576px -2048px', '-608px -2048px', '-640px -2048px', '-672px -2048px', '-704px -2048px', '-736px -2048px', '-768px -2048px', '-800px -2048px', '-832px -2048px', '-864px -2048px', '-896px -2048px', '-928px -2048px', '-960px -2048px', '-992px -2048px', '-1024px -2048px', '-1056px -2048px', '-1088px -2048px', '-1120px -2048px', '-1152px -2048px', '-1184px -2048px', '-1216px -2048px', '-1248px -2048px', '-1280px -2048px', '-1312px -2048px', '-1344px -2048px', '-1376px -2048px', '-1408px -2048px', '-1440px -2048px', '-1472px -2048px', '-1504px -2048px', '-1536px -2048px', '-1568px -2048px', '-1600px -2048px', '-1632px -2048px', '-1664px -2048px', '-1696px -2048px', '-1728px -2048px', '-1760px -2048px', '-1792px -2048px', '-1824px -2048px', '-1856px -2048px', '-1888px -2048px', '-1920px -2048px', '-1952px -2048px', '-1984px -2048px', '-2016px -2048px', '-0px -2080px', '-32px -2080px', '-64px -2080px', '-96px -2080px', '-128px -2080px', '-160px -2080px', '-192px -2080px', '-224px -2080px', '-256px -2080px', '-288px -2080px', '-320px -2080px', '-352px -2080px', '-384px -2080px', '-416px -2080px', '-448px -2080px', '-480px -2080px', '-512px -2080px', '-544px -2080px', '-576px -2080px', '-608px -2080px', '-640px -2080px', '-672px -2080px', '-704px -2080px', '-736px -2080px', '-768px -2080px', '-800px -2080px', '-832px -2080px', '-864px -2080px', '-896px -2080px', '-928px -2080px', '-960px -2080px', '-992px -2080px', '-1024px -2080px', '-1056px -2080px', '-1088px -2080px', '-1120px -2080px', '-1152px -2080px', '-1184px -2080px', '-1216px -2080px', '-1248px -2080px', '-1280px -2080px', '-1312px -2080px', '-1344px -2080px', '-1376px -2080px', '-1408px -2080px', '-1440px -2080px', '-1472px -2080px', '-1504px -2080px', '-1536px -2080px', '-1568px -2080px', '-1600px -2080px', '-1632px -2080px', '-1664px -2080px', '-1696px -2080px', '-1728px -2080px', '-1760px -2080px', '-1792px -2080px', '-1824px -2080px', '-1856px -2080px', '-1888px -2080px', '-1920px -2080px', '-1952px -2080px', '-1984px -2080px', '-2016px -2080px', '-0px -2112px', '-32px -2112px', '-64px -2112px', '-96px -2112px', '-128px -2112px', '-160px -2112px', '-192px -2112px', '-224px -2112px', '-256px -2112px', '-288px -2112px', '-320px -2112px', '-352px -2112px', '-384px -2112px', '-416px -2112px', '-448px -2112px', '-480px -2112px', '-512px -2112px', '-544px -2112px', '-576px -2112px', '-608px -2112px', '-640px -2112px', '-672px -2112px', '-704px -2112px', '-736px -2112px', '-768px -2112px', '-800px -2112px', '-832px -2112px', '-864px -2112px', '-896px -2112px', '-928px -2112px', '-960px -2112px', '-992px -2112px', '-1024px -2112px', '-1056px -2112px', '-1088px -2112px', '-1120px -2112px', '-1152px -2112px', '-1184px -2112px', '-1216px -2112px', '-1248px -2112px', '-1280px -2112px', '-1312px -2112px', '-1344px -2112px', '-1376px -2112px', '-1408px -2112px', '-1440px -2112px', '-1472px -2112px', '-1504px -2112px', '-1536px -2112px', '-1568px -2112px', '-1600px -2112px', '-1632px -2112px', '-1664px -2112px', '-1696px -2112px', '-1728px -2112px', '-1760px -2112px', '-1792px -2112px', '-1824px -2112px', '-1856px -2112px', '-1888px -2112px', '-1920px -2112px', '-1952px -2112px', '-1984px -2112px', '-2016px -2112px', '-0px -2144px', '-32px -2144px', '-64px -2144px', '-96px -2144px', '-128px -2144px', '-160px -2144px', '-192px -2144px', '-224px -2144px', '-256px -2144px', '-288px -2144px', '-320px -2144px', '-352px -2144px', '-384px -2144px', '-416px -2144px', '-448px -2144px', '-480px -2144px', '-512px -2144px', '-544px -2144px', '-576px -2144px', '-608px -2144px', '-640px -2144px', '-672px -2144px', '-704px -2144px', '-736px -2144px', '-768px -2144px', '-800px -2144px', '-832px -2144px', '-864px -2144px', '-896px -2144px', '-928px -2144px', '-960px -2144px', '-992px -2144px', '-1024px -2144px', '-1056px -2144px', '-1088px -2144px', '-1120px -2144px', '-1152px -2144px', '-1184px -2144px', '-1216px -2144px', '-1248px -2144px', '-1280px -2144px', '-1312px -2144px', '-1344px -2144px', '-1376px -2144px', '-1408px -2144px', '-1440px -2144px', '-1472px -2144px', '-1504px -2144px', '-1536px -2144px', '-1568px -2144px', '-1600px -2144px', '-1632px -2144px', '-1664px -2144px', '-1696px -2144px', '-1728px -2144px', '-1760px -2144px', '-1792px -2144px', '-1824px -2144px', '-1856px -2144px', '-1888px -2144px', '-1920px -2144px', '-1952px -2144px', '-1984px -2144px', '-2016px -2144px', '-0px -2176px', '-32px -2176px', '-64px -2176px', '-96px -2176px', '-128px -2176px', '-160px -2176px', '-192px -2176px', '-224px -2176px', '-256px -2176px', '-288px -2176px', '-320px -2176px', '-352px -2176px', '-384px -2176px', '-416px -2176px', '-448px -2176px', '-480px -2176px', '-512px -2176px', '-544px -2176px', '-576px -2176px', '-608px -2176px', '-640px -2176px', '-672px -2176px', '-704px -2176px', '-736px -2176px', '-768px -2176px', '-800px -2176px', '-832px -2176px', '-864px -2176px', '-896px -2176px', '-928px -2176px', '-960px -2176px', '-992px -2176px', '-1024px -2176px', '-1056px -2176px', '-1088px -2176px', '-1120px -2176px', '-1152px -2176px', '-1184px -2176px', '-1216px -2176px', '-1248px -2176px', '-1280px -2176px', '-1312px -2176px', '-1344px -2176px', '-1376px -2176px', '-1408px -2176px', '-1440px -2176px', '-1472px -2176px', '-1504px -2176px', '-1536px -2176px', '-1568px -2176px', '-1600px -2176px', '-1632px -2176px', '-1664px -2176px', '-1696px -2176px', '-1728px -2176px', '-1760px -2176px', '-1792px -2176px', '-1824px -2176px', '-1856px -2176px', '-1888px -2176px', '-1920px -2176px', '-1952px -2176px', '-1984px -2176px', '-2016px -2176px', '-0px -2208px', '-32px -2208px', '-64px -2208px', '-96px -2208px', '-128px -2208px', '-160px -2208px', '-192px -2208px', '-224px -2208px', '-256px -2208px', '-288px -2208px', '-320px -2208px', '-352px -2208px', '-384px -2208px', '-416px -2208px', '-448px -2208px', '-480px -2208px', '-512px -2208px', '-544px -2208px', '-576px -2208px', '-608px -2208px', '-640px -2208px', '-672px -2208px', '-704px -2208px', '-736px -2208px', '-768px -2208px', '-1248px -2336px', '-1280px -2336px', '-1312px -2336px', '-1344px -2336px', '-1376px -2336px', '-1408px -2336px', '-1440px -2336px', '-1472px -2336px', '-1504px -2336px', '-1536px -2336px', '-1568px -2336px', '-1600px -2336px', '-1632px -2336px', '-1664px -2336px', '-1696px -2336px', '-1728px -2336px', '-1760px -2336px', '-1792px -2336px', '-1824px -2336px', '-1856px -2336px', '-1888px -2336px', '-1920px -2336px', '-1952px -2336px', '-1984px -2336px', '-2016px -2336px', '-0px -2368px', '-32px -2368px', '-64px -2368px', '-96px -2368px', '-128px -2368px', '-160px -2368px', '-192px -2368px', '-224px -2368px', '-256px -2368px', '-288px -2368px', '-320px -2368px', '-352px -2368px', '-384px -2368px', '-416px -2368px', '-448px -2368px', '-480px -2368px', '-512px -2368px', '-544px -2368px', '-576px -2368px', '-608px -2368px', '-640px -2368px', '-672px -2368px', '-704px -2368px', '-736px -2368px', '-768px -2368px', '-800px -2368px', '-832px -2368px', '-864px -2368px', '-896px -2368px', '-928px -2368px', '-960px -2368px', '-992px -2368px', '-1024px -2368px', '-1056px -2368px', '-1088px -2368px', '-1120px -2368px', '-1152px -2368px', '-1184px -2368px', '-1216px -2368px', '-1248px -2368px', '-1280px -2368px', '-1312px -2368px', '-1344px -2368px', '-1376px -2368px', '-1408px -2368px', '-1440px -2368px', '-1472px -2368px', '-1504px -2368px', '-1536px -2368px', '-1568px -2368px', '-1600px -2368px', '-1632px -2368px', '-1664px -2368px', '-1696px -2368px', '-1728px -2368px', '-1760px -2368px', '-1792px -2368px', '-1824px -2368px', '-1856px -2368px', '-1888px -2368px', '-1920px -2368px', '-1952px -2368px', '-1984px -2368px', '-2016px -2368px', '-0px -2400px', '-32px -2400px', '-64px -2400px', '-96px -2400px', '-128px -2400px', '-160px -2400px', '-192px -2400px', '-224px -2400px', '-256px -2400px', '-288px -2400px', '-320px -2400px', '-352px -2400px', '-384px -2400px', '-416px -2400px', '-448px -2400px', '-480px -2400px', '-512px -2400px', '-544px -2400px', '-576px -2400px', '-608px -2400px', '-640px -2400px', '-672px -2400px', '-704px -2400px', '-736px -2400px', '-768px -2400px', '-800px -2400px', '-832px -2400px', '-864px -2400px', '-896px -2400px', '-928px -2400px', '-960px -2400px', '-992px -2400px', '-1024px -2400px', '-1056px -2400px', '-1088px -2400px', '-1120px -2400px', '-1152px -2400px', '-1184px -2400px', '-1216px -2400px', '-1248px -2400px', '-1280px -2400px', '-1312px -2400px', '-1344px -2400px', '-1376px -2400px', '-1408px -2400px', '-1440px -2400px', '-1472px -2400px', '-1504px -2400px', '-1536px -2400px', '-1568px -2400px', '-1600px -2400px', '-1632px -2400px', '-1664px -2400px', '-1696px -2400px', '-1728px -2400px', '-1760px -2400px', '-1792px -2400px', '-1824px -2400px', '-1856px -2400px', '-1888px -2400px', '-1920px -2400px', '-1952px -2400px', '-1984px -2400px', '-2016px -2400px', '-0px -2432px', '-32px -2432px', '-64px -2432px', '-96px -2432px', '-128px -2432px', '-160px -2432px', '-192px -2432px', '-224px -2432px', '-256px -2432px', '-288px -2432px', '-320px -2432px', '-352px -2432px', '-384px -2432px', '-416px -2432px', '-448px -2432px', '-480px -2432px', '-512px -2432px', '-544px -2432px', '-576px -2432px', '-608px -2432px', '-640px -2432px', '-672px -2432px', '-704px -2432px', '-736px -2432px', '-768px -2432px', '-800px -2432px', '-832px -2432px', '-864px -2432px', '-896px -2432px', '-928px -2432px', '-960px -2432px', '-992px -2432px', '-1024px -2432px', '-1056px -2432px', '-1088px -2432px', '-1120px -2432px', '-1152px -2432px', '-1184px -2432px', '-1216px -2432px', '-1248px -2432px', '-1280px -2432px', '-1312px -2432px', '-1344px -2432px', '-1376px -2432px', '-1408px -2432px', '-1440px -2432px', '-1472px -2432px', '-1504px -2432px', '-1536px -2432px', '-1568px -2432px', '-1600px -2432px', '-1632px -2432px', '-1664px -2432px', '-1696px -2432px', '-1728px -2432px', '-1760px -2432px', '-1792px -2432px', '-1824px -2432px', '-1856px -2432px', '-1888px -2432px', '-1920px -2432px', '-1952px -2432px', '-1984px -2432px', '-2016px -2432px', '-0px -2464px', '-32px -2464px', '-64px -2464px', '-96px -2464px', '-128px -2464px', '-160px -2464px', '-192px -2464px', '-224px -2464px', '-256px -2464px', '-288px -2464px', '-320px -2464px', '-352px -2464px', '-384px -2464px', '-416px -2464px', '-448px -2464px', '-480px -2464px', '-512px -2464px', '-544px -2464px', '-576px -2464px', '-608px -2464px', '-640px -2464px', '-672px -2464px', '-704px -2464px', '-736px -2464px', '-768px -2464px', '-800px -2464px', '-832px -2464px', '-864px -2464px', '-896px -2464px', '-928px -2464px', '-960px -2464px', '-992px -2464px', '-1024px -2464px', '-1056px -2464px', '-1088px -2464px', '-1120px -2464px', '-1152px -2464px', '-1184px -2464px', '-1216px -2464px', '-1248px -2464px', '-1280px -2464px', '-1312px -2464px', '-1344px -2464px', '-1376px -2464px', '-1408px -2464px', '-1440px -2464px', '-1472px -2464px', '-1504px -2464px', '-1536px -2464px', '-1568px -2464px', '-1600px -2464px', '-1632px -2464px', '-1664px -2464px', '-1696px -2464px', '-1728px -2464px', '-1760px -2464px', '-1792px -2464px', '-1824px -2464px', '-1856px -2464px', '-1888px -2464px', '-1920px -2464px', '-1952px -2464px', '-1984px -2464px', '-2016px -2464px', '-0px -2496px', '-32px -2496px', '-64px -2496px', '-96px -2496px', '-128px -2496px', '-160px -2496px', '-192px -2496px', '-224px -2496px', '-256px -2496px', '-288px -2496px', '-320px -2496px', '-352px -2496px', '-384px -2496px', '-416px -2496px', '-448px -2496px', '-480px -2496px', '-512px -2496px', '-544px -2496px', '-576px -2496px', '-608px -2496px', '-640px -2496px', '-672px -2496px', '-704px -2496px', '-736px -2496px', '-768px -2496px', '-800px -2496px', '-832px -2496px', '-864px -2496px', '-896px -2496px', '-928px -2496px', '-960px -2496px', '-992px -2496px', '-1024px -2496px', '-1056px -2496px', '-1088px -2496px', '-1120px -2496px', '-1152px -2496px', '-1184px -2496px', '-1216px -2496px', '-1248px -2496px', '-1280px -2496px', '-1312px -2496px', '-1344px -2496px', '-1376px -2496px', '-1408px -2496px', '-1440px -2496px', '-1472px -2496px', '-1504px -2496px', '-1536px -2496px', '-1568px -2496px', '-1600px -2496px', '-1632px -2496px', '-1664px -2496px', '-1696px -2496px', '-1728px -2496px', '-1760px -2496px', '-1792px -2496px', '-1824px -2496px', '-1856px -2496px', '-1888px -2496px', '-1920px -2496px', '-1952px -2496px', '-1984px -2496px', '-2016px -2496px', '-0px -2528px', '-32px -2528px', '-64px -2528px', '-96px -2528px', '-128px -2528px', '-160px -2528px', '-192px -2528px', '-224px -2528px', '-0px -3008px', '-32px -3008px', '-64px -3008px', '-96px -3008px', '-128px -3008px', '-160px -3008px', '-192px -3008px', '-224px -3008px', '-256px -3008px', '-288px -3008px', '-320px -3008px', '-352px -3008px', '-384px -3008px', '-416px -3008px', '-448px -3008px', '-480px -3008px', '-512px -3008px', '-544px -3008px', '-576px -3008px', '-608px -3008px', '-640px -3008px', '-672px -3008px', '-704px -3008px'];
let randomCreatureSprite = function() {
  let max = arrayOfCreaturesSprites.length - 1;
  let min = 0;
  return arrayOfCreaturesSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfCoinsSprites = ['-1312px -1280px', '-1344px -1280px', '-1376px -1280px', '-1408px -1280px', '-1440px -1280px', '-1472px -1280px', '-1504px -1280px', '-1536px -1280px', '-1568px -1280px', '-1600px -1280px', '-1632px -1280px', '-1664px -1280px', '-1696px -1280px', '-1728px -1280px'];
let randomCoinsSprite = function() {
  return getRandomElement(arrayOfCoinsSprites);
}

let arrayOfWallSprites = ['-448px -448px', '-480px -448px', '-512px -448px', '-544px -448px', '-704px -448px', '-736px -448px', '-768px -448px', '-800px -448px', '-832px -448px', '-864px -448px', '-896px -448px', '-928px -448px'];
let randomWallSprite = function() {
  let max = arrayOfWallSprites.length - 1;
  let min = 0;
  return arrayOfWallSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfDoorSprites = ['-1248px -352px', '-1504px -320px', '-1536px -320px'];
let randomDoorSprite = function() {
  let max = arrayOfDoorSprites.length - 1;
  let min = 0;
  return arrayOfDoorSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfBlockedDoorsSprites = ['-576px -448px', '-608px -448px', '-640px -448px', '-672px -448px'];
let randomBlockedDoorSprite = function() {
  let max = arrayOfBlockedDoorsSprites.length - 1;
  let min = 0;
  return arrayOfBlockedDoorsSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfCaveFloorSprites = ['-1824px -192px', '-1856px -192px', '-1888px -192px', '-1920px -192px', '-1952px -192px', '-1984px -192px', '-2016px -192px', '-0px -224px', '-32px -224px', '-64px -224px', '-96px -224px', '-128px -224px', '-160px -224px', '-192px -224px', '-224px -224px', '-256px -224px', '-288px -224px', '-320px -224px'];
let randomCaveFloorSprite = function() {
  let max = arrayOfCaveFloorSprites.length - 1;
  let min = 0;
  return arrayOfCaveFloorSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfCaveWallSprites = ['-0px -160px', '-32px -160px', '-64px -160px', '-96px -160px', '-128px -160px', '-160px -160px', '-192px -160px', '-224px -160px'];
let randomCaveWallSprite = function() {
  let max = arrayOfCaveWallSprites.length - 1;
  let min = 0;
  return arrayOfCaveWallSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfDungeonFloorSprites = ['-672px -224px', '-736px -224px', '-800px -224px', '-864px -224px'];
let randomDungeonFloorSprite = function() {
  let max = arrayOfDungeonFloorSprites.length - 1;
  let min = 0;
  return arrayOfDungeonFloorSprites[Math.round(Math.random() * (max - min) + min)];
}

let arrayOfDungeonWallSprites = ['-832px -224px', '-768px -224px', '-704px -224px', '-640px -224px'];
let randomDungeonWallSprite = function() {
  let max = arrayOfDungeonWallSprites.length - 1;
  let min = 0;
  return arrayOfDungeonWallSprites[Math.round(Math.random() * (max - min) + min)];
}

let waterLandPropotionModifier = Number('0.' + getRandomNumber(30, 50));

var Generator = {
  // game of life variables
  chanceToStartAlive: waterLandPropotionModifier, //smth between 0.3 and 0.5;
  deathLimit: 3,
  birthLimit: 3,
  numberOfSteps: 6,
  worldWidth: worldHeight,
  worldHeight: worldWidth,

  generateMap: function() {
    var map = [[]];
    // randomly scatter solid blocks
    this.initialiseMap(map);

    for(var i = 0; i < this.numberOfSteps; i++) {
      map = this.step(map);
    }

    return map;
  },

  initialiseMap: function(map) {
    for(var x = 0;  x < this.worldWidth; x++) {
      map[x] = [];
      for(var y = 0; y < this.worldHeight; y++) {
        map[x][y] = 0;
      }
    }

    for(var x = 0; x < this.worldWidth; x++) {
      for(var y = 0; y < this.worldHeight; y++) {
        if(Math.random() < this.chanceToStartAlive) {
          map[x][y] = 1;
        }
      }
    }

    return map;
  },

  step: function(map) {
    var newMap = [[]];
    for(var x = 0; x < map.length; x++) {
      newMap[x] = [];
      for(var y = 0; y < map[0].length; y++) {
        var nbs = this.countAliveNeighbours(map, x, y);
        if(map[x][y] > 0) {
          // check if should die
          if(nbs < this.deathLimit) {
            newMap[x][y] = 0;
          } else {
            newMap[x][y] = 1;
          }
        } else {
          // tile currently empty
          if(nbs > this.birthLimit) {
            newMap[x][y] = 1;
          } else {
            newMap[x][y] = 0;
          }
        }
      }
    }

    return newMap;
  },

  countAliveNeighbours: function(map, x, y) {
    var count = 0;
    for(var i = -1; i < 2; i++) {
      for(var j = -1; j < 2; j++) {
        var nb_x = i + x;
        var nb_y = j + y;
        if(i === 0 && j === 0) {
          // pass
        } else if(nb_x < 0 || nb_y < 0 || nb_x >= map.length || nb_y >= map[0].length) {
          // if at the edge, consider it a solid
          count = count + 1;
        } else if(map[nb_x][nb_y] === 1) {
          count = count + 1;
        }
      }
    }

    return count;
  },

  placeTreasure: function(limit) {
    for(var x = 0; x < this.worldWidth; x++) {
      for( var y = 0; y < this.worldHeight; y++) {
        if(world[x][y] === 0) {
          var nbs = this.countAliveNeighbours(world, x, y);
          if(nbs >= limit) {
            world[x][y] = 2;
          }
        }
      }
    }
  }
}

let removeAllCreatures = function() {
  let creaturesToRemove = document.querySelectorAll('[data-enemies]');
  creaturesToRemove.forEach(element => {
  let name = element.getAttribute('data-enemies');
  creatures.delete(name);
  });
}

let generateRuins = function(cellToStartFrom, width) {
  let c = Number(cellToStartFrom.split('c')[1]);
  let r = Number(cellToStartFrom.split('c')[0].split('r')[1]);
  var arr1 = [];
  var arr2 = [];
  var arr3 = [];
  var arr4 = [];
  for (var i = 0; i < width; i++) {
    arr1.push((Math.random() < 0.9) ? 1 : 0);
  }
  for (var i = 0; i < width; i++) {
    if (arr1[i] === 1) {
      arr2.push((Math.random() < 0.5) ? 1 : 0);
    } else {
      arr2.push(0);
    }
  }
  for (var i = 0; i < width; i++) {
    if (arr2[i] === 1) {
      arr3.push((Math.random() < 0.5) ? 1 : 0);
    } else {
      arr3.push(0);
    }
  }
  for (var i = 0; i < width; i++) {
    if (arr3[i] === 1) {
      arr4.push((Math.random() < 0.3) ? 1 : 0);
    } else {
      arr4.push(0);
    }
  }
  arr1.forEach((element, index) => {
    let thisCell = 'r' + r + 'c' + (c + index);
    if (element === 1 && !document.getElementById(thisCell).hasAttribute('data-obstacles')) {
      place(thisCell, randomWallSprite());
      document.getElementById(thisCell).setAttribute('data-color', 'DimGray');
      if (Math.random() < 0.1) {
        place(thisCell, randomBlockedDoorSprite());
      } else if (Math.random() > 0.9) {
        place(thisCell, randomDoorSprite());
        placeDoor(thisCell, 'Top', 'Random--Dungeon');
      }
    }
  });
  arr2.forEach((element, index) => {
    let thisCell = 'r' + (r - 1) + 'c' + (c + index);
    if (element === 1 && !document.getElementById(thisCell).hasAttribute('data-obstacles')) {
      place(thisCell, randomWallSprite());
      document.getElementById(thisCell).setAttribute('data-color', 'DimGray');
      placeObstacle(thisCell, 'All');
    }
  });
  arr3.forEach((element, index) => {
    let thisCell = 'r' + (r - 2) + 'c' + (c + index);
    if (element === 1 && !document.getElementById(thisCell).hasAttribute('data-obstacles')) {
      place(thisCell, randomWallSprite());
      document.getElementById(thisCell).setAttribute('data-color', 'DimGray');
      placeObstacle(thisCell, 'All');
    }
  });
  arr4.forEach((element, index) => {
    let thisCell = 'r' + (r - 3) + 'c' + (c + index);
    if (element === 1 && !document.getElementById(thisCell).hasAttribute('data-obstacles')) {
      place(thisCell, randomWallSprite());
      document.getElementById(thisCell).setAttribute('data-color', 'DimGray');
      placeObstacle(thisCell, 'All');
    }
  });
  //console.log(arr4);
  //console.log(arr3);
  //console.log(arr2);
  //console.log(arr1);
}

let enterMap = function() {
  userControlIsBlocked = true;

  obstaclesArr.length = 0;
  wasSeenVisArr.length = 0;

  $('#fogOfWar').find('td').each(function(index, element) {
    document.getElementById(element.id).style.opacity = 0;
  })

  db.maps.get(currentMapName).then(map => {
    isCurrentMapNewToPlayer = !Boolean(map);
    if (isCurrentMapNewToPlayer) {
      console.log('This map is new to player');
      creatures.delete('Dorrator');
      let copyOfCreatures = new Map(creatures);
      creaturesNet.set('OS-RPG-Creatures-at-' + visitedMaps[visitedMaps.length - 1], copyOfCreatures);
      creatures.forEach(creature => {
        creatures.get(creature.props.name).freeze();
        creatures.get(creature.props.name).moveOut(creatures.get(creature.props.name).staysAt);
      });
      creatures.clear();
      creatures.set('Dorrator', dorrator);
      drawRandomWorld(currentMapType);
      drawMiniMap();
      currentMapName = 'OS-RPG-Map--' + currentMapType + '--' + Math.random();
      if (currentMapType === 'Cave' || currentMapType === 'Dungeon') {
        currentMapName = futureMapName;
      }
      if (currentMapType === 'Surface') {
        if (playerCameFrom === 'top') {
          let y = Number(getCoordinateY(currentMapCoordinates)) - 1;
          currentMapCoordinates = getCoordinateX(currentMapCoordinates) + ';' + y;
          mapsNet.set(currentMapCoordinates, currentMapName);
        }
        if (playerCameFrom === 'right') {
          let x = Number(getCoordinateX(currentMapCoordinates)) - 1;
          currentMapCoordinates = x + ';' + getCoordinateY(currentMapCoordinates);
          mapsNet.set(currentMapCoordinates, currentMapName);
        }
        if (playerCameFrom === 'bottom') {
          let y = Number(getCoordinateY(currentMapCoordinates)) + 1;
          currentMapCoordinates = getCoordinateX(currentMapCoordinates) + ';' + y;
          mapsNet.set(currentMapCoordinates, currentMapName);
        }
        if (playerCameFrom === 'left') {
          let x = Number(getCoordinateX(currentMapCoordinates)) + 1;
          currentMapCoordinates = x + ';' + getCoordinateY(currentMapCoordinates);
          mapsNet.set(currentMapCoordinates, currentMapName);
        }
        applyMapBorders();
      }
    }
    if (!isCurrentMapNewToPlayer) {
      creatures.delete('Dorrator');
      let copyOfCreatures = new Map(creatures);
      creaturesNet.set('OS-RPG-Creatures-at-' + visitedMaps[visitedMaps.length - 1], copyOfCreatures);
      creatures.forEach(creature => {
        creatures.get(creature.props.name).freeze();
        creatures.get(creature.props.name).moveOut(creatures.get(creature.props.name).staysAt);
      });
      $('[data-enemies]').removeAttr('data-enemies');
      creatures.clear();
      if (currentMapType === 'Surface' && visitedMaps[visitedMaps.length - 1].split('--')[1] === 'Surface') {
        if (playerCameFrom === 'top') {
          let y = Number(getCoordinateY(currentMapCoordinates)) - 1;
          currentMapCoordinates = getCoordinateX(currentMapCoordinates) + ';' + y;
        }
        if (playerCameFrom === 'right') {
          let x = Number(getCoordinateX(currentMapCoordinates)) - 1;
          currentMapCoordinates = x + ';' + getCoordinateY(currentMapCoordinates);
        }
        if (playerCameFrom === 'bottom') {
          let y = Number(getCoordinateY(currentMapCoordinates)) + 1;
          currentMapCoordinates = getCoordinateX(currentMapCoordinates) + ';' + y;
        }
        if (playerCameFrom === 'left') {
          let x = Number(getCoordinateX(currentMapCoordinates)) + 1;
          currentMapCoordinates = x + ';' + getCoordinateY(currentMapCoordinates);
        }
      }
      creatures = creaturesNet.get('OS-RPG-Creatures-at-' + currentMapName);
      creatures.forEach(creature => {
        creatures.get(creature.props.name).moveTo(creatures.get(creature.props.name).staysAt);
        creatures.get(creature.props.name).unfreeze();
      });
      creatures.set('Dorrator', dorrator);
      loadMap(currentMapName);
    }
    dorrator.moveTo(cellToPlacePlayer);
    changeSoundAmbience();
    userControlIsBlocked = false;
  });
}

let drawRandomWorld = function(mapType = 'Surface') {
  world.remove();
  createWorld(worldWidth, worldHeight);
  if (mapType === 'Cave') {
    Generator.numberOfSteps = 2;
  } else {
    Generator.numberOfSteps = 6;
  }
  numOfRows = world.getElementsByTagName('tbody')[0].getElementsByTagName('tr').length;
  numOfCols = world.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[0].getElementsByTagName('td').length;
  if (mapType === 'Surface') {
    let worldToDraw = Generator.generateMap();
    for (let r = 0; r < numOfRows; r++) {
      for (let c = 0; c < numOfCols; c++) {
        let thisCell = 'r' + r + 'c' + c;
        if (worldToDraw[r][c] === 1) {
          place(thisCell, randomGrassSprite());
          document.getElementById(thisCell).setAttribute('data-surface', 'Grass');
          document.getElementById(thisCell).setAttribute('data-color', 'Green');

          let addWaterLandTransition = function() {
            let topSideWater = '-1280px -672px';
            let topRightCornerWater = '-800px -672px';
            let rightSideWater = '-1184px -672px';
            let bottomRightCornerWater = '-992px -672px';
            let bottomSideWater = '-1376px -672px';
            let bottomLeftCornerWater = '-1088px -672px';
            let leftSideWater = '-1472px -672px';
            let topLeftCornerWater = '-896px -672px';

            let topSideGrass = '-1472px -288px';
            let topRightCornerGrass = '-1344px -288px';
            let rightSideGrass = '-640px -288px';
            let bottomRightCornerGrass = '-1536px -288px';
            let bottomSideGrass = '-1664px -288px';
            let bottomLeftCornerGrass = '-1600px -288px';
            let leftSideGrass = '-1728px -288px';
            let topLeftCornerGrass = '-1408px -288px';

            if (worldToDraw[r - 1]) {
              //console.log(r, c);
              //console.log(worldToDraw[r - 1][c]);
            }
            //console.log(worldToDraw[r][c]);
            if (worldToDraw[r - 1] && worldToDraw[r - 1][c] === 0) {
              //place(thisCell, bottomSideGrass);
              place(thisCell, topSideWater);
            }
            if (worldToDraw[r - 1] && worldToDraw[r - 1][c + 1] === 0) {
              //place(thisCell, bottomLeftCornerGrass);
              place(thisCell, topRightCornerWater);
            }
            if (worldToDraw[r][c + 1] === 0) {
              //place(thisCell, leftSideGrass);
              place(thisCell, rightSideWater);
            }
            if (worldToDraw[r + 1] && worldToDraw[r + 1][c + 1] === 0) {
              //place(thisCell, topLeftCornerGrass);
              place(thisCell, bottomRightCornerWater);
            }
            if (worldToDraw[r + 1] && worldToDraw[r + 1][c] === 0) {
              //place(thisCell, topSideGrass);
              place(thisCell, bottomSideWater);
            }
            if (worldToDraw[r + 1] && worldToDraw[r + 1][c - 1] === 0) {
              //place(thisCell, topRightCornerGrass);
              place(thisCell, bottomLeftCornerWater);
            }
            if (worldToDraw[r][c - 1] === 0) {
              //place(thisCell, rightSideGrass);
              place(thisCell, leftSideWater);
            }
            if (worldToDraw[r - 1] && worldToDraw[r - 1][c - 1] === 0) {
              //place(thisCell, bottomRightCornerGrass);
              place(thisCell, topLeftCornerWater);
            }
          }

          addWaterLandTransition();
          
          if (Math.random() < 0.05) {
            place(thisCell, randomTreeSprite());
            document.getElementById(thisCell).setAttribute('data-surface', 'Grass;Tree');
            document.getElementById(thisCell).setAttribute('data-color', 'DarkOliveGreen');
          }
          if (Math.random() < 0.002) {
            let randomType = getRandomElement(['orc_worker', 'orc_warrior']);
            new Creature({
              name: Math.random() + '',
              cellID: thisCell,
              type: randomType,
              //rightHand: '-320px -2784px',
              reaction: getRandomNumber(100, 1000),
              health: getRandomNumber(10, 50)
            });
          }
          if (Math.random() < 0.001 && r > 1 && c > 1 && r < numOfRows - 1 && c < numOfCols - 1) {
            //pattern for doors:(cellId, type, destinationMap[, destinationCellId])
            place(thisCell, '-1888px -384px');
            placeDoor(thisCell, 'Bottom', 'Random--Cave');
          }
          if (Math.random() < 0.0005 /*0.0005*/) {
            let wpn = new Item(getRandomItemProperties());
            wpn.addToCollection();
            place(thisCell, wpn.inventoryAppearance);
            document.getElementById(thisCell).setAttribute('data-items', wpn.id);
            document.getElementById(thisCell).setAttribute('title', wpn.name + ' (+' + wpn.effect + ')');
          }
          if (chance('0.1%')) {
            let numOfContainerColls = getRandomNumber(2, 4);
            let numOfContainerRows = getRandomNumber(2, 4);
            let containerSize = String(numOfContainerColls + 'x' + numOfContainerRows);
            let numOfItems = getRandomNumber(0, Number(numOfContainerColls * numOfContainerRows));
            let itemsInContainer = [];
            for (let i = 0; i < numOfItems; i++) {
              let itm = new Item(getRandomItemProperties());
              itm.addToCollection();
              itemsInContainer.push(itm.id);
              // TODO: Push items in container in a sporadic order - [itm.id,,,,itm.id,itm.id];
            }
            let container = new Container(itemsInContainer, containerSize);
            document.getElementById(thisCell).setAttribute('data-container', container.id);
            place(thisCell, container.appearance);
          }
        } else {
          place(thisCell, randomSeaSprite());
          document.getElementById(thisCell).setAttribute('data-surface', 'Water');
          document.getElementById(thisCell).setAttribute('data-color', 'Blue');
          placeObstacle(thisCell, 'All');
        }
      }
    }
    let cell1 = 'r' + _.random(3, worldHeight - 5) + 'c' + _.random(4, worldWidth - 9)
    generateRuins(cell1, _.random(3, 8));
    let cell2 = 'r' + _.random(3, worldHeight - 5) + 'c' + _.random(4, worldWidth - 9)
    generateRuins(cell2, _.random(3, 8));
    let cell3 = 'r' + _.random(3, worldHeight - 5) + 'c' + _.random(4, worldWidth - 9)
    generateRuins(cell3, _.random(3, 8));
    let cell4 = 'r' + _.random(3, worldHeight - 5) + 'c' + _.random(4, worldWidth - 9)
    generateRuins(cell4, _.random(3, 8));
  }
  if (mapType === 'Cave') {
    let worldToDraw = Generator.generateMap();

    //cells around player
    let cellToPlacePlayerC = Number(cellToPlacePlayer.split('c')[1]);
    let cellToPlacePlayerR = Number(cellToPlacePlayer.split('c')[0].split('r')[1]);
    worldToDraw[cellToPlacePlayerR][cellToPlacePlayerC] = 0;
    worldToDraw[cellToPlacePlayerR][cellToPlacePlayerC+1] = 0;
    worldToDraw[cellToPlacePlayerR+1][cellToPlacePlayerC+1] = 0;
    worldToDraw[cellToPlacePlayerR+1][cellToPlacePlayerC] = 0;
    worldToDraw[cellToPlacePlayerR+1][cellToPlacePlayerC-1] = 0;
    worldToDraw[cellToPlacePlayerR][cellToPlacePlayerC-1] = 0;
    worldToDraw[cellToPlacePlayerR-1][cellToPlacePlayerC-1] = 0;
    worldToDraw[cellToPlacePlayerR-1][cellToPlacePlayerC] = 0;
    worldToDraw[cellToPlacePlayerR-1][cellToPlacePlayerC+1] = 0;

    for (let r = 0; r < numOfRows; r++) {
      for (let c = 0; c < numOfCols; c++) {
        let thisCell = 'r' + r + 'c' + c;
        if (worldToDraw[r][c] === 0) {
        place(thisCell, randomCaveFloorSprite());
        document.getElementById(thisCell).setAttribute('data-color', 'Peru');
        //document.getElementById('F-' + thisCell).style.opacity = 0.5;
        /*if (Math.random() < 0.05) {
          place(thisCell, randomTreeSprite());
        }*/
        if (Math.random() < 0.001) {
          let wpn = new Item(getRandomItemProperties());
          wpn.addToCollection();
          place(thisCell, wpn.inventoryAppearance);
          document.getElementById(thisCell).setAttribute('data-items', wpn.id);
          document.getElementById(thisCell).setAttribute('title', wpn.name + ' (+' + wpn.effect + ')');
        }
        if (Math.random() < 0.01) {
          //pattern for Creatures: (name, cellID = 'r5c12', appearance = '-864px -1952px', reaction = 1000, stateTowardsPlayer = 'Enemy')
          new Creature({
            name: Math.random() + '',
            cellID: thisCell,
            reaction: getRandomNumber(100, 1000),
            health: getRandomNumber(10, 50)
          });
        } else if (Math.random() < 0.005) {
          place(thisCell, '-1888px -384px');
          placeDoor(thisCell, 'Bottom', 'Random--Cave');
        }
        /*if (Math.random() < 0.01) {
          //pattern for doors:(cellId, type, destinationMap[, destinationCellId])
          place(thisCell, '-1888px -384px');
          placeDoor(thisCell, 'Bottom', 'Random--Cave');
        }*/
        } else {
          place(thisCell, randomCaveWallSprite());
          document.getElementById(thisCell).setAttribute('data-color', 'SaddleBrown');
          placeObstacle(thisCell, 'All');
          //document.getElementById('F-' + thisCell).style.opacity = 0.7;
        }
      }
    }
    place(cellToPlacePlayer, '-224px -608px');
    placeDoor(cellToPlacePlayer, 'Top', visitedMaps[visitedMaps.length - 1]);
    // if (document.getElementById(cellToPlacePlayer).hasAttribute('data-obstacles')) {
    //   document.getElementById(cellToPlacePlayer).removeAttribute('data-obstacles');
    // }
    //document.getElementById('F-' + cellToPlacePlayer).style.opacity = 0.1;
  }
  if (mapType === 'Dungeon') {
    let generatedDungeon = new Dungeon(worldWidth, worldHeight);
    generatedDungeon.generate();
    let worldToDraw = generatedDungeon.getFlattenedTiles();

    //cells around player
    let cellToPlacePlayerC = Number(cellToPlacePlayer.split('c')[1]);
    let cellToPlacePlayerR = Number(cellToPlacePlayer.split('c')[0].split('r')[1]);
    worldToDraw[cellToPlacePlayerR][cellToPlacePlayerC].type = 2;
    worldToDraw[cellToPlacePlayerR][cellToPlacePlayerC+1].type = 2;
    worldToDraw[cellToPlacePlayerR+1][cellToPlacePlayerC+1].type = 2;
    worldToDraw[cellToPlacePlayerR+1][cellToPlacePlayerC].type = 2;
    worldToDraw[cellToPlacePlayerR+1][cellToPlacePlayerC-1].type = 2;
    worldToDraw[cellToPlacePlayerR][cellToPlacePlayerC-1].type = 2;
    worldToDraw[cellToPlacePlayerR-1][cellToPlacePlayerC-1].type = 2;
    worldToDraw[cellToPlacePlayerR-1][cellToPlacePlayerC].type = 2;
    worldToDraw[cellToPlacePlayerR-1][cellToPlacePlayerC+1].type = 2;

    for (let r = 0; r < numOfRows; r++) {
      for (let c = 0; c < numOfCols; c++) {
        if (worldToDraw[r][c] !== null) {
          let thisCell = 'r' + r + 'c' + c;
          if (worldToDraw[r][c].type === 1 /*Wall*/) {
            place(thisCell, randomDungeonWallSprite());
            document.getElementById(thisCell).setAttribute('data-color', 'Black');
            placeObstacle(thisCell, 'All');
          }
          if (worldToDraw[r][c].type === 2 /*Floor*/) {
            place(thisCell, randomDungeonFloorSprite());
            document.getElementById(thisCell).setAttribute('data-color', 'Gray');
            if (Math.random() < 0.01) {
              //pattern for Creatures: (name, cellID = 'r5c12', appearance = '-864px -1952px', reaction = 1000, stateTowardsPlayer = 'Enemy')
              new Creature({
                name: Math.random() + '',
                cellID: thisCell,
                appearance: randomCreatureSprite(),
                reaction: getRandomNumber(100, 1000),
                health: getRandomNumber(10, 50)
              });
            }
            if (Math.random() < 0.005) {
              let wpn = new Item(getRandomItemProperties());
              wpn.addToCollection();
              place(thisCell, wpn.inventoryAppearance);
              document.getElementById(thisCell).setAttribute('data-items', wpn.id);
              document.getElementById(thisCell).setAttribute('title', wpn.name + ' (+' + wpn.effect + ')');
            }
            if (chance('0.5%')) {
              let numOfContainerColls = getRandomNumber(2, 4);
              let numOfContainerRows = getRandomNumber(2, 4);
              let containerSize = String(numOfContainerColls + 'x' + numOfContainerRows);
              let numOfItems = getRandomNumber(0, Number(numOfContainerColls * numOfContainerRows));
              let itemsInContainer = [];
              for (let i = 0; i < numOfItems; i++) {
                let itm = new Item(getRandomItemProperties());
                itm.addToCollection();
                itemsInContainer.push(itm.id);
                // TODO: Push items in container in a sporadic order - [itm.id,,,,itm.id,itm.id];
              }
              let container = new Container(itemsInContainer, containerSize);
              document.getElementById(thisCell).setAttribute('data-container', container.id);
              place(thisCell, container.appearance);
            }
          }
          if (worldToDraw[r][c].type === 3 /*Door*/) {
            place(thisCell, randomDungeonFloorSprite());
            RoomDoors.place(thisCell);
            document.getElementById(thisCell).setAttribute('data-color', 'Gray');
          }
          if (worldToDraw[r][c].type === 0 /*No such type: null instead*/) {
            place(thisCell, '0px -160px');
            //placeObstacle(thisCell, 'All');
          }
          if (worldToDraw[r][c].type === 4 /*Stairs*/) {
            place(thisCell, randomDungeonFloorSprite());
            document.getElementById(thisCell).setAttribute('data-color', 'DimGray');
            place(thisCell, '-1696px -352px' /*Stairway down: E-r11c53*/);
            placeDoor(thisCell, 'Right', 'Random--Dungeon');
          }
        }
      }
    }
    place(cellToPlacePlayer, randomDungeonFloorSprite());
    document.getElementById(cellToPlacePlayer).setAttribute('data-color', 'Gray');
    place(cellToPlacePlayer, '-1728px -352px');
    // if (document.getElementById(cellToPlacePlayer).hasAttribute('data-obstacles')) {
    //   document.getElementById(cellToPlacePlayer).removeAttribute('data-obstacles');
    // }
    placeDoor(cellToPlacePlayer, 'Top', visitedMaps[visitedMaps.length - 1]);
  }
}


drawRandomWorld('Surface');
applyMapBorders();
dorrator.moveTo('r1c1');
let moveCameraInitialy = function() {
  window.scroll({ top: 0, left: 0, behavior: 'smooth' });
}
setTimeout(moveCameraInitialy, 1000);



//REMOVE!!!
// place('r4c4', randomDoorSprite());
// placeDoor('r4c4', 'Top', 'Random--Dungeon');