console.warn('Please, remember that console errors from sounds.js are disabled in your browser, Artsiom!');

const hit1 = 'hit1';
const hit2 = 'hit2';
const hit3 = 'hit3';
const hit4 = 'hit4';
const hit5 = 'hit5';
const hit6 = 'hit6';
const hit7 = 'hit7';
const hit8 = 'hit8';
createjs.Sound.registerSound('./audio/sounds/combat/Batman Punch-SoundBible.com-456755860.mp3', hit1);
createjs.Sound.registerSound('./audio/sounds/combat/Bir Poop Splat-SoundBible.com-157212383.mp3', hit2);
createjs.Sound.registerSound('./audio/sounds/combat/Golf Club Swing-SoundBible.com-1724786007.mp3', hit3);
createjs.Sound.registerSound('./audio/sounds/combat/punch_or_whack_-Vladimir-403040765.mp3', hit4);
createjs.Sound.registerSound('./audio/sounds/combat/Putter Golf Ball-SoundBible.com-1207317863.mp3', hit5);
createjs.Sound.registerSound('./audio/sounds/combat/Realistic_Punch-Mark_DiAngelo-1609462330.mp3', hit6);
createjs.Sound.registerSound('./audio/sounds/combat/Smack-SoundBible.com-1427823671.mp3', hit7);
createjs.Sound.registerSound('./audio/sounds/combat/Upper Cut-SoundBible.com-1272257235.mp3', hit8);

let arrayOfHitSounds = [hit1, hit2, hit3, hit4, hit5, hit6, hit7, hit8];
let randomHitSound = function() {
  let max = arrayOfHitSounds.length - 1;
  let min = 0;
  return arrayOfHitSounds[Math.round(Math.random() * (max - min) + min)];
}



var snd1 = "snd1";
createjs.Sound.registerSound("./audio/sounds/ambient/meadow/1.ogg", snd1);
var s1 = createjs.Sound.play(snd1, {loop: -1});

var snd2 = "snd2";
createjs.Sound.registerSound("./audio/sounds/ambient/meadow/2.ogg", snd2);
var s2 = createjs.Sound.play(snd2);
let playS2 = function() {
    if (!game.isPaused && Math.random() > 0.5) {
        s2.play();
        //console.log('s2 is launched');
    }
}

var snd3 = "snd3";
createjs.Sound.registerSound("./audio/sounds/ambient/meadow/3.ogg", snd3);
var s3 = createjs.Sound.play(snd3);
let playS3 = function() {
    if (!game.isPaused && Math.random() > 0.5) {
        s3.play();
        //console.log('s3 is launched');
    }
}

var snd4 = "snd4";
createjs.Sound.registerSound("./audio/sounds/ambient/meadow/4.ogg", snd4);
var s4 = createjs.Sound.play(snd4, {loop: -1});

var snd5 = "snd5";
createjs.Sound.registerSound("./audio/sounds/ambient/meadow/5.ogg", snd5);
var s5 = createjs.Sound.play(snd5);
let playS5 = function() {
    if (!game.isPaused && Math.random() > 0.5) {
        s5.play();
        //console.log('s5 is launched');
    }
}

var snd6 = "snd6";
createjs.Sound.registerSound("./audio/sounds/ambient/meadow/6.ogg", snd6);
var s6 = createjs.Sound.play(snd6);
let playS6 = function() {
    if (!game.isPaused && Math.random() > 0.5) {
        s6.play();
        //console.log('s6 is launched');
    }
}

var snd7 = "snd7";
createjs.Sound.registerSound("./audio/sounds/ambient/meadow/7.ogg", snd7);
var s7 = createjs.Sound.play(snd7);
let playS7 = function() {
    if (!game.isPaused && Math.random() > 0.5) {
        s7.play();
        //console.log('s7 is launched');
    }
}

var snd8 = "snd8";
createjs.Sound.registerSound("./audio/sounds/ambient/meadow/8.ogg", snd8);
var s8 = createjs.Sound.play(snd8);
let playS8 = function() {
    if (!game.isPaused && Math.random() > 0.5) {
        s8.play();
        //console.log('s8 is launched');
    }
}


let s2Surface = window.setInterval(playS2, 30000);
let s3Surface = window.setInterval(playS3, 85000);

let s5Surface = window.setInterval(playS5, 60000);
let s6Surface = window.setInterval(playS6, 20000);
let s7Surface = window.setInterval(playS7, 20000);
let s8Surface = window.setInterval(playS8, 30000);

window.onload = function() {
    s1.play();
    s4.play();
    //console.log('s1 and s4 are attached');
};





// ------ CAVE ----->





var caveSnd1 = "caveSnd1";
createjs.Sound.registerSound("./audio/sounds/ambient/cave/0.728349904118426.ogg", caveSnd1);
var caveS1 = createjs.Sound.play(caveSnd1, {loop: -1});


var caveSnd3 = "caveSnd3";
createjs.Sound.registerSound("./audio/sounds/ambient/cave/0.4781136917637847.ogg", caveSnd3);
var caveS3 = createjs.Sound.play(caveSnd3, {loop: -1});

var caveSnd4 = "caveSnd4";
createjs.Sound.registerSound("./audio/sounds/ambient/cave/0.14603756669981305.ogg", caveSnd4);
var caveS4 = createjs.Sound.play(caveSnd4, {loop: -1});

var caveSnd5 = "caveSnd5";
createjs.Sound.registerSound("./audio/sounds/ambient/cave/0.23743469551834995.ogg", caveSnd5);
var caveS5 = createjs.Sound.play(caveSnd5, {loop: -1});

var caveSnd6 = "caveSnd6";
createjs.Sound.registerSound("./audio/sounds/ambient/cave/0.9749196887501639.ogg", caveSnd6);
var caveS6 = createjs.Sound.play(caveSnd6);
let playCaveS6 = function() {
    if (!game.isPaused && Math.random() > 0.5) {
        caveS6.play();
        //console.log('caveS6 is launched');
    }
}

var caveSnd7 = "caveSnd7";
createjs.Sound.registerSound("./audio/sounds/ambient/cave/0.21473116217817978.ogg", caveSnd7);
var caveS7 = createjs.Sound.play(caveSnd7, {loop: -1});


//Example: 'https://s3.envato.com/files/253554007/preview.mp3'
const combatMusicIDs = ['253508946', '253508422', '253507652', '253467860', '253465398', '253401798', '253398857', '253398718', '253349136', '253344527', '253344018', '253737931', '253306534', '253272779', '253272510', '253272362', '253250178', '253244520', '253243945', '253183068', '253182042', '253181362', '253055790', '253055566', '253055709', '253515054', '253006727', '253005240', '252955245', '252952888', '252891784', '252876054', '252886607', '252837996', '252837736', '252837424', '252752560', '252752348', '252751719', '252671942', '252670965', '252668612', '252608557', '252205518', '252603924', '252554575', '252553320', '252459386', '252454981', '252453255', '252390339', '252388625', '252384562', '253217783', '252932188', '252325271', '252266164', '252265366', '252264431', '252231404', '252230998', '252230168', '252184235', '252182479', '252180044', '252132397', '252132019', '252127449', '252558967', '252059544', '252058355', '252014922', '252014626', '252014402', '251975912', '252398532', '251974414', '251917231', '251917017', '251845895', '251842787', '251798341', '251690500', '251690125', '252231047', '251625113', '251622970', '251622793', '251833806', '251574057', '251571471', '251520818', '251521336', '251520050', '251470500', '251464745', '251410618', '251343648', '251343061', '251442876', '251848304', '251847443', '251192604', '251788214', '251072710', '251072585', '250978536', '250975613', '250974919', '250891340', '250882396', '250880499', '250779240', '250776929', '250770519', '250673435', '250672479', '250672298', '250600918', '250597921', '250597539', '251379494', '251364499', '250506540', '250434432', '250429979', '250321622', '250321088', '251186378', '250225779', '250223156', '250220910', '250124131', '250120358', '250994274', '250015973', '250014840', '249942249', '249941628', '249941421', '249792126', '249791335', '249790342', '249666822', '249608320', '249666246', '249594683', '249590283', '249585753', '249510160', '249506855', '249505418', '249414940', '249414352', '249414532', '249235794', '249232331', '249232281', '249041326', '249160281', '249159793', '249048447', '249990656', '249048140', '248970363', '248966320', '248966384', '248933765', '248924750', '248924626'];

