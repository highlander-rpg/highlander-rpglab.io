const RoomDoors = {};

RoomDoors.closedSprite = '-1152px -32px';
RoomDoors.openedSprite = '-1632px -32px';

RoomDoors.place = function(cellID, isOpened = false) {
  const sprite = isOpened ? RoomDoors.openedSprite : RoomDoors.closedSprite;
  place(cellID, sprite);

  const doorState = isOpened ? 'opened' : 'closed';
  $('#' + cellID).attr('data-roomdoor', doorState);

  isOpened ? removeObstacle(cellID) : placeObstacle(cellID, 'All');
}

RoomDoors.open = function(cellID) {
  if ($('#' + cellID).attr('data-roomdoor') === 'closed') {
    $('#' + cellID).attr('data-roomdoor', 'opened');
    remove(cellID, RoomDoors.closedSprite);
    place(cellID, RoomDoors.openedSprite);
    removeObstacle(cellID);
  }
}

RoomDoors.close = function(cellID) {
  if ($('#' + cellID).attr('data-roomdoor') === 'opened') {
    $('#' + cellID).attr('data-roomdoor', 'closed');
    remove(cellID, RoomDoors.openedSprite);
    place(cellID, RoomDoors.closedSprite);
    placeObstacle(cellID, 'All');
  }
}

RoomDoors.toggle = function(cellID) {
  if ($('#' + cellID).attr('data-roomdoor') === 'closed') {
    RoomDoors.open(cellID);
  } else {
    RoomDoors.close(cellID);
  }
}


const Trees = {};

Trees.chopTree = function(cellID) {
  //Remove tree sprite
  arrayOfTreesSprites.forEach(sprite => {
    remove(cellID, sprite);
  });
  //Throw 1-2 wooden logs, 2-5 branches, 5-10 leaves
  const treeParts = new Map();

  let treeType = getRandomElement(['Birch', 'Maple', 'Willow', 'Mountain Ash', 'Oak']);
  treeType += ' ';

  for (let i = 0; i < getRandomNumber(1, 2); i++) {
    const woodenLog = new Item({
      name: treeType + 'Trunk',
      inventoryAppearance: '-1760px -1440px',
      onCreatureAppearance: '-1696px -2752px',
      equipmentCategory: 'thing',
      weight: 7
    });
    woodenLog.addToCollection();
    treeParts.set(woodenLog.id, woodenLog);
  }

  for (let i = 0; i < getRandomNumber(2, 5); i++) {
    const branch = new Item({
      name: treeType + 'Branch',
      inventoryAppearance: '-1248px -1376px',
      onCreatureAppearance: '-96px -2880px',
      equipmentCategory: 'thing',
      weight: 0.5
    });
    branch.addToCollection();
    treeParts.set(branch.id, branch);
  }

  for (let i = 0; i < getRandomNumber(5, 10); i++) {
    const leaf = new Item({
      name: treeType + 'Leaf',
      inventoryAppearance: '-736px -3008px',
      onCreatureAppearance: '-1504px -2912px',
      equipmentCategory: 'thing',
      weight: 0.01
    });
    leaf.addToCollection();
    treeParts.set(leaf.id, leaf);
  }

  treeParts.forEach(function(value, key) {
    const freeCellsArr = getFreeCells(cellID, 1);
    let index = getRandomNumber(0, freeCellsArr.length-1);

    let itm = treeParts.get(key);
    treeParts.delete(key);
    let cellToPutItem = freeCellsArr[index];
    //place(cellToPutItem, itm.inventoryAppearance);
    //document.getElementById(cellToPutItem).setAttribute('data-items', itm.id);
    placeItem(cellToPutItem, itm.id)
    document.getElementById(cellToPutItem).setAttribute('title', itm.name);
  });
}


const Traps = {};

Traps.spriteOfActivatedNet = '-1760px -800px';