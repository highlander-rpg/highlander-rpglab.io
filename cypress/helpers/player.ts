/// <reference types="cypress" />

import { assertGameObject, chooseInContextMenu, convertCellIdToSelector, convertSelectorToCellId } from "."

const getInitialId = () => {
  return '_test-player'
}

const clickSelf = () => {
  cy.window().then((window) => {
    const playerObj = window.allGameObjects.get(window.playerId)
    const cellId = playerObj.staysAt

    cy.get(`#${cellId}`).click()
  })
}

const teleportTo = (cellId: string) => {
  const id = convertCellIdToSelector(cellId)
  cy.get(id).click()
  chooseInContextMenu('teleport-here')
  assertGameObject(player.getInitialId(), 'staysAt', convertSelectorToCellId(cellId))
}

const teleportIfNeeded = (cellId: string) => {
  const id = convertSelectorToCellId(cellId)
  cy.window().then((window) => {
    const playerObj = window.allGameObjects.get(player.getInitialId())
    if (playerObj.staysAt !== id) {
      player.teleportTo(id)
    }
  })
}

export const player = {
  getInitialId,
  clickSelf,
  teleportTo,
  teleportIfNeeded,
}
