import { Letter } from '../../src/helpers'
import { ActionId } from '../../src/types'
import { convertCellIdToSelector } from './general'

const trigger = (key: string) => {
  cy.get('body').trigger('keyup', { code: `Key${key.toUpperCase()}`, force: true })
}

const w = () => {
  trigger('w')
}

const d = () => {
  trigger('d')
}

const s = () => {
  trigger('s')
}

const a = () => {
  trigger('a')
}

export const keyboard = {
  w,
  d,
  s,
  a,
}

export const assignHotkeyToAction = (letter: Letter, actionId: ActionId): void => {
  cy.findAllByTestId(actionId)
    .eq(0)
    .trigger('mousemove')
    .trigger('keyup', { code: `Key${letter}`, force: true })
    .should('have.attr', 'data-hotkey', letter)
}

export const applyHotkeyToCell = (letter: Letter, cellId: string): void => {
  cy.get(convertCellIdToSelector(cellId))
    .trigger('mousemove')
    .trigger('keyup', { code: `Key${letter}`, force: true })
}
