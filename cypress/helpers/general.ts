import { MockObjectType, MockObjectProps } from './../../src/types'

export const runApp = (env: 'dev' | 'prod' = 'dev') => {
  cy.setCookie('testing', '1')
  cy.visit(env === 'dev' ? 'http://localhost:9000/' : 'https://highlander-rpg.gitlab.io/app/')
}

//Deprecated - use assertObject() instead
export const assertGameObject = (objectId: string, property: MockObjectProps, expectedValue: string | number | boolean) => {
  let obj: MockObjectType

  cy.window()
    .then((window) => {
      obj = window.allGameObjects.get(objectId)
    })
    .then(() => {
      expect(obj[property]).to.equal(expectedValue)
    })
}

export const assertObject = (objectId: string, assertions: (obj: MockObjectType) => void): void => {
  let obj: MockObjectType

  cy.window()
    .then((window) => {
      obj = window.allGameObjects.get(objectId)
    })
    .then(() => {
      assertions(obj)
    })
}

export const convertCellIdToSelector = (cellId: string): string => {
  return cellId.startsWith('#') ? cellId : '#' + cellId
}

export const convertSelectorToCellId = (cellId: string): string => {
  return cellId.startsWith('#') ? cellId.substring(1) : cellId
}
