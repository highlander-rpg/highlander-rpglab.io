import { ActionId } from '../../src/types'
import { convertCellIdToSelector } from './general'

export const chooseInContextMenu = (actionId: ActionId, subjectId?: string): void => {
  if (!subjectId) {
    cy.findByTestId(actionId).click() 
  } else {
    cy.findAllByTestId(actionId).filter(`[data-subjectId='${subjectId}']`).click()
  }
}

export const performAction = (actionId: ActionId, subjectId: string): void => {
  cy.window().then((window) => {
    const subject = window.allGameObjects.get(subjectId)
    const cellId = subject.staysAt

    cy.get(`#${cellId}`).click()
    cy.findAllByTestId(actionId).filter(`[data-subjectId='${subjectId}']`).click()
  })
}

export const getContextMenuItemByText = (actionText: string): Cypress.Chainable => {
  return cy.findByTestId('context-menu').contains('.js-context-menu-item', actionText)
}

export const act = (actionText: string, cellId: string, processDuration?: number): void => {
  clickOnCell(cellId)
  getContextMenuItemByText(actionText).click()
  
  if (processDuration) {
    cy.wait(processDuration)
  }
}

export const assertContextMenu = (arrOfAvailableActions: ActionId[], arrOfHiddenActions: ActionId[]) => {
  arrOfAvailableActions.forEach((actionId: ActionId) => {
    cy.findByTestId(actionId).should('exist')
  })
  arrOfHiddenActions.forEach((actionId: ActionId) => {
    cy.findByTestId(actionId).should('not.exist')
  })
}

export const assertContextMenuByText = (arrOfAvailableActionsText: string[], arrOfHiddenActionsText: string[]) => {
  arrOfAvailableActionsText.forEach((text: string) => {
    getContextMenuItemByText(text).should('exist')
  })
  arrOfHiddenActionsText.forEach((text: string) => {
    getContextMenuItemByText(text).should('not.exist')
  })
}

export const checkParticularContextMenuItem = (actionId: ActionId, subjectId: string, status: 'exist' | 'not.exist'): void => {
  cy.findAllByTestId(actionId).filter(`[data-subjectId='${subjectId}']`).should(status)
}

export const checkTextInContextMenu = (itemText: string, status: 'exist' | 'not.exist'): void => {
  cy.findByTestId('context-menu').contains(itemText).should(status)
}

export const clickOnCell = (cellId: string): void => {
  cy.get(convertCellIdToSelector(cellId)).click()
}


export const closeContextMenu = () => {
  cy.findByTestId('context-menu-close-btn').click()
}
