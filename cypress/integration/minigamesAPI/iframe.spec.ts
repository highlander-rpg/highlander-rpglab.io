import {
  runApp,
  clickOnCell,
  chooseInContextMenu,
} from "../../helpers"

describe('Minigames Iframe', () => {
  beforeEach(() => {
    runApp()
  })

  it('may be envoked, closed', () => {
    const doorCellId = '#r2c2'
    const startMinigame = (): void => {
      clickOnCell(doorCellId)
      chooseInContextMenu('modify', '_test-door')
    }
    cy.findByTestId('minigame-iframe-wrapper').should('not.exist')

    startMinigame()

    cy.findByTestId('minigame-iframe-wrapper').should('be.visible')

    cy.findByTestId('minigame-close-btn').click()

    cy.findByTestId('minigame-iframe-wrapper').should('not.exist')
  })
})
