import { applyHotkeyToCell, assertGameObject, assignHotkeyToAction, clickOnCell, closeContextMenu, player, runApp } from '../../helpers'

describe('Hotkeys', () => {
  beforeEach(() => {
    runApp()
    cy.clearLocalStorage()
  })

  const cellId = 'r4c4'

  it('should be rendered in context menu correctly', () => {
    clickOnCell(cellId)
    assignHotkeyToAction('T', 'teleport-here')
  })

  it('should apply particular action on cells', () => {
    clickOnCell(cellId)
    assignHotkeyToAction('T', 'teleport-here')
    closeContextMenu()

    assertGameObject(player.getInitialId(), 'staysAt', 'r4c2')

    applyHotkeyToCell('T', cellId)

    assertGameObject(player.getInitialId(), 'staysAt', cellId)
  })
  
  it('should persist on page reload', () => {
    clickOnCell(cellId)
    assignHotkeyToAction('T', 'teleport-here')

    runApp()

    clickOnCell(cellId)
    cy.findAllByTestId('teleport-here').should('have.attr', 'data-hotkey', 'T')
    closeContextMenu()

    applyHotkeyToCell('T', cellId)
    assertGameObject(player.getInitialId(), 'staysAt', cellId)
  })
  
  it.skip('should show confirmation modal on hotkeys override(OK and Cancel)', () => {
    clickOnCell(cellId)
    assignHotkeyToAction('T', 'teleport-here')

    //TODO: assert modal

    assignHotkeyToAction('R', 'teleport-here')
  })
})
