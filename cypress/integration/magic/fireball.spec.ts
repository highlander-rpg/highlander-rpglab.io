import { checkTextInContextMenu, chooseInContextMenu, clickOnCell, runApp } from '../../helpers'

const targetCell = 'r13c5'

describe('Fireball', () => {
  beforeEach(() => {
    runApp()
  })

  it('can be thrown and causes fire', () => {
    clickOnCell(targetCell)
    checkTextInContextMenu('Examine straw', 'exist')

    for (let i = 0; i < 5; i++) {
      clickOnCell(targetCell)
      chooseInContextMenu('throw-fireball')
    }

    //Wait until fire burns straw
    cy.wait(6000)

    clickOnCell(targetCell)
    checkTextInContextMenu('Examine straw', 'not.exist')
  })
})
