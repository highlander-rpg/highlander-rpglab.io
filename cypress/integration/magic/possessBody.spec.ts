import {
  runApp,
  player,
  performAction,
  assertContextMenu,
  assertGameObject,
  closeContextMenu,
  convertSelectorToCellId,
  checkParticularContextMenuItem,
  chooseInContextMenu,
} from '../../helpers'

describe('Magic. Possess somebody:', () => {
  const doorCellId = '#r2c2'
  const grassCellId = '#r4c4'
  
  const doorId = '_test-door'
  const harryId = '_test-harry'

  const clickOnDoor = () => cy.get(doorCellId).click()
  const clickOnGrass = () => cy.get(grassCellId).click()

  const assertNewPlayerId = (targetId: string): void => {
    cy.window().its('playerId').then((playerId) => {
      assertGameObject(playerId, 'id', targetId)
    })
  }

  before(() => {
    runApp()
  })

  it('a creature can possess another living creature', () => {
    performAction('possess-body', harryId)
    assertNewPlayerId(harryId)

    clickOnGrass()
    chooseInContextMenu('teleport-here')

    assertGameObject(harryId, 'staysAt', convertSelectorToCellId(grassCellId))
  })
  
  it('a creature can possess inanimate objects', () => {
    performAction('possess-body', doorId)
    assertNewPlayerId(doorId)

    clickOnDoor()
    assertContextMenu(['open'], [])
    closeContextMenu()
  })

  it('a creature can possess their body back', () => {
    performAction('possess-body', player.getInitialId())
    assertNewPlayerId(player.getInitialId())
  })

  it('a creature cannot possess theirselves if already in their body', () => {
    player.clickSelf()
    checkParticularContextMenuItem('possess-body', player.getInitialId(), 'not.exist')
  })
})
