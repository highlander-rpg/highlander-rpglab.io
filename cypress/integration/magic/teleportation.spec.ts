import {
  runApp,
  player,
  assertContextMenu,
  assertGameObject,
  convertSelectorToCellId,
} from "../../helpers"
//TODO: Fix webpack loader that could not deliver sprites here
// import { getAvailableActions } from '../../../src/Actions/getAvailableActions'
// import { getParticularAction } from "../../../src/helpers"

describe('Teleportation', () => {
  const cell1 = '#r4c2'
  const cell2 = '#r4c4'

  before(() => {
    runApp()
  })

  it('can be performed against new position', () => {
    player.teleportTo(cell2)
    assertGameObject(player.getInitialId(), 'staysAt', convertSelectorToCellId(cell2))
  })

  it('can be performed against previous position', () => {
    player.teleportTo(cell1)
    assertGameObject(player.getInitialId(), 'staysAt', convertSelectorToCellId(cell1))
  })

  it('cannot be performed against a position where the actor already is', () => {
    player.clickSelf()
    assertContextMenu([], ['teleport-here'])
  })

  //TODO: fix uwebpack loader (see the comment in imports section)
  // it('NPCs can teleport by themselves', () => {
  //   cy.window().then((window) => {
  //     const objectsIdsOnCell = window.currentMap.get(cell2)
  //     const subject = window.allGameObjects.get(objectsIdsOnCell[0])
  //     const arrayOfActions = getAvailableActions('_test-harry', subject, cell2)
  //     const action = getParticularAction(arrayOfActions, 'teleport-here')

  //     action.callback()

  //     assertGameObject('_test-harry', 'staysAt', cell2)
  //   })
  // })
})
