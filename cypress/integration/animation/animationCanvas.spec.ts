import { runApp } from '../../helpers'

describe('Animation Canvas', () => {
  beforeEach(() => {
    runApp()
  })

  it('should render Animation Layer', () => {
    cy.findByTestId('animation-layer').should('exist').and('be.visible')
  })

  it('should have Sprite Source hidden', () => {
    cy.findByTestId('sprite-source').should('exist').and('not.be.visible')
  })
})
