import {
  player,
  assertContextMenu,
  assertGameObject,
  closeContextMenu,
  performAction,
  clickOnCell,
  chooseInContextMenu,
} from "../../../helpers"

const assertClosedSubjectContextMenu = () => assertContextMenu(['open', 'break'], ['close', 'repair'])
const assertOpenedSubjectContextMenu = () => assertContextMenu(['close', 'break'], ['open', 'repair'])

export const doorsAndContainersCommonTests = {
  checkOpening: (subjectId: string, subjectCellId: string): void => {
    clickOnCell(subjectCellId)
    assertClosedSubjectContextMenu()
    closeContextMenu()
    performAction('open', subjectId)
    assertGameObject(subjectId, 'isOpen', true)
    clickOnCell(subjectCellId)
    assertOpenedSubjectContextMenu()
    closeContextMenu()
  },

  checkClosing: (subjectId: string, subjectCellId: string): void => {
    performAction('close', subjectId)
    assertGameObject(subjectId, 'isOpen', false)
    clickOnCell(subjectCellId)
    assertClosedSubjectContextMenu()
    closeContextMenu()
  },

  checkBreaking: (subjectId: string, subjectCellId: string): void => {
    performAction('break', subjectId)
    assertGameObject(subjectId, 'isBrokenDown', true)
    assertGameObject(subjectId, 'isOpen', true)
    clickOnCell(subjectCellId)
    assertContextMenu(['repair'], ['close', 'open', 'break'])
    closeContextMenu()
  },

  checkRepairing: (subjectId: string, subjectCellId: string): void => {
    performAction('repair', subjectId)
    assertGameObject(subjectId, 'isBrokenDown', false)
    assertGameObject(subjectId, 'isOpen', true)
    clickOnCell(subjectCellId)
    assertOpenedSubjectContextMenu()
    closeContextMenu()
  },

  checkManipulationDistance: (subjectId: string, subjectCellId: string, nearSubjectCellId: string, farFromSubjectCellId: string): void => {
    clickOnCell(subjectCellId)
    assertClosedSubjectContextMenu()
    closeContextMenu()

    player.teleportTo(farFromSubjectCellId)
    clickOnCell(subjectCellId)
    assertContextMenu([], ['open', 'break', 'close', 'repair'])
    closeContextMenu()

    performAction('train-telekinesis', player.getInitialId())

    clickOnCell(subjectCellId)
    assertClosedSubjectContextMenu()
  },

  checkLockAndUnlock: (subjectCellId: string, nearSubjectCellId: string): void => {
    player.teleportTo(nearSubjectCellId)
    clickOnCell(subjectCellId)
    assertContextMenu(['break'], ['open', 'close', 'repair', 'lock', 'unlock'])
    closeContextMenu()

    const cellWithKey = 'r2c8'

    player.teleportTo(cellWithKey)
    clickOnCell(cellWithKey)
    chooseInContextMenu('take', '_test-key1')

    player.teleportTo(nearSubjectCellId)
    clickOnCell(subjectCellId)
    assertContextMenu(['unlock', 'break'], ['open', 'close', 'repair', 'lock'])

    chooseInContextMenu('unlock')

    clickOnCell(subjectCellId)
    assertContextMenu(['lock', 'open', 'break'], ['close', 'repair', 'unlock'])
    
    chooseInContextMenu('lock')

    clickOnCell(subjectCellId)
    assertContextMenu(['unlock', 'break'], ['open', 'close', 'repair', 'lock'])
    closeContextMenu()
  },

  checkSelfLockAndUnlock: (subjectId: string, subjectCellId: string): void => {
    player.teleportTo('r4c2')

    clickOnCell(subjectCellId)
    chooseInContextMenu('possess-body', subjectId)

    clickOnCell(subjectCellId)
    chooseInContextMenu('unlock')

    assertGameObject(subjectId, 'isLocked', false)

    clickOnCell(subjectCellId)
    chooseInContextMenu('lock')

    assertGameObject(subjectId, 'isLocked', true)
  },
}
