import { chooseInContextMenu, clickOnCell, runApp } from '../../helpers'

const targetCell = 'r13c5'

describe('Fire', () => {
  beforeEach(() => {
    runApp()
  })

  it('can ignite and destroy surrounding objects', () => {
    const initialStrawCount = 49

    cy.window().then((window) => {
      let count = 0
      window.allGameObjects.forEach((obj) => {
        if (obj.name === 'straw') {
          count++
        }
      })

      expect(count).to.equal(initialStrawCount)
    })

    for (let i = 0; i < 5; i++) {
      clickOnCell(targetCell)
      chooseInContextMenu('throw-fireball')
    }

    //Wait until fire burns straw
    cy.wait(6000)

    cy.window().then((window) => {
      let count = 0
      window.allGameObjects.forEach((obj) => {
        if (obj.name === 'straw') {
          count++
        }
      })

      expect(count).to.be.lessThan(initialStrawCount)
    })
  })
})
