import { assertGameObject, chooseInContextMenu, clickOnCell, player, runApp, keyboard } from '../../helpers'
import { doorsAndContainersCommonTests } from './shared'

describe('Door', () => {
  const doorId = '_test-door'
  const doorCell = 'r2c2'
  const nearDoorCell = 'r3c2'
  const farFromDoorCell = 'r5c2'

  const lockedDoorId = '_test-door2'
  const lockedDoorCellId = 'r2c10'
  const nearLockedDoorCellId = 'r3c10'

  beforeEach(() => {
    runApp()
    player.teleportTo(nearDoorCell)
  })

  it('can be opened and closed', () => {
    doorsAndContainersCommonTests.checkOpening(doorId, doorCell)
    doorsAndContainersCommonTests.checkClosing(doorId, doorCell)
  })

  it('can be brocken down and repaired', () => {
    doorsAndContainersCommonTests.checkBreaking(doorId, doorCell)
    doorsAndContainersCommonTests.checkRepairing(doorId, doorCell)
  })

  it('can be manipulated on cell and near, but at the distance with telekinesis only', () => {
    doorsAndContainersCommonTests.checkManipulationDistance(doorId, doorCell, nearDoorCell, farFromDoorCell)
  })

  it('should act as obstacle when closed only', () => {
    assertGameObject(doorId, 'isFullObstacle', true)

    clickOnCell(doorCell)
    chooseInContextMenu('open')

    assertGameObject(doorId, 'isFullObstacle', false)
  })

  it('allows creatures to get out if closed on that very cell', () => {
    clickOnCell(doorCell)
    chooseInContextMenu('open')

    keyboard.w()
    
    assertGameObject(player.getInitialId(), 'staysAt', doorCell)

    clickOnCell(doorCell)
    chooseInContextMenu('close')

    clickOnCell(nearDoorCell)
    chooseInContextMenu('step-to-cell')

    assertGameObject(player.getInitialId(), 'staysAt', nearDoorCell)
  })

  it('can be opened when a creature moves with keyboard onto it and it is not locked', () => {
    assertGameObject(doorId, 'isOpen', false)
    assertGameObject(player.getInitialId(), 'staysAt', nearDoorCell)
    
    keyboard.w()

    assertGameObject(doorId, 'isOpen', true)
    assertGameObject(player.getInitialId(), 'staysAt', nearDoorCell)
  })

  it('cannot be opened when a creature moves with keyboard onto it and it is locked', () => {
    player.teleportTo(nearLockedDoorCellId)
    assertGameObject(lockedDoorId, 'isOpen', false)
    assertGameObject(player.getInitialId(), 'staysAt', nearLockedDoorCellId)

    keyboard.w()

    assertGameObject(doorId, 'isOpen', false)
    assertGameObject(player.getInitialId(), 'staysAt', nearLockedDoorCellId)
  })

  it('stops being an obstacle when broken down', () => {
    clickOnCell(doorCell)
    chooseInContextMenu('break')

    assertGameObject(doorId, 'isFullObstacle', false)
  })

  it('can be locked and unlocked with the key', () => {
    doorsAndContainersCommonTests.checkLockAndUnlock(lockedDoorCellId, nearLockedDoorCellId)
  })

  it('can be locked and unlocked without the key if a magician has posessed its body', () => {
    doorsAndContainersCommonTests.checkSelfLockAndUnlock(lockedDoorId, lockedDoorCellId)
  })
})
