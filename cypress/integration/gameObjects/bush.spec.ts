import { MockObjectType } from "../../../src/types"
import {
  act,
  assertContextMenuByText,
  assertObject,
  clickOnCell,
  closeContextMenu,
  getContextMenuItemByText,
  player,
  runApp,
} from "../../helpers"

const bushId = '_test-predictable-bush'
const cellId = 'r2c18'

const testFieldOfBushes = (isFruiting: boolean, isDry: boolean, assertions: (bushObj: MockObjectType) => void): void => {
  const arrOfBushObjects: MockObjectType[] = []
  const idPrefix = '_test-bush-at-'

  cy.window()
    .then((window) => {
      const fieldCells = window.hl.getCellsInRectangle(5, 3, 'r3c4')
      fieldCells.forEach((cellId) => {
        const objId = `${idPrefix}${cellId}`
        window.hl.addBush(cellId, { id: objId}, isFruiting, isDry)
        arrOfBushObjects.push(window.allGameObjects.get(objId))
      })
    })
    .then(() => {
      arrOfBushObjects.forEach((obj) => assertions(obj))
    })
}

describe('Redcurrant bush', () => {
  beforeEach(() => {
    runApp()
    player.teleportTo('r3c18')
  })

  it('should be young and with foliage', () => {
    testFieldOfBushes(false, false, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.be.equal(0)
      expect(obj.compounds.redCurrantBranch).to.be.at.least(3)
      expect(obj.compounds.redCurrantDryBerry).to.be.at.least(1)
      expect(obj.compounds.redCurrantDryBranch).to.be.at.most(3)
      expect(obj.compounds.redCurrantLeaf).to.be.at.least(1)
    })
  })

  it('should be dry', () => {
    testFieldOfBushes(false, true, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.be.equal(0)
      expect(obj.compounds.redCurrantBranch).to.be.equal(0)
      expect(obj.compounds.redCurrantDryBerry).to.be.at.least(1)
      expect(obj.compounds.redCurrantDryBranch).to.be.at.least(3)
      expect(obj.compounds.redCurrantLeaf).to.be.equal(0)
      expect(obj.compounds.redCurrantDryLeaf).to.be.at.least(1)
    })
  })

  it('should be fruitfull', () => {
    testFieldOfBushes(true, false, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.at.least(1)
      expect(obj.compounds.redCurrantBranch).to.be.at.least(3)
      expect(obj.compounds.redCurrantDryBerry).to.be.at.least(1)
      expect(obj.compounds.redCurrantDryBranch).to.be.at.most(3)
      expect(obj.compounds.redCurrantLeaf).to.be.at.least(1)
      expect(obj.compounds.redCurrantDryLeaf).to.be.at.least(1)
    })
  })

  it('should allow collecting 1 compound', () => {
    assertObject(bushId, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.be.equal(4)
    })
    clickOnCell(cellId)
    assertContextMenuByText([], ['Take red currant berry'])
    closeContextMenu()

    act('Collect one red currant berry', cellId, 2000)

    assertObject(bushId, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.be.equal(3)
    })
    clickOnCell(cellId)
    assertContextMenuByText(['Take red currant berry'], [])
    closeContextMenu()
  })

  it('should allow collecting some compounds', () => {
    assertObject(bushId, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.be.equal(4)
    })
    clickOnCell(cellId)
    assertContextMenuByText([], ['Take red currant berry'])
    closeContextMenu()

    clickOnCell(cellId)
    getContextMenuItemByText('Collect red currant berry').as('btn')
    cy.get('@btn').find('input').should('have.value', '2').clear().type('3')
    cy.get('@btn').click()
    cy.wait(5000)

    assertObject(bushId, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.be.equal(1)
    })
    clickOnCell(cellId)
    assertContextMenuByText(['Take red currant berry'], [])
    closeContextMenu()
  })

  it('allow collecting all compounds', () => {
    assertObject(bushId, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.be.equal(4)
    })
    clickOnCell(cellId)
    assertContextMenuByText([], ['Take red currant berry'])
    closeContextMenu()

    act('Collect all of red currant berry', cellId, 8000)

    assertObject(bushId, (obj) => {
      expect(obj.compounds.redCurrantBerry).to.be.equal(0)
    })
    clickOnCell(cellId)
    assertContextMenuByText(['Take red currant berry'], [])
    closeContextMenu()
  })
})