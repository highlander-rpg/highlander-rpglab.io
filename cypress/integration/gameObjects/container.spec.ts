import { player, runApp } from "../../helpers"
import { doorsAndContainersCommonTests } from "./shared"

describe('Container', () => {
  const chestId = '_test-chest'
  const chestCellId = '#r2c4'
  const nearChestCellId = '#r3c4'
  const farFromChestCellId = '#r4c4'

  beforeEach(() => {
    runApp()
    player.teleportTo(nearChestCellId)
  })

  it('can be opened and closed', () => {
    doorsAndContainersCommonTests.checkOpening(chestId, chestCellId)
    doorsAndContainersCommonTests.checkClosing(chestId, chestCellId)
  })

  it('can be brocken down and repaired', () => {
    doorsAndContainersCommonTests.checkBreaking(chestId, chestCellId)
    doorsAndContainersCommonTests.checkRepairing(chestId, chestCellId)
  })

  it('can be manipulated on cell and near, but at the distance with telekinesis only', () => {
    doorsAndContainersCommonTests.checkManipulationDistance(chestId, chestCellId, nearChestCellId, farFromChestCellId)
  })

  const lockedChestCellId = 'r2c12'

  it('can be locked and unlocked with the key', () => {
    const nearLockedChestCellId = 'r3c12'
    doorsAndContainersCommonTests.checkLockAndUnlock(lockedChestCellId, nearLockedChestCellId)
  })

  it('can be locked and unlocked without the key if a magician has posessed its body', () => {
    const lockedChestId = '_test-chest2'
    doorsAndContainersCommonTests.checkSelfLockAndUnlock(lockedChestId, lockedChestCellId)
  })
})
