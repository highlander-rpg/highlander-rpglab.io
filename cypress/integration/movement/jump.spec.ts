import {
  runApp,
  player,
  assertContextMenu,
  assertGameObject,
  clickOnCell,
  chooseInContextMenu,
} from '../../helpers'

describe('Jumping', () => {
  beforeEach(() => {
    runApp()
  })

  it('can be performed to any 2nd cell from starting position', () => {
    const allowedCell = 'r4c4'
    clickOnCell(allowedCell)
    chooseInContextMenu('jump-to-cell')
    assertGameObject(player.getInitialId(), 'staysAt', allowedCell)
  })

  it('cannot jump on nearest cells', () => {
    const closestCell = 'r4c3'
    clickOnCell(closestCell)
    assertContextMenu([], ['jump-to-cell'])
  })

  it('cannot jump on distant cells', () => {
    const distantCell = 'r4c5'
    clickOnCell(distantCell)
    assertContextMenu([], ['jump-to-cell'])
  })

  it('cannot jump on same cell where the actor already is', () => {
    const playerCell = 'r4c2'
    clickOnCell(playerCell)
    assertContextMenu([], ['jump-to-cell'])
  })

  it('cannot jump on cell with an obstacle', () => {
    const cellWithObstacle = 'r2c2'
    clickOnCell(cellWithObstacle)
    assertContextMenu([], ['jump-to-cell'])
  })
})
