import {
  runApp,
  player,
  assertContextMenu,
  assertGameObject,
  clickOnCell,
  closeContextMenu,
  keyboard,
} from '../../helpers'
import { stepAndAssert } from './helpers'

describe('Stepping', () => {
  const cell1 = 'r4c2'
  const cell2 = 'r4c3'
  const cell3 = 'r4c4'

  beforeEach(() => {
    runApp()
  })

  it('can move forwards and backwards', () => {
    stepAndAssert(cell2)
    stepAndAssert(cell3)
    stepAndAssert(cell2)
  })

  it('cannot step on distant cells', () => {
    clickOnCell(cell3)
    assertContextMenu([], ['step-to-cell'])
  })

  it('cannot step on same cell where the actor already is', () => {
    clickOnCell(cell1)
    assertContextMenu([], ['step-to-cell'])
    closeContextMenu()
  })

  it('can move in 4 directions using keyboard', () => {
    keyboard.w()
    assertGameObject(player.getInitialId(), 'staysAt', 'r3c2')
    keyboard.d()
    assertGameObject(player.getInitialId(), 'staysAt', 'r3c3')
    keyboard.s()
    assertGameObject(player.getInitialId(), 'staysAt', 'r4c3')
    keyboard.a()
    assertGameObject(player.getInitialId(), 'staysAt', 'r4c2')
  })

  it('cannot move out of map using keyboard', () => {
    const cornerCell = 'r0c0'
    const existingCell = 'r1c0'

    player.teleportTo(cornerCell)
    keyboard.w()
    assertGameObject(player.getInitialId(), 'staysAt', cornerCell)
    keyboard.a()
    assertGameObject(player.getInitialId(), 'staysAt', cornerCell)

    keyboard.s()
    assertGameObject(player.getInitialId(), 'staysAt', existingCell)

    //Check bottomRight map corner
    cy.get('#current-map-table .cell').last().invoke('attr', 'id').then((cellId) => {
      player.teleportTo(cellId)
      keyboard.s()
      assertGameObject(player.getInitialId(), 'staysAt', cellId)
      keyboard.d()
      assertGameObject(player.getInitialId(), 'staysAt', cellId)

      keyboard.w()

      const getRow = (cellId: string): number => {
        return Number(cellId.split('c')[0].split('r')[1])
      }
      const getCol = (cellId: string): number => {
        return Number(cellId.split('c')[1])
      }
      const r = getRow(cellId)
      const c = getCol(cellId)
      assertGameObject(player.getInitialId(), 'staysAt', `r${r - 1}c${c}`)
    })
  })

  it('moving using keyboard closes context menu', () => {
    player.clickSelf()
    cy.findByTestId('context-menu').children().should('have.length.greaterThan', 0)
    keyboard.w()
    cy.findByTestId('context-menu').children().should('have.length', 0)
  })
})
