import {
  runApp,
  player,
  assertContextMenu,
  assertGameObject,
  clickOnCell,
  chooseInContextMenu,
  closeContextMenu,
  keyboard,
} from '../../helpers'
import { stepAndAssert } from './helpers'

//Some tests are skipped due to full obstacles are only implemented.
//1,2,3 sided obstacles are not implemented yet because they may affect pathfinding badly
describe('Obstacles', () => {
  beforeEach(() => {
    runApp()
  })

  const moveAndAssertDisabledStepping = (startCell: string, targetCellId: string, moves?: string, areMovesInverted?: boolean): void => {
    stepAndAssert(startCell)

    clickOnCell(targetCellId)
    assertContextMenu([], ['step-to-cell'])

    if (moves) {
      const keys = moves.split('-')
      type KeyboardKeyType = 'w' | 'a' | 's' | 'd'
      const keyboardKey = areMovesInverted ? keys[1] : keys[0]

      keyboard[keyboardKey as KeyboardKeyType]()
    }

    assertGameObject(player.getInitialId(), 'staysAt', startCell)
  }
  
  it.skip('cannot cross obstacle from outside', () => {
    const piedestalMiddleCell = 'r7c3'
    //Outer vs. inner sides of big piedestal obstacle      
    const arrOfOuterAndInnerSides = [
      ['r5c1', 'r6c2'],
      ['r5c2', 'r6c2', 's-w'],
      ['r5c3', 'r6c3', 's-w'],
      ['r5c4', 'r6c4', 's-w'],
      ['r5c5', 'r6c4'],
      ['r6c5', 'r6c4', 'a-d'],
      ['r7c5', 'r7c4', 'a-d'],
      ['r8c5', 'r8c4', 'a-d'],
      ['r9c5', 'r8c4'],
      ['r9c4', 'r8c4', 'w-s'],
      ['r9c3', 'r8c3', 'w-s'],
      ['r9c2', 'r8c2', 'w-s'],
      ['r9c1', 'r8c2'],
      ['r8c1', 'r8c2', 'd-a'],
      ['r7c1', 'r7c2', 'd-a'],
      ['r6c1', 'r6c2', 'd-a'],
    ]

    arrOfOuterAndInnerSides.forEach((sides) => {
      moveAndAssertDisabledStepping(sides[0], sides[1], sides[2])
    })

    //can teleport behind obstacles (in the middle of the piedestal)
    player.teleportTo(piedestalMiddleCell)

    arrOfOuterAndInnerSides.forEach((sides) => {
      moveAndAssertDisabledStepping(sides[1], sides[0], sides[2], true)
    })

    const arrOfCornerDiagonals = [
      'r5c4', 'r6c5',
      'r8c5', 'r9c4',
      'r9c2', 'r8c1',
      'r6c1', 'r5c2',
    ]

    player.teleportTo('r5c3')

    arrOfCornerDiagonals.forEach((diagonals) => {
      player.teleportTo(diagonals[0])
      stepAndAssert(diagonals[1])
    })

    //And other way round
    player.teleportTo('r5c3')

    arrOfCornerDiagonals.forEach((diagonals) => {
      player.teleportTo(diagonals[1])
      stepAndAssert(diagonals[0])
    })
  })

  context('allow freely pass near group of full obstacles', () => {
    const arrOf4FullObstaclesSurroundingCells = [
      'r6c6',
      'r5c6',
      'r5c7',
      'r5c8',
      'r6c8',
      'r6c9',
      'r7c9',
      'r8c9',
      'r8c8',
      'r9c8',
      'r9c7',
      'r9c6',
      'r8c6',
      'r8c5',
      'r7c5',
      'r6c5',
    ]

    it('can go diagonaly', () => {
      player.teleportTo('r5c5')

      arrOf4FullObstaclesSurroundingCells.forEach((cell, idx) => {
        if (idx % 2 === 0) {
          clickOnCell(cell)
          chooseInContextMenu('teleport-here')
        }
      })
    })

    it('can go in a zagged manner', () => {
      player.teleportTo('r5c6')

      arrOf4FullObstaclesSurroundingCells.forEach((cell, idx) => {
        clickOnCell(cell)
        chooseInContextMenu('step-to-cell')
      })
    })
  })

  context('allow freely pass near separate full obstacle', () => {
    const arrOfFullObstacleSurroundingCells = [
      'r7c10',
      'r7c11',
      'r8c11',
      'r9c11',
      'r9c10',
      'r9c9',
      'r8c9',
      'r7c9',
    ]
    
    it('can go diagonaly', () => {
      player.teleportTo('r6c9')

      arrOfFullObstacleSurroundingCells.forEach((cell, idx) => {
        if (idx % 2 === 0) {
          clickOnCell(cell)
          chooseInContextMenu('teleport-here')
        }
      })
    })

    it('can go in a zagged manner', () => {
      player.teleportTo('r6c10')

      arrOfFullObstacleSurroundingCells.forEach((cell, idx) => {
        clickOnCell(cell)
        chooseInContextMenu('step-to-cell')
      })
    })
  })

  const arrOf3SidedObstacleSurroundingCells = [
    'r5c10',
    'r5c11',
    'r6c11',
    'r7c11',
    'r7c10',
    'r7c9',
    'r6c9',
    'r5c9',
  ]

  context.skip('allow freely pass near separate 3-sided obstacle', () => {
    it('can go diagonaly', () => {
      player.teleportTo('r4c9')

      arrOf3SidedObstacleSurroundingCells.forEach((cell, idx) => {
        if (idx % 2 === 0) {
          clickOnCell(cell)
          chooseInContextMenu('step-to-cell')
        }
      })
    })

    it('can go in a zagged manner', () => {
      arrOf3SidedObstacleSurroundingCells.forEach((cell, idx) => {
        clickOnCell(cell)
        chooseInContextMenu('step-to-cell')
      })
    })
  })

  const arrOfIceObstacleSurroundingCells = [
    'r6c12',
    'r6c13',
    'r7c13',
    'r8c13',
    'r8c12',
    'r8c11',
    'r7c11',
    'r6c11',
  ]

  context.skip('allow freely pass near separate obstacle with inner sides', () => {
    it('can go diagonaly', () => {
      player.teleportTo('r5c11')

      arrOfIceObstacleSurroundingCells.forEach((cell, idx) => {
        if (idx % 2 === 0) {
          clickOnCell(cell)
          chooseInContextMenu('step-to-cell')
        }
      })
    })

    it('can go orthogonaly', () => {
      arrOfIceObstacleSurroundingCells.forEach((cell, idx) => {
        clickOnCell(cell)
        chooseInContextMenu('step-to-cell')
      })
    })
  })
  
  it('cannot "squize" through two obstacles diagonaly', () => {
    player.teleportTo('r7c7')

    const surroundingCells = [
      'r6c6',
      'r6c7',
      'r6c8',
      'r7c8',
      'r8c8',
      'r8c7',
      'r8c6',
      'r7c6',
    ]

    surroundingCells.forEach((cell, idx) => {
      clickOnCell(cell)
      assertContextMenu([], ['step-to-cell'])
      closeContextMenu()
    })
  })
    
  it('cannot teleport or step on full obstacle cell', () => {
    clickOnCell('r8c10')
    assertContextMenu([], ['teleport-here', 'step-to-cell'])
  })
  
  it.skip('can teleport on 1,2,3 sided obstacle cell', () => {
    const arrOfCells = [
      'r5c7',
      'r6c12',
      'r6c8',
      'r6c10',
    ]

    arrOfCells.forEach((cell) => {
      player.teleportTo(cell)
    })
  })

  it.skip('cannot abuse 3-sided obstacles', () => {
    const openedSidesCells = ['r8c11', 'r8c12', 'r8c13']
    const blockedSidesCells = arrOf3SidedObstacleSurroundingCells.filter((cell) => {
      return !openedSidesCells.includes(cell)
    })
    const middleCell = 'r6c10'

    openedSidesCells.forEach((cell) => {
      player.teleportTo(cell)
      clickOnCell(middleCell)
      assertContextMenu(['step-to-cell'], [])
      closeContextMenu()
    })

    blockedSidesCells.forEach((cell) => {
      player.teleportTo(cell)
      clickOnCell(middleCell)
      assertContextMenu([], ['step-to-cell'])
      closeContextMenu()
    })

    player.teleportTo(middleCell)
    arrOf3SidedObstacleSurroundingCells.forEach((cell) => {
      clickOnCell(cell)
      if (openedSidesCells.includes(cell)) {
        assertContextMenu(['step-to-cell'], [])
      } else {
        assertContextMenu([], ['step-to-cell'])
      }
    })
  })

  it.skip('cannot cross obstacle from inside', () => {
    const middleCell = 'r7c12'

    arrOfIceObstacleSurroundingCells.forEach((cell) => {
      player.teleportTo(cell)
      clickOnCell(middleCell)
      assertContextMenu([], ['step-to-cell'])
      closeContextMenu()
    })

    player.teleportTo(middleCell)
    arrOfIceObstacleSurroundingCells.forEach((cell) => {
      clickOnCell(cell)
      assertContextMenu([], ['step-to-cell'])
      closeContextMenu()
    })
  })
})
