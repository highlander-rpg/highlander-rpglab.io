import { assertGameObject, chooseInContextMenu, player, clickOnCell } from "../../helpers"

export const stepAndAssert = (targetCellId: string): void => {
  clickOnCell(targetCellId)
  chooseInContextMenu('step-to-cell')
  assertGameObject(player.getInitialId(), 'staysAt', targetCellId)
}
