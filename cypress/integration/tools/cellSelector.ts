import {
  convertSelectorToCellId,
  runApp,
} from "../../helpers"

describe('Cell Selector', () => {
  const doorCellId = '#r2c2'

  Cypress.on('window:before:load', (window) => {
    cy.stub(window.console, 'log').as('consoleLog')
  })

  beforeEach(() => {
    runApp()
  })

  it('higlights a cell, logs its ID and does not evoke context menu', () => {
    cy.get(doorCellId).as('cell')
    cy.get('@cell').click({ altKey: true })
    
    cy.get('@cell').should('have.class', 'debug-selected-cell')
    
    cy.get('@consoleLog').should('be.calledWith', convertSelectorToCellId(doorCellId))
    
    cy.findByTestId('context-menu').should('not.be.visible')
  })
})
