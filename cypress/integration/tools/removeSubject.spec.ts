import {
  runApp,
  clickOnCell,
  chooseInContextMenu,
} from "../../helpers"

describe('Object Removing', () => {
  beforeEach(() => {
    runApp()
  })

  it('works as expected', () => {
    const doorCellId = '#r2c2'
    const doorId = '_test-door'

    clickOnCell(doorCellId)
    chooseInContextMenu('remove', doorId)

    clickOnCell(doorCellId)

    let result: any = 1
    cy.window()
      .then((window) => {
        result = window.allGameObjects.get(doorId)
      })
      .then(() => {
        expect(result).to.be.undefined
      })
  })
})
