import {
  runApp,
  clickOnCell,
  chooseInContextMenu,
  assertGameObject,
} from "../../helpers"

describe('Object Modifying', () => {
  beforeEach(() => {
    runApp('dev')
  })

  it('applies under certain circumstances (cancel, apply, close iframe', () => {
    const doorCellId = '#r2c2'
    const doorId = '_test-door'
    const startMinigame = (): void => {
      clickOnCell(doorCellId)
      chooseInContextMenu('modify', doorId)
      cy.getIframeBody('modify_object_minigame').as('modifyObjectApp')
    }

    assertGameObject(doorId, 'isDoor', true)

    startMinigame()

    cy.get('@modifyObjectApp').findByTestId('input-for-isDoor').click()
    assertGameObject(doorId, 'isDoor', true)
    cy.findByTestId('minigame-close-btn').click()
    assertGameObject(doorId, 'isDoor', true)
    startMinigame()
    cy.get('@modifyObjectApp').findByTestId('input-for-isDoor').click()
    cy.get('@modifyObjectApp').findByTestId('cancel-btn').click()
    assertGameObject(doorId, 'isDoor', true)
    cy.get('@modifyObjectApp').findByTestId('input-for-isDoor').click()
    cy.get('@modifyObjectApp').findByTestId('apply-btn').click()

    cy.get('[data-testid="modify_object_minigame-iframe"]').should('not.exist')
    assertGameObject(doorId, 'isDoor', false)

    startMinigame()
    cy.get('@modifyObjectApp').findByTestId('input-for-isDoor').should('have.attr', 'aria-checked', 'false')
  })

  it('should render compounds number correctly', () => {
    clickOnCell('r2c18')
    chooseInContextMenu('modify', '_test-predictable-bush')

    cy.getIframeBody('modify_object_minigame').as('modifyObjectApp')

    const content = [
      'redCurrantBerry: 4',
      'redCurrantBranch: 4',
      'redCurrantDryBerry: 4',
      'redCurrantDryBranch: 4',
      'redCurrantDryLeaf: 4',
      'redCurrantLeaf: 4',
    ]
    
    cy.get('@modifyObjectApp').findByTestId('container-for-compounds').should('be.visible').and('have.text', content.join(''))
  })

  it('should render sprite layers list correctly', () => {
    clickOnCell('r2c18')
    chooseInContextMenu('modify', '_test-predictable-bush')

    cy.getIframeBody('modify_object_minigame').as('modifyObjectApp')

    const content = [
      'basis: 6044',
      'upperOverlay1: 4345',
      'upperOverlay2: 6041',
    ]
    
    cy.get('@modifyObjectApp').findByTestId('container-for-spriteLayers').should('be.visible').and('have.text', content.join(''))
  })
})
