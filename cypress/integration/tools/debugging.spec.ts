import {
  runApp,
  player,
  clickOnCell,
  closeContextMenu,
  checkParticularContextMenuItem,
} from "../../helpers"

describe('Debugging', () => {
  const playerCellId = '#r4c2'
  const doorCellId = '#r2c2'

  beforeEach(() => {
    runApp()
  })

  it('may be envoken on any game object', () => {
    clickOnCell(playerCellId)
    checkParticularContextMenuItem('debug', player.getInitialId(), 'exist')
    closeContextMenu()

    clickOnCell(doorCellId)
    checkParticularContextMenuItem('debug', '_test-door', 'exist')
  })
})
