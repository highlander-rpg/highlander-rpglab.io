import "@testing-library/cypress/add-commands"
import { MinigameId } from '../../src/types'

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

declare global {
  namespace Cypress {
    interface Chainable {
      getIframeBody: typeof getIframeBody
    }
  }
}

const getIframeBody = (minigameId: MinigameId): Cypress.Chainable => {
  // get the iframe > document > body
  // and retry until the body element is not empty
  return cy
    .findByTestId(`${minigameId}-iframe`, { log: false })
    .its('0.contentDocument.body', { log: false }).should('not.be.empty')
    .then((body) => cy.wrap(body, { log: false }))
}

Cypress.Commands.add('getIframeBody', getIframeBody)
