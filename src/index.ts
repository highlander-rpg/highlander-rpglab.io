import './index.css'

import {
  initHotkeys,
  initPlayer,
  initContextMenu,
  initGameObjects,
  initMap,
  initTestObjects,
  initDebugTools,
  initAnimationLayer,
} from './initiators'

initGameObjects()
initMap()
initAnimationLayer()
initPlayer()
initContextMenu()
initHotkeys()
initTestObjects()
initDebugTools()
