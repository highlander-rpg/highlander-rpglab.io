import Pixelsmith from 'pixelsmith'
import fs from 'fs'
import toArray from 'stream-to-array'
import _ from 'lodash'
import compress_images from 'compress-images'


const dirPath = './sprites'
const tempImg = 'sprite.png'
const allFiles = fs.readdirSync(dirPath)
const tileDimension = 32
const imagesPerRow = 64
const spritesheetWidth = imagesPerRow * tileDimension
const spritesheetHeight = Math.ceil(allFiles.length / imagesPerRow) * tileDimension
const pixelsmith = new Pixelsmith()

pixelsmith.createImages(allFiles.map(el => Number(el.split('.png')[0])).sort((a, b) => a - b).map(idx => `${dirPath}/${idx}.png`), (err, imgs) => {
  if (err) {
    throw err
  }

  const canvas = pixelsmith.createCanvas(spritesheetWidth, spritesheetHeight)

  const data = _.chunk(imgs, imagesPerRow)

  data.forEach((row, rowIdx) => {
    row.forEach((img, colIdx) => {
      canvas.addImage(img, colIdx * tileDimension, rowIdx * tileDimension)
    })
  })

  const resultStream = canvas['export']({ format: 'png' })

  toArray(resultStream)
    .then((parts) => {
      const buffers = parts.map(part => Buffer.from(part))
      
      fs.writeFileSync(tempImg, Buffer.concat(buffers), err => {
        if (err) {
          console.log(err)
        }
      })

      console.log('Spritesheet has been generated')

      compress_images(`./${tempImg}`, '.', { compress_force: false, statistic: true, autoupdate: true }, false,
        { jpg: { engine: 'mozjpeg', command: ['-quality', '60'] } },
        { png: { engine: 'pngquant', command: ['--quality=20-50', '--ext=s.png', '--force'] } },
        { svg: { engine: 'svgo', command: '--multipass' } },
        { gif: { engine: 'gifsicle', command: ['--colors', '64', '--use-col=web'] } },
        (error, completed, statistic) => {
          if (error) {
            console.log(error)
          }

          //Remove temp image
          fs.unlinkSync(`./${tempImg}`)
          fs.rmdirSync(`.${tempImg}`)

          console.log('Spritesheet has been optimized')
        }
      )
    })
})
