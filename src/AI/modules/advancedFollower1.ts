import { AIModule } from '../../types'
import { advancedFollower1Script } from '../scripts'

export type AdvancedFollower1Config = {
  followSubjectId: string,
  maxDistanceToSubject: number,
}

export const advancedFollower1: AIModule = {
  name: 'Advanced Follower by M.A.R.S.E.L.L.E.S.',
  desc: 'This is an advanced script to follow a subject',
  script: advancedFollower1Script,
  priority: 3,
  config: {
    followSubjectId: '_test-player',
    maxDistanceToSubject: 10,
  },
}
