import { AIModule } from '../../types'
import { basicPatrol1Script } from '../scripts'

export type BasicPatrol1Config = {
  patrolPoints: string[],
}

export const basicPatrol1: AIModule = {
  name: 'Basic Patrol by Richard D. Willson',
  desc: 'This is my home-made script for Arduino robots to patrol my backyard garden and defend it from cats',
  script: basicPatrol1Script,
  priority: 2,
  config: {
    patrolPoints: ['r1c2', 'r12c12'],
  },
}
