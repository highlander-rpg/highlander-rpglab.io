import { performAction } from '../../helpers';
import { MockObjectType } from '../../types'
import { BasicPatrol1Config } from '../modules'

export const basicPatrol1Script = (actor: MockObjectType, config: BasicPatrol1Config): void => {
  // This is a basic patrol script for a ROBOT.
  // The robot will patrol between two points.

  // The robot will start at the first point.
  let currentPatrolPointIndex = 0
  let currentPatrolPoint = config.patrolPoints[currentPatrolPointIndex]
  // The robot will move to the next point every 5 seconds.
  const interval = window.setInterval(() => {
    if (actor.isBrokenDown) {
      return
    }

    if (actor.isSleeping) {
      return
    }

    if (actor.components?.processor?.isOn === false) {
      return
    }
    
    // If the robot is at the last point, it will go back to the first point.
    if (currentPatrolPointIndex === config.patrolPoints.length - 1) {
      currentPatrolPointIndex = 0
    } else {
      currentPatrolPointIndex += 1
    }
    
    performAction('go-to', currentPatrolPoint, actor);

    currentPatrolPoint = config.patrolPoints[currentPatrolPointIndex]
  }, 5000)

  actor.intervals.push(interval);
}
