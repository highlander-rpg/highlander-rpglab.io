import { cloneDeep } from 'lodash/fp'
import { getCellsAround, getGameObject, removeObject, updateObject } from '../helpers'
import { getRandomElement, getRandomNumber } from '../utils'
import { ActionConstructor } from '../types'
import { addFire } from '../initiators/gameObjects'

export const keepBurning: ActionConstructor = (actor, subject, cellId) => {
  if (actor.type !== 'fire') {
    return false
  }

  if (actor.staysAt !== cellId) {
    return false
  }

  return {
    content: 'Keep burning',
    id: 'keep-burning',
    subjectId: actor.id,
    callback: () => {
      const fire = cloneDeep(actor)

      if (fire.hitPoints < 0) {
        removeObject(fire.id, cellId)
      } else {
        fire.hitPoints = fire.hitPoints - 1

        const cellsUnderInfluence = getCellsAround(cellId, 1)
        Array(cellId, getRandomElement(cellsUnderInfluence)).forEach((indluencedCellId: string) => {
          const randomObjId = getRandomElement(window.currentMap.get(indluencedCellId))
          const randomObj = cloneDeep(getGameObject(randomObjId))

          if (randomObj.fireResistance <= 0) {
            removeObject(randomObjId, indluencedCellId)
            
            const numOfFlamesMax = getRandomNumber(3, 5)
            for (let i = 2; i < numOfFlamesMax; i++) {
              addFire(indluencedCellId, {})
            }
          } else {
            randomObj.fireResistance = randomObj.fireResistance - 1
            updateObject(randomObj, indluencedCellId)
          }
        })
  
        updateObject(fire, cellId)
      }
    }
  }
}
