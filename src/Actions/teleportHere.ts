import { isCellFullObstacle, updateObject } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { ActionConstructor } from '../types'

export const teleportHere: ActionConstructor = (actor, subject, cellId) => {
  if (actor.staysAt === cellId) {
    return false
  }

  if (isCellFullObstacle(cellId)) {
    return false
  }

  return {
    content: 'Teleport here',
    id: 'teleport-here',
    subjectId: actor.id,
    callback: () => {
      const modifiedActor = cloneDeep(actor)
      
      const oldCellId = modifiedActor.staysAt
      modifiedActor.staysAt = cellId

      updateObject(modifiedActor, oldCellId)
    }
  }
}
