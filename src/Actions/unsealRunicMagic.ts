import { updateObject } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { ActionConstructor } from '../types'

export const unsealRunicMagic: ActionConstructor = (actor, subject, cellId) => {
  if (subject.isSealedWithRunicMagic) {
    return {
      content: 'Remove runic magic seal',
      id: 'unseal-runic',
      subjectId: subject.id,
      callback: () => {
        const minigameWindow = window.open('http://localhost:9000/', 'targetWindow', "resizable=yes,width=300,height=300,top=100,left=200,location=no,toolbar=no,menubar=no,scrollbars=yes")

        const ensureMinigameIsFinished = () => {
          return new Promise((resolve, reject) => {
            (function waitForResult() {
              //Commented for a while because of TS error on window.result
              // if (minigameWindow.result) {
              //   return resolve(minigameWindow.result)
              // }
              setTimeout(waitForResult, 30)
            })()
          })
        }

        ensureMinigameIsFinished().then(minigameResult => {
          console.log(minigameResult)
          const modifiedSubject = cloneDeep(subject)
        
          modifiedSubject.isSealedWithRunicMagic = false

          updateObject(modifiedSubject, cellId)
        })
      }
    }
  }
}
