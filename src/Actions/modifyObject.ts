import { Minigame } from '../minigamesAPI/minigame'
import { ActionConstructor } from '../types'

export const modifyObject: ActionConstructor = (actor, subject, cellId) => {
  return {
    content: `_Modify ${subject.name}`,
    id: 'modify',
    subjectId: subject.id,
    callback: () => {
      const minigame = new Minigame(
        "modify_object_minigame",
        actor,
        subject,
        cellId,
      )
      minigame.init()
    }
  }
}
