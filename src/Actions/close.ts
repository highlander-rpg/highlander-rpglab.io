import { updateObject, isInReach } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { ActionConstructor } from '../types'

export const close: ActionConstructor = (actor, subject, cellId) => {
  if (!(subject.isDoor || subject.isContainer)) {
    return false
  }

  if (!subject.isOpen) {
    return false
  }

  if (subject.isBrokenDown) {
    return false
  }

  if (!isInReach(actor, subject)) {
    return false
  }

  return {
    content: `Close the ${subject.name}`,
    id: 'close',
    subjectId: subject.id,
    callback: () => {
      const modifiedSubject = cloneDeep(subject)
      
      modifiedSubject.spriteLayers.basis = modifiedSubject._spriteClosed
      modifiedSubject.isOpen = false
      if (subject.isDoor) {
        modifiedSubject.isFullObstacle = true
      }

      updateObject(modifiedSubject, cellId)
    }
  }
}
