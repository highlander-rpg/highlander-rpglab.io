const PF = require('pathfinding')

import { getCol, getRow, isCellFullObstacle, isInReach, convertPathfindingPathToArrayOfCells, performAction, updateObject } from '../helpers'
import { ActionConstructor } from '../types'
import { mapDefaultHeight, mapDefaultWidth } from '../config'
import { cloneDeep } from 'lodash';

export const switchOn: ActionConstructor = (actor, subject, cellId) => {
  if (!subject.components.processor) {
    return false
  }

  if (subject.components?.processor?.isOn) {
    return false
  }

  return {
    content: 'Switch on',
    id: 'switch-on',
    subjectId: subject.id,
    callback: () => {
      subject.components.processor.isOn = true

      updateObject(subject, subject.staysAt)

      const currentAIModule = subject.components.AIModulesBoard.modules[0] 
      currentAIModule.script(subject, currentAIModule.config)
    }
  }
}
