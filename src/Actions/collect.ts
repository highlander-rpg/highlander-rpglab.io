import { isEmpty } from 'lodash/fp'
import { processBar } from '../animation'

import { createObject, decrementObjectCompound, getGameObjectFromTemplate, isInReach, performAction } from '../helpers'
import { ActionConstructor, ActionType, ObjectTemplateId } from '../types'
import { contextMenuInputValue, wrapContextMenuInput } from '../ui'

const baseDuration = 1000

export const collect: ActionConstructor = (actor, subject, cellId) => {
  if (!isInReach(actor, subject)) {
    return false
  }

  if (isEmpty(subject.compounds)) {
    return false
  }

  const arrOfActionObjects: ActionType[] = []

  for (const [compoundTemplateId, quantity] of Object.entries(subject.compounds)) {
    if (quantity > 0) {
      const compoundId = compoundTemplateId as ObjectTemplateId
      const objToCollect = getGameObjectFromTemplate(compoundId)

      const collectOneActionText = `Collect one ${objToCollect.name}`

      arrOfActionObjects.push({
        content: collectOneActionText,
        id: 'collect-one',
        subjectId: subject.id,
        callback: () => {
          const successFunc = () => {
            decrementObjectCompound(subject, compoundId, cellId)
            createObject(cellId, objToCollect)
          }

          processBar(actor.staysAt, actor.id, baseDuration, successFunc)
        }
      })

      arrOfActionObjects.push({
        content: `Collect ${wrapContextMenuInput(`<input type="number" value="2" min="0" max="${quantity}" />`)} ${objToCollect.name} `,
        id: 'collect-some',
        subjectId: subject.id,
        callback: () => {
          let isActionPerformed = false

          const enteredValue = contextMenuInputValue.get()

          if (enteredValue) {
            let count = 0
            const interval = setInterval(() => {
              isActionPerformed = performAction('collect-one', cellId, actor, subject, collectOneActionText)
              if (!isActionPerformed || count >= enteredValue - 1) {
                clearInterval(interval)
              }
              count++
            }, baseDuration + 200)
          }
        }
      })

      if (quantity > 1) {
        arrOfActionObjects.push({
          content: `Collect all of ${objToCollect.name}`,
          id: 'collect-all',
          subjectId: subject.id,
          callback: () => {
            let isActionPerformed = false

            const interval = setInterval(() => {
              isActionPerformed = performAction('collect-one', cellId, actor, subject, collectOneActionText)
              if (!isActionPerformed) {
                clearInterval(interval)
              }
            }, baseDuration + 200)
          }
        })
      }
    }
  }

  return arrOfActionObjects
}
