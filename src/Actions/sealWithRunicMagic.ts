import { updateObject } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { ActionConstructor } from '../types'

export const sealWithRunicMagic: ActionConstructor = (actor, subject, cellId) => {
  if (subject.isSealedWithRunicMagic) {
    return false
  }

  if (!subject.isDoor || !subject.isContainer) {
    return false
  }

  return {
    content: 'Seal with runic magic',
    id: 'seal-runic',
    subjectId: subject.id,
    callback: () => {
      const modifiedSubject = cloneDeep(subject)
      
      modifiedSubject.spriteLayers = {
        basis: modifiedSubject._spriteClosed
      }
      modifiedSubject.isOpen = false
      modifiedSubject.isSealedWithRunicMagic = true

      updateObject(modifiedSubject, cellId)
    }
  }
}
