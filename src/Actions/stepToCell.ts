import { isCellFullObstacle, isOrthogonalCells, updateObject } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { ActionConstructor } from '../types'
import { getDistanceBetweenCells } from '../helpers'

export const stepToCell: ActionConstructor = (actor, subject, cellId) => {
  if (getDistanceBetweenCells(actor.staysAt, cellId) !== 1) {
    return false
  }

  if (isCellFullObstacle(cellId)) {
    return false
  }

  if (!isOrthogonalCells(actor.staysAt, cellId)) {
    return false
  }

  return {
    content: 'Step here',
    id: 'step-to-cell',
    subjectId: actor.id,
    callback: () => {
      const modifiedActor = cloneDeep(actor)
      
      const oldCellId = modifiedActor.staysAt
      modifiedActor.staysAt = cellId

      updateObject(modifiedActor, oldCellId)
    }
  }
}
