import { getRandomElement, getPhrase, chance } from '../utils'
import { ActionConstructor } from '../types'
import { isSameGameObject } from '../helpers'

export const examine: ActionConstructor = (actor, subject, cellId) => {
  const arr = []

  arr.push(getPhrase(`[It\'s a|It is a|This is a] ${subject.name}.`))

  const partIs = '[\'s| is| has been]'
  if ((subject.isDoor || subject.isContainer) && !subject.isOpen) {
    arr.push(getPhrase(`It${partIs} closed.`))
  }

  if (subject.isLocked) {
    arr.push(getPhrase(`It${partIs}[ tightly|] locked[, as well|].`))
  }

  if (subject.isSealedWithRunicMagic) {
    arr.push(getPhrase(`[This ${subject.name}|The ${subject.name}|It] is sealed with [runic magic|magic runes].`))
  }

  if (subject.isBrokenDown) {
    const repairPhrases = [
      'Can be repaired[|, maybe].',
      '[May|Can|Maybe it can] be repaired.',
    ]

    const statusPhrase = `[This ${subject.name}|The ${subject.name}|It] [is|was] broken[| down].`

    const randomPhrase = chance('30%') ? `${statusPhrase} ${getRandomElement(repairPhrases)}` :  statusPhrase
    arr.push(getPhrase(randomPhrase))
  }

  const msg = arr.join(' ')
  if (!msg.length) {
    return false
  }

  return {
    content: `Examine ${isSameGameObject(actor, subject) ? 'myself' : subject.name}`,
    id: 'examine',
    subjectId: subject.id,
    callback: () => {
      if (msg.length) {
        console.log(msg)
      }
    }
  }
}
