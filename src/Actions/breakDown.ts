import { updateObject, isInReach } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { ActionConstructor } from '../types'

export const breakDown: ActionConstructor = (actor, subject, cellId) => {
  if (!(subject.isDoor || subject.isContainer)) {
    return false
  }

  if (subject.isBrokenDown) {
    return false
  }

  if (!isInReach(actor, subject)) {
    return false
  }

  return {
    content: `Break down the ${subject.name}`,
    id: 'break',
    subjectId: subject.id,
    callback: () => {
      const modifiedSubject = cloneDeep(subject)
      
      modifiedSubject.spriteLayers.basis = modifiedSubject._spriteOpen
      modifiedSubject.isOpen = true
      modifiedSubject.isLocked = false
      if (subject.isDoor) {
        modifiedSubject.isFullObstacle = false
      }
      modifiedSubject.isBrokenDown = true

      updateObject(modifiedSubject, cellId)
    }
  }
}
