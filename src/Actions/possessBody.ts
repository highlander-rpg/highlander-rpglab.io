import { cloneDeep } from 'lodash'
import { isPlayer, isSameGameObject, updateObject } from '../helpers'
import { ActionConstructor } from '../types'

export const possessBody: ActionConstructor = (actor, subject, cellId) => {
  if (isSameGameObject(actor, subject)) {
    return false
  }
  
  return {
    content: `Possess ${subject.name}`,
    id: 'possess-body',
    subjectId: subject.id,
    callback: () => {
      if (isPlayer(actor)) {
        window.playerId = subject.id
      }

      const updatedActor = cloneDeep(actor)
      updatedActor.isSleeping = true

      updateObject(updatedActor, cellId)
      updateObject(subject, cellId)
    }
  }
}
