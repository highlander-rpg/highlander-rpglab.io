import { ActionConstructor } from "../types"

export const debugSubject: ActionConstructor = (actor, subject, cellId) => {
  return {
    content: `_Debug ${subject.name}`,
    id: 'debug',
    subjectId: subject.id,
    callback: () => {
      console.log(subject)
    }
  }
}
