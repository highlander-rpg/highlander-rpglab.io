const PF = require('pathfinding')

import { getCol, getRow, isCellFullObstacle, isInReach, convertPathfindingPathToArrayOfCells, performAction } from '../helpers'
import { ActionConstructor } from '../types'
import { mapDefaultHeight, mapDefaultWidth } from '../config'
import { getAvailableActions } from './getAvailableActions'

export const goTo: ActionConstructor = (actor, subject, cellId) => {
  if (isInReach(actor, subject)) {
    return false
  }

  if (isCellFullObstacle(cellId)) {
    return false
  }

  return {
    content: 'Go here',
    id: 'go-to',
    subjectId: actor.id,
    callback: () => {
      if (actor.components?.processor?.isOn === false) {
        return
      }

      const grid = new PF.Grid(mapDefaultWidth, mapDefaultHeight)

      document.querySelectorAll('#current-map-table td').forEach((cellEl) => {
        const cellElId = cellEl.getAttribute('id')
        if (isCellFullObstacle(cellElId)) {
          const x = getCol(cellElId)
          const y = getRow(cellElId)
          grid.setWalkableAt(x, y, false)
        }
      })

      const finder = new PF.AStarFinder({
        allowDiagonal: false
      })

      const startX = getCol(actor.staysAt)
      const startY = getRow(actor.staysAt)
      const endX = getCol(cellId)
      const endY = getRow(cellId)

      const path = finder.findPath(startX, startY, endX, endY, grid)
      const arrOfCells = convertPathfindingPathToArrayOfCells(path)

      const makeStep = (cells: string[]): void => {
        const currentCell = cells[0]
        if (currentCell) {
          if (!isCellFullObstacle(currentCell)) {

            performAction('step-to-cell', currentCell, actor)

            window.setTimeout(() => {
              makeStep(cells.slice(1))
            }, 500)
          }
        }
      }

      makeStep(arrOfCells)
    }
  }
}
