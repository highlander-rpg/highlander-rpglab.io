import * as allActions from './index'
import { ActionType, MockObjectType } from '../types'
import { getGameObject, getObjId } from '../helpers'

export const getAvailableActions = (actorObjOrId: MockObjectType | string, subjectObjOrId: MockObjectType | string, cellId: string): ActionType[] => {
  const actor = getGameObject(getObjId(actorObjOrId))
  const subject = getGameObject(getObjId(subjectObjOrId))

  const arr = []

  for (let action of Object.keys(allActions)) {
    type act = keyof typeof allActions
    const actionObjOrArrOfActionObjects = allActions[action as act](actor, subject, cellId)
    if (Array.isArray(actionObjOrArrOfActionObjects)) {
      const arrOfActionObjects = actionObjOrArrOfActionObjects
      arr.push(...arrOfActionObjects)
    } else {
      const actionObj = actionObjOrArrOfActionObjects
      arr.push(actionObj)
    }
  }

  //Get array without falsy elements
  return arr.filter(Boolean) as ActionType[]
};
