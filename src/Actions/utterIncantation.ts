import { ActionConstructor } from '../types'
import { getRandomElement } from '../utils'

export const utterIncantation: ActionConstructor = (actor, subject, cellId) => {
  const arrOfVowels = ['a', 'i', 'o', 'u']
  //Removed: e
  const arrOfConsonants = ['b', 'c', 'd', 'f', 'g', 'h', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'w', 'z']
  //Removed: j, q, y

  const c = () => getRandomElement(arrOfConsonants)
  const v = () => getRandomElement(arrOfVowels)
  const generateSyllable = () => `${c()}${v()}h${c()}`
  const generateMiddle = () => `${c()}${v()}h`

  return {
    content: 'Utter incantation',
    id: 'utter-incantation',
    subjectId: subject.id,
    callback: () => {
      console.log(String(`${generateSyllable()}-${generateMiddle()}-${generateSyllable()}`).toUpperCase())
    }
  }
}
