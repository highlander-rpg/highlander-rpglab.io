import { isCellFullObstacle, updateObject } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { ActionConstructor } from '../types'
import { getDistanceBetweenCells } from '../helpers'

export const jumpToCell: ActionConstructor = (actor, subject, cellId) => {
  if (getDistanceBetweenCells(actor.staysAt, cellId) !== 2) {
    return false
  }

  if (isCellFullObstacle(cellId)) {
    return false
  }

  return {
    content: 'Jump here',
    id: 'jump-to-cell',
    subjectId: actor.id,
    callback: () => {
      const modifiedActor = cloneDeep(actor)
      
      const oldCellId = modifiedActor.staysAt
      modifiedActor.staysAt = cellId

      updateObject(modifiedActor, oldCellId)
    }
  }
}
