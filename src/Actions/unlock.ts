import { cloneDeep } from 'lodash/fp'
import { isSameGameObject, updateObject } from '../helpers'
import { isInReach } from '../helpers'
import { ActionConstructor } from '../types'

export const unlock: ActionConstructor = (actor, subject, cellId) => {
  if (!(subject.isDoor || subject.isContainer)) {
    return false
  }

  if (subject.isOpen) {
    return false
  }

  if (!subject.isLocked) {
    return false
  }

  if (!actor.inventory.includes(subject.keyToUnlock) && !isSameGameObject(actor, subject)) {
    return false
  }

  if (!isInReach(actor, subject)) {
    return false
  }

  return {
    content: `Unlock the ${subject.name}`,
    id: 'unlock',
    subjectId: subject.id,
    callback: () => {
      const modifiedSubject = cloneDeep(subject)
      
      modifiedSubject.isLocked = false

      updateObject(modifiedSubject, cellId)
    }
  }
}
