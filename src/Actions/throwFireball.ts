import { fireball } from '../animation'
import { addFire } from '../initiators/gameObjects'
import { ActionConstructor } from '../types'
import { getRandomNumber } from '../utils'

export const throwFireball: ActionConstructor = (actor, subject, cellId) => {
  if (actor.staysAt === cellId) {
    return false
  }

  //TODO: Prevent throwing if obstacle (100% pervention) or smth that is not flat (some % pervention) is met (change targetCellId when raycasted smth)

  return {
    content: 'Throw fireball',
    id: 'throw-fireball',
    subjectId: subject.id,
    callback: () => {
      const explode = () => {
        const numOfFlamesMax = getRandomNumber(3, 8)
        for (let i = 3; i < numOfFlamesMax; i++) {
          addFire(cellId, {})
        }
      }

      fireball(actor.staysAt, cellId, explode)
    }
  }
}
