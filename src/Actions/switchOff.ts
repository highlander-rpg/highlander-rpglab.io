import { updateObject, getGameObject } from '../helpers'
import { ActionConstructor } from '../types'

export const switchOff: ActionConstructor = (actor, subject, cellId) => {
  if (!subject.components.processor) {
    return false
  }
  
  if (!subject.components?.processor?.isOn) {
    return false
  }

  return {
    content: 'Switch off',
    id: 'switch-off',
    subjectId: subject.id,
    callback: () => {
      subject.components.processor.isOn = false

      subject.intervals.forEach((interval) => {
        window.clearInterval(interval)
      })
      subject.intervals = []

      updateObject(getGameObject(subject.id), getGameObject(subject.id).staysAt)
    }
  }
}
