import { removeObject } from '../helpers'
import { ActionConstructor } from '../types'

export const removeSubject: ActionConstructor = (actor, subject, cellId) => {
  return {
    content: `_Remove ${subject.name}`,
    id: 'remove',
    subjectId: subject.id,
    callback: () => {
      removeObject(subject.id, cellId)
    }
  }
}
