import { flyingStone } from '../animation'
import { ActionConstructor } from '../types'

export const throwStone: ActionConstructor = (actor, subject, cellId) => {
  if (actor.staysAt === cellId) {
    return false
  }

  //TODO: Prevent throwing if obstacle (100% pervention) or smth that is not flat (some % pervention) is met (change targetCellId when raycasted smth)

  return {
    content: 'Throw stone',
    id: 'throw-stone',
    subjectId: subject.id,
    callback: () => {
      flyingStone(actor.staysAt, cellId)
    }
  }
}
