import { cloneDeep } from 'lodash/fp'
import { updateObject } from '../helpers'
import { isInReach } from '../helpers'
import { ActionConstructor } from '../types'

export const open: ActionConstructor = (actor, subject, cellId) => {
  if (!(subject.isDoor || subject.isContainer)) {
    return false
  }

  if (subject.isOpen) {
    return false
  }

  if (subject.isLocked) {
    return false
  }

  if (subject.isSealedWithRunicMagic) {
    return false
  }

  if (!isInReach(actor, subject)) {
    return false
  }

  return {
    content: `Open the ${subject.name}`,
    id: 'open',
    subjectId: subject.id,
    callback: () => {
      const modifiedSubject = cloneDeep(subject)
      
      modifiedSubject.spriteLayers.basis = modifiedSubject._spriteOpen
      modifiedSubject.isOpen = true
      if (subject.isDoor) {
        modifiedSubject.isFullObstacle = false
      }

      updateObject(modifiedSubject, cellId)
    }
  }
}
