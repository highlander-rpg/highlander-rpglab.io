import { updateObject } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { isInReach } from '../helpers'
import { ActionConstructor } from '../types'

export const repair: ActionConstructor = (actor, subject, cellId) => {
  if (!(subject.isDoor || subject.isContainer)) {
    return false
  }

  if (!isInReach(actor, subject)) {
    return false
  }

  if (subject.isBrokenDown) {
    return {
      content: `Repair the ${subject.name}`,
      id: 'repair',
      subjectId: subject.id,
      callback: () => {
        const modifiedSubject = cloneDeep(subject)
        
        modifiedSubject.isBrokenDown = false

        updateObject(modifiedSubject, cellId)
      }
    }
  }
}
