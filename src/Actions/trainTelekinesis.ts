import { updateObject } from '../helpers'
import { cloneDeep } from 'lodash/fp'
import { ActionConstructor } from '../types'
import { isSameGameObject } from '../helpers'

export const trainTelekinesis: ActionConstructor = (actor, subject, cellId) => {
  if (actor.staysAt !== subject.staysAt) {
    return false
  }

  if (!isSameGameObject(actor, subject)) {
    return false
  }

  return {
    content: 'Train telekinesis',
    id: 'train-telekinesis',
    subjectId: actor.id,
    callback: () => {
      const modifiedSubject = cloneDeep(actor)
      
      const newLevel = actor.telekinesisLevel + 1
      
      modifiedSubject.telekinesisLevel = newLevel
      
      updateObject(modifiedSubject, cellId)
    }
  }
}
