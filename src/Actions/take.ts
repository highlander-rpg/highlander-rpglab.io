import { isInReach, rerenderCell } from '../helpers'
import { ActionConstructor } from '../types'

export const take: ActionConstructor = (actor, subject, cellId) => {
  if (!isInReach(actor, subject)) {
    return false
  }

  if (actor.inventory.includes(subject.id)) {
    return false
  }

  if (subject.weight > actor.inventoryCapacity) {
    return false
  }

  return {
    content: `Take ${subject.name}`,
    id: 'take',
    subjectId: subject.id,
    callback: () => {
      const objectsIdsOnCell = window.currentMap.get(cellId)
      const newArrOfObjectsIdsOnCell = objectsIdsOnCell.filter((id) => id !== subject.id)

      window.currentMap.set(cellId, newArrOfObjectsIdsOnCell)

      actor.inventory.push(subject.id)
    
      rerenderCell(cellId)
    }
  }
}
