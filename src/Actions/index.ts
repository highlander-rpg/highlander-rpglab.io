export * from './debugSubject'
export * from './modifyObject'
export * from './removeSubject'
export * from './examine'

export * from './take'
export * from './collect'

export * from './open'
export * from './close'
export * from './breakDown'
export * from './repair'
export * from './lock'
export * from './unlock'

export * from './teleportHere'
export * from './stepToCell'
export * from './goTo'
export * from './jumpToCell'

export * from './sealWithRunicMagic'
export * from './unsealRunicMagic'
export * from './trainTelekinesis'
export * from './utterIncantation'
export * from './possessBody'

export * from './throwStone'
export * from './throwFireball'

export * from './keepBurning'

export * from './switchOn'
export * from './switchOff'
