export const cellSelector = {
  init: () => {
    document.addEventListener('click', (e) => {
      if (e.altKey) {
        const el = e.target as HTMLElement
        const cellId = el.id
        const className = 'debug-selected-cell'

        document.querySelector('.' + className)?.classList.remove(className)
        
        el.classList.add(className)
        
        console.log(cellId)
        navigator.clipboard.writeText(cellId)
      }
    })
  }
}
