import { createObject } from '../helpers'

export const quickAction = {
  init: () => {
    document.addEventListener('click', (e) => {
      if (e.ctrlKey) {
        const el = e.target as HTMLElement
        const cellId = el.id
        const spriteId = prompt("Type sprite id here:")
        if (spriteId) {
          createObject(cellId, {
            spriteLayers: {
              basis: Number(spriteId),
            },
          })
        }
      }
    })
  }
}
