import { convertPathfindingPathToArrayOfCells } from './pathFinding'

describe('`convertPathfindingPathToArrayOfCells` helper', () => {
  test('should handle empty array', () => {
    expect(convertPathfindingPathToArrayOfCells([])).toStrictEqual([])
  })

  test('should handle 1 step', () => {
    expect(convertPathfindingPathToArrayOfCells([[0, 1]])).toStrictEqual(['r1c0'])
  })

  test('should work as expected', () => {
    expect(convertPathfindingPathToArrayOfCells([[0, 1], [2, 10]])).toStrictEqual(['r1c0', 'r10c2'])
  })
})
