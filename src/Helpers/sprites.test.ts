import { convertSpriteId } from './sprites'

describe('"convertSpriteId" helper', () => {
  const cases = [
    [0, '0px 0px'],
    [1, '-32px 0px'],
    [2, '-64px 0px'],
    [62, '-1984px 0px'],
    [63, '-2016px 0px'],
    [64, '0px -32px'],
    [65, '-32px -32px'],
    [66, '-64px -32px'],
    [6039, '-736px -3008px'],
    ['0px 0px', '0px 0px'],
    ['-32px 0px', '-32px 0px'],
    ['-32px -32px', '-32px -32px'],
    ['', ''],
  ]
  
  test.each(cases)(
    'given %p, returns %p',
    (input, output) => {
      const result = convertSpriteId(input as number)
      expect(result).toBe(output)
    }
  )
})
