//[ [ 1, 2 ], [ 1, 1 ], [ 2, 1 ], [ 3, 1 ], [ 3, 2 ], [ 4, 2 ] ]
export const convertPathfindingPathToArrayOfCells = (pathArr: [number, number][]): string[] => {
  return pathArr.map(([x, y]) => `r${y}c${x}`)
}
