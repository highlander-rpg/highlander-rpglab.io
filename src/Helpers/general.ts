import { getAvailableActions } from '../actions/getAvailableActions'
import { ActionId, ActionType, MockObjectType } from '../types'
import { getDistanceBetweenCells } from './cells'
import { getRandomElement } from '../utils'
import { getObjId } from './objects'

export const isPlayer = (objOrIdToCheck: MockObjectType | string): boolean => {
  const idToCheck = getObjId(objOrIdToCheck)
  return idToCheck === window.playerId
}

export const getParticularAction = (arrOfActions: ActionType[], actionId: ActionId, actionSubjectId?: string, actionContent?: string): ActionType => {
  return arrOfActions.find((action) => {
    if (actionSubjectId && actionContent) {
      return action.id === actionId && action.subjectId === actionSubjectId && action.content === actionContent
    } else if (actionSubjectId) {
      return action.id === actionId && action.subjectId === actionSubjectId
    } else {
      return action.id === actionId
    }
  })
}

export const performAction = (
  actionId: ActionId,
  cellId: string,
  actorObjOrId: MockObjectType | string,
  subjectObjOrId?: MockObjectType | string | undefined,
  actionText?: string,
): boolean => {
  const objectsIdsOnCell = window.currentMap.get(cellId) || []
  const subjectId = subjectObjOrId ? getObjId(subjectObjOrId) : undefined
  let subject = window.allGameObjects.get(subjectId)
  if (!subject) {
    subject = window.allGameObjects.get(getRandomElement(objectsIdsOnCell))
  }
  const arrayOfActions = getAvailableActions(actorObjOrId, subject, cellId)
  const action = getParticularAction(arrayOfActions, actionId, subjectId, actionText)

  let isActionPerformed: boolean = false

  if (action) {
    isActionPerformed = true
    action.callback()
  }

  return isActionPerformed
}

export const isInReach = (actor: MockObjectType, subject: MockObjectType): boolean => {
  if (actor.telekinesisLevel > 0) {
    return true
  }

  if (getDistanceBetweenCells(actor.staysAt, subject.staysAt) <= 1) {
    return true
  }

  return false
}

export const closeContextMenu = (): void => {
  const contextMenuPopupEl = document.getElementById('contextMenu')
  
  contextMenuPopupEl.classList.add('hidden')
  contextMenuPopupEl.innerHTML = ''

  document.querySelector('.context-menu-selected-cell')?.classList.remove('context-menu-selected-cell')
}
