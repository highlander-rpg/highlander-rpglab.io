import { cloneDeep, uniqueId } from 'lodash/fp'

import { MockObjectType, ObjectTemplateId } from "../types"
import { updateSprites } from './updateSprites'
import { rerenderCell } from './rerenderCell'
import { mockObject } from '../data/mocks/mockObject'
import { animationLayer } from '../animation'
import { gameObjectsTemplatesLookup } from '../data/templates/gameObjects'
import { clearObjSpriteLayer, getSpriteLayerOfCompound } from './sprites'

export const getObjId = (objOrId: MockObjectType | string): string | undefined => {
  return typeof objOrId === 'string' ? objOrId : objOrId?.id
}

export const getGameObject = (objId: string): MockObjectType | undefined => window.allGameObjects.get(objId)

export const isSameGameObject = (objOrId1: MockObjectType | string, objOrId2: MockObjectType | string): boolean => {
  const id1 = getObjId(objOrId1)
  const id2 = getObjId(objOrId2)

  return id1 === id2
}

export const createObject = (cellId: string, obj: Partial<MockObjectType>): MockObjectType => {
  const templateObj = cloneDeep(mockObject)
  const newObj = { ...templateObj, ...obj }
  
  const arrayOfObjectsIdsOnCell = window.currentMap.get(cellId) || []

  newObj.id = obj.id || uniqueId('')
  newObj.staysAt = cellId

  updateSprites(newObj)

  arrayOfObjectsIdsOnCell.push(newObj.id)

  window.allGameObjects.set(newObj.id, newObj)
  window.currentMap.set(cellId, arrayOfObjectsIdsOnCell)

  rerenderCell(cellId)
  
  return newObj
}

export const removeObject = (objId: string, cellId: string): void => {
  const arrayOfObjectsIdsOnCell = window.currentMap.get(cellId)
  const index = arrayOfObjectsIdsOnCell.indexOf(objId)
  if (index > -1) {
    arrayOfObjectsIdsOnCell.splice(index, 1)
  } else {
    throw new Error(`Object with ID ${objId} does not exist on cell with ID ${cellId}`)
  }

  const obj = getGameObject(objId)
  obj.animations.forEach((animationId) => animationLayer.removeAnimation(animationId))
  obj.intervals.forEach((intervalId) => window.clearInterval(intervalId))

  window.allGameObjects.delete(objId)
  window.currentMap.set(cellId, arrayOfObjectsIdsOnCell)

  rerenderCell(cellId)
}

export const updateObject = (obj: MockObjectType, oldCellId: string): void => {
  const newTargetCell = obj.staysAt
  let arrayOfObjectsIdsOnOldCell = window.currentMap.get(oldCellId) || []
  const arrayOfObjectsIdsOnNewCell = window.currentMap.get(newTargetCell) || []
  
  updateSprites(obj)

  arrayOfObjectsIdsOnOldCell = arrayOfObjectsIdsOnOldCell.filter((objId) => {
    return objId !== obj.id
  })

  if (oldCellId === newTargetCell) {
    arrayOfObjectsIdsOnOldCell.push(obj.id)
  }

  window.currentMap.set(oldCellId, arrayOfObjectsIdsOnOldCell)

  if (oldCellId !== newTargetCell) {
    arrayOfObjectsIdsOnNewCell.push(obj.id)
    window.currentMap.set(newTargetCell, arrayOfObjectsIdsOnNewCell)
    
  }

  window.allGameObjects.set(obj.id, obj)  

  if (oldCellId !== newTargetCell) {
    rerenderCell(oldCellId)
    rerenderCell(newTargetCell)
  } else {
    rerenderCell(oldCellId)
  }
}

export const getGameObjectFromTemplate = (templateId: ObjectTemplateId): MockObjectType => {
  const templatedObj = gameObjectsTemplatesLookup[templateId]
  const initialObj = cloneDeep(mockObject)
  const newObj = { ...initialObj, ...templatedObj }

  return newObj
}

export const decrementObjectCompound = (obj: MockObjectType, compoundId: ObjectTemplateId, cellId: string): void => {
  const prevValue = obj.compounds[compoundId]
  const currentValue = prevValue - 1
  obj.compounds[compoundId] = currentValue
  let isObjectRemoved = false

  if (currentValue <= 0) {
    const layerName = getSpriteLayerOfCompound(compoundId)

    let shouldBeRemoved = true

    Object.entries(obj.compounds).forEach(([key, value]) => {
      if (getSpriteLayerOfCompound(key as ObjectTemplateId) === layerName && value > 0) {
        shouldBeRemoved = false
      }
    })

    if (shouldBeRemoved) {
      clearObjSpriteLayer(obj, layerName)
    }

    if (layerName === 'basis' && shouldBeRemoved) {
      removeObject(obj.id, cellId)
      isObjectRemoved = true

      //Create objects from destructed object's compounds
      Object.entries(obj.compounds).forEach(([key, value]) => {
        if (value > 0) {
          const compoundTemplateId = key as ObjectTemplateId
          
          for (let i = 0; i < value; i++) {
            createObject(cellId, getGameObjectFromTemplate(compoundTemplateId))
          }
        }
      })
    }
  }

  if (!isObjectRemoved) {
    updateObject(obj, cellId)
  }
}

export const getFirstObjectOnCell = (cellId: string): MockObjectType | undefined => {
  const arrayOfObjectsIdsOnCell = window.currentMap.get(cellId) || []
  if (arrayOfObjectsIdsOnCell.length === 0) {
    return undefined
  }
  const firstObjId = arrayOfObjectsIdsOnCell[0]
  return getGameObject(firstObjId)
}
