import { MockObjectType, ObjectTemplateId, SpriteLayerName } from "../types"

export const convertSpriteId = (spriteId: number | string): string => {
  if (spriteId === '') {
    return spriteId
  } else if (typeof spriteId === 'string' && spriteId.includes('px')) {
    return spriteId
  } else {
    const num = Number(spriteId)
    const imagesPerRow = 64
    const withMinus = (input: number): string => input === 0 ? '0' : `-${input}`
    const spriteCoords = `${withMinus(Math.round(num % imagesPerRow) * 32)}px ${withMinus(Math.floor(num / imagesPerRow) * 32)}px`
    return spriteCoords
  }
}

export const getSpriteLayerOfCompound = (compoundTemplateId: ObjectTemplateId): SpriteLayerName | undefined => {
  const lookup: Partial<Record<ObjectTemplateId, SpriteLayerName>> = {
    redCurrantBranch: "basis",
    redCurrantDryBranch: "basis",
    redCurrantLeaf: "upperOverlay1",
    redCurrantBerry: "upperOverlay2",
  }

  return lookup[compoundTemplateId]
}

export const clearObjSpriteLayer = (obj: MockObjectType, spriteLayerName: SpriteLayerName | undefined): void => {
  if (spriteLayerName) {
    obj.spriteLayers[spriteLayerName] = ''
  }
}
