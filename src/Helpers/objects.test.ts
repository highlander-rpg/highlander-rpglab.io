import { initTestGameStorages } from '../testHelpers'
import { MockObjectType } from '../types'
import { createObject, decrementObjectCompound, getGameObject } from '.'
import { getSpriteLayerOfCompound } from './sprites'

jest.mock('./rerenderCell')

const objId = 'test-obj'
const cellId = 'r0c0'

describe('"createObject" helper', () => {
  beforeEach(() => {
    initTestGameStorages()
  })

  it('should create object with particular default params if not provided', () => {
    const obj = createObject(cellId, {})

    expect(obj.name).toBe('_mock Object')
    expect(typeof obj.id).toBe('string')
  })

  it('should create object with particular params overriden if provided', () => {
    const obj = createObject(cellId, {
      id: objId,
      spriteLayers: {
        basis: 6044,
        upperOverlay1: 4345,
        upperOverlay2: 6041,
      },
      weight: 7
    })

    expect(obj.id).toBe(objId)

    expect(obj.spriteLayers.basis).toBe(6044)
    expect(obj.spriteLayers.upperOverlay1).toBe(4345)
    expect(obj.spriteLayers.upperOverlay2).toBe(6041)

    expect(obj.spriteLayers.body).toBe(undefined)

    expect(obj.weight).toBe(7)
  })
})


describe('"decrementObjectCompound" helper', () => {
  beforeEach(() => {
    initTestGameStorages()
  })

  it('should decrease given compound by 1', () => {
    const initialObj = { compounds: { redCurrantBerry: 2 }, id: objId }
    const obj = createObject(cellId, initialObj)
    decrementObjectCompound(obj, 'redCurrantBerry', cellId)
    expect(getGameObject(objId).compounds.redCurrantBerry).toBe(1)
  })

  it('should clear sprite layer that displayed given compound type if no quantity left', () => {
    const spriteCoords = '-32px -64px'
    const initialObj: Partial<MockObjectType> = {
      compounds: {
        redCurrantBerry: 1,
      },
      id: objId,
      spriteLayers: {
        upperOverlay2: spriteCoords
      }
    }
    const obj = createObject(cellId, initialObj)
    const spriteLayerName = getSpriteLayerOfCompound('redCurrantBerry')

    expect(getGameObject(objId).compounds.redCurrantBerry).toBe(1)
    expect(getGameObject(objId).spriteLayers[spriteLayerName]).toBe(spriteCoords)

    decrementObjectCompound(obj, 'redCurrantBerry', cellId)

    expect(getGameObject(objId).compounds.redCurrantBerry).toBe(0)
    expect(getGameObject(objId).spriteLayers[spriteLayerName]).toBe('')
  })

  it('should avoid clearing sprite layer if num of compound types related to it is more than 1 and at least one has quantity greater than 0', () => {
    const spriteIdx = 42
    const initialObj: Partial<MockObjectType> = {
      compounds: {
        redCurrantBranch: 1,
        redCurrantDryBranch: 1,
      },
      id: objId,
      spriteLayers: {
        basis: spriteIdx
      }
    }
    const obj = createObject(cellId, initialObj)
    const spriteLayerName = getSpriteLayerOfCompound('redCurrantBranch')

    expect(getGameObject(objId).compounds.redCurrantBranch).toBe(1)
    expect(getGameObject(objId).compounds.redCurrantDryBranch).toBe(1)
    expect(getGameObject(objId).spriteLayers[spriteLayerName]).toBe(spriteIdx)

    decrementObjectCompound(obj, 'redCurrantBranch', cellId)

    expect(getGameObject(objId).compounds.redCurrantBranch).toBe(0)
    expect(getGameObject(objId).compounds.redCurrantDryBranch).toBe(1)
    expect(getGameObject(objId).spriteLayers[spriteLayerName]).toBe(spriteIdx)
  })

  it('should remove object if sprite basis layer is cleared, spreading its compounds left', () => {
    const numOfBerries = 3
    const spriteIdx = 42
    const initialObj: Partial<MockObjectType> = {
      compounds: {
        redCurrantBranch: 1,
        redCurrantBerry: numOfBerries,
      },
      id: objId,
      spriteLayers: {
        basis: spriteIdx
      }
    }
    const obj = createObject(cellId, initialObj)
    const spriteLayerName = getSpriteLayerOfCompound('redCurrantBranch')

    expect(getGameObject(objId).spriteLayers[spriteLayerName]).toBe(spriteIdx)
    expect(window.allGameObjects.size).toBe(1)
    expect(window.currentMap.get(cellId).length).toBe(1)

    decrementObjectCompound(obj, 'redCurrantBranch', cellId)

    expect(getGameObject(objId)).toBe(undefined)
    expect(window.allGameObjects.size).toBe(numOfBerries)
    expect(window.currentMap.get(cellId).length).toBe(numOfBerries)
  })
})
