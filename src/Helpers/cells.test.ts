/**
 * @jest-environment jsdom
 */

import {
  getRow,
  getCol,
  getDistanceBetweenCells,
  getNearestCell,
  DirectionType,
  isCellWithinMap,
  getCellsAround,
  isOrthogonalCells,
  getCellsInRectangle,
} from './cells'

describe('"getRow" helper', () => {
  const cases = [
    ['r0c-1', 0],
    ['r-1c0', -1],
    ['r0c-0', 0],
    ['r-0c0', -0],
    ['r0c4', 0],
    ['r4c4', 4],
    ['r10c4', 10],
    ['r10c0', 10],
    ['r21c4', 21],
    ['r100c4', 100],
    ['r234c4', 234],
    ['r1000c4', 1000],
  ]
  test.each(cases)(
    'given %p, returns %p',
    (cellId, expectedResult) => {
      const result = getRow(cellId as string)
      expect(result).toBe(expectedResult)
    }
  )
})

describe('"getCol" helper', () => {
  test.each([
    ['r0c-1', -1],
    ['r-1c0', 0],
    ['r0c-0', -0],
    ['r-0c0', 0],
    ['r0c0', 0],
    ['r1c1', 1],
    ['r0c10', 10],
    ['r0c12', 12],
    ['r0c30', 30],
    ['r0c120', 120],
    ['r0c1000', 1000],
    ['r1c8', 8],
    ['r3c203', 203],
  ])(
    'given %p, returns %p',
    (cellId, expectedResult) => {
      const result = getCol(cellId as string)
      expect(result).toBe(expectedResult)
    }
  )
})

describe('"getDistanceBetweenCells" helper', () => {
  const cases = [
    ['r0c0', 'r0c0', 0],
    ['r0c0', 'r4c0', 4],
    ['r0c4', 'r0c0', 4],
    ['r4c4', 'r4c4', 0],
    ['r4c4', 'r5c4', 1],
    ['r4c4', 'r5c3', 1],
    ['r4c4', 'r4c6', 2],
    ['r2c6', 'r4c4', 2],
    ['r7c5', 'r4c4', 3],
    ['r6c1', 'r4c4', 3],
    ['r1c4', 'r4c4', 3],
    ['r8c8', 'r4c4', 4],
    ['r15c10', 'r4c4', 11],
    ['r18c18', 'r4c4', 14],
    ['r4c11', 'r4c4', 7],
    ['r1c18', 'r4c4', 14],
    ['r17c1', 'r4c4', 13],
  ]
  test.each(cases)(
    'given %p and %p, returns %p',
    (cellId1, cellId2, expectedResult) => {
      const result = getDistanceBetweenCells(cellId1 as string, cellId2 as string)
      expect(result).toBe(expectedResult)
    }
  )
})

describe('"isCellWithinMap" helper', () => {
  const cases = [
    [null, 0],
    ['r-1c0', 0],
    ['r0c-1', 0],
    ['r-1c-1', 0],
    ['r10c0', 0],
    ['r0c10', 0],
    ['r10c10', 0],
    ['r11c0', 0],
    ['r0c11', 0],
    ['r11c11', 0],

    ['r-0c0', 1],
    ['r0c-0', 1],
    ['r-0c-0', 1],
    ['r0c0', 1],
    ['r1c0', 1],
    ['r0c1', 1],
    ['r1c1', 1],
    ['r9c0', 1],
    ['r0c9', 1],
    ['r9c9', 1],
  ]
  test.each(cases)(
    '%#) given %p, returns %p',
    (cellId, expectedResult) => {
      const result = isCellWithinMap(cellId as string | null, 10, 10)
      expect(result).toBe(Boolean(expectedResult))
    }
  )
})

describe('"getNearestCell" helper', () => {
  const cases = [
    ['r0c0', 'top', null],
    ['r0c0', 'topRight', null],
    ['r0c0', 'right', 'r0c1'],
    ['r0c0', 'bottomRight', 'r1c1'],
    ['r0c0', 'bottom', 'r1c0'],
    ['r0c0', 'bottomLeft', null],
    ['r0c0', 'left', null],
    ['r0c0', 'topLeft', null],

    ['r1c1', 'top', 'r0c1'],
    ['r1c1', 'topRight', 'r0c2'],
    ['r1c1', 'right', 'r1c2'],
    ['r1c1', 'bottomRight', 'r2c2'],
    ['r1c1', 'bottom', 'r2c1'],
    ['r1c1', 'bottomLeft', 'r2c0'],
    ['r1c1', 'left', 'r1c0'],
    ['r1c1', 'topLeft', 'r0c0'],

    ['r9c9', 'top', 'r8c9'],
    ['r9c9', 'topRight', null],
    ['r9c9', 'right', null],
    ['r9c9', 'bottomRight', null],
    ['r9c9', 'bottom', null],
    ['r9c9', 'bottomLeft', null],
    ['r9c9', 'left', 'r9c8'],
    ['r9c9', 'topLeft', 'r8c8'],
  ]
  test.each(cases)(
    '%#) given %p and %p, returns %p',
    (startCellId, direction, expectedResult) => {
      const result = getNearestCell(startCellId, direction as DirectionType)
      expect(result).toBe(expectedResult)
    }
  )
})

describe('"getCellsAround" helper', () => {
  const cases = [
    ['r0c0', 1, ['r0c1', 'r1c0', 'r1c1']],
    ['r0c1', 1, ['r0c2', 'r1c2', 'r1c1', 'r1c0', 'r0c0']],
    ['r1c1', 1, ['r0c1', 'r0c2', 'r1c2', 'r2c2', 'r2c1', 'r2c0', 'r1c0', 'r0c0']],
    ['r3c5', 2, ['r2c5', 'r2c6', 'r3c6', 'r4c6', 'r4c5', 'r4c4', 'r3c4', 'r2c4', 'r1c5', 'r1c6', 'r1c7', 'r2c7', 'r3c7', 'r4c7', 'r5c7', 'r5c6', 'r5c5', 'r5c4', 'r5c3', 'r4c3', 'r3c3', 'r2c3', 'r1c3', 'r1c4']],
  ]
  test.each(cases)(
    '%#) given %p and %p, returns %p',
    (startCellId, range, cellsAround) => {
      const centerCellId = startCellId as string
      const resultArr = getCellsAround(centerCellId, range as number)
      const expectedCells = cellsAround as string[]

      expect(resultArr.length).toBe(expectedCells.length)
      expect(expectedCells.includes(centerCellId)).toBe(false)

      expect(resultArr).toEqual(expect.arrayContaining(expectedCells))
    }
  )
})

describe('"isOrthogonalCells" helper', () => {
  const cases = [
    ['r0c0', 'r0c0', 1],
    ['r0c0', 'r0c1', 1],
    ['r0c1', 'r0c0', 1],
    ['r1c0', 'r0c0', 1],
    ['r0c0', 'r1c0', 1],
    ['r1c1', 'r0c0', 0],
    ['r0c0', 'r1c1', 0],
  ]
  test.each(cases)(
    '%#) given %p and %p, returns %p',
    (startCellId, targetCellId, expectedResult) => {
      const result = isOrthogonalCells(startCellId as string, targetCellId as string)
      expect(result).toBe(Boolean(expectedResult))
    }
  )
})

describe('"getCellsInRectangle" helper', () => {
  const cases = [
    [0, 0, 'r0c0', []],
    [1, 0, 'r0c0', []],
    [0, 1, 'r0c0', []],
    [1, 1, 'r0c0', ['r0c0']],
    [2, 1, 'r0c0', ['r0c0', 'r0c1']],
    [1, 2, 'r0c0', ['r0c0', 'r1c0']],
    [2, 2, 'r0c0', ['r0c0', 'r1c0', 'r0c1', 'r1c1']],
    [0, 0, 'r1c1', []],
    [1, 0, 'r1c1', []],
    [0, 1, 'r1c1', []],
    [1, 1, 'r1c1', ['r1c1']],
    [2, 1, 'r1c1', ['r1c1', 'r1c2']],
    [1, 2, 'r1c1', ['r1c1', 'r2c1']],
    [2, 2, 'r1c1', ['r1c1', 'r2c1', 'r1c2', 'r2c2']],
    [3, 2, 'r1c1', ['r1c1', 'r2c1', 'r1c2', 'r1c3', 'r2c2', 'r2c3']],
    [2, 2, 'r9c9', ['r9c9']],//Bottom right of the test map
  ]
  test.each(cases)(
    '%#) given %p, %p and %p, returns %p',
    (x, y, topLeftCellId, expectedResult) => {
      const result = getCellsInRectangle(x as number, y as number, topLeftCellId as string)
      expect(result.sort()).toEqual((expectedResult as string[]).sort())
    }
  )
})
