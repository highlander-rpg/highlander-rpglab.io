import 'jest-localstorage-mock'

import { customHotkeysKey, getCustomHotkeyActionIdByLetter, getCustomHotkeyLetterByActionId, HotkeysData, setCustomHotkey } from './hotkeys'

const getLocalStorageData = (): HotkeysData | undefined => {
  const data = localStorage.__STORE__[customHotkeysKey]
  if (data) {
    return JSON.parse(data)
  } else {
    return undefined
  }
}

describe('setCustomHotkey helper', () => {
  beforeEach(() => {
    localStorage.clear()
  })

  it('should handle no localStorage entry intitiated', () => {
    expect(getLocalStorageData()).toBe(undefined)
    
    setCustomHotkey('T', 'teleport-here')

    expect(getLocalStorageData()).toStrictEqual([
      ['T', 'teleport-here']
    ])
  })

  it('should handle no such hotkey assigned yet', () => {
    setCustomHotkey('T', 'teleport-here')
    setCustomHotkey('F', 'throw-fireball')

    expect(getLocalStorageData()).toStrictEqual([
      ['T', 'teleport-here'],
      ['F', 'throw-fireball']
    ])
  })

  it('should handle 1 such hotkey assigned already', () => {
    setCustomHotkey('T', 'teleport-here')
    setCustomHotkey('R', 'teleport-here')

    expect(getLocalStorageData()).toStrictEqual([
      ['R', 'teleport-here']
    ])

    setCustomHotkey('F', 'throw-fireball')

    expect(getLocalStorageData()).toStrictEqual([
      ['R', 'teleport-here'],
      ['F', 'throw-fireball']
    ])

    setCustomHotkey('H', 'throw-fireball')

    expect(getLocalStorageData()).toStrictEqual([
      ['R', 'teleport-here'],
      ['H', 'throw-fireball']
    ])
  })
  
  it('should handle 2 such hotkeys assigned already', () => {
    setCustomHotkey('T', 'teleport-here')
    setCustomHotkey('F', 'throw-fireball')

    setCustomHotkey('R', 'teleport-here')
    setCustomHotkey('H', 'throw-fireball')

    expect(getLocalStorageData()).toStrictEqual([
      ['R', 'teleport-here'],
      ['H', 'throw-fireball']
    ])
  })

  it('should replace hotkeys by letters', () => {
    setCustomHotkey('T', 'teleport-here')
    setCustomHotkey('T', 'throw-fireball')

    expect(getLocalStorageData()).toStrictEqual([
      ['T', 'throw-fireball']
    ])
  })
})

describe('getCustomHotkeyActionIdByLetter helper', () => {
  beforeEach(() => {
    localStorage.clear()
  })

  it('should handle no localStorage entry initiated', () => {
    expect(getCustomHotkeyActionIdByLetter('M')).toBe(undefined)
  })
  
  it('should handle no such hotkey assigned yet', () => {
    setCustomHotkey('T', 'teleport-here')
    expect(getCustomHotkeyActionIdByLetter('M')).toBe(undefined)
  })

  it('should fetch right actionId', () => {
    setCustomHotkey('T', 'teleport-here')
    expect(getCustomHotkeyActionIdByLetter('T')).toBe('teleport-here')
  })
})

describe('getCustomHotkeyLetterByActionId helper', () => {
  beforeEach(() => {
    localStorage.clear()
  })

  it('should handle no localStorage entry initiated', () => {
    expect(getCustomHotkeyLetterByActionId('teleport-here')).toBe(undefined)
  })

  it('should handle no such hotkey assigned yet', () => {
    setCustomHotkey('F', 'throw-fireball')
    expect(getCustomHotkeyLetterByActionId('teleport-here')).toBe(undefined)
  })

  it('should fetch right actionId', () => {
    setCustomHotkey('T', 'teleport-here')
    expect(getCustomHotkeyLetterByActionId('teleport-here')).toBe('T')
  })
})
