import { MockObjectType } from '../types'
import {
  isInReach,
} from './general'

describe('"isInReach" helper', () => {
  const generateActor = (x: number, telekinesisLevel: number) => {
    return {
      staysAt: 'r2c' + x,
      telekinesisLevel,
    }
  }
  const generateSubject = (x: number) => {
    return {
      staysAt: 'r2c' + x,
    }
  }

  const cases = [
    [generateActor(0, 0), generateSubject(0), 1],
    [generateActor(1, 0), generateSubject(0), 1],
    [generateActor(2, 0), generateSubject(0), 0],
    [generateActor(0, 1), generateSubject(0), 1],
    [generateActor(1, 1), generateSubject(0), 1],
    [generateActor(2, 1), generateSubject(0), 1],
    [generateActor(0, 0), generateSubject(2), 0],
    [generateActor(0, 1), generateSubject(2), 1],
  ]
  test.each(cases)(
    'given %p and %p, returns %p',
    (actor, subject, expectedResult) => {
      const result = isInReach(actor as MockObjectType, subject as MockObjectType)
      expect(result).toBe(Boolean(expectedResult))
    }
  )
})
