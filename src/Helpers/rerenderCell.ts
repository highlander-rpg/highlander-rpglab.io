import urlToSprite from '../images/sprites.png'
import { SpriteType } from '../types'

export const rerenderCell = (cellId: string): void => {
  let arrayOfSprites: SpriteType[] = []
  window.currentMap.get(cellId).forEach(objId => {
    const obj = window.allGameObjects.get(objId)
    arrayOfSprites = [...arrayOfSprites, ...obj.sprites]
  })
  arrayOfSprites = arrayOfSprites.reverse()
  const img = `url("${urlToSprite}")`
  document.getElementById(cellId).style.background = `${img} ${arrayOfSprites.join(', ' + img + ' ')}`
}
