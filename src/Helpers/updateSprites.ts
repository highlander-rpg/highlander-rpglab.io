import { MockObjectType, SpriteLayerName } from '../types'
import { convertSpriteId } from './sprites'

export const updateSprites = (obj: MockObjectType): void => {
  obj.sprites = Object.keys(obj.spriteLayers).map((spriteLayerName) => {
    const val = obj.spriteLayers[spriteLayerName as SpriteLayerName]
    return convertSpriteId(val)
  }).filter(Boolean)
}
