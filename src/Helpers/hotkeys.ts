/*
[
  ['T', 'teleport-here'],
  ['F', 'throw-fireball'],
]
*/

import { ActionId } from '../types'

export const customHotkeysKey = 'hl-custom-hotkeys'

export const allowedLetterKeysForOverride = ['B', 'C', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'T', 'U', 'V', 'X', 'Y', 'Z'] as const

export type Letter = typeof allowedLetterKeysForOverride[number]
export type HotkeysData = [Letter, ActionId][]

export const setCustomHotkey = (letter: Letter, actionId: ActionId): void => {
  const data: HotkeysData = JSON.parse(localStorage.getItem(customHotkeysKey)) || []
  const newData: HotkeysData = []

  data.forEach((entity) => {
    if (!entity.includes(letter) && !entity.includes(actionId)) {
      newData.push(entity)
    }
  })

  newData.push([letter, actionId])

  localStorage.setItem(customHotkeysKey, JSON.stringify(newData))
}

export const getCustomHotkeyActionIdByLetter = (letter: Letter): ActionId | undefined => {
  const data: HotkeysData = JSON.parse(localStorage.getItem(customHotkeysKey)) || []
  const hotkey = data.find((entity) => entity[0] === letter)

  return hotkey ? hotkey[1] : undefined
}

export const getCustomHotkeyLetterByActionId = (actionId: ActionId): Letter | undefined => {
  const data: HotkeysData = JSON.parse(localStorage.getItem(customHotkeysKey)) || []
  const hotkey = data.find((entity) => entity[1] === actionId)

  return hotkey ? hotkey[0] : undefined
}
