import { inRange } from 'lodash/fp'

export const getRow = (cellId: string): number => {
  return Number(cellId.split('c')[0].split('r')[1])
}

export const getCol = (cellId: string): number => {
  return Number(cellId.split('c')[1])
}

export const getDistanceBetweenCells = (cellId1: string, cellId2: string): number => {
  return Math.max(Math.abs(getRow(cellId1) - getRow(cellId2)), Math.abs(getCol(cellId1) - getCol(cellId2)))
}

export const getNumOfMapRows = (): number => {
  //if there are no DOM elements - we are in unit tests - return 10
  return document.getElementById('current-map-table')?.querySelectorAll('tr')?.length || 10
}

export const getNumOfMapCols = (): number => {
  //if there are no DOM elements - we are in unit tests - return 10
  return document.getElementById('current-map-table')?.querySelector('tr')?.querySelectorAll('td')?.length || 10
}

export const isCellWithinMap = (cellId: string | null, numOfMapRows: number, numOfMapCols: number): boolean => {
  if (!cellId) {
    return false
  }

  const r = getRow(cellId)
  const c = getCol(cellId)

  return inRange(0, numOfMapRows, r) && inRange(0, numOfMapCols, c)
}

export type DirectionType =
  'top' |
  'topRight' |
  'right' |
  'bottomRight' |
  'bottom' |
  'bottomLeft' |
  'left' |
  'topLeft'

export const getNearestCell = (startCellId: string, direction: DirectionType): string | null => {
  let targetCellId = null

  switch (direction) {
    case 'top':
      targetCellId = `r${getRow(startCellId) - 1}c${getCol(startCellId)}`
      break
    case 'topRight':
      targetCellId = `r${getRow(startCellId) - 1}c${getCol(startCellId) + 1}`
      break
    case 'right':
      targetCellId = `r${getRow(startCellId)}c${getCol(startCellId) + 1}`
      break
    case 'bottomRight':
      targetCellId = `r${getRow(startCellId) + 1}c${getCol(startCellId) + 1}`
      break
    case 'bottom':
      targetCellId = `r${getRow(startCellId) + 1}c${getCol(startCellId)}`
      break
    case 'bottomLeft':
      targetCellId = `r${getRow(startCellId) + 1}c${getCol(startCellId) - 1}`
      break
    case 'left':
      targetCellId = `r${getRow(startCellId)}c${getCol(startCellId) - 1}`
      break
    case 'topLeft':
      targetCellId = `r${getRow(startCellId) - 1}c${getCol(startCellId) - 1}`
      break
  }

  return isCellWithinMap(targetCellId, getNumOfMapRows(), getNumOfMapCols()) ? targetCellId : null
}

export const getCellsAround = (centerCellId: string, range: number): string[] => {
  const arr = []

  const r = getRow(centerCellId)
  const c = getCol(centerCellId)

  for (let i = -range; i <= range; i++) {
    for (let j = -range; j <= range; j++) {
      const cellId = `r${r-i}c${c-j}`
      
      if (isCellWithinMap(cellId, getNumOfMapRows(), getNumOfMapCols())) {
        arr.push(cellId)
      }
    }
  }

  //remove centerCellId from the result array
  arr.splice(arr.indexOf(centerCellId), 1)

  return arr
}

export type GetCellsInRectangleConstructor = (x: number, y: number, topLeftCellId: string) => string[]
export const getCellsInRectangle: GetCellsInRectangleConstructor = (x: number, y: number, topLeftCellId: string): string[] => {
  const arr: string[] = []

  if (x === 0 || y === 0) {
    return arr
  }

  const r = getRow(topLeftCellId)
  const c = getCol(topLeftCellId)

  for (let i = 0; i <= y - 1; i++) {
    for (let j = 0; j <= x - 1; j++) {
      const cellId = `r${r + i}c${c + j}`
      
      if (isCellWithinMap(cellId, getNumOfMapRows(), getNumOfMapCols())) {
        arr.push(cellId)
      }
    }
  }

  return arr
}

export const isCellFullObstacle = (cellId: string): boolean => {
  //Use Wave algorith https://www.youtube.com/watch?v=4B9jZ9UOu50 for pathfinding when partial obstacles will be enabled
  const objectsIds = window.currentMap.get(cellId)
  const arrOfObjectsOnCell = objectsIds.map((id) => window.allGameObjects.get(id))

  return arrOfObjectsOnCell.some((obj) => obj.isFullObstacle)
}

export const isOrthogonalCells = (startCellId: string, targetCellId: string): boolean => {
  const r1 = getRow(startCellId)
  const r2 = getRow(targetCellId)
  if (r1 === r2) {
    return true
  }

  const c1 = getCol(startCellId)
  const c2 = getCol(targetCellId)
  if (c1 === c2) {
    return true
  }

  return false
}
