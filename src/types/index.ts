//Cypress has another file at cypress/types/index.ts

import { AdvancedFollower1Config, BasicPatrol1Config } from '../AI'
import { AnimationId } from '../animation'
import { GetCellsInRectangleConstructor } from '../helpers'
import { AddBushConstructor, AddFireConstructor } from '../initiators/gameObjects'

declare global {
  interface Window {
    playerId: string
    currentMap: Map<string, string[]>
    allGameObjects: Map<string, MockObjectType>
    hl: {
      addBush: AddBushConstructor,
      addFire: AddFireConstructor,
      getCellsInRectangle: GetCellsInRectangleConstructor,
    }
  }
}

//polyfill for es6 Map
interface Map<K, V> {
  clear(): void;
  delete(key: K): boolean;
  entries(): IterableIterator<[K, V]>;
  forEach(callbackfn: (value: V, index: K, map: Map<K, V>) => void, thisArg?: any): void;
  get(key: K): V;
  has(key: K): boolean;
  keys(): IterableIterator<K>;
  set(key: K, value?: V): Map<K, V>;
  size: number;
  values(): IterableIterator<V>;
  [Symbol.iterator](): IterableIterator<[K, V]>;
  [Symbol.toStringTag]: string;
}
interface MapConstructor {
  new <K, V>(): Map<K, V>;
  new <K, V>(iterable: Iterable<[K, V]>): Map<K, V>;
  prototype: Map<any, any>;
}
declare var Map: MapConstructor;

export type ObjectTemplateId =
  | 'redCurrantBerry'
  | 'redCurrantDryBerry'
  | 'redCurrantLeaf'
  | 'redCurrantDryLeaf'
  | 'redCurrantBranch'
  | 'redCurrantDryBranch'

type ActorMemory = {
  cellsOfInterest: string[],
  lastVisitedPatrolPoint: string,
}

//From https://en.wikipedia.org/wiki/Maslow%27s_hierarchy_of_needs
type NeedPriorityLevel = 
  1 | //Physiological needs: Air, water, Heat, Light, Urination, Food, Excretion, Clothes, Hygiene, Sleep, Shelter, Sexual intercourse, No pain
  2 | //Safety needs: Health, Personal security, Emotional security, Financial security
  3 | //Love and social belonging needs: Family, Friendship, Intimacy, Trust, Acceptance, Receiving and giving love and affection
  4 | //Esteem needs: достижение успеха, одобрение, признание;
  5 | //Need for cognition: знать, уметь, исследовать;
  6 | //Aesthetic needs: гармония, порядок, красота;
  7 | //Self-actualization: Partner acquisition, Parenting, Utilizing and developing talents and abilities, Pursuing goals, реализация своих целей, способностей, развитие собственной личности.
  8   //Transcendence needs: One finds the fullest realization in giving oneself to something beyond oneself - for example, in altruism or spirituality. The desire to reach the infinite.

type AIModuleConfig = {}
  | AdvancedFollower1Config
  | BasicPatrol1Config

export type AIModule = {
  name: string,
  desc: string,
  script: (actor: MockObjectType, config: AIModuleConfig) => void,
  priority: NeedPriorityLevel,
  config: AIModuleConfig,
}

type ElectronicComponents = {
  processor: {
    name: string,
    isOn: boolean,
  },
  AIModulesBoard: {
    name: string,
    numOfSlots: number,
    modules: AIModule[],
  },
}

export type SpriteType = string | number

export type SpriteLayerName =
  | 'basis'
  | 'wings'
  | 'cloak'
  | 'body'
  | 'beard'
  | 'hair'
  | 'helmet'
  | 'pants'
  | 'jacket'
  | 'boots'
  | 'leftHand'
  | 'rightHand'
  | 'upperOverlay1'
  | 'upperOverlay2'

export type MockObjectType = {
  _spriteClosed: string,
  _spriteOpen: string,
  canFly: boolean,
  id: string,
  isBrokenDown: boolean,
  isContainer: boolean,
  isDoor: boolean,
  isFullObstacle: boolean,
  isOpen: boolean,
  isLocked: boolean,
  keyToUnlock: string,
  isPortalToAnotherMap: boolean,
  isWater: boolean,
  isSealedWithRunicMagic: boolean,
  isSleeping: boolean,
  name: string,
  obstacleDirectionBottom: boolean,
  obstacleDirectionLeft: boolean,
  obstacleDirectionRight: boolean,
  obstacleDirectionTop: boolean,
  spriteLayers: Partial<Record<SpriteLayerName, SpriteType>>
  sprites: SpriteType[],//Is filled at updateSprites()
  staysAt: string,
  telekinesisLevel: number,
  inventory: string[],
  inventoryCapacity: number,
  weight: number, //kg
  hitPoints: number,
  fireResistance: number,
  animations: AnimationId[],
  type: string,
  intervals: number[],
  compounds: Partial<Record<ObjectTemplateId, number>> | null,
  length: number, //m
  diameter: number, //m
  memory: Partial<ActorMemory>
  components: Partial<ElectronicComponents>
}

export type MockObjectProps = keyof MockObjectType

export type ActionId =
  'debug' |
  'modify' |
  'remove' |
  'teleport-here' |
  'step-to-cell' |
  'go-to' |
  'jump-to-cell' |
  'examine' |
  'take' |
  'collect-one' |
  'collect-some' |
  'collect-all' |
  'open' |
  'close' |
  'lock' |
  'unlock' |
  'break' |
  'repair' |
  'seal-runic' |
  'unseal-runic' |
  'train-telekinesis' |
  'utter-incantation' |
  'possess-body' |
  'throw-stone' |
  'throw-fireball' |
  'keep-burning' |
  'switch-on' |
  'switch-off'

export type ActionType = {
  content: string,
  id: ActionId,
  subjectId: string,
  callback: Function,
}

export type ActionConstructor = (actor: MockObjectType, subject: MockObjectType, cellId: string) => ActionType | ActionType[] | false

export type MinigameId =
  | 'modify_object_minigame'
  | 'change_config_minigame'

export type MinigameParameters = {
  devUrl: string
  prodUrl: string
}
