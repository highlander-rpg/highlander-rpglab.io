import { cellWidth, cellHeight } from '../config'
import { getCol, getRow } from '../helpers'
import { animationLayer } from './animationLayer'
import { getRandomNumber } from '../utils'

export const drawSprite = (placementX: number, placementY: number, spriteX: number, spriteY: number): void => {
  const spriteSheet = document.getElementById('sprite-source') as CanvasImageSource
  animationLayer.getContext().drawImage(spriteSheet, spriteX, spriteY, cellWidth, cellHeight, placementX, placementY, cellWidth, cellHeight)
}

export const getRandomPointWithinCell = (cellId: string): { x: number, y: number } => {
  const w = cellWidth / 2
  const h = cellHeight / 2

  return { x: getCol(cellId) * cellWidth + getRandomNumber(-w,  w), y: getRow(cellId) * cellHeight + getRandomNumber(-h, h) }
}
