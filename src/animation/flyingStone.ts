import { uniqueId } from 'lodash/fp'
import { getCol, getDistanceBetweenCells, getRow } from '../helpers/cells'
import { cellWidth, cellHeight } from '../config'
import { animationLayer } from './animationLayer'

export const flyingStone = (startCellId: string, targetCellId: string): void => {
  const cv = animationLayer.getContext()

  const startPoint = { x: getCol(startCellId) * cellWidth + cellWidth / 2, y: getRow(startCellId) * cellHeight + cellHeight / 2 }
  const endPoint = { x: getCol(targetCellId) * cellWidth + cellWidth / 2, y: getRow(targetCellId) * cellHeight + cellHeight / 2 }

  const speed = 50

  let x = startPoint.x
  let y = startPoint.y

  const animationId = uniqueId('')

  const step = (): void => {
    const currentCellId = `r${Math.floor(y / cellHeight)}c${Math.floor(x / cellWidth)}`

    if (getDistanceBetweenCells(startCellId, currentCellId) < getDistanceBetweenCells(startCellId, targetCellId)) {
      const deltaX = endPoint.x - startPoint.x
      const deltaY = endPoint.y - startPoint.y
      const angle = Math.atan2(deltaY, deltaX)
      x += speed * Math.cos(angle)//Using cos
      y += speed * Math.sin(angle)//or sin

      cv.fillStyle = 'grey'
      cv.fillRect(x, y, 5, 5)
    } else {
      animationLayer.removeAnimation(animationId)
    }
  }

  animationLayer.addAnimation(animationId, step)
}
