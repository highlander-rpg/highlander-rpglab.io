import { uniqueId, inRange } from 'lodash/fp'
import { getDistanceBetweenCells } from '../helpers'
import { cellWidth, cellHeight } from '../config'
import { animationLayer } from './animationLayer'
import { drawSprite, getRandomPointWithinCell } from './helpers'

export const fireball = (startCellId: string, targetCellId: string, callback?: Function): void => {
  const cv = animationLayer.getContext()

  const startPoint = getRandomPointWithinCell(startCellId)
  const endPoint = getRandomPointWithinCell(targetCellId)

  const speed = 50

  let x = startPoint.x
  let y = startPoint.y

  const animationId = uniqueId('')

  const fireballFlightSprites = [[1568, 768], [1600, 768], [1632, 768]]
  const fireballBlastSprites = [[1216, 736], [1248, 736], [1280, 736], [1312, 736]]

  let currentFlyingSpriteIdx = 0
  let currentBlastingSpriteIdx = 0

  const step = (): void => {
    const currentCellId = `r${Math.floor(y / cellHeight)}c${Math.floor(x / cellWidth)}`

    const deltaX = endPoint.x - startPoint.x
    const deltaY = endPoint.y - startPoint.y
    const angle = Math.atan2(deltaY, deltaX)

    //When thrown to bottom right usually flies behind the target
    const fix = inRange(-0.68, 1.54, angle) ? 1 : 0

    if (getDistanceBetweenCells(startCellId, currentCellId) < getDistanceBetweenCells(startCellId, targetCellId) - fix) {
      //Flying to target
      x += speed * Math.cos(angle)//Using cos
      y += speed * Math.sin(angle)//or sin

      const arrOfSprites = fireballFlightSprites
      currentFlyingSpriteIdx = currentFlyingSpriteIdx >= arrOfSprites.length ? 0 : currentFlyingSpriteIdx
      const spriteCoords = arrOfSprites[currentFlyingSpriteIdx]
      drawSprite(x, y, spriteCoords[0], spriteCoords[1])
      currentFlyingSpriteIdx++
    } else {
      //Met target and blast
      if (currentBlastingSpriteIdx < fireballBlastSprites.length) {
        const spriteCoords = fireballBlastSprites[currentBlastingSpriteIdx]
        drawSprite(x, y, spriteCoords[0], spriteCoords[1])
        currentBlastingSpriteIdx++
      } else {
        animationLayer.removeAnimation(animationId)

        if (callback) {
          callback()
        }
      }
    }
  }

  animationLayer.addAnimation(animationId, step)
}
