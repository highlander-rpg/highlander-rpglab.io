import { cellWidth, cellHeight, mapDefaultWidth, mapDefaultHeight, animationUpdateRate } from '../config'
import urlToSprite from '../images/sprites.png'

type AnimationFunction = Function

const canvasWidth = mapDefaultWidth * cellWidth
const canvasHeight = mapDefaultHeight * cellHeight

const animations = new Map()

export const animationLayer = {
  init: (): void => {
    const canvasEl = document.createElement('canvas')
    canvasEl.id = 'animation-canvas'
    canvasEl.classList.add('animation-canvas')
    canvasEl.setAttribute('data-testid', 'animation-layer')
    canvasEl.width = canvasWidth
    canvasEl.height = canvasHeight
    const bodyEl = document.querySelector('body')
    bodyEl.insertAdjacentElement('beforeend', canvasEl)

    const cv = (document.getElementById('animation-canvas') as HTMLCanvasElement).getContext('2d')

    const spriteEl = document.createElement('img')
    spriteEl.id = 'sprite-source'
    spriteEl.classList.add('sprite-source')
    spriteEl.setAttribute('data-testid', 'sprite-source')
    spriteEl.src = urlToSprite
    bodyEl.insertAdjacentElement('beforeend', spriteEl)

    window.setInterval(() => {
      if (animations.size) {
        cv.clearRect(0, 0, canvasWidth, canvasHeight)

        animations.forEach((animationFunction) => {
          animationFunction()
        })
      } else {
        cv.clearRect(0, 0, canvasWidth, canvasHeight)
      }
    }, animationUpdateRate)
  },

  getContext: (): CanvasRenderingContext2D => {
    return (document.getElementById('animation-canvas') as HTMLCanvasElement).getContext('2d')
  },

  addAnimation: (id: string, animationCallback: AnimationFunction): void => {
    animations.set(id, animationCallback)
  },

  removeAnimation: (id: string): void => {
    animations.delete(id)
  },
}
