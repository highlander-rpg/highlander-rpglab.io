import { uniqueId } from 'lodash/fp'
import { animationLayer } from './animationLayer'
import { AnimationId } from './types'
import { getCol, getGameObject, getRow } from '../helpers'
import { animationUpdateRate, cellHeight, cellWidth } from '../config'

export const processBar = (actorInitialCellId: string, actorId: string, duration: number, callback: Function): void => {
  const cv = animationLayer.getContext()

  const rectHeight = 5
  const rectMaxWidth = cellWidth
  let rectCurrentWidth = rectMaxWidth
  const startPoint = {
    x: getCol(actorInitialCellId) * cellWidth,
    y: getRow(actorInitialCellId) * cellHeight + (cellHeight - rectHeight)
  }

  const successTimer = setTimeout(() => {
    animationLayer.removeAnimation(animationId)
    callback()
  }, duration)

  const checkEachStep = (): boolean => getGameObject(actorId).staysAt === actorInitialCellId

  const step = (): void => {
    if (checkEachStep()) {
      cv.fillStyle = 'rgba(0, 225, 0, 0.5)'
      cv.fillRect(startPoint.x, startPoint.y, rectCurrentWidth, rectHeight)
      rectCurrentWidth = rectCurrentWidth - rectMaxWidth / (duration / animationUpdateRate)
    } else {
      animationLayer.removeAnimation(animationId)
      clearTimeout(successTimer)
    }
  }

  const animationId: AnimationId = uniqueId('')
  animationLayer.addAnimation(animationId, step)
}
