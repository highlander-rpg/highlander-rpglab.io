import { uniqueId } from 'lodash/fp'
import { animationLayer } from './animationLayer'
import { AnimationId } from './types'
import { drawSprite, getRandomPointWithinCell } from './helpers'

export const fire = (cellId: string): AnimationId => {
  const cv = animationLayer.getContext()

  const { x, y } = getRandomPointWithinCell(cellId)

  const animationId = uniqueId('')
  const sprites = [[1568, 768], [1600, 768], [1632, 768]]
  let currentFireSpriteIdx = 0

  const step = (): void => {
    currentFireSpriteIdx = currentFireSpriteIdx >= sprites.length ? 0 : currentFireSpriteIdx
    const spriteCoords = sprites[currentFireSpriteIdx]
    drawSprite(x, y, spriteCoords[0], spriteCoords[1])
    currentFireSpriteIdx++
  }

  animationLayer.addAnimation(animationId, step)

  return animationId// Remove this animation by ID somewhere in the code
}
