import { MockObjectType } from '../../../types'
import { getRandomNumber } from '../../../utils'

export const redCurrantBerry = (): Partial<MockObjectType> => {
  return {
    name: 'red currant berry',
    weight: getRandomNumber(0.002, 0.003),
    spriteLayers: {
      basis: 6047,
    },
    hitPoints: 1,
    fireResistance: 5,
    length: 0.01,
    diameter: 0.01,
  }
}
