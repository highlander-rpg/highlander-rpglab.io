import { MockObjectType } from '../../../types'
import { getRandomNumber } from '../../../utils'

export const redCurrantDryLeaf = (): Partial<MockObjectType> => {
  return {
    name: 'red currant dry leaf',
    weight: getRandomNumber(0.001, 0.002),
    spriteLayers: {
      basis: 6050,
    },
    hitPoints: 1,
    fireResistance: 0,
    length: 0.1,
    diameter: 0.005,
  }
}
