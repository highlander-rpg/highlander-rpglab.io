import { MockObjectType } from '../../../types'
import { getRandomNumber } from '../../../utils'

export const redCurrantDryBranch = (): Partial<MockObjectType> => {
  return {
    name: 'dry red currant branch',
    weight: getRandomNumber(0.0005, 0.001),
    spriteLayers: {
      basis: 6049,
    },
    hitPoints: 1,
    fireResistance: 1,
    length: getRandomNumber(0.3, 1),
    diameter: getRandomNumber(1, 2, true),
  }
}
