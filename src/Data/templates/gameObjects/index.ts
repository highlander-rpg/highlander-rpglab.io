import { MockObjectType, ObjectTemplateId } from '../../../types'
import { redCurrantBerry } from './redCurrantBerry'
import { redCurrantBranch } from './redCurrantBranch'
import { redCurrantDryBerry } from './redCurrantDryBerry'
import { redCurrantDryBranch } from './redCurrantDryBranch'
import { redCurrantDryLeaf } from './redCurrantDryLeaf'
import { redCurrantLeaf } from './redCurrantLeaf'

export const gameObjectsTemplatesLookup: Record<ObjectTemplateId, Partial<MockObjectType>> = {
  redCurrantBerry: redCurrantBerry(),
  redCurrantDryBerry: redCurrantDryBerry(),
  redCurrantBranch: redCurrantBranch(),
  redCurrantDryBranch: redCurrantDryBranch(),
  redCurrantLeaf: redCurrantLeaf(),
  redCurrantDryLeaf: redCurrantDryLeaf(),
}