import { MockObjectType } from '../../../types'
import { getRandomNumber } from '../../../utils'

export const redCurrantLeaf = (): Partial<MockObjectType> => {
  return {
    name: 'red currant leaf',
    weight: getRandomNumber(0.002, 0.003),
    spriteLayers: {
      basis: 6039,
    },
    hitPoints: 1,
    fireResistance: 2,
    length: 0.1,
    diameter: 0.005,
  }
}
