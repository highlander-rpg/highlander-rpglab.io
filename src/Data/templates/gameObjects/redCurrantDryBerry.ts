import { MockObjectType } from '../../../types'
import { getRandomNumber } from '../../../utils'

export const redCurrantDryBerry = (): Partial<MockObjectType> => {
  return {
    name: 'dry red currant berry',
    weight: getRandomNumber(0.0005, 0.001),
    spriteLayers: {
      basis: 6048,
    },
    hitPoints: 1,
    fireResistance: 2,
    length: 0.01,
    diameter: 0.01,
  }
}
