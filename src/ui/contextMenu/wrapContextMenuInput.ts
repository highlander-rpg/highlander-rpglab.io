export const wrapContextMenuInput = (content: string): string => {
  return `<div class="context-menu-input-wrapper js-context-menu-input-wrapper">${content}</div>`
}
