import { contextMenuInputValue } from "./contextMenuInputValue"

export const addContextMenuInputListeners = (): void => {
  document.querySelectorAll('.js-context-menu-input-wrapper').forEach((el) => {
    el.addEventListener('change', (e) => {
      const inputEl = e.target as HTMLInputElement

      contextMenuInputValue.set(Number(inputEl.value))
    })
  })
}
