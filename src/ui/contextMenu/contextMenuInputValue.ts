type ContextMenuInputValue = number

let value: ContextMenuInputValue = 2

export const contextMenuInputValue = {
  get: (): ContextMenuInputValue => value,
  set: (newVal: ContextMenuInputValue): void => {
    value = newVal
  } 
}