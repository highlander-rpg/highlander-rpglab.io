import { animationLayer } from '../animation'

export const initAnimationLayer = () => {
  animationLayer.init()
}
