import { cloneDeep } from 'lodash/fp'
import { mockObject } from '../../data/mocks/mockObject'
import { createObject } from '../../helpers'

export const addTestDoors = (): void => {
  const door1 = cloneDeep(mockObject)
  door1.isDoor = true
  door1.isOpen = false
  door1.isFullObstacle = true
  door1._spriteOpen = '-1632px -32px'
  door1._spriteClosed = '-1152px -32px'
  door1.spriteLayers = {
    basis: door1.isOpen ? door1._spriteOpen : door1._spriteClosed,
  }
  door1.name = 'door'
  door1.id = '_test-door'
  createObject('r2c2', door1)

  const door2 = cloneDeep(mockObject)
  door2.isDoor = true
  door2.isOpen = false
  door2.isFullObstacle = true
  door2._spriteOpen = '-288px -352px'
  door2._spriteClosed = '-256px -352px'
  door2.spriteLayers = {
    basis: door2.isOpen ? door2._spriteOpen : door2._spriteClosed,
  }
  door2.name = 'door2'
  door2.id = '_test-door2'
  door2.isLocked = true
  door2.keyToUnlock = '_test-key1'
  createObject('r2c10', door2)

  const key1 = cloneDeep(mockObject)
  key1.spriteLayers = {
    basis: '-1760px -1280px',
  }
  key1.name = 'key'
  key1.id = '_test-key1'
  key1.weight = 300
  createObject('r2c8', key1)
}
