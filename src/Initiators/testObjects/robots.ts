import { advancedFollower1, basicPatrol1 } from "../../AI"
import { addRobot } from "../gameObjects"

export const addTestRobots = (): void => {
  addRobot('r2c20', {
    components: {
      processor: {
        name: 'Megatron 4000',
        isOn: true,
      },
      AIModulesBoard: {
        name: 'Seawood Inc.',
        numOfSlots: 3,
        modules: [
          basicPatrol1,
          advancedFollower1,
          {
            name: 'Species Plantarum. Washington Botanical Garden edition',
            desc: 'Some desc',
            script: () => {},
            priority: 4,
            config: {},
          },
        ],
      },
    }
  })
}
