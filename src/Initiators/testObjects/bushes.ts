import { addBush } from "../gameObjects"

export const addTestBushes = (): void => {
  addBush('r2c14', { id: '_test-bush-no-berries' }, false)
  addBush('r2c15', { id: '_test-bush-with-berries' })
  addBush('r2c16', { id: '_test-bush-dry' }, false, true)

  addBush('r2c18', {
    id: '_test-predictable-bush',
    compounds: {
      redCurrantBerry: 4,
      redCurrantBranch: 4,
      redCurrantDryBerry: 4,
      redCurrantDryBranch: 4,
      redCurrantDryLeaf: 4,
      redCurrantLeaf: 4,
    },
    spriteLayers: {
      basis: 6044,
      upperOverlay1: 4345,
      upperOverlay2: 6041,
    }
  })
}
