import { cloneDeep } from 'lodash/fp'
import { mockObject } from '../../data/mocks/mockObject'
import { arrayOfWallSprites } from '../../data/sprites'
import { createObject } from '../../helpers'
import { getRandomElement } from '../../utils'

export const addTestObstacles = (): void => {
  //Big square piedestal
  const ob1 = cloneDeep(mockObject)
  ob1.spriteLayers = {
    basis: '-480px -224px',
  }
  ob1.obstacleDirectionTop = true
  ob1.obstacleDirectionLeft = true
  ob1.name = 'obstacle-part-1'
  ob1.id = '_test-obstacle-part-1'
  createObject('r6c2', ob1)

  const ob2 = cloneDeep(mockObject)
  ob2.spriteLayers = {
    basis: '-416px -224px',
  }
  ob2.obstacleDirectionTop = true
  ob2.name = 'obstacle-part-2'
  ob2.id = '_test-obstacle-part-2'
  createObject('r6c3', ob2)

  const ob3 = cloneDeep(mockObject)
  ob3.spriteLayers = {
    basis: '-448px -224px',
  }
  ob3.obstacleDirectionTop = true
  ob3.obstacleDirectionRight = true
  ob3.name = 'obstacle-part-3'
  ob3.id = '_test-obstacle-part-3'
  createObject('r6c4', ob3)

  const ob4 = cloneDeep(mockObject)
  ob4.spriteLayers = {
    basis: '-352px -224px',
  }
  ob4.obstacleDirectionRight = true
  ob4.name = 'obstacle-part-4'
  ob4.id = '_test-obstacle-part-4'
  createObject('r7c4', ob4)

  const ob5 = cloneDeep(mockObject)
  ob5.spriteLayers = {
    basis: '-544px -224px',
  }
  ob5.obstacleDirectionRight = true
  ob5.obstacleDirectionBottom = true
  ob5.name ='obstacle-part-5'
  ob5.id = '_test-obstacle-part-5'
  createObject('r8c4', ob5)

  const ob6 = cloneDeep(mockObject)
  ob6.spriteLayers = {
    basis: '-512px -224px',
  }
  ob6.obstacleDirectionBottom = true
  ob6.name = 'obstacle-part-6'
  ob6.id = '_test-obstacle-part-6'
  createObject('r8c3', ob6)

  const ob7 = cloneDeep(mockObject)
  ob7.spriteLayers = {
    basis: '-576px -224px',
  }
  ob7.obstacleDirectionLeft = true
  ob7.obstacleDirectionBottom = true
  ob7.name = 'obstacle-part-7'
  ob7.id = '_test-obstacle-part-7'
  createObject('r8c2', ob7)

  const ob8 = cloneDeep(mockObject)
  ob8.spriteLayers = {
    basis: '-608px -224px',
  }
  ob8.obstacleDirectionLeft = true
  ob8.name = 'obstacle-part-8'
  ob8.id = '_test-obstacle-part-8'
  createObject('r7c2', ob8)

  //middle of the piedestal
  const ob9 = cloneDeep(mockObject)
  ob9.spriteLayers = {
    basis: '-384px -224px',
  }
  ob9.name = 'obstacle-part-9'
  ob9.id = '_test-obstacle-part-9'
  createObject('r7c3', ob9)

  //4 full-sided obstacles
  const getRandomWallSprite = (): string => getRandomElement(arrayOfWallSprites)

  const fullOb1 = cloneDeep(mockObject)
  fullOb1.spriteLayers = {
    basis: getRandomWallSprite(),
    upperOverlay1: '-1248px -1856px',
  }
  // fullOb1.obstacleDirectionTop = true
  // fullOb1.obstacleDirectionRight = true
  // fullOb1.obstacleDirectionBottom = true
  // fullOb1.obstacleDirectionLeft = true
  fullOb1.isFullObstacle = true
  fullOb1.name = 'obstacle-full-1'
  fullOb1.id = '_test-obstacle-full-1'
  createObject('r7c6', fullOb1)

  const fullOb2 = cloneDeep(mockObject)
  fullOb2.spriteLayers = {
    basis: getRandomWallSprite(),
    upperOverlay1: '-1248px -1856px',
  }
  // fullOb2.obstacleDirectionTop = true
  // fullOb2.obstacleDirectionRight = true
  // fullOb2.obstacleDirectionBottom = true
  // fullOb2.obstacleDirectionLeft = true
  fullOb2.isFullObstacle = true
  fullOb2.name = 'obstacle-full-2'
  fullOb2.id = '_test-obstacle-full-2'
  createObject('r6c7', fullOb2)

  const fullOb3 = cloneDeep(mockObject)
  fullOb3.spriteLayers = {
    basis: getRandomWallSprite(),
    upperOverlay1: '-1248px -1856px',
  }
  // fullOb3.obstacleDirectionTop = true
  // fullOb3.obstacleDirectionRight = true
  // fullOb3.obstacleDirectionBottom = true
  // fullOb3.obstacleDirectionLeft = true
  fullOb3.isFullObstacle = true
  fullOb3.name = 'obstacle-full-3'
  fullOb3.id = '_test-obstacle-full-3'
  createObject('r7c8', fullOb3)

  const fullOb4 = cloneDeep(mockObject)
  fullOb4.spriteLayers = {
    basis: getRandomWallSprite(),
    upperOverlay1: '-1248px -1856px',
  }
  // fullOb4.obstacleDirectionTop = true
  // fullOb4.obstacleDirectionRight = true
  // fullOb4.obstacleDirectionBottom = true
  // fullOb4.obstacleDirectionLeft = true
  fullOb4.isFullObstacle = true
  fullOb4.name = 'obstacle-full-4'
  fullOb4.id = '_test-obstacle-full-4'
  createObject('r8c7', fullOb4)

  //Distant 4-sided obstacle
  const fullOb5 = cloneDeep(mockObject)
  fullOb5.spriteLayers = {
    basis: getRandomWallSprite(),
    upperOverlay1: '-1248px -1856px',
  }
  // fullOb5.obstacleDirectionTop = true
  // fullOb5.obstacleDirectionRight = true
  // fullOb5.obstacleDirectionBottom = true
  // fullOb5.obstacleDirectionLeft = true
  fullOb5.isFullObstacle = true
  fullOb5.name = 'obstacle-full-5'
  fullOb5.id = '_test-obstacle-full-5'
  createObject('r8c10', fullOb5)

  //1 3-sided obstacle
  const obWith3 = cloneDeep(mockObject)
  obWith3.spriteLayers = {
    basis: '-1920px -320px',
    upperOverlay1: '-1216px -1856px',
  }
  obWith3.obstacleDirectionTop = true
  obWith3.obstacleDirectionRight = true
  obWith3.obstacleDirectionLeft = true
  obWith3.name = 'obstacle-3-sided'
  obWith3.id = '_test-obstacle-3-sided'
  createObject('r6c10', obWith3)

  //1 cell surrounded by ice obstacles
  const iceSide1 = cloneDeep(mockObject)
  iceSide1.spriteLayers = {
    basis: '-1408px -288px',
    upperOverlay1: '-672px -704px',
  }
  iceSide1.name = 'obstacle-ice-side-1'
  iceSide1.id = '_test-obstacle-ice-side-1'
  createObject('r6c11', iceSide1)

  const iceSide2 = cloneDeep(mockObject)
  iceSide2.spriteLayers = {
    basis: '-1472px -288px',
    upperOverlay1: '-928px -704px',
  }
  iceSide2.obstacleDirectionBottom = true
  iceSide2.name = 'obstacle-ice-side-2'
  iceSide2.id = '_test-obstacle-ice-side-2'
  createObject('r6c12', iceSide2)

  const iceSide3 = cloneDeep(mockObject)
  iceSide3.spriteLayers = {
    basis: '-1344px -288px',
    upperOverlay1: '-736px -704px',
  }
  iceSide3.name = 'obstacle-ice-side-3'
  iceSide3.id = '_test-obstacle-ice-side-3'
  createObject('r6c13', iceSide3)

  const iceSide4 = cloneDeep(mockObject)
  iceSide4.spriteLayers = {
    basis: '-640px -288px',
    upperOverlay1: '-992px -704px',
  }
  iceSide4.obstacleDirectionLeft = true
  iceSide4.name = 'obstacle-ice-side-4'
  iceSide4.id = '_test-obstacle-ice-side-4'
  createObject('r7c13', iceSide4)

  const iceSide5 = cloneDeep(mockObject)
  iceSide5.spriteLayers = {
    basis: '-1536px -288px',
    upperOverlay1: '-608px -704px',
  }
  iceSide5.name = 'obstacle-ice-side-5'
  iceSide5.id = '_test-obstacle-ice-side-5'
  createObject('r8c13', iceSide5)

  const iceSide6 = cloneDeep(mockObject)
  iceSide6.spriteLayers = {
    basis: '-1664px -288px',
    upperOverlay1: '-864px -704px',
  }
  iceSide6.obstacleDirectionTop = true
  iceSide6.name = 'obstacle-ice-side-6'
  iceSide6.id = '_test-obstacle-ice-side-6'
  createObject('r8c12', iceSide6)

  const iceSide7 = cloneDeep(mockObject)
  iceSide7.spriteLayers = {
    basis: '-1600px -288px',
    upperOverlay1: '-544px -704px',
  }
  iceSide7.name = 'obstacle-ice-side-7'
  iceSide7.id = '_test-obstacle-ice-side-7'
  createObject('r8c11', iceSide7)

  const iceSide8 = cloneDeep(mockObject)
  iceSide8.spriteLayers = {
    basis: '-1728px -288px',
    upperOverlay1: '-800px -704px',
  }
  iceSide8.obstacleDirectionRight = true
  iceSide8.name = 'obstacle-ice-side-8'
  iceSide8.id = '_test-obstacle-ice-side-8'
  createObject('r7c11', iceSide8)

  //middle cell
  const iceSide9 = cloneDeep(mockObject)
  iceSide9.spriteLayers = {
    basis: '-1952px -544px',
    upperOverlay1: '-1152px -576px',
  }
  iceSide9.name = 'obstacle-ice-side-9'
  iceSide9.id = '_test-obstacle-ice-side-9'
  createObject('r7c12', iceSide9)
}
