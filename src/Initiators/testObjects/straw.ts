import { cloneDeep } from 'lodash/fp'
import { mockObject } from '../../data/mocks/mockObject'
import { arrayOfStrawSprites } from '../../data/sprites'
import { getCellsAround } from '../../helpers'
import { createObject } from '../../helpers'
import { getRandomElement } from '../../utils'

export const addTestStraw = (): void => {
  const centerCellId = 'r13c5'
  const cells = getCellsAround(centerCellId, 3)
  cells.push(centerCellId)
  cells.forEach((cellId) => {
    const straw = cloneDeep(mockObject)
    straw.name = 'straw'
    straw.fireResistance = 0
    straw.spriteLayers = {
      basis: getRandomElement(arrayOfStrawSprites),
    }
    straw.isFullObstacle = true

    createObject(cellId, straw)
  })
}
