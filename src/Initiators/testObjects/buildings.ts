import { cloneDeep } from 'lodash/fp'
import { mockObject } from '../../data/mocks/mockObject'
import { createObject } from '../../helpers'

const createDoor = (staysAt: string) => {
  const door = cloneDeep(mockObject)
  door.isDoor = true
  door.isOpen = false
  door.isFullObstacle = true
  door._spriteOpen = '-1632px -32px'
  door._spriteClosed = '-1152px -32px'
  door.spriteLayers = {
    basis: door.isOpen ? door._spriteOpen : door._spriteClosed,
  }
  door.name = 'door'
  door.id = `_test-door-${Math.random().toString(36).substring(2, 15)}`
  createObject(staysAt, door)
}

const createWallBlock = (staysAt: string) => {
  const wallBlock = cloneDeep(mockObject)
  wallBlock.name = 'wall'
  wallBlock.id = `_test-wall-block-${Math.random().toString(36).substring(2, 15)}`
  createObject(staysAt, wallBlock)
}

export const addTestBuildings = (): void => {
  createWallBlock('r10c15')
  createWallBlock('r11c15')
  createWallBlock('r12c15')
  createDoor('r12c16')
  createDoor('r12c18')
}
