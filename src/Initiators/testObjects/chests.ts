import { cloneDeep } from 'lodash/fp'
import { mockObject } from '../../data/mocks/mockObject'
import { createObject } from '../../helpers'

export const addTestChests = (): void => {
  const chest = cloneDeep(mockObject)
  chest.isContainer = true
  chest.isOpen = false
  chest.obstacleDirectionTop = true
  chest._spriteOpen = '-256px 0px'
  chest._spriteClosed = '-224px 0px'
  chest.spriteLayers = {
    basis: chest.isOpen ? chest._spriteOpen : chest._spriteClosed,
  }
  chest.name = 'chest'
  chest.id = '_test-chest'
  createObject('r2c4', chest)

  const chest2 = cloneDeep(mockObject)
  chest2.isContainer = true
  chest2.isOpen = false
  chest2._spriteOpen = '-256px 0px'
  chest2._spriteClosed = '-224px 0px'
  chest2.spriteLayers = {
    basis: chest2.isOpen ? chest2._spriteOpen : chest2._spriteClosed,
  }
  chest2.name = 'chest2'
  chest2.id = '_test-chest2'
  chest2.isLocked = true
  chest2.keyToUnlock = '_test-key1'
  createObject('r2c12', chest2)
}
