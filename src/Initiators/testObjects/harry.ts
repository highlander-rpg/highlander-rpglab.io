import { cloneDeep } from 'lodash/fp'
import { mockObject } from '../../data/mocks/mockObject'
import { createObject } from '../../helpers'
import { getAvailableActions } from '../../actions/getAvailableActions'
import { getRandomElement, getRandomNumber, chance } from '../../utils'
import { ActionType, MockObjectType } from '../../types'
import { getParticularAction } from '../../helpers'

export const addTestHarry = (): void => {
  const creature = cloneDeep(mockObject)
  creature.spriteLayers.body = '-288px -2560px'

  // creature.spriteBeard = '-384px -2592px'

  //creature.spriteCloak = '-576px -2688px'

  // creature.spriteWings = '-928px -2688px'
  // creature.spriteJacket = '-1952px -2624px'
  // creature.spriteBoots = '0px -2688px'
  // creature.spritePants = '-1024px -2976px'
  // creature.spriteLeftHand = '-1760px -2720px'
  // creature.spriteRightHand = '-704px -2912px'
  // creature.spriteHair = '-672px -2720px'

  //creature.spriteHelmet = '-1792px -2944px'

  creature.spriteLayers.helmet = '-768px -3008px'

  creature.name = 'Harry'
  creature.id = '_test-harry'
  createObject('r2c6', creature)

  //AI interacts with objects
  // const AIStep = () => {
  //   const cellID = `r${getRandomNumber(0, 13)}c${getRandomNumber(0, 13)}`
  //   const harry = window.allGameObjects.get(creature.id)
  //   const objectsIdsOnCell = window.currentMap.get(cellID) || []
  //   const subject = window.allGameObjects.get(getRandomElement(objectsIdsOnCell))
  //   const arrayOfActions = getAvailableActions(harry, subject, cellID)
  //   const randomAction = getRandomElement(arrayOfActions)
  //   console.log(randomAction.text)
  //   randomAction.callback()
  // }
  // window.setInterval(AIStep, 3000)

  // document.addEventListener('click', (e) => {
  //   if (e.altKey) {
  //     const el = e.target as HTMLElement
  //     const cellId = el.id
  //     const objectsIdsOnCell = window.currentMap.get(cellId)
  //     const subject = window.allGameObjects.get(objectsIdsOnCell[0])
  //     const arrayOfActions = getAvailableActions('_test-harry', subject, cellId)
  //     const action = getParticularAction(arrayOfActions, 'teleport-here')

  //     if (action) {
  //       action.callback()
  //     }

  //     //For quicker development
  //     navigator.clipboard.writeText(cellId)
  //   }
  // })
}
