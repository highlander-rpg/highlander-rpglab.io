import { mapDefaultWidth, mapDefaultHeight } from '../config'
import { generateMap } from '../generators'

export const initMap = (mapID?: string): void => {
  window.currentMap = new Map()

  if (!mapID) {
    generateMap(mapDefaultWidth, mapDefaultHeight)
  }
}
