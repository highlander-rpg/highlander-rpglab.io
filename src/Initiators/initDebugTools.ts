import { cellSelector, quickAction } from './../debug'
import { addBush, addFire } from './gameObjects'
import { getCellsInRectangle } from '../helpers'

export const initDebugTools = () => {
  cellSelector.init()
  quickAction.init()

  window.hl = {
    addBush,
    addFire,
    getCellsInRectangle,
  }
}
