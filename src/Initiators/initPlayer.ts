import { createObject } from '../helpers'
import { mockObject } from '../data/mocks/mockObject'
import { cloneDeep } from 'lodash/fp'

export const initPlayer = (): void => {
  const player = cloneDeep(mockObject)
  player.id = '_test-player'
  player.spriteLayers.body = '-288px -2560px'
  player.spriteLayers.beard = '-384px -2592px'
  player.spriteLayers.jacket = '-832px -2656px'
  player.spriteLayers.boots = '0px -2688px'
  player.spriteLayers.pants = '-1024px -2976px'
  player.spriteLayers.rightHand = '-96px -2880px'
  player.spriteLayers.hair = '-672px -2720px'
  player.spriteLayers.helmet = '-704px -2976px'

  createObject('r4c2', player)

  window.playerId = player.id
}
