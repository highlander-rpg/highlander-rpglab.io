import { cloneDeep } from 'lodash/fp'
import { mockObject } from '../../data/mocks/mockObject'
import { createObject } from '../../helpers'
import { getRandomNumber, getRandomElement } from '../../utils'
import { MockObjectType } from '../../types'
import { updateObject } from '../../helpers'
import { AddBushConstructor } from './types'

export const addBush: AddBushConstructor = (cellId: string, objParams: Partial<MockObjectType>, isFruiting = true, isDry = false): void => {
  let bush: MockObjectType = cloneDeep(mockObject)
  bush.name = 'redcurrant bush'
  bush.weight = 10
  bush.fireResistance = 2
  bush.compounds = {
    redCurrantBerry: isFruiting && !isDry ? getRandomNumber(1, 300) : 0,
    redCurrantDryBerry: getRandomNumber(1, 30),
    redCurrantLeaf: isDry ? 0 : getRandomNumber(10, 300),
    redCurrantDryLeaf: isDry ? getRandomNumber(1, 30) : getRandomNumber(1, 20),
    redCurrantBranch: isDry ? 0 : getRandomNumber(3, 30),
    redCurrantDryBranch: isDry ? getRandomNumber(3, 30) : getRandomNumber(0, 3),
  }
 
  const arrOfBushSticksSprites = [6044, 6045, 6046]
  const arrOfBushLeavesSprites = [4345, 4346, 4347]
  const arrOfBushBerriesSprites = [6041, 6042, 6043]
  bush.spriteLayers.basis = getRandomElement(arrOfBushSticksSprites)
  bush.spriteLayers.upperOverlay1 = (isDry || objParams.compounds?.redCurrantLeaf === 0 || bush.compounds.redCurrantLeaf === 0) ? '' : getRandomElement(arrOfBushLeavesSprites)
  bush.spriteLayers.upperOverlay2 = (isFruiting || objParams.compounds?.redCurrantBerry > 0 || bush.compounds.redCurrantBerry > 0) ? getRandomElement(arrOfBushBerriesSprites) : ''

  bush = createObject(cellId, { ...bush, ...objParams })

  updateObject(bush, cellId)
}
