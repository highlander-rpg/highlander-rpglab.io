import { getGameObject } from '../../helpers'
import { initTestGameStorages } from '../../testHelpers'
import { addBush } from './addBush'

jest.mock('../../helpers/rerenderCell')

const cellId = 'r0c0'
const bushId = '_test-bush'

describe('"addBush" helper', () => {
  beforeEach(() => {
    initTestGameStorages()
  })

  it('should render bush with particular spites if provided', () => {
    addBush(cellId, {
      id: bushId,
      spriteLayers: {
        basis: 6044,
        upperOverlay1: 4345,
        upperOverlay2: 6041,
      }
    })

    const obj = getGameObject(bushId)

    expect(obj.spriteLayers.basis).toBe(6044)
    expect(obj.spriteLayers.upperOverlay1).toBe(4345)
    expect(obj.spriteLayers.upperOverlay2).toBe(6041)

    expect(obj.spriteLayers.body).toBe(undefined)
  })
})
