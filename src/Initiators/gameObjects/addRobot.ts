import { cloneDeep } from 'lodash/fp'
import { mockObject } from '../../data/mocks/mockObject'
import { createObject } from '../../helpers'
import { MockObjectType } from '../../types'
import { updateObject } from '../../helpers'
import { AddRobotConstructor } from './types'

export const addRobot: AddRobotConstructor = (cellId: string, objParams: Partial<MockObjectType>): void => {
  let robot: MockObjectType = cloneDeep(mockObject)
  robot.name = 'ROBOT 3000'
  robot.spriteLayers.basis = 6051
  

  robot = createObject(cellId, { ...robot, ...objParams })
  updateObject(robot, cellId)

  const currentAIModule = robot.components.AIModulesBoard.modules[0] 
  currentAIModule.script(robot, currentAIModule.config)
}
