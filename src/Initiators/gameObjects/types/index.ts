import { MockObjectType } from '../../../types'

export type AddBushConstructor = (cellId: string, objParams: Partial<MockObjectType>, isFruiting?: boolean, isDry?: boolean) => void
export type AddFireConstructor = (cellId: string, objParams: Partial<MockObjectType>) => void
export type AddRobotConstructor = (cellId: string, objParams: Partial<MockObjectType>) => void
