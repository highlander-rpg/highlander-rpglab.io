import { cloneDeep } from 'lodash/fp'

import { mockObject } from '../../data/mocks/mockObject'
import { createObject } from '../../helpers'
import { getRandomNumber } from '../../utils'
import { MockObjectType } from '../../types'
import { fire as animateFire } from '../../animation'
import { performAction, updateObject } from '../../helpers'
import { AddFireConstructor } from './types'

export const addFire: AddFireConstructor = (cellId: string, objParams: Partial<MockObjectType>): void => {
  let fire: MockObjectType = cloneDeep(mockObject)
  fire.name = 'fire'
  fire.type = 'fire'
  fire.hitPoints = getRandomNumber(1, 3)
  fire.weight = 0
  fire.fireResistance = Infinity

  const animationId = animateFire(cellId)
  fire.animations = [animationId]

  fire = createObject(cellId, { ...objParams, ...fire })

  const intervalId = window.setInterval(() => {
    performAction('keep-burning', cellId, fire)
  }, getRandomNumber(1000, 3000))
  fire.intervals = [intervalId]

  updateObject(fire, cellId)
}
