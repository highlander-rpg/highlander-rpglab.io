type ItemStorageType = {
  itemType: "cell" | "contextMenuItem" | null
  itemId: string | null
}

const storage: ItemStorageType = {
  itemType: null,
  itemId: null,
}

export const hoveredItemStorage = {
  init: (): void => {
    document.addEventListener('mousemove', (e) => {
      const el = e.target as HTMLElement
      if (el.classList.contains('js-cell') || el.classList.contains('js-context-menu-item')) {
        if (el.classList.contains('js-cell')) {
          storage.itemType = 'cell'
          storage.itemId = el.id
        } else {
          storage.itemType = 'contextMenuItem'
          storage.itemId = el.getAttribute("data-action")
        }
      } else {
        storage.itemType = null
        storage.itemId = null
      }
    })
  },

  get: (): ItemStorageType => storage,
}
