import { getAvailableActions } from '../actions/getAvailableActions'
import { closeContextMenu, getCustomHotkeyLetterByActionId } from '../helpers'
import { addContextMenuInputListeners } from '../ui'
import { checkIfParentsHaveClass } from '../utils'

export const initContextMenu = () => {
  const contextMenuPopupEl = document.createElement('div')
  contextMenuPopupEl.id = 'contextMenu'
  contextMenuPopupEl.setAttribute('data-testId', 'context-menu')
  contextMenuPopupEl.classList.add('context-menu-wrapper')
  contextMenuPopupEl.classList.add('hidden')
  document.addEventListener('click', (e: MouseEvent) => {
    if (!checkIfParentsHaveClass('js-context-menu-input-wrapper', e)) {
      closeContextMenu()
    }
  })
  document.getElementsByTagName('body')[0].appendChild(contextMenuPopupEl)

  document.addEventListener('click', e => {
    const cellEl = e.target as HTMLElement
    if (cellEl.classList.contains('js-cell') && !e.ctrlKey && !e.altKey) {
      const cellID = cellEl.id
      cellEl.classList.add("context-menu-selected-cell")
      const objectsIdsOnCell = window.currentMap.get(cellID) || []
  
      const contextMenuEl = document.getElementById('contextMenu')
      const arrayOfActionsContents: string[] = []

      const btns: HTMLDivElement[] = []
      
      objectsIdsOnCell.forEach(objectIdOnCell => {
        const player = window.allGameObjects.get(window.playerId)
        const subject = window.allGameObjects.get(objectIdOnCell)
        const arrayOfActions = getAvailableActions(player, subject, cellID)
        arrayOfActions.forEach(action => {
          if (!arrayOfActionsContents.includes(action.content)) {
            arrayOfActionsContents.push(action.content)

            const btn = document.createElement('div')

            btn.classList.add('context-menu-item', 'js-context-menu-item')

            btn.setAttribute('data-testid', action.id)
            btn.setAttribute('data-action', action.id)
            btn.setAttribute('data-subjectId', action.subjectId)

            const hotkey = getCustomHotkeyLetterByActionId(action.id)
            if (hotkey) {
              btn.setAttribute('data-hotkey', hotkey)
            }

            btn.innerHTML = action.content

            const callback = (e: MouseEvent) => {
              if (!checkIfParentsHaveClass('js-context-menu-input-wrapper', e)) {
                action.callback()
              }
            }
            btn.addEventListener('click', callback)

            btns.push(btn)
          }
        })
      })

      //Sort buttons alphabetically and insert at once
      const tempBtnsContainer = document.createDocumentFragment()
      btns.sort((a, b) => a.innerHTML.localeCompare(b.innerHTML)).forEach((btnEl) => {
        tempBtnsContainer.appendChild(btnEl)
      })
      contextMenuEl.appendChild(tempBtnsContainer)

      const closeBtn = document.createElement('button')
      closeBtn.classList.add('context-menu-close-btn')
      closeBtn.setAttribute('data-testid', 'context-menu-close-btn')
      closeBtn.innerText = '×'
      contextMenuEl.appendChild(closeBtn)

      addContextMenuInputListeners()
  
      contextMenuEl.classList.remove('hidden')
    }
  })
}
