import * as allTestObjects from './testObjects'

export const initTestObjects = () => {
  for (let initTestObj of Object.keys(allTestObjects)) {
    type initiator = keyof typeof allTestObjects
    allTestObjects[initTestObj as initiator]()
  }
}
