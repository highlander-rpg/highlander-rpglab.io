import { getAvailableActions } from '../actions/getAvailableActions'
import { allowedLetterKeysForOverride, closeContextMenu, DirectionType, getCustomHotkeyActionIdByLetter, getGameObject, getNearestCell, getParticularAction, setCustomHotkey } from '../helpers'
import { ActionId, MockObjectType } from '../types'
import { keyCodeToLetter } from '../utils'
import { hoveredItemStorage } from './hoveredItemStorage'

const openDoors = (cellId: string, playerObj: MockObjectType) => {
  const objectsIdsOnCell = window.currentMap.get(cellId)
  const subjectsOnCell = objectsIdsOnCell.map((id) => {
    return window.allGameObjects.get(id)
  })
  const closedDoorsObjectsOnCell = subjectsOnCell.filter((obj) => obj.isDoor && !obj.isOpen)
  closedDoorsObjectsOnCell.forEach((doorObj) => {
    const arrayOfActions = getAvailableActions(playerObj, doorObj, cellId)
    const action = getParticularAction(arrayOfActions, 'open')
  
    if (action) {
      action.callback()
    }
  })
}

const stepOn = (direction: DirectionType): void => {
  closeContextMenu()

  const playerObj = getGameObject(window.playerId)
  const targetCellId = getNearestCell(playerObj.staysAt, direction)

  if (targetCellId) {
    const objectsIdsOnCell = window.currentMap.get(targetCellId)
    const subject = window.allGameObjects.get(objectsIdsOnCell[0])
    const arrayOfActions = getAvailableActions(playerObj, subject, targetCellId)
    const action = getParticularAction(arrayOfActions, 'step-to-cell')

    if (action) {
      action.callback()
    } else {
      openDoors(targetCellId, playerObj)
    }
  }
}

export const initHotkeys = () => {
  hoveredItemStorage.init()
  
  document.addEventListener('keyup', e => {
    switch (e.code) {
      case 'KeyW':
        stepOn('top')
        break
      case 'KeyD':
        stepOn('right')
        break
      case 'KeyS':
        stepOn('bottom')
        break
      case 'KeyA':
        stepOn('left')
        break
      
      case 'Escape':
        closeContextMenu()
        break
    }

    if (allowedLetterKeysForOverride.includes(keyCodeToLetter(e.code))) {
      const hoveredItem = hoveredItemStorage.get()

      const letter = keyCodeToLetter(e.code)

      if (hoveredItem.itemType === 'contextMenuItem') {
        const oldActionId = getCustomHotkeyActionIdByLetter(letter)

        setCustomHotkey(letter, hoveredItem.itemId as ActionId)

        if (oldActionId) {
          if (oldActionId !== hoveredItem.itemId) {
            document.querySelectorAll(`[data-action="${oldActionId}"]`).forEach((btn) => {
              btn.removeAttribute('data-hotkey')
            })
            document.querySelectorAll(`[data-action="${hoveredItem.itemId}"]`).forEach((btn) => {
              btn.setAttribute('data-hotkey', keyCodeToLetter(e.code))
            })
          } else {
            document.querySelectorAll(`[data-action="${hoveredItem.itemId}"]`).forEach((btn) => {
              btn.setAttribute('data-hotkey', keyCodeToLetter(e.code))
            })
          }
        } else {
          document.querySelectorAll(`[data-action="${hoveredItem.itemId}"]`).forEach((btn) => {
            btn.setAttribute('data-hotkey', keyCodeToLetter(e.code))
          })
        }
      } else {
        const actionId = getCustomHotkeyActionIdByLetter(letter)

        if (actionId) {
          const playerObj = getGameObject(window.playerId)
          const targetCellId = hoveredItem.itemId
          const objectsIdsOnCell = window.currentMap.get(targetCellId)
          const subject = window.allGameObjects.get(objectsIdsOnCell[0])
          const arrayOfActions = getAvailableActions(playerObj, subject, targetCellId)
          const action = getParticularAction(arrayOfActions, actionId)
  
          if (action) {
            action.callback()
          }
        }
      }
    }
  })
}
