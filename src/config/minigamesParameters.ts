import { MinigameId, MinigameParameters } from '../types'

export const minigamesParameters: Map<MinigameId, MinigameParameters> = new Map([
  ['modify_object_minigame', {
      devUrl: 'http://localhost:4000',
      prodUrl: 'https://highlander-rpg.gitlab.io/app/mg/modify-object/',
    }
  ],
  ['change_config_minigame', {
      devUrl: 'http://localhost:4001',
      prodUrl: 'https://highlander-rpg.gitlab.io/app/mg/change-config/',
    }
  ],
])
