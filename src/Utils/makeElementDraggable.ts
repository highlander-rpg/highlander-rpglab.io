export const makeElementDraggable = (wrapperElementSelector: string, dragHandlerElementSelector: string): void => {
  let selected: any = null, // Object of the element to be moved
    x_pos = 0, y_pos = 0, // Stores x & y coordinates of the mouse pointer
    x_elem = 0, y_elem = 0 // Stores top, left values (edge) of the element

  // Will be called when user starts dragging an element
  const _drag_init = (elem: any) => {
    // Store the object of the element which needs to be moved
    selected = elem
    x_elem = x_pos - selected.offsetLeft
    y_elem = y_pos - selected.offsetTop
  }

  // Will be called when user dragging an element
  const _move_elem = (e: any): void => {
    x_pos = e.pageX
    y_pos = e.pageY
    if (selected !== null) {
      selected.style.left = (x_pos - x_elem) + 'px'
      selected.style.top = (y_pos - y_elem) + 'px'
    }
  }

  // Destroy the object when we are done
  const _destroy = (): void => {
    selected = null
  }

  // Bind the functions...
  document.getElementById(dragHandlerElementSelector).onmousedown = (): false => {
    _drag_init(document.getElementById(wrapperElementSelector))
    return false
  }

  document.onmousemove = (e) => { _move_elem(e) }
  document.onmouseup = () => { _destroy() }
}
