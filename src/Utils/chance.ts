export const chance = (percentage: string = '100%'): boolean => {
  const percent = Number(percentage.split('%')[0]) / 100
  if (Math.random() < percent) {
    return true
  } else {
    return false
  }
}
