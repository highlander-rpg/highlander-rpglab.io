export const checkIfParentsHaveClass = (className: string, e: MouseEvent): boolean => {
  const arrOfTraversedElements = e.composedPath() as HTMLElement[]
  // Remove window, document, html and body from the array
  arrOfTraversedElements.splice(-4)

  const classNameWithoutDot = className.startsWith('.') ? className.slice(1) : className

  return arrOfTraversedElements.some((el) => el.classList.contains(classNameWithoutDot))
}
