import { getRandomElement } from './general'

const convertTemplateToArray = (str: string): string[] => {
  //'[dog|cat|duck]' => [dog, cat, duck]
  return str.substring(1, str.length - 1).split('|')
}

export const getPhrase = (sourceStr: string): string => {
  if (!sourceStr.includes('[')) {
    return sourceStr
  } else {
    let newPhrase = ''
    let startingIndex = 0
    let isHalted = false
    sourceStr.split('').forEach((char, index) => {
      if (!isHalted) {
        if (char === '[') {
          startingIndex = index
        } else if (char === ']') {
          isHalted = true
          const templateOfVariants = sourceStr.substring(startingIndex, index + 1)
          const chosenWord = getRandomElement(convertTemplateToArray(templateOfVariants))
          const reducedPhrase = sourceStr.replace(templateOfVariants, chosenWord)
          newPhrase = reducedPhrase
        }
      }
    })

    return getPhrase(newPhrase)
  }
}
