import { random } from 'lodash'

import { Letter } from '../helpers'

export const getRandomElement = (arr: any[]) => {
  return arr[Math.floor(Math.random() * (arr.length - 0) + 0)]
}

export const getRandomNumber = (min: number, max: number, isfloating?: boolean): number => {
  return random(min, max, isfloating)
}

export const isDevEnv = window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1'

export const letterToKeyCode = (letter: Letter): string => {
  //F -> KeyF
  return `Key${letter}`
}

export const keyCodeToLetter = (keyCode: string): Letter => {
  //KeyF -> F
  return keyCode?.split('Key')[1]?.toUpperCase() as Letter
}
