import { minigamesParameters } from '../config'
import { gripVerticalIcon, timesIcon, windowMaximizeIcon } from '../images/icons'
import { MinigameId, MinigameParameters, MockObjectType } from '../types'
import { isDevEnv, makeElementDraggable } from '../utils'
import { communicationScenarios } from './communicationScenarios'
import { MinigameMessage } from './types'

export class Minigame {
  constructor(id: MinigameId, actor: MockObjectType, subject: MockObjectType, cellId: string) {
    this.id = id
    this.actor = actor
    this.subject = subject
    this.cellId = cellId
    this.parameters = minigamesParameters.get(id)
    this.url = isDevEnv ? this.parameters.devUrl : this.parameters.prodUrl
  }

  id: MinigameId
  actor: MockObjectType
  subject: MockObjectType
  cellId: string
  parameters: MinigameParameters
  url: string
  reactToMsgFunc: (e: MessageEvent) => void

  init(): void {
    this.destroy()
    this.render()
    this.maximize()
    this.enableDragging()

    this.reactToMsgFunc = this.reactToMessage.bind(this)
    window.addEventListener('message', this.reactToMsgFunc, false)
  }
  
  destroy(): void {
    document.getElementById(`${this.id}-iframe-wrapper`)?.remove()
    window.removeEventListener('message', this.reactToMsgFunc, false)
  }

  maximize(): void {
    const iframe = document.getElementById(`${this.id}-iframe`)
    //TODO: set 85vw if screen width is less than 550
    iframe.style.width = '90vw'
    iframe.style.height = '90vh'

    const wrapper = document.getElementById(`${this.id}-iframe-wrapper`)
    wrapper.style.top = '10px'
    wrapper.style.left = '10px'
  }

  render(): void {
    const wrapper = document.createElement('div')
    wrapper.id = `${this.id}-iframe-wrapper`
    wrapper.classList.add('minigame-iframe-wrapper')
    wrapper.setAttribute('data-testid', 'minigame-iframe-wrapper')

    const frame = document.createElement('iframe')
    frame.id = `${this.id}-iframe`
    frame.setAttribute('data-testid', `${this.id}-iframe`)
    frame.classList.add('minigame-iframe')
    frame.src = this.url
    frame.allow = 'clipboard-write'

    const controls = document.createElement('div')
    controls.classList.add('minigame-controls')

    const closeBtn = document.createElement('div')
    closeBtn.classList.add('btn', 'close-btn')
    closeBtn.setAttribute('data-testid', 'minigame-close-btn')
    closeBtn.innerHTML = timesIcon
    closeBtn.title = 'Close'
    closeBtn.onclick = () => { this.destroy() }

    const dragBtn = document.createElement('div')
    dragBtn.id = `${this.id}-iframe-drag-handler`
    dragBtn.setAttribute('data-testid', 'minigame-drag-btn')
    dragBtn.classList.add('btn', 'drag-btn')
    dragBtn.innerHTML = gripVerticalIcon
    dragBtn.title = 'Drag'

    const maximizeBtn = document.createElement('div')
    maximizeBtn.classList.add('btn', 'maximize-btn')
    maximizeBtn.setAttribute('data-testid', 'minigame-maximize-btn')
    maximizeBtn.innerHTML = windowMaximizeIcon
    maximizeBtn.title = 'Maximize window'
    maximizeBtn.onclick = () => { this.maximize() }

    //TODO: Add minify btn

    //TODO: Save current minigame iframe position
    //TODO: Save current minigame iframe dimensions

    controls.appendChild(closeBtn)
    controls.appendChild(dragBtn)
    controls.appendChild(maximizeBtn)
    wrapper.appendChild(controls)
    wrapper.appendChild(frame)
    document.body.appendChild(wrapper)
  }

  sendMessage(msg: MinigameMessage) {
    (document.getElementById(`${this.id}-iframe`) as HTMLIFrameElement)?.contentWindow?.postMessage(msg, this.url)
  }

  reactToMessage(event: MessageEvent): void {
    if (event.origin !== (isDevEnv ? this.url : 'https://highlander-rpg.gitlab.io')) {
      return
    }

    const msg = event.data as MinigameMessage
    communicationScenarios.get(this.id)(this, msg)
  }

  enableDragging(): void {
    makeElementDraggable(`${this.id}-iframe-wrapper`, `${this.id}-iframe-drag-handler`)
  }
}

