export type MinigameMessage = {
  text: string
  payload: any
}
