import { cloneDeep } from 'lodash/fp'
import { updateObject } from '../../helpers'
import { CommunicationScenraio } from './types'

export const change_config_minigame: CommunicationScenraio = (minigame, msg): void => {
  if (msg.text === `${minigame.id} is ready to receive data`) {
    const answer = {
      text: 'Here is the initial config',
      payload: {
        obj: minigame.subject,
        configToChange: minigame.subject.components?.AIModulesBoard?.modules[0]?.config,
      }
    }

    minigame.sendMessage(answer)
  }

  if (msg.text === 'Here is the modified config') {
    const modifiedConfig = cloneDeep(msg.payload)
    if (minigame.subject.components?.AIModulesBoard?.modules[0]) {
      minigame.subject.components.AIModulesBoard.modules[0].config = modifiedConfig
    }

    updateObject(minigame.subject, minigame.subject.staysAt)

    minigame.destroy()
  }
}
