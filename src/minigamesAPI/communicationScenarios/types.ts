import { Minigame } from '../minigame'
import { MinigameMessage } from '../types'

export type CommunicationScenraio = (minigame: Minigame, msg: MinigameMessage) => void
