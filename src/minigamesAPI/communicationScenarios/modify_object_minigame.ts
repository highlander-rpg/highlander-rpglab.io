import { cloneDeep } from 'lodash/fp'
import { updateObject } from '../../helpers'
import { MockObjectProps } from '../../types'
import { CommunicationScenraio } from './types'

export const modify_object_minigame: CommunicationScenraio = (minigame, msg): void => {
  const propertiesToOmit: MockObjectProps[] = [
    'id',
    'staysAt',
    'sprites',
    'animations',
    'intervals',
    'inventory',
  ]

  if (msg.text === `${minigame.id} is ready to receive data`) {
    const answer = {
      text: 'Here is the initial game object and protected properties',
      payload: {
        obj: minigame.subject,
        protectedProperties: propertiesToOmit,
      }
    }

    minigame.sendMessage(answer)
  }

  if (msg.text === 'Here is the modified object') {
    const modifiedObj = cloneDeep(msg.payload)
    propertiesToOmit.forEach((prop) => {
      modifiedObj[prop] = cloneDeep(minigame.subject[prop])
    })

    updateObject(modifiedObj, minigame.subject.staysAt)

    minigame.destroy()
  }
}
