import { MinigameId } from '../../types'
import { CommunicationScenraio } from './types'
import { modify_object_minigame } from './modify_object_minigame'
import { change_config_minigame } from './change_config_minigame';

export const communicationScenarios: Map<MinigameId, CommunicationScenraio> = new Map([
  ['modify_object_minigame', modify_object_minigame],
  ['change_config_minigame', change_config_minigame],
])
