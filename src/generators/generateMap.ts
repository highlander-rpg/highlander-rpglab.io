import { cellWidth } from '../config/dimensions'
import { cellHeight } from '../config/dimensions'
import urlToSprite from '../images/sprites.png'
import { createObject } from '../helpers'
import { mockObject } from '../data/mocks/mockObject'
import { getRandomElement, getRandomNumber } from '../utils'
import { cloneDeep } from 'lodash/fp'
import { arrayOfGraniteSprites, arrayOfGrassSprites } from '../data/sprites'

export const generateMap = (numOfCols: number, numORows: number) => {
  const body = document.getElementsByTagName('body')[0]
  const tbl = document.createElement('table')
  tbl.style.borderCollapse = 'collapse'
  tbl.style.width = cellWidth * numOfCols + 'px'
  const tblBody = document.createElement('tbody')
  tblBody.id = 'current-map-table'
  
  for (let i = 0; i < numORows; i++) {
    const row = document.createElement('tr')
    row.id = 'row' + i

    for (let j = 0; j < numOfCols; j++) {
      const cell = document.createElement('td')
      cell.id = 'r' + i + 'c' + j
      cell.classList.add('cell', 'js-cell')
      cell.style.width = cellWidth + 'px'
      cell.style.height = cellHeight + 'px'
      cell.style.boxSizing = 'border-box'
      cell.style.padding = '0'
      cell.style.background = `url(${urlToSprite}) 0px -160px`
      row.appendChild(cell)

      window.currentMap.set(cell.id, [])
    }
  
    tblBody.appendChild(row)
  }
  
  tbl.appendChild(tblBody)
  body.appendChild(tbl)

  for (let i = 0; i < numORows; i++) {
    for (let j = 0; j < numOfCols; j++) {
      const cellID = 'r' + i + 'c' + j

      const granite = cloneDeep(mockObject)
      granite.spriteLayers.basis = getRandomElement(arrayOfGraniteSprites)
      granite.name = 'granite'
      granite.type = 'granite'
      granite.fireResistance = 1250

      createObject(cellID, granite)
  
      const grass = cloneDeep(mockObject)
      grass.spriteLayers.basis = getRandomElement(arrayOfGrassSprites)
      grass.name = 'grass'
      grass.type = 'grass'
      grass.fireResistance = getRandomNumber(1, 3)

      createObject(cellID, grass)
    }
  }
}
